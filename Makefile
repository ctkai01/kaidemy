.PHONY: run
run:
	@./scripts/run.sh mail_service
	@./scripts/run.sh user_service

push-git:
	git add .
	git commit -m "Fix bug image"
	git push origin master