#!/bin/bash

set -e

readonly service="$1"

if [ -n "$service"  ]; then
    cd "./internal/services/$service"
    kubectl delete -f "k8s/${service}_depl.yaml"
    kubectl create -f "k8s/${service}_depl.yaml"
fi

