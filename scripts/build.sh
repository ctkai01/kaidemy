#!/bin/bash

set -e

readonly service="$1"

if [ -n "$service"  ]; then
    cd "./internal/services/$service"
    make push-docker
fi

