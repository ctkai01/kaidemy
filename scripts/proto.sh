#!/bin/bash
set -e

readonly service="$1"
readonly name="$2"


directory_service_proto="internal/services/${service}_service/internal/$name/grpc/proto/service_clients/"
echo $directory_service_proto
if [ ! -d "$directory_service_proto" ]; then
  mkdir -p "$directory_service_proto"
fi


protoc \
  "--proto_path=api/proto/$name" "api/proto/$name/$name.proto" \
  "--go_out=${directory_service_proto}" --go_opt=paths=source_relative \
  --go-grpc_opt=require_unimplemented_servers=false \
  "--go-grpc_out=${directory_service_proto}" --go-grpc_opt=paths=source_relative
