package params

import (
	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"go.uber.org/fx"

	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
)

type LectureNormalsRouteParams struct {
	fx.In

	Logger            logger.Logger
	LectureNormalsGroup     *echo.Group `name:"lectures-normal-echo-group"`
	Validator         *validator.Validate
	LectureRepository data.LectureRepository
}
