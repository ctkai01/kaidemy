package params

import (
	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"go.uber.org/fx"

	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
)

type CurriculumsRouteParams struct {
	fx.In

	Logger               logger.Logger
	CurriculumsGroup     *echo.Group `name:"curriculums-echo-group"`
	Validator            *validator.Validate
	CurriculumRepository data.CurriculumRepository
}
