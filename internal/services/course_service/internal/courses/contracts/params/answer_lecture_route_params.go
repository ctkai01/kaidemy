package params

import (
	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"go.uber.org/fx"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
)

type AnswerLectureRouteParams struct {
	fx.In

	Logger                logger.Logger
	AnswerLecturesGroup *echo.Group `name:"answer-lectures-echo-group"`
	Validator             *validator.Validate
}
