package params

import (
	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"go.uber.org/fx"

	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
)

type QuestionsRouteParams struct {
	fx.In

	Logger             logger.Logger
	QuestionsGroup     *echo.Group `name:"questions-echo-group"`
	Validator          *validator.Validate
	QuestionRepository data.QuestionRepository
}
