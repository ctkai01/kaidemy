package params

import (
	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"go.uber.org/fx"

	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
)

type AssetsRouteParams struct {
	fx.In

	Logger          logger.Logger
	AssetsGroup     *echo.Group `name:"assets-echo-group"`
	Validator       *validator.Validate
	AssetRepository data.AssetRepository
}
