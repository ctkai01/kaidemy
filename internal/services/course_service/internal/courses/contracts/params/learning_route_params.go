package params

import (
	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"go.uber.org/fx"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
)

type LearningsRouteParams struct {
	fx.In

	Logger         logger.Logger
	LearningsGroup *echo.Group `name:"learnings-echo-group"`
	Validator      *validator.Validate
}
