package params

import (
	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"go.uber.org/fx"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
)

type QuestionLectureRouteParams struct {
	fx.In

	Logger         logger.Logger
	QuestionLecturesGroup     *echo.Group `name:"question-lectures-echo-group"`
	Validator      *validator.Validate
}
