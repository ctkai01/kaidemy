package params

import (
	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"go.uber.org/fx"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
)

type ReportsRouteParams struct {
	fx.In

	Logger         logger.Logger
	ReportsGroup     *echo.Group `name:"reports-echo-group"`
	Validator      *validator.Validate
}
