package params

import (
	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"go.uber.org/fx"

	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
)

type CoursesAdminRouteParams struct {
	fx.In

	Logger           logger.Logger
	CoursesGroup     *echo.Group `name:"courses-admin-echo-group"`
	Validator        *validator.Validate
	CourseRepository data.CourseRepository
}
