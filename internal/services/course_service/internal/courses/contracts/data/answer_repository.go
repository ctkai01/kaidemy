package data

import (
	"context"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	// uuid "github.com/satori/go.uuid"

	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"
)

type AnswerRepository interface {
	// GetAllProducts(
	// 	ctx context.Context,
	// 	listQuery *utils.ListQuery,
	// ) (*utils.ListResult[*models.User], error)
	// SearchProducts(
	// 	ctx context.Context,
	// 	searchText string,
	// 	listQuery *utils.ListQuery,
	// ) (*utils.ListResult[*models.User], error)
	GetAnswerByID(ctx context.Context, id int) (*models.Answer, error)
	// GetUserByEmail(ctx context.Context, email string) (*models.Course, error)
	// GetUserByEmailToken(ctx context.Context, emailToken string) (*models.Course, error)
	CreateAnswer(ctx context.Context, answer *models.Answer) error
	UpdateAnswer(ctx context.Context, answer *models.Answer) error
	DeleteAnswerByID(ctx context.Context, id int) error

	// UpdateEmailToken(ctx context.Context, id int, mailToken string) error
	// UpdateProduct(ctx context.Context, product *models.User) (*models.User, error)
	// DeleteProductByID(ctx context.Context, uuid uuid.UUID) error
}
