package data

import (
	"context"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	// uuid "github.com/satori/go.uuid"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/core/data/specification"

	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"
)

type CourseRepository interface {
	// GetAllProducts(
	// 	ctx context.Context,
	// 	listQuery *utils.ListQuery,
	// ) (*utils.ListResult[*models.User], error)
	// SearchProducts(
	// 	ctx context.Context,
	// 	searchText string,
	// 	listQuery *utils.ListQuery,
	// ) (*utils.ListResult[*models.User], error)
	GetCourseByID(ctx context.Context, id int) (*models.Course, error)
	// GetUserByEmail(ctx context.Context, email string) (*models.Course, error)
	// GetUserByEmailToken(ctx context.Context, emailToken string) (*models.Course, error)
	CreateCourse(ctx context.Context, course *models.Course) error
	// UpdateUser(ctx context.Context, user *models.Course) error
	UpdateCourse(ctx context.Context, course *models.Course) error
	GetCourseByIDRelation(ctx context.Context, relations []string, id int) (*models.Course, error)
	
	FindCourseRelationPaginate(ctx context.Context, relations []string,  listQuery *utils.ListQuery) (*utils.ListResult[*models.Course], error)
	FindIdsCourseAuthor(ctx context.Context,  idUser int) ([]int, error)
	GetCoursesAuthor(ctx context.Context,  idUser int, query *utils.ListQuery) (*utils.ListResult[*models.Course], error)
	GetAllCourses(
		ctx context.Context,
		listQuery *utils.ListQuery,
	) (*utils.ListResult[*models.Course], error)
	GetTopCourseGroupCategory(ctx context.Context, listQuery *utils.ListQuery) ([]*models.TopCategory, error)
	GetCoursesCategoryIDPaginate(ctx context.Context, categoryID int, levelQuery []string, sortQuery *string, query *utils.ListQuery) (*utils.ListResult[*models.Course], error) 
	GetCoursesBySearchPaginate(ctx context.Context, search string, levelQuery []string, sortQuery *string, query *utils.ListQuery) (*utils.ListResult[*models.Course], error) 
	GetCoursesBySearchNormalPaginate(ctx context.Context, search string, query *utils.ListQuery) (*utils.ListResult[*models.Course], error) 
	GetAllCourseInThisYear(ctx context.Context) ([]*models.Course, error)
	
	
	// UpdateEmailToken(ctx context.Context, id int, mailToken string) error
	// UpdateProduct(ctx context.Context, product *models.User) (*models.User, error)
	// DeleteProductByID(ctx context.Context, uuid uuid.UUID) error
}
