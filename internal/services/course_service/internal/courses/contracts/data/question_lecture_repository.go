package data

import (
	"context"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	// uuid "github.com/satori/go.uuid"

	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"
)

type QuestionLectureRepository interface {
	// GetAllProducts(
	// 	ctx context.Context,
	// 	listQuery *utils.ListQuery,
	// ) (*utils.ListResult[*models.User], error)
	// SearchProducts(
	// 	ctx context.Context,
	// 	searchText string,
	// 	listQuery *utils.ListQuery,
	// ) (*utils.ListResult[*models.User], error)
	// GetUserByID(ctx context.Context, id int) (*models.Course, error)
	// GetUserByEmail(ctx context.Context, email string) (*models.Course, error)
	// GetUserByEmailToken(ctx context.Context, emailToken string) (*models.Course, error)
	CreateQuestionLecture(ctx context.Context, questionLecture *models.QuestionLecture) error
		// GetUserByEmailToken(ctx context.Context, emailToken string) (*models.Course, error)
	// GetAllReports(
	// 	ctx context.Context,
	// 	listQuery *utils.ListQuery,
	// ) (*utils.ListResult[*models.Report], error)
	GetQuestionLectureByID(ctx context.Context, id int) (*models.QuestionLecture, error)
	UpdateQuestionLecture(ctx context.Context, questionLecture *models.QuestionLecture) error
	DeleteQuestionLectureByID(ctx context.Context, id int) error
	// GetAllQuestionLectures(ctx context.Context, listQuery *utils.ListQuery) (*utils.ListResult[*models.QuestionLecture], error)
	FindQuestionLecturesRelationPaginate(ctx context.Context, relations []string,  listQuery *utils.ListQuery) (*utils.ListResult[*models.QuestionLecture], error)
	FindQuestionLecturesAuthorRelationPaginate(ctx context.Context, courseIDs []int, sortQuery *string, search *string, listQuery *utils.ListQuery) (*utils.ListResult[*models.QuestionLecture], error)

	// UpdateUser(ctx context.Context, user *models.Course) error

	// UpdateEmailToken(ctx context.Context, id int, mailToken string) error
	// UpdateProduct(ctx context.Context, product *models.User) (*models.User, error)
	// DeleteProductByID(ctx context.Context, uuid uuid.UUID) error
}
