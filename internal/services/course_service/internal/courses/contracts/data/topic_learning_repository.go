package data

import (
	"context"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	// uuid "github.com/satori/go.uuid"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/core/data/specification"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"
)

type TopicLearningRepository interface {
	// GetAllProducts(
	// 	ctx context.Context,
	// 	listQuery *utils.ListQuery,
	// ) (*utils.ListResult[*models.User], error)
	// SearchProducts(
	// 	ctx context.Context,
	// 	searchText string,
	// 	listQuery *utils.ListQuery,
	// ) (*utils.ListResult[*models.User], error)
	GetTopicLearningByID(ctx context.Context, id int) (*models.TopicLearning, error)
	// GetUserByEmail(ctx context.Context, email string) (*models.Course, error)
	// GetUserByEmailToken(ctx context.Context, emailToken string) (*models.Course, error)
	CreateTopicLearning(ctx context.Context, topicLearning *models.TopicLearning) error
	// GetUserCourseByID(ctx context.Context, id int) (*models.UserCourse, error)
	// // GetQuizByID(ctx context.Context, id int) (*models.Quiz, error)
	UpdateTopicLearning(ctx context.Context, topicLearning *models.TopicLearning) error
	DeleteTopicLearningByID(ctx context.Context, id int) error
	FindTopicLearningRelation(ctx context.Context, specification specification.Specification, relations []string) ([]*models.TopicLearning, error)
	GetTopicLearningByIDRelation(ctx context.Context, relations []string, id int) (*models.TopicLearning, error)

	FindTopicLearningRelationPaginate(ctx context.Context, relations []string, listQuery *utils.ListQuery) (*utils.ListResult[*models.TopicLearning], error)

	FindAssociationLearningExist(ctx context.Context, topicLearning *models.TopicLearning, learning *models.Learning) error
	FindAssociationLearning(ctx context.Context, topicLearning *models.TopicLearning) ([]*models.Learning, error)
	AppendAssociationLearning(ctx context.Context, topicLearning *models.TopicLearning, learning *models.Learning) error
	RemoveAssociationLearning(ctx context.Context, topicLearning *models.TopicLearning, learning *models.Learning) error
	// GetUserCourseByUserCourseID(ctx context.Context, userID int, courseID int) (*models.UserCourse, error)
	// DeleteUserCourseByID(ctx context.Context, id int) error
	// GetAllLearnings(
	// 	ctx context.Context,
	// 	listQuery *utils.ListQuery,
	// ) (*utils.ListResult[*models.UserCourse], error)

	// UpdateUser(ctx context.Context, user *models.Course) error

	// UpdateEmailToken(ctx context.Context, id int, mailToken string) error
	// UpdateProduct(ctx context.Context, product *models.User) (*models.User, error)
}
