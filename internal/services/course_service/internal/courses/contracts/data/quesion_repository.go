package data

import (
	"context"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	// uuid "github.com/satori/go.uuid"

	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"
)

type QuestionRepository interface {
	// GetAllProducts(
	// 	ctx context.Context,
	// 	listQuery *utils.ListQuery,
	// ) (*utils.ListResult[*models.User], error)
	// SearchProducts(
	// 	ctx context.Context,
	// 	searchText string,
	// 	listQuery *utils.ListQuery,
	// ) (*utils.ListResult[*models.User], error)
	GetQuestionByID(ctx context.Context, id int) (*models.Question, error)
	CreateQuestion(ctx context.Context, question *models.Question) error
	UpdateQuestion(ctx context.Context, question *models.Question) error
	DeleteQuestionByID(ctx context.Context, id int) error
	GetQuestionByIDRelation(ctx context.Context, relations []string, id int) (*models.Question, error)

	// GetUserByEmail(ctx context.Context, email string) (*models.Course, error)
	// GetUserByEmailToken(ctx context.Context, emailToken string) (*models.Course, error)
	// UpdateUser(ctx context.Context, user *models.Course) error

	// UpdateEmailToken(ctx context.Context, id int, mailToken string) error
	// UpdateProduct(ctx context.Context, product *models.User) (*models.User, error)
	// DeleteProductByID(ctx context.Context, uuid uuid.UUID) error
}
