package grpc

import (
	"context"
	"fmt"

	// productsService
	"emperror.dev/errors"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/mapper"

	catalogsError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"

	getCourseByIdDto "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_course_by_id/dtos"
	getCourseIdQuery "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_course_by_id/queries"

	getCurriculumsByCourseIdDto "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_curriculums_by_course_id/dtos"
	getCurriculumsByCourseIdQuery "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_curriculums_by_course_id/queries"

	coursesService "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
)

type CourseGrpcServiceServer struct {
	logger logger.Logger
}

func NewCourseGrpcService(
	logger logger.Logger,

) *CourseGrpcServiceServer {

	return &CourseGrpcServiceServer{
		logger,
	}
}

func (s *CourseGrpcServiceServer) GetCourseByID(ctx context.Context, req *coursesService.GetCourseByIdReq) (*coursesService.GetCourseByIdRes, error) {
	query, errValidate := getCourseIdQuery.NewGetCourseById(
		int(req.CourseID),
		nil,
	)
	if errValidate != nil && errValidate.ErrorType != nil {

		validationErr := customErrors.NewValidationErrorWrap(
			errValidate.ErrorType,
			"[getCourseById_handler.StructCtx] command validation failed",
		)
		s.logger.Errorf(
			fmt.Sprintf("[getCourseById_handler.StructCtx] err: {%v}", validationErr),
		)

		return nil, errValidate.ErrorType
	}

	result, err := mediatr.Send[*getCourseIdQuery.GetCourseById, *getCourseByIdDto.GetCourseByIdResponseDto](
		ctx,
		query,
	)

	if err != nil {
		if err.Error() == catalogsError.WrapError(customErrors.ErrCourseNotFound).Error() {
			s.logger.Error(
				fmt.Sprintf(
					"[CourseGrpcServiceServer_GetCourseById.Send] id: {%d}, err: %v",
					query.IdCourse,
					err,
				),
			)
			return nil, customErrors.ErrCourseNotFound
		}
		err = errors.WithMessage(
			err,
			"[CourseGrpcServiceServer_GetCourseById.Send] error in sending GetCourseById",
		)
		s.logger.Errorw(
			fmt.Sprintf(
				"[CourseGrpcServiceServer_GetCourseById.Send] id: {%d}, err: %v",
				query.IdCourse,
				err,
			),
			logger.Fields{"CourseId": query.IdCourse},
		)
		return nil, err
	}
	fmt.Println(result.Course)
	course, err := mapper.Map[*coursesService.Course](result.Course)
	if err != nil {
		err = errors.WithMessage(
			err,
			"[CourseGrpcServiceServer_GetCourseById.Map] error in mapping user",
		)
		return nil, err
	}

	return &coursesService.GetCourseByIdRes{
		Course: course,
	}, nil
}

func (s *CourseGrpcServiceServer) GetCourseDetailByID(ctx context.Context, req *coursesService.GetCourseDetailByIdReq) (*coursesService.GetCourseDetailByIdRes, error) {
	query, errValidate := getCurriculumsByCourseIdQuery.NewGetCurriculumsByCourseID(
		int(req.CourseID),
	)
	if errValidate != nil && errValidate.ErrorType != nil {

		validationErr := customErrors.NewValidationErrorWrap(
			errValidate.ErrorType,
			"[getCourseDetailById_handler.StructCtx] command validation failed",
		)
		s.logger.Errorf(
			fmt.Sprintf("[getCourseDetailById_handler.StructCtx] err: {%v}", validationErr),
		)

		return nil, errValidate.ErrorType
	}
// getCurriculumsByCourseIdDto
// getCurriculumsByCourseIdQuery
	result, err := mediatr.Send[*getCurriculumsByCourseIdQuery.GetCurriculumsByCourseID, *getCurriculumsByCourseIdDto.GetCurriculumsByCourseIDResponseDto](
		ctx,
		query,
	)

	if err != nil {
		if err.Error() == catalogsError.WrapError(customErrors.ErrCourseNotFound).Error() {
			s.logger.Error(
				fmt.Sprintf(
					"[CourseGrpcServiceServer_getCourseDetailById.Send] id: {%d}, err: %v",
					query.CourseID,
					err,
				),
			)
			return nil, customErrors.ErrCourseNotFound
		}
		err = errors.WithMessage(
			err,
			"[CourseGrpcServiceServer_getCourseDetailById.Send] error in sending getCourseDetailById",
		)
		s.logger.Errorw(
			fmt.Sprintf(
				"[CourseGrpcServiceServer_getCourseDetailById.Send] id: {%d}, err: %v",
				query.CourseID,
				err,
			),
			logger.Fields{"CourseId": query.CourseID},
		)
		return nil, err
	}
	fmt.Println(result.Course.CountReview)
	
	course, err := mapper.Map[*coursesService.CourseDetail](&result.Course)
	if err != nil {
		err = errors.WithMessage(
			err,
			"[CourseGrpcServiceServer_getCourseDetailById.Map] error in mapping course",
		)
		return nil, err
	}
fmt.Println("course: ", course)
	return &coursesService.GetCourseDetailByIdRes{
		Course: course,
	}, nil
}