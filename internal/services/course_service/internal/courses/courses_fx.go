package courses

import (
	// "fmt"

	// customEcho "gitlab.com/ctkai01/kaidemy/internal/pkg/http/custom_echo"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "fmt"

	"github.com/labstack/echo/v4"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/repositories"
	"go.uber.org/fx"

	// "github.com/mehdihadeli/go-ecommerce-microservices/internal/services/catalogreadservice/internal/products/data/repositories"
	// getProductByIdV1 "github.com/mehdihadeli/go-ecommerce-microservices/internal/services/catalogreadservice/internal/products/features/get_product_by_id/v1/endpoints"
	// changePassword "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/change_password/endpoints"
	// forgotPassword "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/forgot_password/endpoints"
	// loginStandard "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login/endpoints"
	// loginByGoogle "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login_by_google/endpoints"
	// register "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/register/endpoints"
	// requestLoginByGoogle "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/endpoints"
	// resetPassword "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/reset_password/endpoints"
	// createCourse "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/"
	createCourse "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/endpoints"
	updateCourse "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_course/endpoints"
	getCourseByID "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_course_by_id/endpoints"

	createCurriculum "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_curriculum/endpoints"
	deleteCurriculum "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_curriculum/endpoints"
	updateCurriculum "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_curriculum/endpoints"

	createLecture "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_lecture/endpoints"
	deleteLecture "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_lecture/endpoints"
	updateLecture "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_lecture/endpoints"

	createQuiz "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_quiz/endpoints"
	deleteQuiz "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_quiz/endpoints"
	updateQuiz "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_quiz/endpoints"

	createQuestion "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_question/endpoints"
	deleteQuestion "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_question/endpoints"
	updateQuestion "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_question/endpoints"

	createAnswer "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_answer/endpoints"
	deleteAnswer "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_answer/endpoints"
	updateAnswer "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_answer/endpoints"

	createWishCourse "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_wish_course/endpoints"
	getLearnings "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_learnings/endpoints"
	deleteWishCourse "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/remove_wish_course/endpoints"
	updateLearning "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_learning/endpoints"

	addLearningTopicLearning "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/add_learning_topic_learning/endpoints"
	createTopicLearning "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_topic_learning/endpoints"
	deleteTopicLearning "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_topic_learning/endpoints"
	getTopicLearning "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_topic_learning/endpoints"
	removeLearningTopicLearning "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/remove_learning_topic_learning/endpoints"
	updateTopicLearning "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_topic_learning/endpoints"

	createReport "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_report/endpoints"
	getReports "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_reports/endpoints"

	createQuestionLecture "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_question_lecture/endpoints"
	deleteQuestionLecture "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_question_lecture/endpoints"
	updateQuestionLecture "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_question_lecture/endpoints"

	createAnswerLecture "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_answer_lecture/endpoints"
	deleteAnswerLecture "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_answer_lecture/endpoints"
	updateAnswerLecture "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_answer_lecture/endpoints"

	getCurriculumsByCourseID "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_curriculums_by_course_id/endpoints"
	getCoursesByUserId "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_courses_by_user_id/endpoints"
	submitReviewCourse "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/submit_review_course/endpoints"
	getCoursesReviews "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_courses_reviews/endpoints"
	getCourses "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_courses/endpoints"
	approvalReviewCourse "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/approval_review_course/endpoints"
	getTopCourses "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_top_courses/endpoints"
	markLecture "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/mark_lecture/endpoints"
	getCoursePublicByUserID "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_courses_public_by_user_id/endpoints"
	getInfoAuthor "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_info_author/endpoints"
	getReviewsByCourseID "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_reviews_by_course_id/endpoints"
	getOverallReviewByCourseID "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_overall_review_by_course_id/endpoints"
	getQuestionLectures "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_lecture_questions/endpoints"
	getLectureLectures "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_lecture_answers/endpoints"
	getCoursesByCategoryID "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_courses_by_category_id/endpoints"
	getAuthCoursesByCategoryID "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_auth_courses_by_category_id/endpoints"
	getAuthCoursesBySearch "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_auth_courses_by_search/endpoints"
	getCoursesBySearch "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_courses_by_search/endpoints"
	getSearch "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_search/endpoints"
	getReviewsByAuthor "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_reviews_by_author/endpoints"
	getCoursesByAuthor "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_courses_by_author/endpoints"
	getUsersByAuthor "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_users_by_author/endpoints"
	getLectureLecturesAuthor "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_lecture_questions_author/endpoints"
	getOverviewCourseAuthor "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_overview_course_by_author/endpoints"
	getOverviewAdmin "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_overview_by_admin/endpoints"
	getStudentCurriculumByCourseID "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_student_curriculums_by_course_id/endpoints"
	
	
	// searchProductV1 "github.com/mehdihadeli/go-ecommerce-microservices/internal/services/catalogreadservice/internal/products/features/searching_products/v1/endpoints"
	customEcho "gitlab.com/ctkai01/kaidemy/internal/pkg/http/custom_echo"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/http/middlewares"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"

	// googleOauth "gitlab.com/ctkai01/kaidemy/internal/pkg/oauth/google"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc"
)

var Module = fx.Module(
	"coursesfx",

	// Other provides
	// fx.Provide(repositories.NewRedisProductRepository),
	// fx.Provide(NewHTTPServer),

	fx.Provide(repositories.NewMySqlCourseRepository),
	fx.Provide(repositories.NewMySqlAnswerRepository),
	fx.Provide(repositories.NewMySqlCurriculumRepository),
	fx.Provide(repositories.NewMySqlLectureRepository),
	fx.Provide(repositories.NewMySqlQuestionRepository),
	// fx.Provide(repositories.NewMySqlQuizRepository),
	fx.Provide(repositories.NewMySqlAssetRepository),
	fx.Provide(repositories.NewMySqlLearningRepository),
	fx.Provide(repositories.NewMySqlTopicLearningRepository),
	fx.Provide(repositories.NewMySqlReportRepository),
	fx.Provide(repositories.NewMySqlQuestionLectureRepository),
	fx.Provide(repositories.NewMySqlAnswerLectureRepository),
	// fx.Provide(repositories.NewMySqlTopicLearningCourseRepository),

	fx.Provide(grpc.NewCourseGrpcService),

	fx.Provide(fx.Annotate(func(catalogsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		catalogsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			v1.Use(middlewares.AuthMiddleware)
			// v1.Use(middlewares.TeacherMiddleware)
			group := v1.Group("/courses")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"courses-echo-group"`))),

	fx.Provide(fx.Annotate(func(catalogsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		catalogsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			// v1.Use(middlewares.TeacherMiddleware)
			group := v1.Group("/courses")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"courses-not_auth-echo-group"`))),

	fx.Provide(fx.Annotate(func(catalogsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		catalogsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			v1.Use(middlewares.AuthMiddleware)
			v1.Use(middlewares.AdminMiddleware)
			group := v1.Group("/courses")
			g = group
		})

		return g 
	}, fx.ResultTags(`name:"courses-admin-echo-group"`))),

	fx.Provide(fx.Annotate(func(catalogsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		catalogsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			v1.Use(middlewares.AuthMiddleware)
			v1.Use(middlewares.TeacherMiddleware)
			group := v1.Group("/curriculums")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"curriculums-echo-group"`))),

	fx.Provide(fx.Annotate(func(catalogsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		catalogsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			v1.Use(middlewares.AuthMiddleware)
			v1.Use(middlewares.TeacherMiddleware)
			group := v1.Group("/lectures")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"lectures-echo-group"`))),

	fx.Provide(fx.Annotate(func(catalogsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		catalogsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			v1.Use(middlewares.AuthMiddleware)
			group := v1.Group("/lectures")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"lectures-normal-echo-group"`))),

	fx.Provide(fx.Annotate(func(catalogsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		catalogsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			v1.Use(middlewares.AuthMiddleware)
			v1.Use(middlewares.TeacherMiddleware)
			group := v1.Group("/quizs")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"quizs-echo-group"`))),

	fx.Provide(fx.Annotate(func(catalogsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		catalogsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			v1.Use(middlewares.AuthMiddleware)
			v1.Use(middlewares.TeacherMiddleware)
			group := v1.Group("/questions")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"questions-echo-group"`))),

	fx.Provide(fx.Annotate(func(catalogsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		catalogsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			v1.Use(middlewares.AuthMiddleware)
			v1.Use(middlewares.TeacherMiddleware)
			group := v1.Group("/answers")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"answers-echo-group"`))),

	fx.Provide(fx.Annotate(func(catalogsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		catalogsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			v1.Use(middlewares.AuthMiddleware)
			group := v1.Group("/learnings")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"learnings-echo-group"`))),

	fx.Provide(fx.Annotate(func(catalogsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		catalogsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			v1.Use(middlewares.AuthMiddleware)
			group := v1.Group("/topic-learnings")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"topic-learnings-echo-group"`))),

	fx.Provide(fx.Annotate(func(catalogsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		catalogsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			v1.Use(middlewares.AuthMiddleware)
			group := v1.Group("/reports")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"reports-echo-group"`))),

	fx.Provide(fx.Annotate(func(catalogsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		catalogsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			v1.Use(middlewares.AuthMiddleware)
			v1.Use(middlewares.AdminMiddleware)
			group := v1.Group("/reports")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"reports-admin-echo-group"`))),

	fx.Provide(fx.Annotate(func(catalogsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		catalogsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			v1.Use(middlewares.AuthMiddleware)
			group := v1.Group("/question-lectures")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"question-lectures-echo-group"`))),

	fx.Provide(fx.Annotate(func(catalogsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		catalogsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			v1.Use(middlewares.AuthMiddleware)
			group := v1.Group("/answer-lectures")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"answer-lectures-echo-group"`))),

	fx.Provide(
		// //Course route
		route.AsRoute(createCourse.NewCreateCourseEndpoint, "courses-routes"),
		route.AsRoute(updateCourse.NewUpdateCourseEndpoint, "courses-routes"),
		route.AsRoute(getCourseByID.NewGetCourseByIdEndpoint, "courses-routes"),
		route.AsRoute(getCoursesByUserId.NewGetCourseByUserIDEndpoint, "courses-routes"),
		route.AsRoute(submitReviewCourse.NewSubmitReviewCourseEndpoint, "courses-routes"),
		route.AsRoute(getCoursesReviews.NewGetCourseReviewsEndpoint, "courses-routes"),
		route.AsRoute(approvalReviewCourse.NewApprovalReviewCourseEndpoint, "courses-routes"),
		route.AsRoute(getCourses.NewGetCourseEndpoint, "courses-routes"),
		route.AsRoute(getTopCourses.NewGetTopCourseEndpoint, "courses-routes"),
		route.AsRoute(getCoursePublicByUserID.NewGetCourseByUserIDEndpoint, "courses-routes"),
		route.AsRoute(getInfoAuthor.NewGetInfoAuthorEndpoint, "courses-routes"),
		route.AsRoute(getReviewsByCourseID.NewGetReviewsByCourseIDEndpoint, "courses-routes"),
		route.AsRoute(getOverallReviewByCourseID.NewGetOverallReviewsByCourseIDEndpoint, "courses-routes"),
		route.AsRoute(getCoursesByCategoryID.NewGetCourseByCategoryIDEndpoint, "courses-routes"),
		route.AsRoute(getAuthCoursesByCategoryID.NewGetAuthCourseByCategoryIDEndpoint, "courses-routes"),
		route.AsRoute(getAuthCoursesBySearch.NewGetAuthCourseBySearchEndpoint, "courses-routes"),
		route.AsRoute(getCoursesBySearch.NewGetCourseBySearchEndpoint, "courses-routes"),
		route.AsRoute(getSearch.NewGetSearchEndpoint, "courses-routes"),
		route.AsRoute(getReviewsByAuthor.NewGetReviewsByAuthorEndpoint, "courses-routes"),
		route.AsRoute(getCoursesByAuthor.NewGetCoursesByAuthorEndpoint, "courses-routes"),
		route.AsRoute(getUsersByAuthor.NewGetUsersByAuthorEndpoint, "courses-routes"),
		route.AsRoute(getLectureLecturesAuthor.NewGetLectureQuestionAuthorEndpoint, "courses-routes"),
		route.AsRoute(getLectureLecturesAuthor.NewGetLectureQuestionAuthorEndpoint, "courses-routes"),
		route.AsRoute(getOverviewCourseAuthor.NewOverviewCourseByAuthorEndpoint, "courses-routes"),
		route.AsRoute(getOverviewAdmin.NewOverviewByAdminEndpoint, "courses-routes"),
		route.AsRoute(getStudentCurriculumByCourseID.NewGetStudentCurriculumsByCourseIDEndpoint, "courses-routes"),

		
		
		// Curriculum route
		route.AsRoute(createCurriculum.NewCurriculumEndpoint, "courses-routes"),
		route.AsRoute(updateCurriculum.NewUpdateCurriculumEndpoint, "courses-routes"),
		route.AsRoute(deleteCurriculum.NewDeleteCurriculumEndpoint, "courses-routes"),

		// Lecture route
		route.AsRoute(createLecture.NewLessonsRouteParamsEndpoint, "courses-routes"),
		route.AsRoute(deleteLecture.NewDeleteLectureEndpoint, "courses-routes"),
		route.AsRoute(updateLecture.NewUpdateLectureEndpoint, "courses-routes"),
		route.AsRoute(markLecture.NewMarkLectureParamsEndpoint, "courses-routes"),
		// route.AsRoute(updateLesson.NewUpdateLessonEndpoint, "courses-routes"),
		
		//Quiz route
		route.AsRoute(createQuiz.NewQuizsRouteParamsEndpoint, "courses-routes"),
		route.AsRoute(updateQuiz.NewUpdateQuizEndpoint, "courses-routes"),
		route.AsRoute(deleteQuiz.NewDeleteQuizEndpoint, "courses-routes"),

		// Question route
		route.AsRoute(createQuestion.NewQuestionsRouteParamsEndpoint, "courses-routes"),
		route.AsRoute(updateQuestion.NewUpdateQuestionEndpoint, "courses-routes"),
		route.AsRoute(deleteQuestion.NewDeleteQuestionEndpoint, "courses-routes"),

		// Answer route
		route.AsRoute(createAnswer.NewAnswersRouteParamsEndpoint, "courses-routes"),
		route.AsRoute(updateAnswer.NewUpdateAnswerEndpoint, "courses-routes"),
		route.AsRoute(deleteAnswer.NewDeleteAnswerEndpoint, "courses-routes"),

		// Learning route
		route.AsRoute(updateLearning.NewUpdateLearningEndpoint, "courses-routes"),
		route.AsRoute(createWishCourse.NewCreateWishCourseEndpoint, "courses-routes"),
		route.AsRoute(deleteWishCourse.NewRemoveWishCourseEndpoint, "courses-routes"),
		route.AsRoute(getLearnings.NewGetLearningsEndpoint, "courses-routes"),

		// Topic Learning route
		route.AsRoute(createTopicLearning.NewCreateTopicLearningEndpoint, "courses-routes"),
		route.AsRoute(addLearningTopicLearning.NewAddLearningTopicLearningEndpoint, "courses-routes"),
		route.AsRoute(updateTopicLearning.NewUpdateTopicLearningEndpoint, "courses-routes"),
		route.AsRoute(deleteTopicLearning.NewDeleteTopicLearningEndpoint, "courses-routes"),
		route.AsRoute(getTopicLearning.NewGetTopicLearningEndpoint, "courses-routes"),
		route.AsRoute(removeLearningTopicLearning.NewRemoveLearningTopicLearningEndpoint, "courses-routes"),

		// Report route
		route.AsRoute(createReport.NewCreateReportEndpoint, "courses-routes"),
		route.AsRoute(getReports.NewGetReportsEndpoint, "courses-routes"),

		// Question lecture route
		route.AsRoute(createQuestionLecture.NewCreateQuestionLectureEndpoint, "courses-routes"),
		route.AsRoute(updateQuestionLecture.NewQuestionLectureEndpoint, "courses-routes"),
		route.AsRoute(deleteQuestionLecture.NewDeleteQuestionLectureEndpoint, "courses-routes"),
		route.AsRoute(getQuestionLectures.NewGetLectureQuestionEndpoint, "courses-routes"),

		// Answer lecture route
		route.AsRoute(createAnswerLecture.NewCreateAnswerLectureEndpoint, "courses-routes"),
		route.AsRoute(updateAnswerLecture.NewUpdateAnswerLectureEndpoint, "courses-routes"),
		route.AsRoute(deleteAnswerLecture.NewDeleteAnswerLectureEndpoint, "courses-routes"),
		route.AsRoute(getLectureLectures.NewGetLectureAnswerEndpoint, "courses-routes"),

		
		//helper
		route.AsRoute(getCurriculumsByCourseID.NewGetCurriculumsByCourseIDEndpoint, "courses-routes"),
	),
)

//   lc.Append(fx.Hook{
//     OnStart: func(ctx context.Context) error {
//       ln, err := net.Listen("tcp", srv.Addr)
//       if err != nil {
//         return err
//       }
//       fmt.Println("Starting HTTP server at", srv.Addr)
//       go srv.Serve(ln)
//       return nil
//     },
//     OnStop: func(ctx context.Context) error {
//       return srv.Shutdown(ctx)
//     },
//   })
