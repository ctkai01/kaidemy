package repositories

// https://github.com/Kamva/mgm
// https://github.com/mongodb/mongo-go-driver
// https://blog.logrocket.com/how-to-use-mongodb-with-go/
// https://www.mongodb.com/docs/drivers/go/current/quick-reference/
// https://www.mongodb.com/docs/drivers/go/current/fundamentals/bson/
// https://www.mongodb.com/docs

import (
	"context"
	"errors"
	"fmt"
	"math"
	"strings"
	"time"

	// "errors"

	// "fmt"

	// "emperror.dev/errors"
	// uuid2 "github.com/satori/go.uuid"
	"gorm.io/gorm"

	// "go.mongodb.org/mongo-driver/mongo"
	// attribute2 "go.opentelemetry.io/otel/attribute"

	data2 "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/core/data"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/core/data"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/gorm_mysql/repository"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/mongodb"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/mongodb/repository"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/otel/tracing"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// userErrors "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/otel/tracing/attribute"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
)

// const (
// 	productCollection = "products"
// )

type mysqlCourseRepository struct {
	log                   logger.Logger
	gormGenericRepository data.GenericRepository[*models.Course]
}

func NewMySqlCourseRepository(
	log logger.Logger,
	db *gorm.DB,
) data2.CourseRepository {
	gormRepository := repository.NewGenericGormRepository[*models.Course](db)
	return &mysqlCourseRepository{
		log:                   log,
		gormGenericRepository: gormRepository,
	}
}

func (p *mysqlCourseRepository) CreateCourse(
	ctx context.Context,
	course *models.Course,
) error {
	err := p.gormGenericRepository.Add(ctx, course)

	if err != nil {
		return err
	}

	return nil
}

func (p *mysqlCourseRepository) GetCourseByID(
	ctx context.Context,
	id int,
) (*models.Course, error) {
	course, err := p.gormGenericRepository.GetById(ctx, id)

	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, customErrors.ErrCourseNotFound
		}
		return nil, err
	}

	return course, nil
}

func (p *mysqlCourseRepository) UpdateCourse(ctx context.Context, course *models.Course) error {
	err := p.gormGenericRepository.Update(ctx, course)

	if err != nil {
		return err
	}

	return nil
}

func (p *mysqlCourseRepository) GetCourseByIDRelation(
	ctx context.Context,
	relations []string,
	id int,
) (*models.Course, error) {
	category, err := p.gormGenericRepository.GetRelatedByID(ctx, relations, id)

	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, customErrors.ErrCourseNotFound
		}
		return nil, err
	}

	return category, nil
}

func (p *mysqlCourseRepository) FindCourseRelationPaginate(ctx context.Context, relations []string, listQuery *utils.ListQuery) (*utils.ListResult[*models.Course], error) {
	courses, err := p.gormGenericRepository.FindWithRelationPaginate(ctx, relations, listQuery)

	if err != nil {
		return nil, err
	}

	return courses, nil
}

func (p *mysqlCourseRepository) GetAllCourses(
	ctx context.Context,
	listQuery *utils.ListQuery,
) (*utils.ListResult[*models.Course], error) {

	result, err := p.gormGenericRepository.GetAll(ctx, listQuery)
	if err != nil {
		return nil, err
	}

	return result, nil
}

type CourseCount struct {
	CategoryID int
	Count      int
}

func (p *mysqlCourseRepository) FindIdsCourseAuthor(ctx context.Context, idUser int) ([]int, error) {
	// course, err := p.gormGenericRepository.FindWithRelation(ctx, specification, relations)
	var courseIDs []int
	if err := p.gormGenericRepository.CustomDB(ctx).Model(&models.Course{}).Where("user_id", idUser).Select("id").Find(&courseIDs).Error; err != nil {
		if err != nil {
			return nil, err
		}
	}

	return courseIDs, nil
}

func (p *mysqlCourseRepository) GetCoursesAuthor(ctx context.Context,  idUser int, query *utils.ListQuery) (*utils.ListResult[*models.Course], error) {
	// course, err := p.gormGenericRepository.FindWithRelation(ctx, specification, relations)
	var courses []*models.Course
	var totalRows int64
	db := p.gormGenericRepository.CustomDB(ctx).Model(&models.Course{}).Where("user_id", idUser)
		

	db.Count(&totalRows)

	db = db.Offset(query.GetOffset()).
		Limit(query.GetLimit()).
		Order(query.GetOrderBy())
		
	if err := db.Find(&courses).Error; err != nil {
		return nil, err
	}
	d := float64(totalRows) / float64(query.GetSize())

	listResult := &utils.ListResult[*models.Course]{
		Size:       query.GetSize(),
		Page:       query.GetPage(),
		TotalPage: int(math.Ceil(d)),
		TotalItems: totalRows,
		Items:      courses,
	}
	return listResult, nil
}


func (p *mysqlCourseRepository) GetCoursesCategoryIDPaginate(ctx context.Context, categoryID int, levelQuery []string, sortQuery *string, query *utils.ListQuery) (*utils.ListResult[*models.Course], error) {
	var courses []*models.Course
	var totalRows int64

	db := p.gormGenericRepository.CustomDB(ctx).Model(&models.Course{}).Where("(category_id = ? OR sub_category_id = ?) AND review_status = ?", categoryID, categoryID, constants.REVIEW_VERIFY_STATUS)

	db = db.Preload("Curriculums.Lectures.Assets").Preload("Learnings", "(type = ? OR type = ?)", constants.STANDARD_TYPE, constants.ARCHIE)
	// for _, association := range preload {
	// 	db = db.Preload(association, func(db *gorm.DB) *gorm.DB {
	// 		return db.Order("created_at desc") // Order orders by the Amount field in descending order
	// 	})
	// }
	if levelQuery != nil {
		whereQuery := fmt.Sprintf("%s IN (?)", "level_id")

		db = db.Where(whereQuery, levelQuery)
	}

	if sortQuery != nil {
		if *sortQuery == constants.NEWEST_SORT {
			db = db.Order("updated_at DESC")
		}
	}

	if query.Filters != nil {
		for _, filter := range query.Filters {

			column := filter.Field
			action := filter.Comparison
			value := filter.Value
			switch action {
			case "equals":
				whereQuery := fmt.Sprintf("%s = ?", column)
				if value != nil {
					db = db.WithContext(ctx).Where(whereQuery, *value)
				} else {
					whereQuery := fmt.Sprintf("%s IS NULL", column)

					db = db.Debug().WithContext(ctx).Where(whereQuery)
				}
				break
			case "contains":
				whereQuery := fmt.Sprintf("%s LIKE ?", column)
				if value != nil {
					db = db.WithContext(ctx).Where(whereQuery, "%"+*value+"%")
				} else {
					db = db.WithContext(ctx).Where(whereQuery, "%%")
				}

				break
			case "in":
				whereQuery := fmt.Sprintf("%s IN (?)", column)

				queryArray := strings.Split(*value, ",")
				db = db.WithContext(ctx).Where(whereQuery, queryArray)
				break
			}
		}
	}

	db.Count(&totalRows)

	// db = db.Offset(query.GetOffset()).
	// 	Limit(query.GetLimit())
		// Order(query.GetOrderBy())

	if err := db.Find(&courses).Error; err != nil {
		return nil, err
	}
	d := float64(totalRows) / float64(query.GetSize())

	listResult := &utils.ListResult[*models.Course]{
		Size:       query.GetSize(),
		Page:       int(math.Ceil(d)),
		TotalItems: totalRows,
		Items:      courses,
	}
	return listResult, nil
}

func (p *mysqlCourseRepository) GetCoursesBySearchPaginate(ctx context.Context, search string, levelQuery []string, sortQuery *string, query *utils.ListQuery) (*utils.ListResult[*models.Course], error) {
	var courses []*models.Course
	var totalRows int64

	db := p.gormGenericRepository.CustomDB(ctx).Model(&models.Course{}).Where("(title LIKE ?) AND review_status = ?", "%"+search+"%", constants.REVIEW_VERIFY_STATUS)

	db = db.Preload("Curriculums.Lectures.Assets").Preload("Learnings", "(type = ? OR type = ?)", constants.STANDARD_TYPE, constants.ARCHIE)
	// for _, association := range preload {
	// 	db = db.Preload(association, func(db *gorm.DB) *gorm.DB {
	// 		return db.Order("created_at desc") // Order orders by the Amount field in descending order
	// 	})
	// }
	if levelQuery != nil {
		whereQuery := fmt.Sprintf("%s IN (?)", "level_id")

		db = db.Where(whereQuery, levelQuery)
	}

	if sortQuery != nil {
		if *sortQuery == constants.NEWEST_SORT {
			db = db.Order("updated_at DESC")
		}
	}

	if query.Filters != nil {
		for _, filter := range query.Filters {

			column := filter.Field
			action := filter.Comparison
			value := filter.Value
			switch action {
			case "equals":
				whereQuery := fmt.Sprintf("%s = ?", column)
				if value != nil {
					db = db.WithContext(ctx).Where(whereQuery, *value)
				} else {
					whereQuery := fmt.Sprintf("%s IS NULL", column)

					db = db.Debug().WithContext(ctx).Where(whereQuery)
				}
				break
			case "contains":
				whereQuery := fmt.Sprintf("%s LIKE ?", column)
				if value != nil {
					db = db.WithContext(ctx).Where(whereQuery, "%"+*value+"%")
				} else {
					db = db.WithContext(ctx).Where(whereQuery, "%%")
				}

				break
			case "in":
				whereQuery := fmt.Sprintf("%s IN (?)", column)

				queryArray := strings.Split(*value, ",")
				db = db.WithContext(ctx).Where(whereQuery, queryArray)
				break
			}
		}
	}

	db.Count(&totalRows)

	// db = db.Offset(query.GetOffset()).
	// 	Limit(query.GetLimit())
		// Order(query.GetOrderBy())

	if err := db.Find(&courses).Error; err != nil {
		return nil, err
	}
	d := float64(totalRows) / float64(query.GetSize())

	listResult := &utils.ListResult[*models.Course]{
		Size:       query.GetSize(),
		Page:       int(math.Ceil(d)),
		TotalItems: totalRows,
		Items:      courses,
	}
	return listResult, nil
}

func (p *mysqlCourseRepository) GetCoursesBySearchNormalPaginate(ctx context.Context, search string, query *utils.ListQuery) (*utils.ListResult[*models.Course], error)  {
	var courses []*models.Course
	var totalRows int64

	db := p.gormGenericRepository.CustomDB(ctx).Model(&models.Course{}).Where("(title LIKE ?) AND review_status = ?", "%"+search+"%", constants.REVIEW_VERIFY_STATUS)

	// db = db.Preload("Curriculums.Lectures.Assets").Preload("Learnings", "(type = ? OR type = ?)", constants.STANDARD_TYPE, constants.ARCHIE)
	// for _, association := range preload {
	// 	db = db.Preload(association, func(db *gorm.DB) *gorm.DB {
	// 		return db.Order("created_at desc") // Order orders by the Amount field in descending order
	// 	})
	// }
	// if levelQuery != nil {
	// 	whereQuery := fmt.Sprintf("%s IN (?)", "level_id")

	// 	db = db.Where(whereQuery, levelQuery)
	// }

	// if sortQuery != nil {
	// 	if *sortQuery == constants.NEWEST_SORT {
	// 		db = db.Order("updated_at DESC")
	// 	}
	// }

	// if query.Filters != nil {
	// 	for _, filter := range query.Filters {

	// 		column := filter.Field
	// 		action := filter.Comparison
	// 		value := filter.Value
	// 		switch action {
	// 		case "equals":
	// 			whereQuery := fmt.Sprintf("%s = ?", column)
	// 			if value != nil {
	// 				db = db.WithContext(ctx).Where(whereQuery, *value)
	// 			} else {
	// 				whereQuery := fmt.Sprintf("%s IS NULL", column)

	// 				db = db.Debug().WithContext(ctx).Where(whereQuery)
	// 			}
	// 			break
	// 		case "contains":
	// 			whereQuery := fmt.Sprintf("%s LIKE ?", column)
	// 			if value != nil {
	// 				db = db.WithContext(ctx).Where(whereQuery, "%"+*value+"%")
	// 			} else {
	// 				db = db.WithContext(ctx).Where(whereQuery, "%%")
	// 			}

	// 			break
	// 		case "in":
	// 			whereQuery := fmt.Sprintf("%s IN (?)", column)

	// 			queryArray := strings.Split(*value, ",")
	// 			db = db.WithContext(ctx).Where(whereQuery, queryArray)
	// 			break
	// 		}
	// 	}
	// }

	db.Count(&totalRows)

	// db = db.Offset(query.GetOffset()).
	// 	Limit(query.GetLimit())
		// Order(query.GetOrderBy())

	if err := db.Find(&courses).Error; err != nil {
		return nil, err
	}
	d := float64(totalRows) / float64(query.GetSize())

	listResult := &utils.ListResult[*models.Course]{
		Size:       query.GetSize(),
		Page:       int(math.Ceil(d)),
		TotalItems: totalRows,
		Items:      courses,
	}
	return listResult, nil
}
// 

func (p *mysqlCourseRepository) GetTopCourseGroupCategory(
	ctx context.Context,
	listQuery *utils.ListQuery,
) ([]*models.TopCategory, error) {
	var top5Categories []CourseCount
	err := p.gormGenericRepository.CustomDB(ctx).Model(&models.Course{}).
		Select("category_id, COUNT(*) as count").
		Where("review_status", constants.REVIEW_VERIFY_STATUS).
		Group("category_id").
		Order("count DESC").
		Limit(5).
		Find(&top5Categories).Error

	if err != nil {
		return nil, err
	}

	categoryIDs := make([]int, len(top5Categories))

	for i, cc := range top5Categories {
		categoryIDs[i] = cc.CategoryID
	}

	var topCourses []*models.TopCategory

	for _, categoryID := range categoryIDs {
		var courses []*models.Course
		err := p.gormGenericRepository.CustomDB(ctx).Model(&models.Course{}).Preload("Curriculums.Lectures.Assets").Preload("Learnings").Where("category_id = ?", categoryID).Where("review_status = ?", constants.REVIEW_VERIFY_STATUS).Order("updated_at DESC").Find(&courses).Error
		if err != nil {
			return nil, err
		}
		topCourses = append(topCourses, &models.TopCategory{
			CategoryID: categoryID,
			Courses:    courses,
		})
	}
	// err = p.gormGenericRepository.CustomDB(ctx).
	// 	Where("category_id IN (?)", categoryIDs).
	// 	Find(&coursesInTopCategories).Error

	// if err != nil {
	// 	return nil, err
	// }
	return topCourses, nil
}

func (p *mysqlCourseRepository) GetAllCourseInThisYear(ctx context.Context) ([]*models.Course, error) {
	var course []*models.Course
	// var learningsUnique []*models.Learning
	currentYear := time.Now().Year()
	db := p.gormGenericRepository.CustomDB(ctx).Model(&models.Course{}).Where("EXTRACT(YEAR FROM created_at) = ?", currentYear)
	// uniqueStudent := make(map[int]bool)

	if err := db.Find(&course).Error; err != nil {
		return nil, err
	}

	// for _, learning := range learnings {
	// 	if _, exists := uniqueStudent[learning.UserID]; exists {
	// 		uniqueStudent[learning.UserID] = true
	// 		learningsUnique = append(learningsUnique, learning)
	// 	}
	// }

	return course, nil
	
}