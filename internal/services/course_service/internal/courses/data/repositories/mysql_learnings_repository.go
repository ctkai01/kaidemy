package repositories

// https://github.com/Kamva/mgm
// https://github.com/mongodb/mongo-go-driver
// https://blog.logrocket.com/how-to-use-mongodb-with-go/
// https://www.mongodb.com/docs/drivers/go/current/quick-reference/
// https://www.mongodb.com/docs/drivers/go/current/fundamentals/bson/
// https://www.mongodb.com/docs

import (
	"context"
	"errors"
	"fmt"
	"math"
	"strings"
	"time"

	// "errors"

	// "errors"

	// "fmt"

	// "emperror.dev/errors"
	// uuid2 "github.com/satori/go.uuid"
	"gorm.io/gorm"

	// "go.mongodb.org/mongo-driver/mongo"
	// attribute2 "go.opentelemetry.io/otel/attribute"

	data2 "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/core/data"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/core/data"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/core/data/specification"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/gorm_mysql/repository"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/mongodb"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/mongodb/repository"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/otel/tracing"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// userErrors "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/otel/tracing/attribute"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
)

// const (
// 	productCollection = "products"
// )

type mysqlLearningRepository struct {
	log                   logger.Logger
	gormGenericRepository data.GenericRepository[*models.Learning]
}

func NewMySqlLearningRepository(
	log logger.Logger,
	db *gorm.DB,
) data2.LearningRepository {
	gormRepository := repository.NewGenericGormRepository[*models.Learning](db)
	return &mysqlLearningRepository{
		log:                   log,
		gormGenericRepository: gormRepository,
	}
}

func (p *mysqlLearningRepository) CreateLearning(
	ctx context.Context,
	learning *models.Learning,
) error {
	err := p.gormGenericRepository.Add(ctx, learning)

	if err != nil {
		return err
	}

	return nil
}

func (p *mysqlLearningRepository) GetLearningByID(
	ctx context.Context,
	id int,
) (*models.Learning, error) {
	learning, err := p.gormGenericRepository.GetById(ctx, id)

	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, customErrors.ErrLearningNotFound
		}
		return nil, err
	}

	return learning, nil
}

func (p *mysqlLearningRepository) UpdateLearning(ctx context.Context, learning *models.Learning) error {
	err := p.gormGenericRepository.Update(ctx, learning)

	if err != nil {
		return err
	}

	return nil
}

func (p *mysqlLearningRepository) GetLearningByIDRelation(
	ctx context.Context,
	relations []string,
	id int,
) (*models.Learning, error) {
	learning, err := p.gormGenericRepository.GetRelatedByID(ctx, relations, id)

	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, customErrors.ErrLearningNotFound
		}
		return nil, err
	}

	return learning, nil
}
func (p *mysqlLearningRepository) GetCountLearningForCourseIDs(ctx context.Context, courseIDs []int) (int, error) {
	var count int64

	if err := p.gormGenericRepository.CustomDB(ctx).Model(&models.Learning{}).Distinct("user_id").Where("course_id IN (?) AND (type = ? OR type = ?) AND (star_count IS NOT NULL)", courseIDs, constants.STANDARD_TYPE, constants.ARCHIE).Count(&count).Error; err != nil {
		return 0, err
	}

	return int(count), nil
}

func (p *mysqlLearningRepository) GetReviewsCourseIDs(ctx context.Context, courseIDs []int) ([]*models.Learning, error) {
	var learnings []*models.Learning

	if err := p.gormGenericRepository.CustomDB(ctx).Model(&models.Learning{}).Where("course_id IN (?) AND (type = ? OR type = ?) AND (star_count IS NOT NULL)", courseIDs, constants.STANDARD_TYPE, constants.ARCHIE).Find(&learnings).Error; err != nil {
		return nil, err
	}

	return learnings, nil
}

func (p *mysqlLearningRepository) GetReviewsByCourseIDsPaginate(ctx context.Context, courseIDs []int, sort *string, reply []string, rating []string, query *utils.ListQuery) (*utils.ListResult[*models.Learning], error) {
	var learnings []*models.Learning
	var totalRows int64

	db := p.gormGenericRepository.CustomDB(ctx).Model(&models.Learning{}).Where("course_id IN (?) AND (type = ? OR type = ?) AND (star_count IS NOT NULL)", courseIDs, constants.STANDARD_TYPE, constants.ARCHIE).Preload("Course")

	if sort != nil {
		if *sort == constants.NEWEST_SORT {
			db = db.Order("updated_at DESC")
		}
	}

	var responseReplyExists, noResponseReplyExists bool

	for _, opt := range reply {
		if opt == constants.RESPONSE_REPLY {
			responseReplyExists = true
		}
		if opt == constants.NO_RESPONSE_REPLY {
			noResponseReplyExists = true
		}
	}

	if responseReplyExists && noResponseReplyExists {
		db = db.Where("comment IS NOT NULL OR comment IS NULL")
	} else {
		for _, opt := range reply {
			if opt == constants.RESPONSE_REPLY {
				db = db.Where("comment IS NOT NULL")
			}
			if opt == constants.NO_RESPONSE_REPLY {
				db = db.Where("comment IS NULL")
			}
		}
	}

	if rating != nil {
		whereQuery := fmt.Sprintf("%s IN (?)", "star_count")

		db = db.Where(whereQuery, rating)
	}
	// if reply != nil {

	db.Count(&totalRows)

	db = db.Offset(query.GetOffset()).
		Limit(query.GetLimit()).
		Order(query.GetOrderBy())

	if err := db.Find(&learnings).Error; err != nil {
		return nil, err
	}
	d := float64(totalRows) / float64(query.GetSize())

	listResult := &utils.ListResult[*models.Learning]{
		Size:       query.GetSize(),
		Page:       query.GetPage(),
		TotalPage:  int(math.Ceil(d)),
		TotalItems: totalRows,
		Items:      learnings,
	}
	return listResult, nil

}

func (p *mysqlLearningRepository) GetEnrollsByCourseIDsPaginate(ctx context.Context, courseIDs []int, query *utils.ListQuery) (*utils.ListResult[*models.Learning], int, error) {
	var learnings []*models.Learning
	var totalRows int64

	db := p.gormGenericRepository.CustomDB(ctx).Model(&models.Learning{}).Where("course_id IN (?) AND (type = ? OR type = ?)", courseIDs, constants.STANDARD_TYPE, constants.ARCHIE)
	uniqueStudent := make(map[int]bool)
	db.Count(&totalRows)

	if err := db.Find(&learnings).Error; err != nil {
		return nil, 0, err
	}
	for _, learning := range learnings {
		uniqueStudent[learning.UserID] = true
	}

	db = db.Preload("Course").Order("created_at DESC")
	db = db.Offset(query.GetOffset()).
		Limit(query.GetLimit()).
		Order(query.GetOrderBy())

	if err := db.Find(&learnings).Error; err != nil {
		return nil, 0, err
	}
	d := float64(totalRows) / float64(query.GetSize())

	listResult := &utils.ListResult[*models.Learning]{
		Size:       query.GetSize(),
		Page:       query.GetPage(),
		TotalPage:  int(math.Ceil(d)),
		TotalItems: totalRows,
		Items:      learnings,
	}
	return listResult, len(uniqueStudent), nil

}

func (p *mysqlLearningRepository) GetEnrollsByCourseIDs(ctx context.Context, courseIDs []int) ([]*models.Learning, error) {
	var learnings []*models.Learning
	var learningsUnique []*models.Learning

	db := p.gormGenericRepository.CustomDB(ctx).Model(&models.Learning{}).Where("course_id IN (?) AND (type = ? OR type = ?)", courseIDs, constants.STANDARD_TYPE, constants.ARCHIE)
	uniqueStudent := make(map[int]bool)

	if err := db.Find(&learnings).Error; err != nil {
		return nil, err
	}

	for _, learning := range learnings {
		if _, exists := uniqueStudent[learning.UserID]; exists {
			uniqueStudent[learning.UserID] = true
			learningsUnique = append(learningsUnique, learning)
		}
	}

	return learningsUnique, nil
}

func (p *mysqlLearningRepository) GetEnrollsByCourseIDsAuthorThisYear(ctx context.Context, courseIDs []int, idUser int) ([]*models.Learning, error) {
	var learnings []*models.Learning
	// var learningsUnique []*models.Learning
	currentYear := time.Now().Year()
	db := p.gormGenericRepository.CustomDB(ctx).Model(&models.Learning{}).Where("course_id IN (?) AND (type = ? OR type = ?)", courseIDs, constants.STANDARD_TYPE, constants.ARCHIE).Where("EXTRACT(YEAR FROM created_at) = ?", currentYear).Not("user_id = ?", idUser)
	// uniqueStudent := make(map[int]bool)

	if err := db.Find(&learnings).Error; err != nil {
		return nil, err
	}

	// for _, learning := range learnings {
	// 	if _, exists := uniqueStudent[learning.UserID]; exists {
	// 		uniqueStudent[learning.UserID] = true
	// 		learningsUnique = append(learningsUnique, learning)
	// 	}
	// }

	return learnings, nil
}

func (p *mysqlLearningRepository) GetLearningByUserCourseID(ctx context.Context, idUser int, idCourse int) (*models.Learning, error) {
	specUserID := specification.Equal("user_id", idUser)
	specCourseID := specification.Equal("course_id", idCourse)
	filter := specification.And(specUserID, specCourseID)

	learnings, err := p.gormGenericRepository.Find(ctx, filter)

	if err != nil {
		return nil, err
	}

	if len(learnings) == 0 {
		return nil, customErrors.ErrLearningNotFound
	}

	return learnings[0], nil
}

func (p *mysqlLearningRepository) GetReviewsCourseIDPaginate(ctx context.Context, courseID int, query *utils.ListQuery) (*utils.ListResult[*models.Learning], error) {

	var learnings []*models.Learning
	var totalRows int64
	db := p.gormGenericRepository.CustomDB(ctx).Model(&models.Learning{}).Where("course_id = ? AND (type = ? OR type = ?) AND (star_count IS NOT NULL OR comment IS NOT NULL)", courseID, constants.STANDARD_TYPE, constants.ARCHIE)

	if query.Filters != nil {
		for _, filter := range query.Filters {

			column := filter.Field
			action := filter.Comparison
			value := filter.Value
			switch action {
			case "equals":
				whereQuery := fmt.Sprintf("%s = ?", column)
				if value != nil {
					db = db.WithContext(ctx).Where(whereQuery, *value)
				} else {
					whereQuery := fmt.Sprintf("%s IS NULL", column)

					db = db.Debug().WithContext(ctx).Where(whereQuery)
				}
				break
			case "contains":
				whereQuery := fmt.Sprintf("%s LIKE ?", column)
				if value != nil {
					db = db.WithContext(ctx).Where(whereQuery, "%"+*value+"%")
				} else {
					db = db.WithContext(ctx).Where(whereQuery, "%%")
				}

				break
			case "in":
				whereQuery := fmt.Sprintf("%s IN (?)", column)

				queryArray := strings.Split(*value, ",")
				db = db.WithContext(ctx).Where(whereQuery, queryArray)
				break
			}
		}
	}

	db.Count(&totalRows)

	db = db.Offset(query.GetOffset()).
		Limit(query.GetLimit()).
		Order(query.GetOrderBy())

	if err := db.Find(&learnings).Error; err != nil {
		return nil, err
	}
	d := float64(totalRows) / float64(query.GetSize())

	listResult := &utils.ListResult[*models.Learning]{
		Size:       query.GetSize(),
		Page:       int(math.Ceil(d)),
		TotalItems: totalRows,
		Items:      learnings,
	}

	return listResult, nil
}

func (p *mysqlLearningRepository) GetOverallReviewsCourseID(ctx context.Context, courseID int) ([]*models.Learning, error) {
	var learnings []*models.Learning
	db := p.gormGenericRepository.CustomDB(ctx).Model(&models.Learning{}).Where("course_id = ? AND (type = ? OR type = ?) AND (star_count IS NOT NULL OR comment IS NOT NULL)", courseID, constants.STANDARD_TYPE, constants.ARCHIE)

	if err := db.Find(&learnings).Error; err != nil {
		return nil, err
	}
	return learnings, nil
}

func (p *mysqlLearningRepository) DeleteLearningByID(ctx context.Context, id int) error {
	err := p.gormGenericRepository.Delete(ctx, id)

	if err != nil {
		return err
	}

	return nil
}

func (p *mysqlLearningRepository) AppendAssociationLectures(ctx context.Context, learning *models.Learning, lecture *models.Lecture) error {
	if err := p.gormGenericRepository.CustomDB(ctx).Model(&learning).Association("Lectures").Append(lecture); err != nil {
		return err
	}

	// Get the pivot record to update isDone
	var learningLecture models.LearningLecture
	if err := p.gormGenericRepository.CustomDB(ctx).
		Where("learning_id = ? AND lecture_id = ?", learning.ID, lecture.ID).
		First(&learningLecture).Error; err != nil {
		return err
	}

	// Update the isDone field
	learningLecture.IsDone = true
	return p.gormGenericRepository.CustomDB(ctx).Save(&learningLecture).Error
}

// CountAssociationLectures
func (p *mysqlLearningRepository) CountAssociationLectures(ctx context.Context, learning *models.Learning, lecture *models.Lecture) (int, error) {

	var count int64
	// Get the pivot record to update isDone
	if err := p.gormGenericRepository.CustomDB(ctx).Table(models.LearningLecture{}.TableName()).
		Where("learning_id = ? AND lecture_id = ? AND is_done = ?", learning.ID, lecture.ID, 1).
		Count(&count).Error; err != nil {
		return 0, err
	}

	return int(count), nil
}

func (p *mysqlLearningRepository) FindLearningsRelationPaginate(ctx context.Context, relations []string, listQuery *utils.ListQuery) (*utils.ListResult[*models.Learning], error) {
	learnings, err := p.gormGenericRepository.FindWithRelationPaginate(ctx, relations, listQuery)

	if err != nil {
		return nil, err
	}

	return learnings, nil
}

func (p *mysqlLearningRepository) GetAllLearnings(
	ctx context.Context,
	listQuery *utils.ListQuery,
) (*utils.ListResult[*models.Learning], error) {

	result, err := p.gormGenericRepository.GetAll(ctx, listQuery)
	if err != nil {
		return nil, err
	}

	// p.log.Infow(
	// 	"[mysqlCategoryRepository_GetAllCategories] categories loaded",
	// 	logger.Fields{"CategoriesResult": result},
	// )

	return result, nil
}

// if err := mysqlRepo.db.Table(user.TableName()).Where("email = ?", email).First(&user).Error; err != nil {
// 	fmt.Println(err)
// 	if err == gorm.ErrRecordNotFound {
// 		return nil, models.ErrUserNotFound
// 	}
// 	return nil, err
// }
// return user, nil
// ctx, span := p.tracer.Start(ctx, "postgresProductRepository.CreateProduct")
// defer span.End()

// err := p.gormGenericRepository.Add(ctx, product)
// if err != nil {
// 	return nil, tracing.TraceErrFromSpan(
// 		span,
// 		errors.WrapIf(
// 			err,
// 			"[postgresProductRepository_CreateProduct.Create] error in the inserting product into the database.",
// 		),
// 	)
// }

// return nil, nil
// }
// func (p *mysqlUserRepository) GetAllProducts(
// 	ctx context.Context,
// 	listQuery *utils.ListQuery,
// ) (*utils.ListResult[*models.User], error) {
// 	ctx, span := p.tracer.Start(ctx, "postgresProductRepository.GetAllProducts")
// 	defer span.End()

// 	result, err := p.gormGenericRepository.GetAll(ctx, listQuery)
// 	if err != nil {
// 		return nil, tracing.TraceErrFromContext(
// 			ctx,
// 			errors.WrapIf(
// 				err,
// 				"[postgresProductRepository_GetAllProducts.Paginate] error in the paginate",
// 			),
// 		)
// 	}

// 	p.log.Infow(
// 		"[postgresProductRepository.GetAllProducts] products loaded",
// 		logger.Fields{"ProductsResult": result},
// 	)
// 	span.SetAttributes(attribute.Object("ProductsResult", result))

// 	return result, nil
// }

// func (p *mysqlUserRepository) SearchProducts(
// 	ctx context.Context,
// 	searchText string,
// 	listQuery *utils.ListQuery,
// ) (*utils.ListResult[*models.User], error) {
// 	ctx, span := p.tracer.Start(ctx, "postgresProductRepository.SearchProducts")
// 	span.SetAttributes(attribute2.String("SearchText", searchText))
// 	defer span.End()

// 	result, err := p.gormGenericRepository.Search(ctx, searchText, listQuery)
// 	if err != nil {
// 		return nil, tracing.TraceErrFromContext(
// 			ctx,
// 			errors.WrapIf(
// 				err,
// 				"[postgresProductRepository_SearchProducts.Paginate] error in the paginate",
// 			),
// 		)
// 	}

// 	p.log.Infow(
// 		fmt.Sprintf(
// 			"[postgresProductRepository.SearchProducts] products loaded for search term '%s'",
// 			searchText,
// 		),
// 		logger.Fields{"ProductsResult": result},
// 	)
// 	span.SetAttributes(attribute.Object("ProductsResult", result))

// 	return result, nil
// }

// func (p *mysqlUserRepository) GetProductById(
// 	ctx context.Context,
// 	uuid uuid2.UUID,
// ) (*models.User, error) {
// 	ctx, span := p.tracer.Start(ctx, "postgresProductRepository.GetProductById")
// 	span.SetAttributes(attribute2.String("ProductId", uuid.String()))
// 	defer span.End()

// 	product, err := p.gormGenericRepository.GetById(ctx, uuid)
// 	if err != nil {
// 		return nil, tracing.TraceErrFromSpan(
// 			span,
// 			errors.WrapIf(
// 				err,
// 				fmt.Sprintf(
// 					"[postgresProductRepository_GetProductById.First] can't find the product with id %s into the database.",
// 					uuid,
// 				),
// 			),
// 		)
// 	}

// 	span.SetAttributes(attribute.Object("Product", product))
// 	p.log.Infow(
// 		fmt.Sprintf(
// 			"[postgresProductRepository.GetProductById] product with id %s laoded",
// 			uuid.String(),
// 		),
// 		logger.Fields{"Product": product, "ProductId": uuid},
// 	)

// 	return product, nil
// }

// 	span.SetAttributes(attribute.Object("Product", product))
// 	p.log.Infow(
// 		fmt.Sprintf(
// 			"[postgresProductRepository.CreateProduct] product with id '%s' created",
// 			product.ProductId,
// 		),
// 		logger.Fields{"Product": product, "ProductId": product.ProductId},
// 	)

// 	return product, nil
// }

// func (p *mysqlUserRepository) UpdateProduct(
// 	ctx context.Context,
// 	updateProduct *models.User,
// ) (*models.User, error) {
// 	ctx, span := p.tracer.Start(ctx, "postgresProductRepository.UpdateProduct")
// 	defer span.End()

// 	err := p.gormGenericRepository.Update(ctx, updateProduct)
// 	if err != nil {
// 		return nil, tracing.TraceErrFromSpan(
// 			span,
// 			errors.WrapIf(
// 				err,
// 				fmt.Sprintf(
// 					"[postgresProductRepository_UpdateProduct.Save] error in updating product with id %s into the database.",
// 					updateProduct.ProductId,
// 				),
// 			),
// 		)
// 	}

// 	span.SetAttributes(attribute.Object("Product", updateProduct))
// 	p.log.Infow(
// 		fmt.Sprintf(
// 			"[postgresProductRepository.UpdateProduct] product with id '%s' updated",
// 			updateProduct.ProductId,
// 		),
// 		logger.Fields{"Product": updateProduct, "ProductId": updateProduct.ProductId},
// 	)

// 	return updateProduct, nil
// }

// func (p *mysqlUserRepository) DeleteProductByID(ctx context.Context, uuid uuid2.UUID) error {
// 	ctx, span := p.tracer.Start(ctx, "postgresProductRepository.UpdateProduct")
// 	span.SetAttributes(attribute2.String("ProductId", uuid.String()))
// 	defer span.End()

// 	err := p.gormGenericRepository.Delete(ctx, uuid)
// 	if err != nil {
// 		return tracing.TraceErrFromSpan(span, errors.WrapIf(err, fmt.Sprintf(
// 			"[postgresProductRepository_DeleteProductByID.Delete] error in the deleting product with id %s into the database.",
// 			uuid,
// 		)))
// 	}

// 	p.log.Infow(
// 		fmt.Sprintf(
// 			"[postgresProductRepository.DeleteProductByID] product with id %s deleted",
// 			uuid,
// 		),
// 		logger.Fields{"Product": uuid},
// 	)

// 	return nil
// }
