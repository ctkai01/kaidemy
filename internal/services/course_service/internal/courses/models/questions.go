package models

import (
	"time"
)

type Question struct {
	ID        int    `json:"id" gorm:"primary_key"`
	Title     string `json:"title" gorm:"not null"`
	LectureID int    `json:"lecture_id" gorm:"type:int;not null"`

	Answers []Answer `json:"answers" gorm:"constraint:OnDelete:CASCADE"` 

	UpdatedAt *time.Time `json:"updated_at" gorm:"column:updated_at"`
	CreatedAt *time.Time `json:"created_at" gorm:"column:created_at"`
}

func (Question) TableName() string { return "questions" }
