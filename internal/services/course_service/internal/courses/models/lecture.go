package models

import (
	"time"
)

type Lecture struct {
	ID            int         `json:"id" gorm:"primary_key"`
	Title         string      `json:"title" gorm:"type:text;not null"`
	Article       *string     `json:"article" gorm:"type:text"`
	Description   *string     `json:"description" gorm:"type:text"`
	Assets        []*Asset    `json:"assets" gorm:"constraint:OnDelete:CASCADE"`
	Questions     []*Question `json:"questions" gorm:"constraint:OnDelete:CASCADE"`
	IsPromotional bool        `json:"is_promotional" gorm:"default:false"`
	CurriculumID  int         `json:"curriculum_id" gorm:"not null"`
	Type          int         `json:"type" gorm:"not null"`
	Order         int         `json:"order" gorm:"not null"`
	Learnings     []*Learning `json:"learnings" gorm:"many2many:learning_lectures"`
	UpdatedAt     *time.Time  `json:"updated_at" gorm:"column:updated_at"`
	CreatedAt     *time.Time  `json:"created_at" gorm:"column:created_at"`
}

func (Lecture) TableName() string { return "lectures" }
