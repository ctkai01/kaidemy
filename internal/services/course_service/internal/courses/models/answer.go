package models

import (
	"time"
)

type Answer struct {
	ID         int     `json:"id" gorm:"primary_key"`
	AnswerText string  `json:"answer_text" gorm:"type:varchar(200);not null"`
	IsCorrect  bool    `json:"is_correct" gorm:"not null"`
	Explain    *string `json:"explain"`

	QuestionID int `json:"question_id" gorm:"type:int;not null"`

	UpdatedAt *time.Time `json:"updated_at" gorm:"column:updated_at"`
	CreatedAt *time.Time `json:"created_at" gorm:"column:created_at"`
}

func (Answer) TableName() string { return "answers" }
