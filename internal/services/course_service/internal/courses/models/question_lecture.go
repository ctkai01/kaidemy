package models

import (
	"time"
)

type QuestionLecture struct {
	ID        int `json:"id" gorm:"primary_key"`
	UserID    int `json:"user_id" gorm:"type:int;not null"`
	CourseID  int `json:"course_id" gorm:"type:int;not null"`
	LectureID int `json:"lecture_id" gorm:"type:int;not null"`
	// Type      int `json:"type" gorm:"type:int;not null"`
	Course         *Course          `json:"course"`
	Title          string           `json:"title" gorm:"type:varchar(150);not null"`
	Description    *string          `json:"description" gorm:"type:text;"`
	AnswerLectures []*AnswerLecture `json:"answerLectures" gorm:"constraint:OnDelete:CASCADE"`

	UpdatedAt *time.Time `json:"updated_at" gorm:"column:updated_at"`
	CreatedAt *time.Time `json:"created_at" gorm:"column:created_at"`
}

func (QuestionLecture) TableName() string { return "question_lectures" }

type QuestionLectureShow struct {
	ID        int                  `json:"id"`
	User      *UserQuestionLecture `json:"user"`
	CourseID  int                  `json:"course_id"`
	LectureID int                  `json:"lecture_id"`

	Title       string     `json:"title"`
	Description *string    `json:"description"`
	TotalAnswer int        `json:"total_answer"`
	UpdatedAt   *time.Time `json:"updated_at"`
	CreatedAt   *time.Time `json:"created_at"`
}

type UserQuestionLecture struct {
	ID     int     `json:"id"`
	Avatar *string `json:"avatar"`
	Name   string  `json:"name"`
}

type QuestionLectureAuthorShow struct {
	ID          int                    `json:"id"`
	User        *UserQuestionLecture   `json:"user"`
	Course      *CourseQuestionLecture `json:"course"`
	LectureID   int                    `json:"lecture_id"`
	Title       string                 `json:"title"`
	Description *string                `json:"description"`
	TotalAnswer int                    `json:"total_answer"`
	UpdatedAt   *time.Time             `json:"updated_at"`
	CreatedAt   *time.Time             `json:"created_at"`
}

type CourseQuestionLecture struct {
	ID    int     `json:"id"`
	Image *string `json:"image"`
	Title string  `json:"title"`
}
