package models

import (
	"time"
)

type AnswerLecture struct {
	ID                int `json:"id" gorm:"primary_key"`
	UserID            int `json:"user_id" gorm:"type:int;not null"`
	QuestionLectureID int `json:"question_lecture_id" gorm:"type:int;not null"`

	Answer string `json:"answer" gorm:"type:text;not null"`

	UpdatedAt *time.Time `json:"updated_at" gorm:"column:updated_at"`
	CreatedAt *time.Time `json:"created_at" gorm:"column:created_at"`
}

func (AnswerLecture) TableName() string { return "answer_lectures" }

type AnswerLectureShow struct {
	ID                int                `json:"id"`
	User              *UserAnswerLecture `json:"user"`
	QuestionLectureID int                `json:"question_lecture_id"`

	Answer string `json:"answer"`

	UpdatedAt *time.Time `json:"updated_at"`
	CreatedAt *time.Time `json:"created_at"`
}

type UserAnswerLecture struct {
	ID     int     `json:"id"`
	Avatar *string `json:"avatar"`
	Name   string  `json:"name"`
}
