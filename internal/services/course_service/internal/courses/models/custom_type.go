package models

import (
	"database/sql/driver"
	"errors"
	"strings"
)


type StringArray []string


func (sa *StringArray) Scan(value interface{}) error {
    if value == nil {
        *sa = nil
        return nil
    }
    if stringValue, ok := value.([]byte); ok {
        if (len(stringValue) > 0) {
            *sa = strings.Split(string(stringValue), ",")
        } else {
            *sa = []string{}
        }
        
        return nil
    }
    return errors.New("invalid string array data")
}

func (sa StringArray) Value() (driver.Value, error) {
    return strings.Join(sa, ","), nil
}