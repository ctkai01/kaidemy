package models

import (
	"time"
)

type TopicLearning struct {
	ID          int     `json:"id" gorm:"primary_key"`
	Title       string  `json:"title" gorm:"type:varchar(100);not null"`
	Description *string `json:"description" gorm:"type:varchar(200)"`

	UserID    int         `json:"user_id" gorm:"type:int;not null"`
	Learnings []*Learning `json:"learnings" gorm:"many2many:learning_topic_learnings;"`

	UpdatedAt *time.Time `json:"updated_at" gorm:"column:updated_at"`
	CreatedAt *time.Time `json:"created_at" gorm:"column:created_at"`
}

func (TopicLearning) TableName() string { return "topic_learnings" }

type TopicLearningShow struct {
	ID          int     `json:"id"`
	Title       string  `json:"title"`
	Description *string `json:"description"`

	UserID    int             `json:"user_id"`
	Learnings []*LearningShow `json:"learnings"`

	UpdatedAt *time.Time `json:"updated_at" gorm:"column:updated_at"`
	CreatedAt *time.Time `json:"created_at" gorm:"column:created_at"`
}
