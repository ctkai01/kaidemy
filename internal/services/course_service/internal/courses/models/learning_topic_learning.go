package models

import (
	"time"
)

type LearningTopicLearning struct {
    LearningID      int        `json:"learning_id" gorm:"primary_key"`
    TopicLearningID int        `json:"topic_learning_id" gorm:"primary_key"`
    CreatedAt       time.Time  `json:"created_at"`
    UpdatedAt       time.Time  `json:"updated_at"`
}


func (LearningTopicLearning) TableName() string { return "learning_topic_learnings" }
