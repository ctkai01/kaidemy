package models

import (
)

type LearningLecture struct {
    LearningID int `gorm:"primaryKey"`
    LectureID  int `gorm:"primaryKey"`
    IsDone     bool
}


func (LearningLecture) TableName() string { return "learning_lectures" }
