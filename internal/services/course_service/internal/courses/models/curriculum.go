package models

import (
	"time"
)

type Curriculum struct {
	ID          int     `json:"id" gorm:"primary_key"`
	Title       string  `json:"title" gorm:"type:varchar(150);not null"`
	Description *string `json:"description" gorm:"type:text"`
	CourseID    int     `json:"course_id" gorm:"not null"`

	Lectures []Lecture `json:"lectures" gorm:"constraint:OnDelete:CASCADE"`
	Course   Course    `json:"course"`

	UpdatedAt *time.Time `json:"updated_at" gorm:"column:updated_at"`
	CreatedAt *time.Time `json:"created_at" gorm:"column:created_at"`
}

func (Curriculum) TableName() string { return "curriculums" }
