package models

import (
	"time"
)

type Report struct {
	ID int `json:"id" gorm:"primary_key"`
	// UserID        string  `json:"title" gorm:"type:varchar(150);not null"`
	// Description  *string `json:"description" gorm:"type:text;"`
	UserID      int        `json:"user_id" gorm:"type:int;not null"`
	CourseID    int        `json:"course_id" gorm:"type:int;not null"`
	IssueTypeID int        `json:"issue_type_id" gorm:"type:int;not null"`
	IssueDetail string     `json:"issue_detail" gorm:"type:varchar(150);not null"`
	Course      *Course    `json:"course"`
	UpdatedAt   *time.Time `json:"updated_at" gorm:"column:updated_at"`
	CreatedAt   *time.Time `json:"created_at" gorm:"column:created_at"`
}

func (Report) TableName() string { return "reports" }
