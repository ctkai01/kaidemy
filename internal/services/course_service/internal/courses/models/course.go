package models

import (
	"time"
)

type Course struct {
	ID              int          `json:"id" gorm:"primary_key"`
	OutComes        *StringArray `json:"out_comes" gorm:"type:text"`
	IntendedFor     *StringArray `json:"intended_for" gorm:"type:text"`
	Requirement     *StringArray `json:"requirements" gorm:"type:text"`
	ProductIdStripe string       `json:"product_id_stripe" gorm:"type:varchar(60);not null"`

	LevelID *int `json:"level_id" gorm:"type:int"`

	CategoryID    int    `json:"category_id" gorm:"type:int;not null"`
	SubCategoryID int    `json:"sub_category_id" gorm:"type:int;not null"`
	Title         string `json:"title" gorm:"type:varchar(60);not null"`

	WelcomeMessage         *string `json:"welcome_message" gorm:"type:text"`
	CongratulationsMessage *string `json:"congratulations_message" gorm:"type:text"`
	Subtitle               *string `json:"subtitle" gorm:"type:varchar(120)"`
	PrimarilyTeach         *string `json:"primarily_teach" gorm:"type:varchar(120)"`

	Description  *string `json:"description" gorm:"type:text"`
	Status       int     `json:"status" gorm:"type:int;default:0"`
	LanguageID   *int    `json:"language_id" gorm:"type:int"`
	PriceID      *int    `json:"price_id" gorm:"type:int"`
	ReviewStatus int     `json:"review_status" gorm:"type:int;default:0"`

	UserID int `json:"user_id" gorm:"type:int;not null"`

	PromotionalVideo *string `json:"promotional_video" gorm:"type:varchar(60)"`
	Image            *string `json:"image" gorm:"type:varchar(120)"`

	Curriculums []Curriculum `json:"curriculums"  gorm:"constraint:OnDelete:CASCADE"`
	Learnings   []Learning   `json:"learnings" gorm:"constraint:OnDelete:CASCADE"`

	UpdatedAt *time.Time `json:"updated_at" gorm:"column:updated_at"`
	CreatedAt *time.Time `json:"created_at" gorm:"column:created_at"`
}

func (Course) TableName() string { return "courses" }

//	type ProductsList struct {
//		TotalCount int64      `json:"totalCount" bson:"totalCount"`
//		TotalPages int64      `json:"totalPages" bson:"totalPages"`
//		Page       int64      `json:"page" bson:"page"`
//		Size       int64      `json:"size" bson:"size"`
//		Products   []*User `json:"products" bson:"products"`
//	}
type CourseResponse struct {
	ID              int          `json:"id"`
	OutComes        *StringArray `json:"out_comes"`
	IntendedFor     *StringArray `json:"intended_for"`
	Requirement     *StringArray `json:"requirements"`
	ProductIdStripe string       `json:"product_id_stripe"`

	Level *LevelResponse `json:"level"`

	Category    *CategoryResponse `json:"category"`
	SubCategory *CategoryResponse `json:"sub_category"`
	Title       string            `json:"title"`

	WelcomeMessage         *string `json:"welcome_message"`
	CongratulationsMessage *string `json:"congratulations_message"`
	Subtitle               *string `json:"subtitle"`
	PrimarilyTeach         *string `json:"primarily_teach"`

	Description  *string           `json:"description"`
	Status       int               `json:"status"`
	Language     *LanguageResponse `json:"language"`
	Price        *PriceResponse    `json:"price"`
	ReviewStatus int               `json:"review_status"`

	User UserResponse `json:"user"`

	PromotionalVideo *string `json:"promotional_video"`
	Image            *string `json:"image"`

	Curriculums   []Curriculum`json:"curriculums"`
	AverageReview float32                `json:"average_review"`
	CountStudent  int                    `json:"count_student"`
	CountReview   int                    `json:"count_review"`
	UpdatedAt     *time.Time             `json:"updated_at"`
	CreatedAt     *time.Time             `json:"created_at"`
}

type CourseResponseDetail struct {
	ID              int          `json:"id"`
	OutComes        *StringArray `json:"out_comes"`
	IntendedFor     *StringArray `json:"intended_for"`
	Requirement     *StringArray `json:"requirements"`
	ProductIdStripe string       `json:"product_id_stripe"`

	Level *LevelResponse `json:"level"`

	Category    *CategoryResponse `json:"category"`
	SubCategory *CategoryResponse `json:"sub_category"`
	Title       string            `json:"title"`

	WelcomeMessage         *string `json:"welcome_message"`
	CongratulationsMessage *string `json:"congratulations_message"`
	Subtitle               *string `json:"subtitle"`
	PrimarilyTeach         *string `json:"primarily_teach"`

	Description  *string           `json:"description"`
	Status       int               `json:"status"`
	Language     *LanguageResponse `json:"language"`
	Price        *PriceResponse    `json:"price"`
	ReviewStatus int               `json:"review_status"`

	User UserResponse `json:"user"`

	PromotionalVideo *string `json:"promotional_video"`
	Image            *string `json:"image"`

	Curriculums   []*CurriculumShowDetail`json:"curriculums"`
	AverageReview float32                `json:"average_review"`
	CountStudent  int                    `json:"count_student"`
	CountReview   int                    `json:"count_review"`
	UpdatedAt     *time.Time             `json:"updated_at"`
	CreatedAt     *time.Time             `json:"created_at"`
}

type CurriculumShowDetail struct {
	ID          int     `json:"id" `
	Title       string  `json:"title"`
	Description *string `json:"description"`
	CourseID    int     `json:"course_id"`

	Lectures []*LectureShowDetail `json:"lectures"`
	Course   Course              `json:"course"`

	UpdatedAt *time.Time `json:"updated_at"`
	CreatedAt *time.Time `json:"created_at"`
}

type LectureShowDetail struct {
	ID            int         `json:"id" `
	Title         string      `json:"title"`
	Article       *string     `json:"article"`
	Description   *string     `json:"description"`
	Assets        []*Asset    `json:"assets"`
	Questions     []*Question `json:"questions"`
	IsPromotional bool        `json:"is_promotional"`
	CurriculumID  int         `json:"curriculum_id"`
	Type          int         `json:"type"`
	Order         int         `json:"order"`
	IsDone        bool     `json:"is_done"`
	Learnings     []*Learning `json:"learnings"`
	UpdatedAt     *time.Time  `json:"updated_at"`
	CreatedAt     *time.Time  `json:"created_at"`
}

type CourseCategory struct {
	ID int `json:"id"`

	Level         *LevelResponse `json:"level"`
	IsPurchased   bool           `json:"is_purchased"`
	CategoryID    int            `json:"category_id"`
	SubCategoryID int            `json:"sub_category_id"`
	Title         string         `json:"title"`
	OutComes      *StringArray   `json:"out_comes"`
	Subtitle      *string        `json:"subtitle"`
	// Curriculums []Curriculum `json:"curriculums"`

	// Language     *LanguageResponse `json:"language"`
	Price        *PriceResponse `json:"price"`
	ReviewStatus int            `json:"review_status"`
	User         UserResponse   `json:"user"`
	Image        *string        `json:"image"`

	AverageRating float32    `json:"average_rating"`
	TotalRating   int        `json:"total_review"`
	Duration      int64      `json:"duration"`
	TotalLecture  int        `json:"total_lecture"`
	UpdatedAt     *time.Time `json:"updated_at"`
	CreatedAt     *time.Time `json:"created_at"`
}

type TopCategory struct {
	CategoryID int       `json:"id"`
	Courses    []*Course `json:"courses"`
}

type TopCategoryShow struct {
	CategoryID int               `json:"id"`
	Courses    []*CourseResponse `json:"courses"`
}

type CategoryResponse struct {
	ID       int    `json:"id"`
	Name     string `json:"name"`
	ParentID *int   `json:"parent_id"`
}

type CategoryShow struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type LevelResponse struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type LanguageResponse struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type PriceResponse struct {
	ID    int    `json:"id"`
	Tier  string `json:"tier"`
	Value int    `json:"value"`
}

type UserResponse struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type CourseLearning struct {
	ID int `json:"id"`

	Level    *LevelResponse `json:"level"`
	OutComes *StringArray   `json:"out_comes"`

	Title        string         `json:"title"`
	Status       int            `json:"status"`
	Price        *PriceResponse `json:"price"`
	ReviewStatus int            `json:"review_status"`
	Subtitle     *string        `json:"subtitle"`
	User         UserResponse   `json:"user"`
	Image        *string        `json:"image"`

	Curriculums []Curriculum `json:"curriculums" gorm:"constraint:OnDelete:CASCADE"`

	UpdatedAt *time.Time `json:"updated_at"`
	CreatedAt *time.Time `json:"created_at"`
}

type OverallReviewsByCourseID struct {
	AverageReview float32      `json:"average_review"`
	TotalReview   int          `json:"total_review"`
	Overall       *OverallStar `json:"overall"`
}

type OverallStar struct {
	FiveStar  int `json:"five_star"`
	FourStar  int `json:"four_star"`
	ThreeStar int `json:"three_star"`
	TwoStar   int `json:"two_star"`
	OneStar   int `json:"one_star"`
}
