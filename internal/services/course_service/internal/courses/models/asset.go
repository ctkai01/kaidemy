package models

import (
	"time"
)

const (
	ResourceType = 1
	WatchType    = 2
)

type Asset struct {
	ID  int    `json:"id" gorm:"primary_key"`
	URL string `json:"url" gorm:"type:varchar(240);not null"`

	LectureID int        `json:"lecture_id" gorm:"type:int;not null"`
	BunnyID   string     `json:"bunnyID"`
	Type      int        `json:"type" gorm:"type:int;not null"`
	Duration  int32      `json:"duration" gorm:"type:int"`
	Size      int64      `json:"size" gorm:"type:int"`
	Name      string     `json:"name"`
	UpdatedAt *time.Time `json:"updated_at" gorm:"column:updated_at"`
	CreatedAt *time.Time `json:"created_at" gorm:"column:created_at"`
}

func (Asset) TableName() string { return "assets" }
