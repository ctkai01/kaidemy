package models

import (
	"time"
)

type Learning struct {
	ID             int              `json:"id" gorm:"primary_key"`
	UserID         int              `json:"user_id" gorm:"type:int;not null"`
	CourseID       int              `json:"course_id" gorm:"type:int;not null"`
	Course         Course           `json:"course"`
	Process        *int             `json:"process" gorm:"type:int"`
	Type           int              `json:"type" gorm:"type:int;not null"`
	StarCount      *int             `json:"star_count" gorm:"type:int"`
	Comment        *string          `json:"comment" gorm:"type:varchar(200)"`
	TopicLearnings []*TopicLearning `json:"learnings" gorm:"many2many:learning_topic_learnings;"`
	Lectures       []*Lecture       `json:"lectures" gorm:"many2many:learning_lectures"`
	UpdatedAt      *time.Time       `json:"updated_at" gorm:"column:updated_at"`
	CreatedAt      *time.Time       `json:"created_at" gorm:"column:created_at"`
}

func (Learning) TableName() string { return "learnings" }

type LearningShow struct {
	ID            int            `json:"id"`
	UserID        int            `json:"user_id"`
	CourseID      int            `json:"course_id"`
	Course        CourseLearning `json:"course"`
	Process       *int           `json:"process"`
	Type          int            `json:"type"`
	StarCount     *int           `json:"star_count"`
	Comment       *string        `json:"comment"`
	CountReview   *int           `json:"count_review"`
	AverageReview *float32           `json:"average_review"`
	// TopicLearnings []*TopicLearning `json:"learnings"`
	// Lectures       []*Lecture       `json:"lectures"`
	UpdatedAt *time.Time `json:"updated_at" gorm:"column:updated_at"`
	CreatedAt *time.Time `json:"created_at" gorm:"column:created_at"`
}

type LearningReview struct {
	ID        int         `json:"id"`
	User      *UserReview `json:"user"`
	Type      int         `json:"type"`
	StarCount *int        `json:"star_count"`
	Comment   *string     `json:"comment"`
	UpdatedAt *time.Time  `json:"updated_at" gorm:"column:updated_at"`
	CreatedAt *time.Time  `json:"created_at" gorm:"column:created_at"`
}

type UserReview struct {
	ID     int    `json:"id"`
	Name   string `json:"name"`
	Avatar string `json:"avatar"`
}

type AuthorStatisticInfo struct {
	User          *AuthorInfo `json:"user"`
	AverageReview float32     `json:"average_review"`
	CountReview   int         `json:"count_review"`
	CountStudent  int         `json:"count_student"`
	TotalCourse   int         `json:"total_course"`
}

type AuthorInfo struct {
	ID        int     `json:"id"`
	Name      string  `json:"name"`
	Avatar    *string `json:"avatar"`
	Biography *string `json:"biography"`
}
