package mediator

import (
	// "emperror.dev/errors"
	// "github.com/mehdihadeli/go-mediatr"

	"github.com/mehdihadeli/go-mediatr"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"

	// forgotPasswordCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/forgot_password/commands"
	// forgotPasswordDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/forgot_password/dtos"
	// loginCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login/commands"
	// loginDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login/dtos"
	// loginGoogleCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login_by_google/commands"
	// loginGoogleDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login_by_google/dtos"
	// loginGoogleQuery "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login_by_google/queries"
	// registerCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/register/commands"
	// registerDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/register/dtos"
	// requestLoginGoogleCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/commands"
	// requestLoginGoogleDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	// resetPasswordCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/reset_password/commands"
	// resetPasswordDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/reset_password/dtos"

	// changePasswordCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/change_password/commands"
	// changePasswordDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/change_password/dtos"

	createCourseCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	createCourseDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/dtos"

	updateCourseCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_course/commands"
	updateCourseDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_course/dtos"

	createCurriculumCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_curriculum/commands"
	createCurriculumDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_curriculum/dtos"

	updateCurriculumCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_curriculum/commands"
	updateCurriculumDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_curriculum/dtos"

	deleteCurriculumCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_curriculum/commands"
	deleteCurriculumDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_curriculum/dtos"

	createLectureCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_lecture/commands"
	createLectureDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_lecture/dtos"

	deleteLectureCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_lecture/commands"
	deleteLectureDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_lecture/dtos"

	updateLectureCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_lecture/commands"
	updateLectureDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_lecture/dtos"

	createQuizCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_quiz/commands"
	createQuizDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_quiz/dtos"

	updateQuizCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_quiz/commands"
	updateQuizDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_quiz/dtos"

	deleteQuizCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_quiz/commands"
	deleteQuizDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_quiz/dtos"

	createQuestionCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_question/commands"
	createQuestionDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_question/dtos"

	updateQuestionCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_question/commands"
	updateQuestionDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_question/dtos"

	deleteQuestionCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_question/commands"
	deleteQuestionDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_question/dtos"

	createAnswerCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_answer/commands"
	createAnswerDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_answer/dtos"

	updateAnswerCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_answer/commands"
	updateAnswerDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_answer/dtos"

	deleteAnswerCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_answer/commands"
	deleteAnswerDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_answer/dtos"

	getCourseDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_course_by_id/dtos"
	getCourseQuery "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_course_by_id/queries"

	registerCourseCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/register_course/commands"
	registerCourseDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/register_course/dtos"

	updateLearningCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_learning/commands"
	updateLearningDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_learning/dtos"

	createWishCourseCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_wish_course/commands"
	createWishCourseDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_wish_course/dtos"

	removeWishCourseCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/remove_wish_course/commands"
	removeWishCourseDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/remove_wish_course/dtos"

	getLearningsDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_learnings/dtos"
	getLearningsQueries "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_learnings/queries"

	createTopicLearningCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_topic_learning/commands"
	createTopicLearningDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_topic_learning/dtos"

	addLearningTopicLearningCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/add_learning_topic_learning/commands"
	addLearningTopicLearningDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/add_learning_topic_learning/dtos"

	updateTopicLearningCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_topic_learning/commands"
	updateTopicLearningDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_topic_learning/dtos"

	deleteTopicLearningCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_topic_learning/commands"
	deleteTopicLearningDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_topic_learning/dtos"

	getTopicLearningDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_topic_learning/dtos"
	getTopicLearningQueries "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_topic_learning/queries"

	removeLearningTopicLearningCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/remove_learning_topic_learning/commands"
	removeLearningTopicLearningDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/remove_learning_topic_learning/dtos"

	createReportCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_report/commands"
	createReportDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_report/dtos"

	getReportsDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_reports/dtos"
	getReportsQuery "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_reports/queries"

	createQuestionLectureCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_question_lecture/commands"
	createQuestionLectureDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_question_lecture/dtos"

	updateQuestionLectureCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_question_lecture/commands"
	updateQuestionLectureDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_question_lecture/dtos"

	deleteQuestionLectureCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_question_lecture/commands"
	deleteQuestionLectureDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_question_lecture/dtos"

	createAnswerLectureCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_answer_lecture/commands"
	createAnswerLectureDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_answer_lecture/dtos"

	updateAnswerLectureCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_answer_lecture/commands"
	updateAnswerLectureDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_answer_lecture/dtos"

	deleteAnswerLectureCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_answer_lecture/commands"
	deleteAnswerLectureDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_answer_lecture/dtos"

	getCurriculumByCourseIDDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_curriculums_by_course_id/dtos"
	getCurriculumByCourseIDQueries "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_curriculums_by_course_id/queries"

	getCoursesByUserIDDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_courses_by_user_id/dtos"
	getCoursesByUserIDQueries "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_courses_by_user_id/queries"

	submitReviewCourseCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/submit_review_course/commands"
	submitReviewCourseDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/submit_review_course/dtos"

	getCoursesReviewsDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_courses_reviews/dtos"
	getCoursesReviewsQueries "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_courses_reviews/queries"

	approvalReviewCourseCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/approval_review_course/commands"
	approvalReviewCourseDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/approval_review_course/dtos"

	getCoursesDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_courses/dtos"
	getCoursesQueries "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_courses/queries"

	getTopCoursesDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_top_courses/dtos"
	getTopCoursesQueries "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_top_courses/queries"

	markLectureCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/mark_lecture/commands"
	markLectureDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/mark_lecture/dtos"

	getCoursePublicByUserIDDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_courses_public_by_user_id/dtos"
	getCoursePublicByUserIDQueries "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_courses_public_by_user_id/queries"

	getInfoAuthorDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_info_author/dtos"
	getInfoAuthorQueries "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_info_author/queries"

	getReviewsByCourseIDDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_reviews_by_course_id/dtos"
	getReviewsByCourseIDQueries "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_reviews_by_course_id/queries"

	getOverallReviewByCourseIDDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_overall_review_by_course_id/dtos"
	getOverallReviewByCourseIDQueries "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_overall_review_by_course_id/queries"

	getLectureQuestionsIDDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_lecture_questions/dtos"
	getLectureQuestionsIDQueries "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_lecture_questions/queries"

	getLectureAnswersIDDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_lecture_answers/dtos"
	getLectureAnswersIDQueries "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_lecture_answers/queries"

	getCourseByCategoryIDDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_courses_by_category_id/dtos"
	getCourseByCategoryIDQueries "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_courses_by_category_id/queries"

	getAuthCourseByCategoryIDDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_auth_courses_by_category_id/dtos"
	getAuthCourseByCategoryIDQueries "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_auth_courses_by_category_id/queries"

	getAuthCourseBySearchDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_auth_courses_by_search/dtos"
	getAuthCourseBySearchQueries "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_auth_courses_by_search/queries"

	getCourseBySearchDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_courses_by_search/dtos"
	getCourseBySearchQueries "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_courses_by_search/queries"

	getSearchDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_search/dtos"
	getSearchQueries "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_search/queries"

	getReviewsByAuthorDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_reviews_by_author/dtos"
	getReviewsByAuthorQueries "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_reviews_by_author/queries"

	getCoursesByAuthorDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_courses_by_author/dtos"
	getCoursesByAuthorQueries "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_courses_by_author/queries"

	getUsersByAuthorDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_users_by_author/dtos"
	getUsersByAuthorQueries "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_users_by_author/queries"

	getLectureQuestionsAuthorDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_lecture_questions_author/dtos"
	getLectureQuestionsAuthorQueries "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_lecture_questions_author/queries"

	getOverviewCourseAuthorDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_overview_course_by_author/dtos"
	getOverviewCourseAuthorQueries "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_overview_course_by_author/queries"

	getOverviewAdminDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_overview_by_admin/dtos"
	getOverviewAdminQueries "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_overview_by_admin/queries"

	getStudentCurriculumByCourseIDDtos "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_student_curriculums_by_course_id/dtos"
	getStudentCurriculumByCourseIDQueries "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_student_curriculums_by_course_id/queries"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
)

// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"

func ConfigCourseMediator(
	logger logger.Logger,
	courseRepository data.CourseRepository,
	answerRepository data.AnswerRepository,
	curriculumRepository data.CurriculumRepository,
	lectureRepository data.LectureRepository,
	questionRepository data.QuestionRepository,
	// quizRepository data.QuizRepository,
	assetRepository data.AssetRepository,
	learningRepository data.LearningRepository,
	topicLearningRepository data.TopicLearningRepository,
	reportRepository data.ReportRepository,
	questionLectureRepository data.QuestionLectureRepository,
	answerLectureRepository data.AnswerLectureRepository,
	// topicLearningCourseRepository data.TopicLearningCourseRepository,
	grpcClient grpc.GrpcClient,
) error {
	err := mediatr.RegisterRequestHandler[*createCourseCommand.CreateCourse, *createCourseDtos.CreateCourseResponseDto](
		createCourseCommand.NewCreateCourseHandler(logger, courseRepository, learningRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*updateCourseCommand.UpdateCourse, *updateCourseDtos.UpdateCourseResponseDto](
		updateCourseCommand.NewUpdateCourseHandler(logger, curriculumRepository, courseRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*createCurriculumCommand.CreateCurriculum, *createCurriculumDtos.CreateCurriculumResponseDto](
		createCurriculumCommand.NewCreateCurriculumHandler(logger, curriculumRepository, courseRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*updateCurriculumCommand.UpdateCurriculum, *updateCurriculumDtos.UpdateCurriculumResponseDto](
		updateCurriculumCommand.NewUpdateCurriculumHandler(logger, curriculumRepository, courseRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*deleteCurriculumCommand.DeleteCurriculum, *deleteCurriculumDtos.DeleteCurriculumResponseDto](
		deleteCurriculumCommand.NewDeleteCurriculumHandler(logger, curriculumRepository, courseRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*createLectureCommand.CreateLecture, *createLectureDtos.CreateLectureResponseDto](
		createLectureCommand.NewCreateLectureHandler(logger, curriculumRepository, lectureRepository, courseRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*deleteLectureCommand.DeleteLecture, *deleteLectureDtos.DeleteLectureResponseDto](
		deleteLectureCommand.NewDeleteLectureHandler(logger, curriculumRepository, lectureRepository, courseRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*updateLectureCommand.UpdateLecture, *updateLectureDtos.UpdateLectureResponseDto](
		updateLectureCommand.NewUpdateLectureHandler(logger, curriculumRepository, lectureRepository, assetRepository, courseRepository, grpcClient),
	)

	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*createQuizCommand.CreateQuiz, *createQuizDtos.CreateQuizResponseDto](
		createQuizCommand.NewCreateQuizHandler(logger, curriculumRepository, lectureRepository, courseRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*updateQuizCommand.UpdateQuiz, *updateQuizDtos.UpdateQuizResponseDto](
		updateQuizCommand.NewUpdateQuizHandler(logger, curriculumRepository, lectureRepository, courseRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*deleteQuizCommand.DeleteQuiz, *deleteQuizDtos.DeleteQuizResponseDto](
		deleteQuizCommand.NewDeleteQuizHandler(logger, curriculumRepository, lectureRepository, courseRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*createQuestionCommand.CreateQuestion, *createQuestionDtos.CreateQuestionResponseDto](
		createQuestionCommand.NewCreateQuestionHandler(logger, curriculumRepository, lectureRepository, questionRepository, courseRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*updateQuestionCommand.UpdateQuestion, *updateQuestionDtos.UpdateQuestionResponseDto](
		updateQuestionCommand.NewUpdateQuestionHandler(logger, curriculumRepository, lectureRepository, questionRepository, courseRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*deleteQuestionCommand.DeleteQuestion, *deleteQuestionDtos.DeleteQuestionResponseDto](
		deleteQuestionCommand.NewDeleteQuestionHandler(logger, curriculumRepository, lectureRepository, questionRepository, courseRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*createAnswerCommand.CreateAnswer, *createAnswerDtos.CreateAnswerResponseDto](
		createAnswerCommand.NewCreateAnswerHandler(logger, curriculumRepository, lectureRepository, questionRepository, answerRepository, courseRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*updateAnswerCommand.UpdateAnswer, *updateAnswerDtos.UpdateAnswerResponseDto](
		updateAnswerCommand.NewUpdateAnswerHandler(logger, curriculumRepository, lectureRepository, questionRepository, answerRepository, courseRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*deleteAnswerCommand.DeleteAnswer, *deleteAnswerDtos.DeleteAnswerResponseDto](
		deleteAnswerCommand.NewDeleteAnswerHandler(logger, curriculumRepository, lectureRepository, questionRepository, answerRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getCourseQuery.GetCourseById, *getCourseDtos.GetCourseByIdResponseDto](
		getCourseQuery.NewGetCourseByIdHandler(logger, courseRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*registerCourseCommand.RegisterCourse, *registerCourseDtos.RegisterCourseResponseDto](
		registerCourseCommand.NewRegisterCourseHandler(logger, learningRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*updateLearningCommand.UpdateLearning, *updateLearningDtos.UpdateLearningResponseDto](
		updateLearningCommand.NewUpdateLearningHandler(logger, learningRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*createWishCourseCommand.CreateWishCourse, *createWishCourseDtos.CreateWishCourseResponseDto](
		createWishCourseCommand.NewCreateWishCourseHandler(logger, learningRepository, courseRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*removeWishCourseCommand.DeleteWishCourse, *removeWishCourseDtos.RemoveWishCourseResponseDto](
		removeWishCourseCommand.NewRemoveWishCourseHandler(logger, learningRepository, courseRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getLearningsQueries.GetLearnings, *getLearningsDtos.GetLearningsResponseDto](
		getLearningsQueries.NewGetLearningsHandler(logger, learningRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*createTopicLearningCommand.CreateTopicLeaning, *createTopicLearningDtos.CreateTopicLearningResponseDto](
		createTopicLearningCommand.NewCreateTopicLearningHandler(logger, topicLearningRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*addLearningTopicLearningCommand.AddLearningTopicLeaning, *addLearningTopicLearningDtos.AddLearningTopicLeaningResponseDto](
		addLearningTopicLearningCommand.NewAddLearningTopicLearningHandler(logger, topicLearningRepository, learningRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*updateTopicLearningCommand.UpdateTopicLeaning, *updateTopicLearningDtos.UpdateTopicLearningResponseDto](
		updateTopicLearningCommand.NewUpdateTopicLearningHandler(logger, topicLearningRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*deleteTopicLearningCommand.DeleteTopicLearning, *deleteTopicLearningDtos.DeleteTopicLearningResponseDto](
		deleteTopicLearningCommand.NewDeleteTopicLearningHandler(logger, topicLearningRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getTopicLearningQueries.GetTopicLearning, *getTopicLearningDtos.GetTopicLearningResponseDto](
		getTopicLearningQueries.NewGetTopicLearningHandler(logger, topicLearningRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*removeLearningTopicLearningCommand.RemoveLearningTopicLeaning, *removeLearningTopicLearningDtos.RemoveLearningTopicLeaningResponseDto](
		removeLearningTopicLearningCommand.NewRemoveLearningTopicLearningHandler(logger, topicLearningRepository, learningRepository, grpcClient),
	)

	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*createReportCommand.CreateReport, *createReportDtos.CreateReportResponseDto](
		createReportCommand.NewCreateReportHandler(logger, courseRepository, reportRepository, grpcClient),
	)

	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getReportsQuery.GetReports, *getReportsDtos.GetReportsResponseDto](
		getReportsQuery.NewGetReportsHandler(logger, reportRepository, courseRepository, grpcClient),
	)

	if err != nil {
		return err
	}

	// Question lecture route
	err = mediatr.RegisterRequestHandler[*createQuestionLectureCommand.CreateQuestionLecture, *createQuestionLectureDtos.CreateQuestionLectureResponseDto](
		createQuestionLectureCommand.NewCreateQuestionLectureHandler(logger, courseRepository, learningRepository, lectureRepository, curriculumRepository, questionLectureRepository, grpcClient),
	)

	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*updateQuestionLectureCommand.UpdateQuestionLecture, *updateQuestionLectureDtos.UpdateQuestionLectureResponseDto](
		updateQuestionLectureCommand.NewUpdateQuestionLectureHandler(logger, questionLectureRepository, grpcClient),
	)

	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*deleteQuestionLectureCommand.DeleteQuestionLecture, *deleteQuestionLectureDtos.DeleteQuestionLectureResponseDto](
		deleteQuestionLectureCommand.NewDeleteQuestionLectureHandler(logger, questionLectureRepository, grpcClient),
	)

	if err != nil {
		return err
	}

	// Answer lecture route
	err = mediatr.RegisterRequestHandler[*createAnswerLectureCommand.CreateAnswerLecture, *createAnswerLectureDtos.CreateAnswerLectureResponseDto](
		createAnswerLectureCommand.NewCreateAnswerLectureHandler(logger, questionLectureRepository, learningRepository, answerLectureRepository, grpcClient),
	)

	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*updateAnswerLectureCommand.UpdateAnswerLecture, *updateAnswerLectureDtos.UpdateAnswerLectureResponseDto](
		updateAnswerLectureCommand.NewUpdateAnswerLectureHandler(logger, answerLectureRepository, grpcClient),
	)

	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*deleteAnswerLectureCommand.DeleteAnswerLecture, *deleteAnswerLectureDtos.DeleteAnswerLectureResponseDto](
		deleteAnswerLectureCommand.NewDeleteAnswerLectureHandler(logger, answerLectureRepository, grpcClient),
	)

	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getCurriculumByCourseIDQueries.GetCurriculumsByCourseID, *getCurriculumByCourseIDDtos.GetCurriculumsByCourseIDResponseDto](
		getCurriculumByCourseIDQueries.NewGetCurriculumsByCourseIDHandler(logger, courseRepository, curriculumRepository, grpcClient),
	)

	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getCoursesByUserIDQueries.GetCoursesByUserId, *getCoursesByUserIDDtos.GetCoursesByUserIDResponseDto](
		getCoursesByUserIDQueries.NewGetTopicLearningHandler(logger, courseRepository, grpcClient),
	)

	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*submitReviewCourseCommand.SubmitReviewCourse, *submitReviewCourseDtos.SubmitReviewCourseResponseDto](
		submitReviewCourseCommand.NewSubmitReviewCourseHandler(logger, courseRepository),
	)

	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getCoursesReviewsQueries.GetCoursesReviews, *getCoursesReviewsDtos.GetCoursesReviewsResponseDto](
		getCoursesReviewsQueries.NewGetCoursesReviewsHandler(logger, courseRepository, grpcClient),
	)

	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*approvalReviewCourseCommand.ApprovalReviewCourse, *approvalReviewCourseDtos.ApprovalReviewCourseResponseDto](
		approvalReviewCourseCommand.NewApprovalReviewCourseHandler(logger, courseRepository),
	)

	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getCoursesQueries.GetCourses, *getCoursesDtos.GetCoursesResponseDto](
		getCoursesQueries.NewGetCoursesHandler(logger, courseRepository, grpcClient),
	)

	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getTopCoursesQueries.GetTopCourses, *getTopCoursesDtos.GetTopCoursesResponseDto](
		getTopCoursesQueries.NewGetTopCoursesHandler(logger, courseRepository, grpcClient),
	)

	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*markLectureCommand.MarkLecture, *markLectureDtos.MarkLectureResponseDto](
		markLectureCommand.NewMarkLectureHandler(logger, curriculumRepository, lectureRepository, courseRepository, learningRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getCoursePublicByUserIDQueries.GetCoursesPublicByUserId, *getCoursePublicByUserIDDtos.GetCoursesPublicByUserIDResponseDto](
		getCoursePublicByUserIDQueries.NewGetCoursesPublicByUserIDHandler(logger, learningRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getInfoAuthorQueries.GetInfoAuthor, *getInfoAuthorDtos.GetInfoAuthorResponseDto](
		getInfoAuthorQueries.NewGetInfoAuthorHandler(logger, courseRepository, learningRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getReviewsByCourseIDQueries.GetReviewsByCourseID, *getReviewsByCourseIDDtos.GetReviewsByCourseIDResponseDto](
		getReviewsByCourseIDQueries.NewGetReviewsByCourseIDHandler(logger, learningRepository, courseRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getOverallReviewByCourseIDQueries.GetOverallReviewByCourseID, *getOverallReviewByCourseIDDtos.GetOverallReviewByCourseIDResponseDto](
		getOverallReviewByCourseIDQueries.NewGetOverallReviewByCourseIDHandler(logger, learningRepository, courseRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getLectureQuestionsIDQueries.GetLectureQuestions, *getLectureQuestionsIDDtos.GetLectureQuestionsResponseDto](
		getLectureQuestionsIDQueries.NewGetLectureQuestionsHandler(logger, questionLectureRepository, courseRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getLectureAnswersIDQueries.GetLectureAnswers, *getLectureAnswersIDDtos.GetLectureAnswersResponseDto](
		getLectureAnswersIDQueries.NewGetLectureAnswersHandler(logger, answerLectureRepository, courseRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getCourseByCategoryIDQueries.GetCoursesByCategoryId, *getCourseByCategoryIDDtos.GetCoursesByCategoryIDResponseDto](
		getCourseByCategoryIDQueries.NewGetCoursesByCategoryIDHandler(logger, courseRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getAuthCourseByCategoryIDQueries.GetAuthCoursesByCategoryId, *getAuthCourseByCategoryIDDtos.GetAuthCoursesByCategoryIDResponseDto](
		getAuthCourseByCategoryIDQueries.NewGetAuthCoursesByCategoryIDHandler(logger, courseRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getAuthCourseBySearchQueries.GetAuthCoursesBySearch, *getAuthCourseBySearchDtos.GetAuthCoursesBySearchResponseDto](
		getAuthCourseBySearchQueries.NewGetAuthCoursesBySearchHandler(logger, courseRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getCourseBySearchQueries.GetCoursesBySearch, *getCourseBySearchDtos.GetCoursesBySearchResponseDto](
		getCourseBySearchQueries.NewGetCoursesBySearchHandler(logger, courseRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getSearchQueries.GetSearch, *getSearchDtos.GetSearchResponseDto](
		getSearchQueries.NewGetSearchHandler(logger, courseRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getReviewsByAuthorQueries.GetReviewsByAuthor, *getReviewsByAuthorDtos.GetReviewByAuthorResponseDto](
		getReviewsByAuthorQueries.NewGetReviewsByAuthorHandler(logger, courseRepository, learningRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getCoursesByAuthorQueries.GetCoursesByAuthor, *getCoursesByAuthorDtos.GetCoursesByAuthorResponseDto](
		getCoursesByAuthorQueries.NewGetCoursesByUserIDHandler(logger, courseRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getUsersByAuthorQueries.GetUsersByAuthor, *getUsersByAuthorDtos.GetUsersByAuthorResponseDto](
		getUsersByAuthorQueries.NewGetUsersByAuthorHandler(logger, courseRepository, learningRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getLectureQuestionsAuthorQueries.GetLectureQuestionsAuthor, *getLectureQuestionsAuthorDtos.GetLectureQuestionsAuthorResponseDto](
		getLectureQuestionsAuthorQueries.NewGetLectureQuestionsAuthorHandler(logger, questionLectureRepository, courseRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getOverviewCourseAuthorQueries.GetOverviewCourseByAuthor, *getOverviewCourseAuthorDtos.GetOverviewCourseByAuthorResponseDto](
		getOverviewCourseAuthorQueries.NewGetOverviewCourseByAuthorHandler(logger, courseRepository, learningRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getOverviewAdminQueries.GetOverviewByAdmin, *getOverviewAdminDtos.GetOverviewByAdminResponseDto](
		getOverviewAdminQueries.NewGetOverviewByAdminHandler(logger, courseRepository, learningRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getStudentCurriculumByCourseIDQueries.GetStudentCurriculumsByCourseID, *getStudentCurriculumByCourseIDDtos.GetStudentCurriculumsByCourseIDResponseDto](
		getStudentCurriculumByCourseIDQueries.NewGetStudentCurriculumsByCourseIDHandler(logger, courseRepository, curriculumRepository, learningRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	


	return nil
}
