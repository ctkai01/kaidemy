package mappings

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/mapper"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/dto"
	coursesService "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func ConfigureCoursesMappings() error {
	err := mapper.CreateMap[*models.Learning, *models.Learning]()

	if err != nil {
		return err
	}

	err = mapper.CreateMap[*models.TopicLearning, *models.TopicLearning]()

	if err != nil {
		return err
	}

	err = mapper.CreateMap[*models.Report, *models.Report]()

	if err != nil {
		return err
	}

	err = mapper.CreateCustomMap[*models.Course, *coursesService.Course](
		func(course *models.Course) *coursesService.Course {
			if course == nil {
				return nil
			}
			var image string

			if course.Image != nil {
				image = *course.Image
			}
			var priceID int

			if course.PriceID != nil {
				priceID = *course.PriceID
			}

			return &coursesService.Course{
				ID:              int32(course.ID),
				Title:           course.Title,
				Image:           image,
				UserID:          int32(course.UserID),
				ProductStripeID: course.ProductIdStripe,
				PriceID:         int32(priceID),
				CreatedAt:       timestamppb.New(*course.CreatedAt),
				UpdatedAt:       timestamppb.New(*course.UpdatedAt),
			}
		},
	)

	if err != nil {
		return err
	}

	err = mapper.CreateCustomMap[*models.CourseResponse, *coursesService.CourseDetail](
		func(course *models.CourseResponse) *coursesService.CourseDetail {
			if course == nil {
				return nil
			}
			var image string

			if course.Image != nil {
				image = *course.Image
			}

			var curriculumCourse []*coursesService.CurriculumCourse

			for _, curriculum := range course.Curriculums {
				var lectureCourse []*coursesService.LectureCourse
				for _, lecture := range curriculum.Lectures {
					var assetCourse []*coursesService.AssetCourse

					for _, asset := range lecture.Assets {
						assetCourse = append(assetCourse, &coursesService.AssetCourse{
							ID:       int32(asset.ID),
							Type:     int32(asset.Type),
							Duration: asset.Duration,
						})
					}
					lectureCourse = append(lectureCourse, &coursesService.LectureCourse{
						ID:     int32(lecture.ID),
						Type:   int32(lecture.Type),
						Assets: assetCourse,
					})
				}

				curriculumCourse = append(curriculumCourse, &coursesService.CurriculumCourse{
					ID:       int32(curriculum.ID),
					Lectures: lectureCourse,
				})
			}

			return &coursesService.CourseDetail{
				ID:    int32(course.ID),
				Title: course.Title,
				Image: image,
				User: &coursesService.UserCourse{
					ID:   int32(course.User.ID),
					Name: course.User.Name,
				},
				ProductStripeID: course.ProductIdStripe,
				Level: &coursesService.LevelCourse{
					ID:   int32(course.Level.ID),
					Name: course.Level.Name,
				},
				Price: &coursesService.PriceCourse{
					ID:    int32(course.Price.ID),
					Tier:  course.Price.Tier,
					Value: int32(course.Price.Value),
				},
				Curriculums: curriculumCourse,
				CountReview: int32(course.CountReview),
				AverageReview: course.AverageReview,
				CreatedAt:   timestamppb.New(*course.CreatedAt),
				UpdatedAt:   timestamppb.New(*course.UpdatedAt),
				// Title:           course.Title,
				// Image:           image,
				// UserID:          int32(course.UserID),
				// ProductStripeID: course.ProductIdStripe,
			}
		},
	)

	if err != nil {
		return err
	}

	return nil
}
