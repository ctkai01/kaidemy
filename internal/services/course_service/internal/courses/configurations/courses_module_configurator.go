package configurations

import (
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/configurations/mappings"

	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/configurations/mediator"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/data"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp/contracts"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	logger2 "gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	courseservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	googleGrpc "google.golang.org/grpc"

	grpcServer "gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/otel/tracing"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/configurations/mediator"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc"
)

type CoursesModuleConfigurator struct {
	contracts.Application
}

func NewCoursesModuleConfigurator(
	app contracts.Application,
) *CoursesModuleConfigurator {
	return &CoursesModuleConfigurator{
		Application: app,
	}
}

func (c *CoursesModuleConfigurator) ConfigureCoursesModule() {
	c.ResolveFunc(
		func(logger logger2.Logger, courseRepository data.CourseRepository,
			answerRepository data.AnswerRepository,
			curriculumRepository data.CurriculumRepository,
			lectureRepository data.LectureRepository,
			questionRepository data.QuestionRepository,
			// quizRepository data.QuizRepository,
			assetRepository data.AssetRepository,
			learningRepository data.LearningRepository,
			topicLearningRepository data.TopicLearningRepository,
			reportRepository data.ReportRepository,
			questionLectureRepository data.QuestionLectureRepository,
			answerLectureRepository data.AnswerLectureRepository,
			// topicLearningCourseRepository data.TopicLearningCourseRepository,
			grpcClient grpcServer.GrpcClient,
		) error {
			// Config Products Mediators
			err := mediator.ConfigCourseMediator(logger, courseRepository,
				answerRepository,
				curriculumRepository,
				lectureRepository,
				questionRepository,
				// quizRepository,
				assetRepository,
				learningRepository,
				topicLearningRepository,
				reportRepository,
				questionLectureRepository,
				answerLectureRepository,
				// topicLearningCourseRepository,
				grpcClient,
			)
			if err != nil {
				return err
			}

			// Config Products Mappings
			err = mappings.ConfigureCoursesMappings()
			if err != nil {
				return err
			}
			return nil
		},
	)
}

func (c *CoursesModuleConfigurator) MapCoursesEndpoints() {
	// Config Course Http Endpoints
	c.ResolveFuncWithParamTag(func(endpoints []route.Endpoint) {
		for _, endpoint := range endpoints {
			endpoint.MapEndpoint()
		}
	}, `group:"courses-routes"`,
	)

	c.ResolveFunc(
		func(courseGrpcServer grpcServer.GrpcServer, grpcService *grpc.CourseGrpcServiceServer) error {
			courseGrpcServer.GrpcServiceBuilder().RegisterRoutes(func(server *googleGrpc.Server) {
				courseservice.RegisterCoursesServiceServer(server, grpcService)
			})

			return nil
		},
	)
}
