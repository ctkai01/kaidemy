package dtos

//https://echo.labstack.com/guide/response/

type CreateQuestionLectureRequestDto struct {
	CourseID    int     `form:"course_id"`
	LectureID   int     `form:"lecture_id"`
	Title       string  `form:"title"`
	Description *string `form:"description"`
}
