package dtos

import "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

//https://echo.labstack.com/guide/response/

type CreateQuestionLectureResponseDto struct {
	Message         string                 `json:"message"`
	QuestionLecture *models.QuestionLectureShow `json:"question_lecture"`
}
