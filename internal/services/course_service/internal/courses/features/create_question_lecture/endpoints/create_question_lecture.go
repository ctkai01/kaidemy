package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/params"
	// coursesError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_question_lecture/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_question_lecture/dtos"
)

type createQuestionLectureEndpoint struct {
	params.QuestionLectureRouteParams
}

func NewCreateQuestionLectureEndpoint(
	params params.QuestionLectureRouteParams,
) route.Endpoint {
	return &createQuestionLectureEndpoint{
		QuestionLectureRouteParams: params,
	}
}

func (ep *createQuestionLectureEndpoint) MapEndpoint() {
	ep.QuestionLecturesGroup.POST("", ep.handler())
}

func (ep *createQuestionLectureEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.CreateQuestionLectureRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[createQuestionLecture.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[createQuestionLectureEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}

		userId := utils.GetUserId(c)

		command, errValidate := commands.NewCreateQuestionLecture(
			userId,
			request.CourseID,
			request.LectureID,
			request.Title,
			request.Description,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[createQuestionLectureEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[createQuestionLectureEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.CreateQuestionLecture, *dtos.CreateQuestionLectureResponseDto](
			ctx,
			command,
		)

		if err != nil {
			// if err.Error() == coursesError.WrapError(customErrors.ErrorCannotPermission).Error() {
			// 	errCustom := errors.WithMessage(
			// 		err,
			// 		"[createQuestionEndpoint_handler.Send] error in sending create Question",
			// 	)
			// 	ep.Logger.Error(
			// 		fmt.Sprintf(
			// 			"[createQuestion.Send], err: %v",
			// 			errCustom,
			// 		),
			// 	)
			// 	return customErrors.NewForbiddenErrorHttp(c, err.Error())
			// }

			err = errors.WithMessage(
				err,
				"[createQuestionLectureEndpoint_handler.Send] error in sending create Question lecture",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[createQuestionLecture.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusCreated, result)
	}
}
