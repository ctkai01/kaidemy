package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type CreateQuestionLecture struct {
	IdUser      int     `validate:"required,numeric"`
	CourseID    int     `validate:"required,numeric"`
	LectureID   int     `validate:"required,numeric"`
	Title       string  `validate:"required,gte=1,lte=150"`
	Description *string `validate:"omitempty,gte=1,lte=500"`
}

func NewCreateQuestionLecture(
	idUser int, 
	courseID int, 
	typeQuestionLecture int,
	title string,
	description *string,

) (*CreateQuestionLecture, *validator.ValidateError) {
	command := &CreateQuestionLecture{idUser, courseID, typeQuestionLecture, title, description}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
