package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_question_lecture/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	courseservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type CreateQuestionLectureHandler struct {
	log                       logger.Logger
	courseRepository          data.CourseRepository
	learningRepository        data.LearningRepository
	lectureRepository         data.LectureRepository
	curriculumRepository      data.CurriculumRepository
	questionLectureRepository data.QuestionLectureRepository
	grpcClient                grpc.GrpcClient
}

func NewCreateQuestionLectureHandler(
	log logger.Logger,
	courseRepository data.CourseRepository,
	learningRepository data.LearningRepository,
	lectureRepository data.LectureRepository,
	curriculumRepository data.CurriculumRepository,
	questionLectureRepository data.QuestionLectureRepository,
	grpcClient grpc.GrpcClient,

) *CreateQuestionLectureHandler {
	return &CreateQuestionLectureHandler{log: log,
		courseRepository:          courseRepository,
		lectureRepository:         lectureRepository,
		questionLectureRepository: questionLectureRepository,
		curriculumRepository:      curriculumRepository,
		learningRepository:        learningRepository,
		grpcClient:                grpcClient,
	}
}

func (c *CreateQuestionLectureHandler) Handle(ctx context.Context, command *CreateQuestionLecture) (*dtos.CreateQuestionLectureResponseDto, error) {
	course, err := c.courseRepository.GetCourseByID(ctx, command.CourseID)

	if err != nil {
		return nil, err
	}

	lecture, err := c.lectureRepository.GetLectureByID(ctx, command.LectureID)

	if err != nil {
		return nil, err
	}
	curriculum, err := c.curriculumRepository.GetCurriculumByID(ctx, lecture.CurriculumID)

	if err != nil {
		return nil, err
	}

	if curriculum.CourseID != course.ID {
		return nil, customErrors.ErrLectureNotBelongToCourse
	}

	//Check whether user has this course ?
	_, err = c.learningRepository.GetLearningByUserCourseID(ctx, command.IdUser, command.CourseID)

	if err != nil {
		return nil, err
	}

	createQuestionLecture := &models.QuestionLecture{
		UserID:      command.IdUser,
		CourseID:    course.ID,
		LectureID:   lecture.ID,
		Title:       command.Title,
		Description: command.Description,
	}

	if err := c.questionLectureRepository.CreateQuestionLecture(ctx, createQuestionLecture); err != nil {
		return nil, err
	}
	connUser, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}
	clientUser := courseservice.NewUsersServiceClient(connUser)

	dataUser, err := clientUser.GetUserByID(context.Background(), &courseservice.GetUserByIdReq{
		UserId: int32(createQuestionLecture.UserID),
	})
	if err != nil {
		return nil, err
	}

	questionLectureShow := &models.QuestionLectureShow{
		ID: createQuestionLecture.ID,
				User: &models.UserQuestionLecture{
					ID: int(dataUser.User.ID),
					Avatar: &dataUser.User.Avatar,
					Name: dataUser.User.Name,
				},
				TotalAnswer: len(createQuestionLecture.AnswerLectures),
				CourseID: createQuestionLecture.CourseID,
				LectureID: createQuestionLecture.LectureID,
				Title: createQuestionLecture.Title,
				Description: createQuestionLecture.Description,
				UpdatedAt: createQuestionLecture.UpdatedAt,
				CreatedAt: createQuestionLecture.CreatedAt,
	}
	// c.log.Info("Create question success: ")

	return &dtos.CreateQuestionLectureResponseDto{
		Message:         "Create question lecture successfully!",
		QuestionLecture: questionLectureShow,
	}, nil
}
