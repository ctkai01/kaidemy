package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type UpdateAnswerLecture struct {
	IdUser          int     `validate:"required,numeric"`
	AnswerLectureID int     `validate:"required,numeric"`
	Answer          string `validate:"required,gte=1,lte=500"`
}

func NewUpdateAnswerLecture(idUser int, answerLectureID int, answer string) (*UpdateAnswerLecture, *validator.ValidateError) {
	command := &UpdateAnswerLecture{idUser, answerLectureID, answer}

	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
