package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"

	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_answer_lecture/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	courseservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
)

type UpdateAnswerLectureHandler struct {
	log                     logger.Logger
	answerLectureRepository data.AnswerLectureRepository
	grpcClient              grpc.GrpcClient
}

func NewUpdateAnswerLectureHandler(
	log logger.Logger,
	answerLectureRepository data.AnswerLectureRepository,
	grpcClient grpc.GrpcClient,

) *UpdateAnswerLectureHandler {
	return &UpdateAnswerLectureHandler{log: log, answerLectureRepository: answerLectureRepository, grpcClient: grpcClient}
}

func (c *UpdateAnswerLectureHandler) Handle(ctx context.Context, command *UpdateAnswerLecture) (*dtos.UpdateAnswerLectureResponseDto, error) {
	answerLecture, err := c.answerLectureRepository.GetAnswerLectureByID(ctx, command.AnswerLectureID)

	if err != nil {
		return nil, err
	}

	if answerLecture.UserID != command.IdUser {
		return nil, customErrors.ErrorCannotPermission
	}

	answerLecture.Answer = command.Answer

	err = c.answerLectureRepository.UpdateAnswerLecture(ctx, answerLecture)

	if err != nil {
		return nil, err
	}

	connUser, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}
	clientUser := courseservice.NewUsersServiceClient(connUser)

	dataUser, err := clientUser.GetUserByID(context.Background(), &courseservice.GetUserByIdReq{
		UserId: int32(answerLecture.UserID),
	})
	if err != nil {
		return nil, err
	}
	answerLectureShow := &models.AnswerLectureShow{
		ID: answerLecture.ID,
		User: &models.UserAnswerLecture{
			ID:     int(dataUser.User.ID),
			Avatar: &dataUser.User.Avatar,
			Name:   dataUser.User.Name,
		},
		QuestionLectureID: answerLecture.QuestionLectureID,
		Answer:            answerLecture.Answer,
		UpdatedAt:         answerLecture.UpdatedAt,
		CreatedAt:         answerLecture.CreatedAt,
	}


	return &dtos.UpdateAnswerLectureResponseDto{
		Message:       "Update answer lecture successfully!",
		AnswerLecture: answerLectureShow,
	}, nil
}
