package dtos

//https://echo.labstack.com/guide/response/

type UpdateAnswerLectureRequestDto struct {
	AnswerLectureID int    `json:"-" param:"id"`
	Answer          string `form:"answer"` //80
}
