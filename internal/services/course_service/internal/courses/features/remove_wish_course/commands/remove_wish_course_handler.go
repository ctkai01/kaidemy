package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/remove_wish_course/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type RemoveWishCourseHandler struct {
	log logger.Logger
	// curriculumRepository data.CurriculumRepository
	// quizRepository       data.QuizRepository
	// questionRepository   data.QuestionRepository
	// answerRepository     data.AnswerRepository
	learningRepository data.LearningRepository
	courseRepository data.CourseRepository
	grpcClient           grpc.GrpcClient
}

func NewRemoveWishCourseHandler(
	log logger.Logger,
	learningRepository data.LearningRepository,
	courseRepository data.CourseRepository,
	grpcClient grpc.GrpcClient,

) *RemoveWishCourseHandler {
	return &RemoveWishCourseHandler{
		log: log,
		grpcClient:           grpcClient,
		learningRepository: learningRepository,
		courseRepository: courseRepository,
	}
}

func (c *RemoveWishCourseHandler) Handle(ctx context.Context, command *DeleteWishCourse) (*dtos.RemoveWishCourseResponseDto, error) {
	learning, err := c.learningRepository.GetLearningByID(ctx, command.LearningID)

	if err != nil {
		return nil, err
	}

	if learning.Type != constants.WISH_LIST_TYPE {
		return nil, customErrors.ErrMustWishList
	}

	if learning.UserID != command.IdUser {
		return nil, customErrors.ErrorCannotPermission
	}

	if err := c.learningRepository.DeleteLearningByID(ctx, learning.ID); err != nil {
		return nil, err
	}

	return &dtos.RemoveWishCourseResponseDto{
		Message: "Remove wish course successfully!!",
	}, nil
}
