package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type DeleteWishCourse struct {
	IdUser       int `validate:"required,numeric"`
	LearningID int `validate:"required,numeric"`
}

func NewDeleteWishCourse(
	idUser int,
	learningID int,

) (*DeleteWishCourse, *validator.ValidateError) {
	command := &DeleteWishCourse{
		idUser,
		learningID,
	}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
