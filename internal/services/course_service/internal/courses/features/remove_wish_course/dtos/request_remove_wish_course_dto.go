package dtos

//https://echo.labstack.com/guide/response/

type DeleteWishCourseRequestDto struct {
	UserCourseID int `param:"id"`
}
