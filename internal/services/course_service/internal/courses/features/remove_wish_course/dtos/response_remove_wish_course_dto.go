package dtos

//https://echo.labstack.com/guide/response/

type RemoveWishCourseResponseDto struct {
	Message        string             `json:"message"`
}
