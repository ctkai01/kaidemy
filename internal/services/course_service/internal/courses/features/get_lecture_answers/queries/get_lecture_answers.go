package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetLectureAnswers struct {
	*utils.ListQuery
}

func NewGetLectureAnswer(
	query *utils.ListQuery,

) (*GetLectureAnswers, *validator.ValidateError) {
	q := &GetLectureAnswers{
		query,
	}
	
	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
