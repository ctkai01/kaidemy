package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_answer/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type DeleteAnswerHandler struct {
	log                  logger.Logger
	curriculumRepository data.CurriculumRepository
	lectureRepository       data.LectureRepository
	questionRepository   data.QuestionRepository
	answerRepository     data.AnswerRepository
	grpcClient           grpc.GrpcClient
}

func NewDeleteAnswerHandler(
	log logger.Logger,
	curriculumRepository data.CurriculumRepository,
	lectureRepository data.LectureRepository,
	questionRepository data.QuestionRepository,
	answerRepository data.AnswerRepository,
	grpcClient grpc.GrpcClient,
) *DeleteAnswerHandler {
	return &DeleteAnswerHandler{
		log:                  log,
		curriculumRepository: curriculumRepository,
		lectureRepository:       lectureRepository,
		questionRepository:   questionRepository,
		grpcClient:           grpcClient,
		answerRepository:     answerRepository,
	}
}

func (c *DeleteAnswerHandler) Handle(ctx context.Context, command *DeleteAnswer) (*dtos.DeleteAnswerResponseDto, error) {
	answer, err := c.answerRepository.GetAnswerByID(ctx, command.AnswerID)

	if err != nil {
		return nil, err
	}

	question, err := c.questionRepository.GetQuestionByID(ctx, answer.QuestionID)

	if err != nil {
		return nil, err
	}

	lecture, err := c.lectureRepository.GetLectureByID(ctx, question.LectureID)

	if err != nil {
		return nil, err
	}

	if lecture == nil {
		return nil, customErrors.ErrLectureNotFound
	}

	curriculum, err := c.curriculumRepository.GetCurriculumByIDRelation(ctx, []string{"Course"}, lecture.CurriculumID)

	if err != nil {
		return nil, err
	}

	if curriculum == nil {
		return nil, customErrors.ErrCurriculumNotFound
	}

	if curriculum.Course.UserID != command.IdUser {
		return nil, customErrors.ErrorCannotPermission
	}

	if err := c.answerRepository.DeleteAnswerByID(ctx, command.AnswerID); err != nil {
		return nil, err
	}
	c.log.Info("Delete answer success: ")

	return &dtos.DeleteAnswerResponseDto{
		Message: "Delete answer successfully!",
	}, nil
}
