package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type DeleteAnswer struct {
	IdUser   int
	AnswerID int `validate:"required,numeric"`
}

func NewDeleteAnswer(idUser int, answerID int) (*DeleteAnswer, *validator.ValidateError) {
	command := &DeleteAnswer{idUser, answerID}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
