package dtos

//https://echo.labstack.com/guide/response/

type DeleteAnswerRequestDto struct {
	AnswerID int `json:"-" param:"id"`
}
