package dtos

//https://echo.labstack.com/guide/response/

type DeleteAnswerResponseDto struct {
	Message string `json:"message"`
}
