package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/params"
	coursesError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_answer/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_answer/dtos"
)

type deleteAnswerEndpoint struct {
	params.AnswersRouteParams
}

func NewDeleteAnswerEndpoint(
	params params.AnswersRouteParams,
) route.Endpoint {
	return &deleteAnswerEndpoint{
		AnswersRouteParams: params,
	}
}

func (ep *deleteAnswerEndpoint) MapEndpoint() {
	ep.AnswersGroup.DELETE("/:id", ep.handler())
}

func (ep *deleteAnswerEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.DeleteAnswerRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[DeleteAnswer.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[DeleteAnswerEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}

		userId := utils.GetUserId(c)

		command, errValidate := commands.NewDeleteAnswer(
			userId,
			request.AnswerID,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[DeleteAnswerEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[DeleteAnswerEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.DeleteAnswer, *dtos.DeleteAnswerResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == coursesError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[DeleteAnswerEndpoint_handler.Send] error in sending Delete Answer",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[DeleteAnswer.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			if err.Error() == coursesError.WrapError(customErrors.ErrorCannotPermission).Error() {
				errCustom := errors.WithMessage(
					err,
					"[DeleteAnswerEndpoint_handler.Send] error in sending delete Answer",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[DeleteAnswer.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewForbiddenErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[DeleteAnswerEndpoint_handler.Send] error in sending Delete Answer",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[DeleteAnswer.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
