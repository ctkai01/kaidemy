package dtos

//https://echo.labstack.com/guide/response/

type MarkLectureRequestDto struct {
	LectureID  int `param:"id"`
	CourseID int `form:"course_id"`
}
