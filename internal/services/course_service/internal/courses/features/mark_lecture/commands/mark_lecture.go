package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type MarkLecture struct {
	IdUser       int
	LectureID int    `validate:"required,numeric"`
	CourseID int    `validate:"required,numeric"`
}

func NewMarkLecture(idUser int, lectureID int, courseID int) (*MarkLecture, *validator.ValidateError) {
	command := &MarkLecture{idUser, lectureID, courseID}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
