package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"

	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/mark_lecture/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
)

type MarkLectureHandler struct {
	log                  logger.Logger
	curriculumRepository data.CurriculumRepository
	lectureRepository    data.LectureRepository
	courseRepository     data.CourseRepository
	learningRepository   data.LearningRepository
	grpcClient           grpc.GrpcClient
}

func NewMarkLectureHandler(
	log logger.Logger,
	curriculumRepository data.CurriculumRepository,
	lectureRepository data.LectureRepository,
	courseRepository data.CourseRepository,
	learningRepository data.LearningRepository,
	grpcClient grpc.GrpcClient,

) *MarkLectureHandler {
	return &MarkLectureHandler{log: log, learningRepository: learningRepository, curriculumRepository: curriculumRepository, lectureRepository: lectureRepository, grpcClient: grpcClient, courseRepository: courseRepository}
}

func (c *MarkLectureHandler) Handle(ctx context.Context, command *MarkLecture) (*dtos.MarkLectureResponseDto, error) {
	// curriculum, err := c.curriculumRepository.GetCurriculumByIDRelation(ctx, []string{"Course"}, command.CurriculumID)
	
	learning, err := c.learningRepository.GetLearningByUserCourseID(ctx, command.IdUser, command.CourseID)

	if err != nil {
		return nil, err
	}

	if learning.UserID != command.IdUser {
		return nil, customErrors.ErrorCannotPermission
	}
	lecture, err := c.lectureRepository.GetLectureByID(ctx, command.LectureID)

	if err != nil {
		return nil, err
	}

	curriculum, err := c.curriculumRepository.GetCurriculumByID(ctx, lecture.CurriculumID)

	if err != nil {
		return nil, err
	}

	if curriculum.CourseID != learning.CourseID {
		return nil, customErrors.ErrorCannotPermission
	}

	//Append
	
	if err := c.learningRepository.AppendAssociationLectures(ctx, learning, lecture); err != nil {
		return nil, err
	}

	

	// if curriculum == nil {
	// 	return nil, customErrors.ErrCurriculumNotFound
	// }

	// if curriculum.Course.UserID != command.IdUser {
	// 	return nil, customErrors.ErrorCannotPermission
	// }

	// lectureCreate := &models.Lecture{
	// 	Title:        command.Title,
	// 	CurriculumID: command.CurriculumID,
	// 	Type:         constants.LECTURE_TYPE,
	// }
	// if err := c.lectureRepository.CreateLecture(ctx, lectureCreate); err != nil {
	// 	return nil, err
	// }
	// c.log.Info("Create lecture success: ")

	// course := &curriculum.Course
	// course.ReviewStatus = constants.REVIEW_INIT_STATUS
	// if err := c.courseRepository.UpdateCourse(ctx, course); err != nil {
	// 	return nil, err
	// }

	return &dtos.MarkLectureResponseDto{
		Message: "Mark lecture successfully!",
		// Lecture: *lectureCreate,
	}, nil
}
