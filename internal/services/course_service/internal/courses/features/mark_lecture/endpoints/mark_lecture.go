package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/params"
	coursesError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/mark_lecture/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/mark_lecture/dtos"
)

type markLectureEndpoint struct {
	params.LectureNormalsRouteParams
}

func NewMarkLectureParamsEndpoint(
	params params.LectureNormalsRouteParams,
) route.Endpoint {
	return &markLectureEndpoint{
		LectureNormalsRouteParams: params,
	}
}

func (ep *markLectureEndpoint) MapEndpoint() {
	ep.LectureNormalsGroup.POST("/:id/mark", ep.handler())
}

func (ep *markLectureEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.MarkLectureRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[markLecture.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[markLectureEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}

		userId := utils.GetUserId(c)

		command, errValidate := commands.NewMarkLecture(
			userId,
			request.LectureID,
			request.CourseID,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[markLectureEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[markLectureEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.MarkLecture, *dtos.MarkLectureResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == coursesError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[markLectureEndpoint_handler.Send] error in sending create Lecture",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[markLecture.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			if err.Error() == coursesError.WrapError(customErrors.ErrorCannotPermission).Error() {
				errCustom := errors.WithMessage(
					err,
					"[markLectureEndpoint_handler.Send] error in sending create Lecture",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[markLecture.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewForbiddenErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[markLectureEndpoint_handler.Send] error in sending create Lecture",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[markLecture.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusCreated, result)
	}
}
