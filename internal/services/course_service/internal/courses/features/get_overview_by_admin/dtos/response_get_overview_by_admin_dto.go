package dtos

// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

type OverviewAdmin struct {
	Courses *Stats   `json:"course"`
	Users   *Stats   `json:"users"`
	Revenue *Revenue `json:"revenues"`
}

type Stats struct {
	Total          int   `json:"total"`
	TotalThisMonth int   `json:"total_this_month"`
	DetailStats    []int `json:"detail_stats"`
}

type Revenue struct {
	Total    float32       `json:"total"`
	Fee      *RevenueStats `json:"fee"`
	Net      *RevenueStats `json:"net"`
	Currency string        `json:"currency"`
}

type RevenueStats struct {
	Total          float32   `json:"total"`
	TotalThisMonth float32   `json:"total_this_month"`
	DetailStats    []float32 `json:"detail_stats"`
}

type GetOverviewByAdminResponseDto struct {
	Message string         `json:"message"`
	Result  *OverviewAdmin `json:"result"`
}
