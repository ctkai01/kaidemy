package queries

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"math"
	"time"

	// "math"

	// "strings"

	// "strings"

	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"

	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	stripe "github.com/stripe/stripe-go/v76"
	stripeBalanceTransaction "github.com/stripe/stripe-go/v76/balancetransaction"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_overview_by_admin/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	courseservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type GetOverviewByAdminHandler struct {
	log                logger.Logger
	courseRepository   data.CourseRepository
	learningRepository data.LearningRepository
	grpcClient         grpc.GrpcClient
}

func NewGetOverviewByAdminHandler(
	log logger.Logger,
	courseRepository data.CourseRepository,
	learningRepository data.LearningRepository,
	grpcClient grpc.GrpcClient,

) *GetOverviewByAdminHandler {
	return &GetOverviewByAdminHandler{
		log:                log,
		courseRepository:   courseRepository,
		learningRepository: learningRepository,
		grpcClient:         grpcClient,
	}
}

func (c *GetOverviewByAdminHandler) Handle(ctx context.Context, command *GetOverviewByAdmin) (*dtos.GetOverviewByAdminResponseDto, error) {
	// if command.IdUser != command.IdUserCall {
	// 	return nil, customErrors.ErrorCannotPermission
	// }
	// courseIDsByAuthor, err := c.courseRepository.FindIdsCourseAuthor(ctx, command.IdUser)

	// if err != nil {
	// 	return nil, err
	// }

	// var courseIDFilter []int
	// if command.CourseID != nil {
	// 	n, _ := strconv.Atoi(*command.CourseID)
	// 	isExist := false
	// 	for _, id := range courseIDsByAuthor {
	// 		if id == n {
	// 			isExist = true
	// 			break
	// 		}
	// 	}

	// 	if !isExist {
	// 		return nil, customErrors.ErrorCannotPermission
	// 	}

	// 	courseIDFilter = append(courseIDFilter, n)
	// } else {
	// 	courseIDFilter = courseIDsByAuthor
	// }
	connUser, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}
	clientUser := courseservice.NewUsersServiceClient(connUser)

	courses, err := c.courseRepository.GetAllCourseInThisYear(ctx)

	if err != nil {
		return nil, err
	}

	// var usersCourse []*dtos.UserCourseAuthorReview

	// connUser, err := c.grpcClient.GetGrpcConnection("user_service")
	// if err != nil {
	// 	return nil, err
	// }
	// clientUser := courseservice.NewUsersServiceClient(connUser)
	monthlyCourseCount := make([]int, 12)
	totalCourseThisMonth := 0

	currentMonth := time.Now().Month()
	for _, course := range courses {
		if course.CreatedAt != nil {
			month := course.CreatedAt.Month()
			if month == currentMonth {
				totalCourseThisMonth++
			}
			monthlyCourseCount[month-1]++ // Adjusting month index to start from 0
		}
	}

	statUserData, err := clientUser.GetOverViewStudents(ctx, &courseservice.GetOverViewStudentsReq{
		UserId: int32(command.IdUser),
	})

	if err != nil {
		return nil, err
	}

	var detailStatsUSer []int

	for _, item := range statUserData.MonthlyCount {
		detailStatsUSer = append(detailStatsUSer, int(item))
	}

	//Revenue
	stripe.Key = "sk_test_51NUmJOFEsDSfMpHunO8Zscoo1KwMISvWpj00QwhHnLTjYQVkQF1uH0WBgCd8pWgfd5gAKBguz9MfszFPlQwOVPXl00ZUdeBaS5"

	typeTransaction := "application_fee"
	typeChargeTransaction := "charge"

	// Get the Unix timestamp
	startYear := time.Date(time.Now().Year(), 1, 1, 0, 0, 0, 0, time.Local)

	params := &stripe.BalanceTransactionListParams{
		ListParams: stripe.ListParams{
			Single: true,
		},
		CreatedRange: &stripe.RangeQueryParams{
			LesserThanOrEqual: time.Now().Unix(),
			GreaterThan:       startYear.Unix(),
		},
	}
	resultTx := stripeBalanceTransaction.List(params)

	monthlyNetCount := make([]float32, 12)
	monthlyFeeCount := make([]float32, 12)
	var totalNetThisMonth float32
	var totalNet float32
	var totalFeeThisMonth float32
	var totalFee float32

	var totalRevenue float32
	currency := ""
	for resultTx.Next() {

		if string(resultTx.BalanceTransaction().Type) == typeTransaction || string(resultTx.BalanceTransaction().Type) == typeChargeTransaction {
			monthCreated := time.Unix(resultTx.BalanceTransaction().Created, 0).Month()

			if string(resultTx.BalanceTransaction().Type) == typeTransaction {
				netValue := float32(resultTx.BalanceTransaction().Net) / float32(100)
				if monthCreated == currentMonth {
					totalNetThisMonth += netValue
				}

				totalRevenue = totalRevenue + netValue
				totalNet += netValue
				monthlyNetCount[monthCreated-1] = monthlyNetCount[monthCreated-1] + netValue

			}

			if string(resultTx.BalanceTransaction().Type) == typeChargeTransaction {
				feeValue := float32(resultTx.BalanceTransaction().Fee) / float32(100)
				if monthCreated == currentMonth {
					totalFeeThisMonth += feeValue
				}

				totalFee += feeValue
				monthlyFeeCount[monthCreated-1] = monthlyFeeCount[monthCreated-1] + feeValue
			}

			// feeValue := float32(resultTx.BalanceTransaction().Fee) / float32(100)

			// if monthCreated == currentMonth {
			// 	totalNetThisMonth += netValue
			// 	totalFeeThisMonth += feeValue
			// }
			// totalRevenue = totalRevenue + netValue
			// totalNet += netValue
			// totalFee += feeValue

			if currency == "" {
				currency = string(resultTx.BalanceTransaction().Currency)
			}
			// monthlyFeeCount[monthCreated-1] = monthlyFeeCount[monthCreated-1] + feeValue
		}

	}

	for i, item := range monthlyNetCount {
		monthlyNetCount[i] = float32(math.Ceil(float64(item)*100) / 100)
	}

	for i, item := range monthlyFeeCount {
		monthlyFeeCount[i] = float32(math.Ceil(float64(item)*100) / 100)
	}

	revenue := &dtos.Revenue{
		Total: float32(math.Ceil(float64(totalRevenue)*100) / 100),
		Fee: &dtos.RevenueStats{
			Total:          float32(math.Ceil(float64(totalFee)*100) / 100),
			TotalThisMonth: float32(math.Ceil(float64(totalFeeThisMonth)*100) / 100),
			DetailStats:    monthlyFeeCount,
		},
		Net: &dtos.RevenueStats{
			Total:          float32(math.Ceil(float64(totalNet)*100) / 100),
			TotalThisMonth: float32(math.Ceil(float64(totalNetThisMonth)*100) / 100),
			DetailStats:    monthlyNetCount,
		},
		Currency: currency,
	}

	result := &dtos.OverviewAdmin{
		Courses: &dtos.Stats{
			Total:          len(courses),
			TotalThisMonth: totalCourseThisMonth,
			DetailStats:    monthlyCourseCount,
		},
		Users: &dtos.Stats{
			Total:          int(statUserData.Total),
			TotalThisMonth: int(statUserData.TotalThisMonth),
			DetailStats:    detailStatsUSer,
		},
		Revenue: revenue,
	}

	return &dtos.GetOverviewByAdminResponseDto{
		Message: "Get data successfully!",
		Result:  result,
	}, nil
}
