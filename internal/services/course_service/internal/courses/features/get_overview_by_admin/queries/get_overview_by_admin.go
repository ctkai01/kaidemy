package queries

import (
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetOverviewByAdmin struct {
	IdUser int
	// *utils.ListQuery
}

func NewGetOverviewByAdmin(
	idUser int,
	// search string,
	// query *utils.ListQuery,

) (*GetOverviewByAdmin, *validator.ValidateError) {
	q := &GetOverviewByAdmin{
		idUser,
	}
	
	errValidate := validator.Validate(q)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
