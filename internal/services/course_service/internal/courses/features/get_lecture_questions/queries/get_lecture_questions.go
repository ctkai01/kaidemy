package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetLectureQuestions struct {
	*utils.ListQuery
}

func NewGetLectureQuestions(
	query *utils.ListQuery,

) (*GetLectureQuestions, *validator.ValidateError) {
	q := &GetLectureQuestions{
		query,
	}
	
	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
