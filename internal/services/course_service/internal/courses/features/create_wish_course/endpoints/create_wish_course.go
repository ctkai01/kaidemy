package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/params"
	// coursesError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_wish_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_wish_course/dtos"
)

type createWishCourseEndpoint struct {
	params.LearningsRouteParams
}

func NewCreateWishCourseEndpoint(
	params params.LearningsRouteParams,
) route.Endpoint {
	return &createWishCourseEndpoint{
		LearningsRouteParams: params,
	}
}

func (ep *createWishCourseEndpoint) MapEndpoint() {
	ep.LearningsGroup.POST("/wish-course", ep.handler())
}

func (ep *createWishCourseEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.CreateWishCourseRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[CreateWishCourse.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[CreateWishCourseEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())
			
		}

		userId := utils.GetUserId(c)

		command, errValidate := commands.NewCreateWishCourse(
			userId,
			request.CourseID,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[CreateWishCourseEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[CreateWishCourseEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.CreateWishCourse, *dtos.CreateWishCourseResponseDto](
			ctx,
			command,
		)

		if err != nil {
			// if err.Error() == coursesError.WrapError(customErrors.ErrorCannotPermission).Error() {
			// 	errCustom := errors.WithMessage(
			// 		err,
			// 		"[UpdateLearningEndpoint_handler.Send] error in sending Update Course User",
			// 	)
			// 	ep.Logger.Error(
			// 		fmt.Sprintf(
			// 			"[UpdateLearning.Send], err: %v",
			// 			errCustom,
			// 		),
			// 	)
			// 	return customErrors.NewForbiddenErrorHttp(c, err.Error())
			// }

			err = errors.WithMessage(
				err,
				"[CreateWishCourseEndpoint_handler.Send] error in sending Create wish course",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[CreateWishCourse.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusCreated, result)
	}
}
