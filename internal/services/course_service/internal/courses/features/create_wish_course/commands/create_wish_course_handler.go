package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"strings"

	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_wish_course/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	courseservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"

	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type CreateWishCourseHandler struct {
	log logger.Logger
	// curriculumRepository data.CurriculumRepository
	// quizRepository       data.QuizRepository
	// questionRepository   data.QuestionRepository
	// answerRepository     data.AnswerRepository
	learningRepository data.LearningRepository
	courseRepository   data.CourseRepository
	grpcClient         grpc.GrpcClient
}

func NewCreateWishCourseHandler(
	log logger.Logger,
	learningRepository data.LearningRepository,
	courseRepository data.CourseRepository,
	grpcClient grpc.GrpcClient,

) *CreateWishCourseHandler {
	return &CreateWishCourseHandler{
		log:                log,
		grpcClient:         grpcClient,
		learningRepository: learningRepository,
		courseRepository:   courseRepository,
	}
}

func (c *CreateWishCourseHandler) Handle(ctx context.Context, command *CreateWishCourse) (*dtos.CreateWishCourseResponseDto, error) {
	_, err := c.courseRepository.GetCourseByID(ctx, command.CourseID)

	if err != nil {
		return nil, err
	}

	_, err = c.learningRepository.GetLearningByUserCourseID(ctx, command.IdUser, command.CourseID)

	isLearningExist := true
	if err != nil {
		if err == customErrors.ErrLearningNotFound {
			isLearningExist = false
		} else {
			return nil, err
		}
	}

	if isLearningExist {
		return nil, customErrors.ErrCannotAddWishList
	}

	createLearning := &models.Learning{
		UserID:   command.IdUser,
		CourseID: command.CourseID,
		Type:     constants.WISH_LIST_TYPE,
	}

	if err := c.learningRepository.CreateLearning(ctx, createLearning); err != nil {
		return nil, err
	}

	learning, err := c.learningRepository.GetLearningByIDRelation(ctx, []string{"Course.Curriculums.Lectures.Assets"}, createLearning.ID)

	if err != nil {
		return nil, err
	}
	// c.learningRepository.

	connUser, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}
	clientUser := courseservice.NewUsersServiceClient(connUser)

	conn, err := c.grpcClient.GetGrpcConnection("catalog_service")
	if err != nil {
		return nil, err
	}
	client := courseservice.NewCatalogsServiceClient(conn)

	//
	var levelID int

	if learning.Course.LevelID == nil {
		levelID = -1
	} else {
		levelID = *learning.Course.LevelID
	}

	dataLevel, err := client.GetLevelByID(context.Background(), &courseservice.GetLevelByIdReq{
		// UserId:  int32(command.IdUser),
		LevelId: int32(levelID),
	})

	if err != nil {
		if !strings.Contains(err.Error(), "level not found") {
			return nil, err

		}
	}

	//Get price
	var priceID int

	if learning.Course.PriceID == nil {
		priceID = -1
	} else {
		priceID = *learning.Course.PriceID
	}

	dataPrice, err := client.GetPriceByID(context.Background(), &courseservice.GetPriceByIdReq{
		PriceId: int32(priceID),
	})

	if err != nil {
		if !strings.Contains(err.Error(), "price not found") {
			return nil, err
		}
	}

	//Get data user service
	dataUser, err := clientUser.GetUserByID(context.Background(), &courseservice.GetUserByIdReq{
		UserId: int32(learning.Course.UserID),
	})
	if err != nil {
		return nil, err
	}

	var levelRes *models.LevelResponse

	if dataLevel == nil {
		levelRes = nil
	} else {
		levelRes = &models.LevelResponse{
			ID:   int(dataLevel.Level.ID),
			Name: dataLevel.Level.Name,
		}
	}

	var priceRes *models.PriceResponse

	if dataPrice == nil {
		priceRes = nil
	} else {
		priceRes = &models.PriceResponse{
			ID:    int(dataPrice.Price.ID),
			Tier:  dataPrice.Price.Tier,
			Value: int(dataPrice.Price.Value),
		}
	}
	learningShows := &models.LearningShow{
		ID:        learning.ID,
		UserID:    learning.UserID,
		CourseID:  learning.CourseID,
		Process:   learning.Process,
		Type:      learning.Type,
		StarCount: learning.StarCount,
		Comment:   learning.Comment,
		Course: models.CourseLearning{
			ID:           learning.Course.ID,
			Title:        learning.Course.Title,
			Status:       learning.Course.Status,
			ReviewStatus: learning.Course.ReviewStatus,
			Image:        learning.Course.Image,
			Curriculums:  learning.Course.Curriculums,
			Level:        levelRes,
			Price:        priceRes,
			User: models.UserResponse{
				ID:   int(dataUser.User.ID),
				Name: dataUser.User.Name,
			},
			UpdatedAt: learning.Course.UpdatedAt,
			CreatedAt: learning.Course.CreatedAt,
		},
		UpdatedAt: learning.UpdatedAt,
		CreatedAt: learning.CreatedAt,
	}

	return &dtos.CreateWishCourseResponseDto{
		Message:  "Create wish course successfully!!",
		Learning: learningShows,
	}, nil
}
