package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type CreateWishCourse struct {
	IdUser   int `validate:"required,numeric"`
	CourseID int `validate:"required,numeric"`
}

func NewCreateWishCourse(
	idUser int,
	courseID int,
	
) (*CreateWishCourse, *validator.ValidateError) {
	command := &CreateWishCourse{
		idUser,
		courseID,
	}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
