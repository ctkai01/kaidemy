package dtos

//https://echo.labstack.com/guide/response/

type CreateWishCourseRequestDto struct {
	CourseID int `form:"course_id"`
}
