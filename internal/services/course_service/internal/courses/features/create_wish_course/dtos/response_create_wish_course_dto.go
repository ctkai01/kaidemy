package dtos

import "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

//https://echo.labstack.com/guide/response/

type CreateWishCourseResponseDto struct {
	Message  string           `json:"message"`
	Learning *models.LearningShow `json:"learning"`
}
