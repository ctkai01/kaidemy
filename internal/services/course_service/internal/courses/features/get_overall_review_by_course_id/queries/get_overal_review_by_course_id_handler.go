package queries

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"math"

	// "fmt"
	// "strings"

	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitla.b.com/ctkai01/kaidemy/internal/pkg/utils"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_overall_review_by_course_id/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// courseservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
)

type GetOverallReviewByCourseIDHandler struct {
	log                logger.Logger
	learningRepository data.LearningRepository
	courseRepository   data.CourseRepository
	grpcClient         grpc.GrpcClient
}

func NewGetOverallReviewByCourseIDHandler(
	log logger.Logger,
	learningRepository data.LearningRepository,
	courseRepository data.CourseRepository,
	grpcClient grpc.GrpcClient,

) *GetOverallReviewByCourseIDHandler {
	return &GetOverallReviewByCourseIDHandler{
		log:                log,
		learningRepository: learningRepository,
		courseRepository:   courseRepository,
		grpcClient:         grpcClient,
	}
}

func (c *GetOverallReviewByCourseIDHandler) Handle(ctx context.Context, command *GetOverallReviewByCourseID) (*dtos.GetOverallReviewByCourseIDResponseDto, error) {

	_, err := c.courseRepository.GetCourseByID(ctx, command.IdCourse)
	if err != nil {
		return nil, err
	}

	learnings, err := c.learningRepository.GetOverallReviewsCourseID(ctx, command.IdCourse)

	if err != nil {
		return nil, err
	}

	// var learningReviews = make([]*models.LearningReview, 0)
	// connUser, err := c.grpcClient.GetGrpcConnection("user_service")
	// if err != nil {
	// 	return nil, err
	// }
	// clientUser := courseservice.NewUsersServiceClient(connUser)
	var totalReviewCount int

	var fiveStarCount int
	var fourStarCount int
	var threeStarCount int
	var twoStarCount int
	var oneStarCount int

	// if learnings.Items != nil {
	for _, learning := range learnings {
		if learning.StarCount != nil {
			totalReviewCount += *learning.StarCount

			if *learning.StarCount == 5 {
				fiveStarCount++
			} else if *learning.StarCount == 4 {
				fourStarCount++

			} else if *learning.StarCount == 3 {
				threeStarCount++

			} else if *learning.StarCount == 2 {
				twoStarCount++

			} else {
				oneStarCount++
			}
		}
	}
	// }

	// fiveStarCount
	// fourStarCount
	// threeStarCount
	// twoReviewCount
	// oneReviewCount

	var averageReview float32

	if len(learnings) != 0 {
		averageReview = float32(totalReviewCount) / float32(len(learnings))
		fiveStarCount = int(math.Round((float64(fiveStarCount) / float64(len(learnings))) * 100))
		fourStarCount = int(math.Round((float64(fourStarCount) / float64(len(learnings))) * 100))
		threeStarCount = int(math.Round((float64(threeStarCount) / float64(len(learnings))) * 100))
		twoStarCount = int(math.Round((float64(twoStarCount) / float64(len(learnings))) * 100))
		oneStarCount = int(math.Round((float64(oneStarCount) / float64(len(learnings))) * 100))
	}

	result := &models.OverallReviewsByCourseID{
		AverageReview: averageReview,
		TotalReview:   int(len(learnings)),
		Overall: &models.OverallStar{
			FiveStar:  fiveStarCount,
			FourStar:  fourStarCount,
			ThreeStar: threeStarCount,
			TwoStar:   twoStarCount,
			OneStar:   oneStarCount,
		},
	}
	return &dtos.GetOverallReviewByCourseIDResponseDto{
		Message: "Get learnings successfully!",
		Result:  result,
	}, nil
}
