package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetOverallReviewByCourseID struct {
	IdCourse int
}

func NewGetOverallReviewByCourseID(
	idCourse int,

) (*GetOverallReviewByCourseID, *validator.ValidateError) {
	q := &GetOverallReviewByCourseID{
		idCourse,
	}
	
	errValidate := validator.Validate(q)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
