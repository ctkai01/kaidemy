package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/params"
	// coursesError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_overall_review_by_course_id/queries"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_overall_review_by_course_id/dtos"
)

type getOverallReviewsByCourseIDEndpoint struct {
	params.CoursesNoAuthRouteParams
}

func NewGetOverallReviewsByCourseIDEndpoint(
	params params.CoursesNoAuthRouteParams,
) route.Endpoint {
	return &getOverallReviewsByCourseIDEndpoint{
		CoursesNoAuthRouteParams: params,
	}
}

func (ep *getOverallReviewsByCourseIDEndpoint) MapEndpoint() {
	ep.CoursesGroup.GET("/:id/reviews-overall", ep.handler())
}

func (ep *getOverallReviewsByCourseIDEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()
		
		request := &dtos.GetOverallReviewByCourseIDRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[getOverallReviewsByCourseIDDEndpoint.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getOverallReviewsByCourseIDDEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())
		}

		query, errValidate := queries.NewGetOverallReviewByCourseID(
			request.ID,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[getOverallReviewsByCourseIDEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getOverallReviewsByCourseIDEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return customErrors.NewBadRequestErrorHttp(c, validationErr.Error())

		}

		result, err := mediatr.Send[*queries.GetOverallReviewByCourseID, *dtos.GetOverallReviewByCourseIDResponseDto](
			ctx,
			query,
		)

		if err != nil {
			// if err.Error() == coursesError.WrapError(customErrors.ErrUserNotFound).Error() {
			// 	errCustom := errors.WithMessage(
			// 		err,
			// 		"[RegisterNotificationToken_handler.Send] error in sending create Answer",
			// 	)
			// 	ep.Logger.Error(
			// 		fmt.Sprintf(
			// 			"[createAnswer.Send], err: %v",
			// 			errCustom,
			// 		),
			// 	)
			// 	return customErrors.NewBadRequestErrorHttp(c, err.Error())
			// }

			err = errors.WithMessage(
				err,
				"[getOverallReviewsByCourseIDEndpoint_handler.Send] error in sending get learnings",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[GetLearnings.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
