package dtos

//https://echo.labstack.com/guide/response/

type GetOverallReviewByCourseIDRequestDto struct {
	ID int `param:"id"`
}
