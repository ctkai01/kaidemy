package dtos

import "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"



type GetOverallReviewByCourseIDResponseDto struct {
	Message string                    `json:"message"`
	Result  *models.OverallReviewsByCourseID `json:"result"`
}
