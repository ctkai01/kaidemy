package dtos

import "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

//https://echo.labstack.com/guide/response/

type UpdateQuestionResponseDto struct {
	Message  string          `json:"message"`
	Question models.Question `json:"question"`
}
