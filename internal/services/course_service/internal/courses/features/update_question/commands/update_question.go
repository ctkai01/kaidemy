package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type UpdateQuestion struct {
	IdUser     int
	Title      string `validate:"required,gte=1,lte=600"`
	QuestionID int    `validate:"required,numeric"`
}

func NewUpdateQuestion(idUser int, title string, questionID int) (*UpdateQuestion, *validator.ValidateError) {
	command := &UpdateQuestion{idUser, title, questionID}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
