package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/params"
	coursesError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_course/dtos"
)

type updateCourseEndpoint struct {
	params.CoursesRouteParams
}

func NewUpdateCourseEndpoint(
	params params.CoursesRouteParams,
) route.Endpoint {
	return &updateCourseEndpoint{
		CoursesRouteParams: params,
	}
}

func (ep *updateCourseEndpoint) MapEndpoint() {
	ep.CoursesGroup.PUT("/:id", ep.handler())
}

func (ep *updateCourseEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.UpdateCourseRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[UpdateCourse.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[UpdateCourseEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}

		userId := utils.GetUserId(c)
		file, err := c.FormFile("image")

		if err != nil {
			if err != http.ErrMissingFile {
				return customErrors.NewInternalServerErrorHttp(c, err.Error())
			}
		}

		video, err := c.FormFile("promotional_video")

		if err != nil {
			if err != http.ErrMissingFile {
				return customErrors.NewInternalServerErrorHttp(c, err.Error())
			}
		}
		command, errValidate := commands.NewUpdateCourse(
			userId,
			request.WelcomeMessage,
			request.CongratulationsMessage,
			request.Subtitle,
			request.PrimarilyTeach,
			request.Title,
			request.Description,
			request.LanguageID,
			request.CategoryID,
			request.SubCategoryID,
			request.LevelID,
			request.PriceID,
			request.CourseID,
			file,
			request.Outcomes,
			request.Requirements,
			request.IntendedFor,
			video,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[UpdateCourseEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[UpdateCourseEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.UpdateCourse, *dtos.UpdateCourseResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == coursesError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[UpdateCourseEndpoint_handler.Send] error in sending Update Course",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[UpdateCourse.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[UpdateCourseEndpoint_handler.Send] error in sending Update Course",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[UpdateCourse.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
