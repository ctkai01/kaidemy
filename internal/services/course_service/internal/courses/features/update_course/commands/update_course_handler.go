package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"fmt"
	"io"
	"strings"

	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_course/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	stripe "github.com/stripe/stripe-go/v76"
	stripePrice "github.com/stripe/stripe-go/v76/price"
	stripeProduct "github.com/stripe/stripe-go/v76/product"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	catalogService "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"

	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	grpcService "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
)

type UpdateCourseHandler struct {
	log              logger.Logger
	courseRepository data.CourseRepository
	// courseRepository data.CourseRepository
	grpcClient grpc.GrpcClient
}

func NewUpdateCourseHandler(
	log logger.Logger,
	curriculumRepository data.CurriculumRepository,
	courseRepository data.CourseRepository,
	grpcClient grpc.GrpcClient,

) *UpdateCourseHandler {
	return &UpdateCourseHandler{log: log, courseRepository: courseRepository, grpcClient: grpcClient}
}

func (c *UpdateCourseHandler) Handle(ctx context.Context, command *UpdateCourse) (*dtos.UpdateCourseResponseDto, error) {
	courseUpdate, err := c.courseRepository.GetCourseByIDRelation(ctx, []string{"Curriculums"} ,command.CourseID)

	if err != nil {
		return nil, err
	}

	if courseUpdate.UserID != command.IdUser {
		return nil, customErrors.ErrorCannotPermission

	}
	stripe.Key = "sk_test_51NUmJOFEsDSfMpHunO8Zscoo1KwMISvWpj00QwhHnLTjYQVkQF1uH0WBgCd8pWgfd5gAKBguz9MfszFPlQwOVPXl00ZUdeBaS5"
	productStripeParams := &stripe.ProductParams{}

	courseUpdate.WelcomeMessage = command.WelcomeMessage
	courseUpdate.CongratulationsMessage = command.CongratulationsMessage

	courseUpdate.Title = command.Title

	fmt.Println("command.Outcomes: ", command.Outcomes)

	if len(command.Outcomes) > 0 {
		if  command.Outcomes[0] == "" {
			courseUpdate.OutComes = nil
		} else {
			courseUpdate.OutComes = (*models.StringArray)(&command.Outcomes)
		}
	}

	if len(command.Requirements) > 0 {
		if  command.Requirements[0] == "" {
			courseUpdate.Requirement = nil
		} else {
			courseUpdate.Requirement = (*models.StringArray)(&command.Requirements)
		}
	}
	if len(command.IntendedFor) > 0  {
		if  command.IntendedFor[0] == "" {
			courseUpdate.IntendedFor = nil
		} else {
			courseUpdate.IntendedFor = (*models.StringArray)(&command.IntendedFor)
		}
	}

	if command.Description != nil {
		courseUpdate.Description = command.Description

	}

	if command.Subtitle != nil {
		courseUpdate.Subtitle = command.Subtitle
		productStripeParams.Description = command.Subtitle
	}

	if command.PrimarilyTeach != nil {
		courseUpdate.PrimarilyTeach = command.PrimarilyTeach
	}

	if command.Title != "" {
		productStripeParams.Name = &command.Title
	}
	{
		conn, err := c.grpcClient.GetGrpcConnection("catalog_service")
		if err != nil {
			return nil, err
		}

		client := catalogService.NewCatalogsServiceClient(conn)
		dataCategory, err := client.GetCategoryByID(context.Background(), &catalogService.GetCategoryByIdReq{
			// UserId:     int32(command.IdUser),
			CategoryId: int32(command.SubCategoryID),
		})

		if err != nil {
			if strings.Contains(err.Error(), customErrors.ErrCategoryNotFound.Error()) {
				return nil, customErrors.ErrCategoryNotFound

			}
			return nil, err
		}

		if dataCategory.Category.ParentID != int32(command.CategoryID) {
			return nil, customErrors.ErrParentCategoryNotCorrect
		}

		courseUpdate.CategoryID = command.CategoryID
		courseUpdate.SubCategoryID = command.SubCategoryID
	}

	{
		//LanguageID
		if command.LanguageID != nil {
			conn, err := c.grpcClient.GetGrpcConnection("catalog_service")
			if err != nil {
				return nil, err
			}

			client := catalogService.NewCatalogsServiceClient(conn)
			dataLevel, err := client.GetLanguageByID(context.Background(), &catalogService.GetLanguageByIdReq{
				// UserId:     int32(command.IdUser),
				LanguageId: int32(*command.LanguageID),
			})

			if err != nil {
				if strings.Contains(err.Error(), customErrors.ErrLanguageNotFound.Error()) {
					return nil, customErrors.ErrLanguageNotFound

				}
				return nil, err
			}

			if dataLevel.Language == nil {
				return nil, customErrors.ErrLanguageNotFound
			}

			courseUpdate.LanguageID = command.LanguageID
		}
	}

	{
		//LevelID
		if command.LevelID != nil {
			conn, err := c.grpcClient.GetGrpcConnection("catalog_service")
			if err != nil {
				return nil, err
			}

			client := catalogService.NewCatalogsServiceClient(conn)
			dataLevel, err := client.GetLevelByID(context.Background(), &catalogService.GetLevelByIdReq{
				// UserId:  int32(command.IdUser),
				LevelId: int32(*command.LevelID),
			})

			if err != nil {
				if strings.Contains(err.Error(), customErrors.ErrLevelNotFound.Error()) {
					return nil, customErrors.ErrLevelNotFound

				}
				return nil, err
			}

			if dataLevel.Level == nil {
				return nil, customErrors.ErrLevelNotFound
			}

			courseUpdate.LevelID = command.LevelID
		}
	}

	{
		//PriceID

		if command.PriceID != nil {
			conn, err := c.grpcClient.GetGrpcConnection("catalog_service")
			if err != nil {
				return nil, err
			}
			fmt.Println("PriceID: ", *command.PriceID)
			client := catalogService.NewCatalogsServiceClient(conn)
			dataLevel, err := client.GetPriceByID(context.Background(), &catalogService.GetPriceByIdReq{
				// UserId:  int32(command.IdUser),
				PriceId: int32(*command.PriceID),
			})

			if err != nil {
				if strings.Contains(err.Error(), customErrors.ErrPriceNotFound.Error()) {
					return nil, customErrors.ErrPriceNotFound

				}
				return nil, err
			}

			if dataLevel.Price == nil {
				return nil, customErrors.ErrPriceNotFound
			}
			fmt.Println("What: ", dataLevel.GetPrice().Value)
			courseUpdate.PriceID = command.PriceID
			fmt.Println("up: ", productStripeParams.DefaultPriceData)
			// productStripeParams.DefaultPrice = 1
			priceStripeParams := &stripe.PriceParams{
				Currency:   stripe.String(string(stripe.CurrencyUSD)),
				UnitAmount: stripe.Int64(int64(dataLevel.GetPrice().Value) * int64(100)),
				Product:    stripe.String(courseUpdate.ProductIdStripe),
			}

			priceStripeCreate, err := stripePrice.New(priceStripeParams)

			if err != nil {
				return nil, err
			}

			productStripeParams.DefaultPrice = stripe.String(priceStripeCreate.ID)
		}
	}

	//Update avatar
	if command.Image != nil {
		connUpload, err := c.grpcClient.GetGrpcConnection("upload_service")
		if err != nil {
			return nil, err
		}
		clientUpload := grpcService.NewUploadsServiceClient(connUpload)
		// Remove previous image
		if courseUpdate.Image != nil {
			resourceFile := strings.Split(*courseUpdate.Image, "/")
			resourceFile = resourceFile[len(resourceFile)-2:]
			resourceFileStr := strings.Join(resourceFile, "/")
			resourceFileUrl := fmt.Sprintf("https://sg.storage.bunnycdn.com/kaidemy/%s", resourceFileStr)

			fmt.Println("URL delete: ", resourceFileUrl)
			if _, err := clientUpload.DeleteResource(ctx, &grpcService.DeleteResourceReq{
				Url: resourceFileUrl,
			}); err != nil {
				c.log.Err("Error remove resource: ", err)
			}
		}

		//Upload GRPC

		stream, err := clientUpload.UploadResource(ctx)

		if err != nil {
			return nil, err
		}
		file, err := command.Image.Open()

		if err != nil {
			return nil, err
		}

		buf := make([]byte, 1048576)
		batchNumber := 1
		for {
			num, err := file.Read(buf)
			if err == io.EOF {
				break
			}
			if err != nil {
				return nil, err
			}
			chunk := buf[:num]

			if err := stream.Send(&grpcService.UploadResourceReq{
				Title:     "Image",
				Type:      int32(constants.MediaType),
				Data:      chunk,
				Extension: command.Image.Header["Content-Type"][0],
				Size:      int64(len(chunk)),
				Name:      command.Image.Filename,
			}); err != nil {
				return nil, err
			}
			c.log.Info("Sent - batch #%v - size - %v\n", batchNumber, len(chunk))
			batchNumber += 1

		}

		res, err := stream.CloseAndRecv()
		c.log.Info("Err: ", err)

		if err != nil {
			return nil, err
		}
		c.log.Info("URL: ", res.GetUrl())
		courseUpdate.Image = &res.Url
		productStripeParams.Images = stripe.StringSlice([]string{res.Url})
	}

	//Update video promotional
	// if command.videoPromotional != nil {
	// 	connUpload, err := c.grpcClient.GetGrpcConnection("upload_service")
	// 	if err != nil {
	// 		return nil, err
	// 	}

	// 	clientUpload := grpcService.NewUploadsServiceClient(connUpload)

	// 	// Remove previous video
	// 	if courseUpdate.PromotionalVideo != nil {
	// 		//Delete
	// 		if _, err := clientUpload.DeleteVideo(ctx, &grpcService.DeleteVideoReq{
	// 			VideoID: *courseUpdate.PromotionalVideo,
	// 		}); err != nil {
	// 			return nil, err
	// 		}
	// 	}

	// 	//Upload
	// 	stream, err := clientUpload.UploadVideo(ctx)

	// 	if err != nil {
	// 		return nil, err
	// 	}

	// 	file, err := command.videoPromotional.Open()

	// 	if err != nil {
	// 		return nil, err
	// 	}

	// 	buf := make([]byte, 1048576)
	// 	batchNumber := 1
	// 	for {
	// 		num, err := file.Read(buf)
	// 		if err == io.EOF {
	// 			break
	// 		}
	// 		if err != nil {
	// 			return nil, err
	// 		}
	// 		chunk := buf[:num]
	// 		if err := stream.Send(&grpcService.UploadVideoReq{
	// 			Title:     lecture.Title,
	// 			Data:      chunk,
	// 			Extension: command.videoPromotional.Header["Content-Type"][0],
	// 			Size:      int64(len(chunk)),
	// 		}); err != nil {
	// 			return nil, err
	// 		}
	// 		c.log.Info("Sent - batch #%v - size - %v\n", batchNumber, len(chunk))
	// 		batchNumber += 1

	// 	}

	// 	res, err := stream.CloseAndRecv()
	// 	if err != nil {
	// 		return nil, err
	// 	}
	// clientUpload := grpcService.NewUploadsServiceClient(connUpload)
	// stream, err := clientUpload.UploadVideo(ctx)

	// if err != nil {
	// 		return nil, err
	// 	}
	// }

	_, err = stripeProduct.Update(
		courseUpdate.ProductIdStripe,
		productStripeParams,
	)

	if err != nil {
		return nil, err
	}
	courseUpdate.ReviewStatus = constants.REVIEW_INIT_STATUS
	if err := c.courseRepository.UpdateCourse(ctx, courseUpdate); err != nil {
		return nil, err
	}
	c.log.Info("Update course success: ")
	
	return &dtos.UpdateCourseResponseDto{
		Message: "Update course successfully!",
		Course:  courseUpdate,
	}, nil
}
