package commands

import (
	"fmt"
	"mime/multipart"

	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type UpdateCourse struct {
	IdUser                 int
	WelcomeMessage         *string   `validate:"omitempty,gte=1,lte=500"`
	CongratulationsMessage *string   `validate:"omitempty,gte=1,lte=500"`
	Subtitle               *string   `validate:"omitempty,gte=1,lte=120"`
	PrimarilyTeach         *string   `validate:"omitempty,gte=1,lte=120"`
	Title                  string    `validate:"required,gte=1,lte=60"`
	Description            *string   `validate:"omitempty,gte=1,lte=2000"`
	LanguageID             *int      `validate:"omitempty,numeric"`
	PriceID                *int      `validate:"omitempty,numeric"`
	CategoryID             int       `validate:"required,numeric"`
	SubCategoryID          int       `validate:"required,numeric"`
	LevelID                *int      `validate:"omitempty,numeric"`
	CourseID               int       `validate:"required,numeric"`
	Outcomes               []string `validate:"omitempty,dive,max=160"`
	Requirements           []string `validate:"omitempty"`
	IntendedFor            []string `validate:"omitempty"`

	Image *multipart.FileHeader
	videoPromotional *multipart.FileHeader
}

func NewUpdateCourse(
	idUser int,
	welcomeMessage *string,
	congratulationsMessage *string,
	subtitle *string,
	primarilyTeach *string,
	title string,
	description *string,
	languageID *int,
	categoryID int,
	subCategoryID int,
	levelID *int,
	priceID *int,
	courseID int,
	image *multipart.FileHeader,
	outComes []string,
	requirements []string,
	intendedFor []string,
	videoPromotional *multipart.FileHeader,
) (*UpdateCourse, *validator.ValidateError) {
	command := &UpdateCourse{
		idUser, welcomeMessage, congratulationsMessage, subtitle, primarilyTeach, title,
		description, languageID, priceID, categoryID, subCategoryID, levelID, courseID,
		outComes, requirements, intendedFor, image, videoPromotional,
	}
	fmt.Println("Command: ", command.Title)
	fmt.Println("priceID: ", command.PriceID)
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	if image != nil {
		errValidate := checkImageUpload(image)

		if errValidate != nil && errValidate.ErrorType != nil {
			return nil, errValidate
		}
	}

	if videoPromotional != nil {
		errValidate := checkVideoUpload(videoPromotional)

		if errValidate != nil && errValidate.ErrorType != nil {
			return nil, errValidate
		}
	}

	return command, nil
}

func checkVideoUpload(video *multipart.FileHeader) *validator.ValidateError {
	allowFileType := []string{"video/mp4"}
	if video.Size > 1048576*100 {
		valdiateErr := validator.ValidateError{
			ErrorValue: []*validator.CustomValidationError{
				{
					Field:   "video",
					Message: customErrors.ErrVideoToMax.Error(),
				},
			},
			ErrorType: customErrors.ErrVideoToMax,
		}
		return &valdiateErr
	}

	isSupportType := false
	for _, v := range allowFileType {
		if v == video.Header["Content-Type"][0] {
			isSupportType = true
		}
	}
	if !isSupportType {
		valdiateErr := validator.ValidateError{
			ErrorValue: []*validator.CustomValidationError{
				{
					Field:   "video",
					Message: customErrors.ErrNotVideoType.Error(),
				},
			},
			ErrorType: customErrors.ErrNotVideoType,
		}
		return &valdiateErr
	}
	return nil
}

func checkImageUpload(image *multipart.FileHeader) *validator.ValidateError {
	allowFileType := []string{"image/png", "image/jpeg"}
	if image.Size > 1048576*10 {
		valdiateErr := validator.ValidateError{
			ErrorValue: []*validator.CustomValidationError{
				{
					Field:   "image",
					Message: customErrors.ErrVideoToMax.Error(),
				},
			},
			ErrorType: customErrors.ErrImageToMax,
		}
		return &valdiateErr
	}

	isSupportType := false
	for _, v := range allowFileType {
		if v == image.Header["Content-Type"][0] {
			isSupportType = true
		}
	}
	if !isSupportType {
		valdiateErr := validator.ValidateError{
			ErrorValue: []*validator.CustomValidationError{
				{
					Field:   "image",
					Message: customErrors.ErrNotImageType.Error(),
				},
			},
			ErrorType: customErrors.ErrNotImageType,
		}
		return &valdiateErr
	}
	return nil
}
