package dtos

//https://echo.labstack.com/guide/response/

type UpdateCourseRequestDto struct {
	CourseID               int       `json:"-" param:"id"`
	WelcomeMessage         *string   `form:"welcome_message"`
	CongratulationsMessage *string   `form:"congratulations_message"`
	Subtitle               *string   `form:"subtitle"`
	PrimarilyTeach         *string   `form:"primarily_teach"`
	Title                  string    `form:"title"`
	Description            *string   `form:"description"`
	Status                 *int      `form:"status"`
	LanguageID             *int      `form:"language_id"`
	CategoryID             int       `form:"category_id"`
	SubCategoryID          int       `form:"sub_category_id"`
	LevelID                *int      `form:"level_id"`
	PriceID                *int      `form:"price_id"`
	Outcomes               []string `form:"outcomes"`
	Requirements           []string `form:"requirements"`
	IntendedFor            []string `form:"intended_for"`
}
