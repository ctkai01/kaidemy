package queries

import (
	"fmt"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetStudentCurriculumsByCourseID struct {
	IdUser   int `validate:"required,numeric"`
	CourseID int `validate:"required,numeric"`
	// RoleUser int `validate:"numeric"`
}

func NewGetStudentCurriculumsByCourseID(
	idUser int,
	courseID int,
	// roleUser int,


) (*GetStudentCurriculumsByCourseID, *validator.ValidateError) {
	command := &GetStudentCurriculumsByCourseID{
		idUser,
		courseID,
		// roleUser,
	}
	errValidate := validator.Validate(command)
	fmt.Println("Err: ", errValidate)
	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
