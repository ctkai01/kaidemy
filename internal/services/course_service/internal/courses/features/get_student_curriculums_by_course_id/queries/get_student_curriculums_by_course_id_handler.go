package queries

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"fmt"
	"strings"

	// // "io"
	// "strings"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_student_curriculums_by_course_id/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	courseservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
)

type GetStudentCurriculumsByCourseIDHandler struct {
	log                  logger.Logger
	courseRepository     data.CourseRepository
	curriculumRepository data.CurriculumRepository
	learningRepository   data.LearningRepository

	grpcClient grpc.GrpcClient
}

func NewGetStudentCurriculumsByCourseIDHandler(
	log logger.Logger,
	courseRepository data.CourseRepository,
	curriculumRepository data.CurriculumRepository,
	learningRepository data.LearningRepository,
	grpcClient grpc.GrpcClient,

) *GetStudentCurriculumsByCourseIDHandler {
	return &GetStudentCurriculumsByCourseIDHandler{
		log:                  log,
		courseRepository:     courseRepository,
		learningRepository:   learningRepository,
		curriculumRepository: curriculumRepository,
		grpcClient:           grpcClient,
	}
}

func (c *GetStudentCurriculumsByCourseIDHandler) Handle(ctx context.Context, command *GetStudentCurriculumsByCourseID) (*dtos.GetStudentCurriculumsByCourseIDResponseDto, error) {
	course, err := c.courseRepository.GetCourseByIDRelation(ctx, []string{"Curriculums.Lectures.Assets", "Curriculums.Lectures.Questions.Answers", "Learnings"}, command.CourseID)

	if err != nil {
		return nil, err
	}
	// if course.UserID != command.IdUser && command.RoleUser != constants.ADMIN && command.RoleUser != constants.SUPPER_ADMIN {
	// 	return nil, customErrors.ErrorCannotPermission
	// }
	fmt.Println("Course learning: ", course.Learnings)
	var totalReviewCountStar int
	var totalReviewCount int
	var totalStudent int
	var learningUser *models.Learning
	total := 0
	for _, learning := range course.Learnings {
		if learning.StarCount != nil {
			totalReviewCountStar += *learning.StarCount
			totalReviewCount++
		}

		if learning.Type == constants.STANDARD_TYPE || learning.Type == constants.ARCHIE {
			totalStudent++
			if learning.UserID == command.IdUser && learning.CourseID == command.CourseID {
				copyLearning := learning // Create a copy of learning
				learningUser = &copyLearning
				fmt.Println("learningUser cho: ", learning)
			}
		}
	}

	// for _, learning := range course.Learnings {

	// 	if learning.Type == constants.STANDARD_TYPE || learning.Type == constants.ARCHIE {
	// 		if learning.UserID == command.IdUser && learning.CourseID == command.CourseID {
	// 			learningUser = &learning
	// 			fmt.Println("learningUser cho: ", learning)
	// 			break
	// 		}
	// 	}
	// }
	fmt.Println("learningUser: ", learningUser)
	fmt.Println("command.IdUser: ", command.IdUser)
	fmt.Println("total: ", total)
	if learningUser == nil {
		return nil, customErrors.ErrorCannotPermission
	}

	var averageReview float32

	if totalReviewCount != 0 {
		averageReview = float32(totalReviewCountStar) / float32(totalReviewCount)

	}
	//Get data catalog service
	conn, err := c.grpcClient.GetGrpcConnection("catalog_service")
	if err != nil {
		return nil, err
	}
	client := courseservice.NewCatalogsServiceClient(conn)

	dataCategory, err := client.GetCategoryByID(context.Background(), &courseservice.GetCategoryByIdReq{
		// UserId:     int32(command.IdUser),
		CategoryId: int32(course.CategoryID),
	})

	if err != nil {
		return nil, err
	}

	dataSubCategory, err := client.GetCategoryByID(context.Background(), &courseservice.GetCategoryByIdReq{
		// UserId:     int32(command.IdUser),
		CategoryId: int32(course.SubCategoryID),
	})

	if err != nil {
		return nil, err
	}

	fmt.Println("dataCategory: ", dataCategory.Category.ParentID)
	fmt.Println("dataSubCategory: ", dataSubCategory.Category.Name)

	var levelID int

	if course.LevelID == nil {
		levelID = -1
	} else {
		levelID = *course.LevelID
	}

	dataLevel, err := client.GetLevelByID(context.Background(), &courseservice.GetLevelByIdReq{
		// UserId:  int32(command.IdUser),
		LevelId: int32(levelID),
	})

	if err != nil {
		if !strings.Contains(err.Error(), "level not found") {
			return nil, err

		}
	}

	//Get Language

	var languageID int

	if course.LanguageID == nil {
		languageID = -1
	} else {
		languageID = *course.LanguageID
	}

	dataLanguage, err := client.GetLanguageByID(context.Background(), &courseservice.GetLanguageByIdReq{
		// UserId:     int32(command.IdUser),
		LanguageId: int32(languageID),
	})

	if err != nil {
		if !strings.Contains(err.Error(), "language not found") {
			return nil, err
		}
	}

	//Get price
	var priceID int

	if course.PriceID == nil {
		priceID = -1
	} else {
		priceID = *course.PriceID
	}

	dataPrice, err := client.GetPriceByID(context.Background(), &courseservice.GetPriceByIdReq{
		// UserId:  int32(command.IdUser),
		// UserId:  int32(command.IdUser),
		PriceId: int32(priceID),
	})

	if err != nil {
		if !strings.Contains(err.Error(), "price not found") {
			return nil, err
		}
	}

	//Get data user service

	connUser, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}
	clientUser := courseservice.NewUsersServiceClient(connUser)

	dataUser, err := clientUser.GetUserByID(context.Background(), &courseservice.GetUserByIdReq{
		// UserId:  int32(command.IdUser),
		UserId: int32(course.UserID),
	})
	if err != nil {
		return nil, err
	}
	// updatedAtLevel := dataLevel.Level.UpdatedAt.AsTime()
	// createAtLevel := dataLevel.Level.CreatedAt.AsTime()

	// updatedAtCategory := dataCategory.Category.UpdatedAt.AsTime()
	// createAtCategory := dataCategory.Category.CreatedAt.AsTime()

	// updatedAtSubCategory := dataSubCategory.Category.UpdatedAt.AsTime()
	// createAtSubCategory := dataSubCategory.Category.CreatedAt.AsTime()

	var parentIDSubCategory *int

	if dataSubCategory.Category.ParentID == 0 {
		parentIDSubCategory = nil
	} else {
		parentId := int(dataSubCategory.Category.ParentID)
		parentIDSubCategory = &parentId
	}
	var levelRes *models.LevelResponse

	if dataLevel == nil {
		levelRes = nil
	} else {
		levelRes = &models.LevelResponse{
			ID:   int(dataLevel.Level.ID),
			Name: dataLevel.Level.Name,
		}
	}

	//
	var categoryRes *models.CategoryResponse

	if dataCategory == nil {
		categoryRes = nil
	} else {
		categoryRes = &models.CategoryResponse{
			ID:       int(dataCategory.Category.ID),
			Name:     dataCategory.Category.Name,
			ParentID: nil,
		}
	}

	//
	var subCategoryRes *models.CategoryResponse

	if dataSubCategory == nil {
		subCategoryRes = nil
	} else {
		subCategoryRes = &models.CategoryResponse{
			ID:       int(dataSubCategory.Category.ID),
			Name:     dataSubCategory.Category.Name,
			ParentID: parentIDSubCategory,
		}
	}

	//
	var languageRes *models.LanguageResponse

	if dataLanguage == nil {
		languageRes = nil
	} else {
		languageRes = &models.LanguageResponse{
			ID:   int(dataLanguage.Language.ID),
			Name: dataLanguage.Language.Name,
		}
	}

	//
	var priceRes *models.PriceResponse

	if dataPrice == nil {
		priceRes = nil
	} else {
		priceRes = &models.PriceResponse{
			ID:    int(dataPrice.Price.ID),
			Tier:  dataPrice.Price.Tier,
			Value: int(dataPrice.Price.Value),
		}
	}

	var curriculumShowDetail []*models.CurriculumShowDetail
	for _, curriculum := range course.Curriculums {
		var curriculumShowDetailItem = &models.CurriculumShowDetail{
			ID:          curriculum.ID,
			Title:       curriculum.Title,
			Description: curriculum.Description,
			CourseID:    curriculum.CourseID,
			Course:      curriculum.Course,

			UpdatedAt: curriculum.UpdatedAt,
			CreatedAt: curriculum.CreatedAt,
		}

		var lectures []*models.LectureShowDetail
		for _, lecture := range curriculum.Lectures {
			count, err := c.learningRepository.CountAssociationLectures(ctx, learningUser, &lecture)

			if err != nil {
				return nil, err
			}
			isDone := false
			if count == 1 {
				// markCount++
				isDone = true
			}

			lectures = append(lectures, &models.LectureShowDetail{
				ID:            lecture.ID,
				Title:         lecture.Title,
				Article:       lecture.Article,
				Description:   lecture.Description,
				Assets:        lecture.Assets,
				Questions:     lecture.Questions,
				IsPromotional: lecture.IsPromotional,
				CurriculumID:  lecture.CurriculumID,
				Type:          lecture.Type,
				Order:         lecture.Order,
				IsDone:        isDone,
				Learnings:     lecture.Learnings,
				UpdatedAt:     lecture.UpdatedAt,
				CreatedAt:     lecture.CreatedAt,
			})
		}
		curriculumShowDetailItem.Lectures = lectures

		curriculumShowDetail = append(curriculumShowDetail, curriculumShowDetailItem)
	}

	courseResponse := &models.CourseResponseDetail{
		ID:                     course.ID,
		OutComes:               course.OutComes,
		IntendedFor:            course.IntendedFor,
		Requirement:            course.Requirement,
		ProductIdStripe:        course.ProductIdStripe,
		Level:                  levelRes,
		Category:               categoryRes,
		SubCategory:            subCategoryRes,
		Title:                  course.Title,
		WelcomeMessage:         course.WelcomeMessage,
		CongratulationsMessage: course.WelcomeMessage,
		Subtitle:               course.Subtitle,
		PrimarilyTeach:         course.PrimarilyTeach,
		Description:            course.Description,
		Status:                 course.Status,
		Language:               languageRes,
		Price:                  priceRes,
		ReviewStatus:           course.ReviewStatus,
		User: models.UserResponse{
			ID:   int(dataUser.User.ID),
			Name: dataUser.User.Name,
		},
		PromotionalVideo: course.PromotionalVideo,
		Image:            course.Image,
		Curriculums:      curriculumShowDetail,
		AverageReview:    averageReview,
		CountReview:      totalReviewCount,
		CountStudent:     totalStudent,
		UpdatedAt:        course.UpdatedAt,
		CreatedAt:        course.CreatedAt,
	}

	// if err != nil {
	// 	return nil, err
	// }
	// fmt.Println("dataPrice: ", dataPrice.Price.Value)
	// conn.
	// c.grpcClient.GetGrpcConnection((""))
	// if topicLearning.UserID != command.IdUser {
	// 	return nil, customErrors.ErrTopicLearningNotBelongToUser
	// }

	// topicLearning.Title = command.Title

	// if command.Description != nil {
	// 	topicLearning.Description = command.Description
	// }

	// if err := c.topicLearningRepository.UpdateTopicLearning(ctx, topicLearning); err != nil {
	// 	return nil, err
	// }

	return &dtos.GetStudentCurriculumsByCourseIDResponseDto{
		Message: "Get course successfully!",
		Course:  *courseResponse,
		// TopicLearning: topicLearning,
	}, nil
}
