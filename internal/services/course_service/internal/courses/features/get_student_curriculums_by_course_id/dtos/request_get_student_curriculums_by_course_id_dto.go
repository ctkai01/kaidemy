package dtos

//https://echo.labstack.com/guide/response/

type GetStudentCurriculumsByCourseIDRequestDto struct {
	CourseID int  `param:"id"`
}
