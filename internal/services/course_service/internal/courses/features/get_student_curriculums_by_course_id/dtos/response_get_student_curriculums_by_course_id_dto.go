package dtos

import "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

//https://echo.labstack.com/guide/response/

type GetStudentCurriculumsByCourseIDResponseDto struct {
	Message string        `json:"message"`
	Course  models.CourseResponseDetail `json:"course"`
	// TopicLearning *models.TopicLearning `json:"topic_learning"`
}
