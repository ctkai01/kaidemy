package dtos

//https://echo.labstack.com/guide/response/

type CreateAnswerRequestDto struct {
	UserID     int
	QuestionID int     `form:"question_id"`
	AnswerText string  `form:"answer_text"`
	IsCorrect  bool    `form:"is_correct"`
	Explain    *string `form:"explain"`
}
