package dtos

import "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

//https://echo.labstack.com/guide/response/

type CreateAnswerResponseDto struct {
	Message string        `json:"message"`
	Answer  models.Answer `json:"answer"`
}
