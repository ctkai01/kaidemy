package commands

import (
	"fmt"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type CreateAnswer struct {
	IdUser     int
	AnswerText string `validate:"required,gte=1,lte=200"`
	QuestionID int    `validate:"required,numeric"`
	IsCorrect  bool
	Explain    *string `validate:"omitempty,gte=1,lte=500"`
}

func NewCreateAnswer(
	idUser int,
	answerText string,
	questionID int,
	isCorrect bool,
	explain *string,
) (*CreateAnswer, *validator.ValidateError) {
	command := &CreateAnswer{
		idUser,
		answerText,
		questionID,
		isCorrect,
		explain,
	}
	errValidate := validator.Validate(command)
	fmt.Println("Err: ", errValidate)
	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
