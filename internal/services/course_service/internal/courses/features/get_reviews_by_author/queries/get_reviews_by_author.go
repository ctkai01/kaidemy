package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetReviewsByAuthor struct {
	IdUser int
	Reply []string
	Sort *string
	Rating []string
	CourseID *string 
	
	*utils.ListQuery
}

func NewGetReviewsByAuthor(
	idUser int,
	reply []string,
	sort *string,
	rating []string,
	courseID *string,
	
	query *utils.ListQuery,

) (*GetReviewsByAuthor, *validator.ValidateError) {
	q := &GetReviewsByAuthor{
		idUser,
		reply,
		sort,
		rating,
		courseID,
		query,
	}
	
	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
