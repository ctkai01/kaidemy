package queries

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"fmt"
	"strconv"

	// "fmt"
	// "strings"

	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"

	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_reviews_by_author/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	courseservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type GetReviewsByAuthorHandler struct {
	log                logger.Logger
	courseRepository   data.CourseRepository
	learningRepository data.LearningRepository
	grpcClient         grpc.GrpcClient
}

func NewGetReviewsByAuthorHandler(
	log logger.Logger,
	courseRepository data.CourseRepository,
	learningRepository data.LearningRepository,
	grpcClient grpc.GrpcClient,

) *GetReviewsByAuthorHandler {
	return &GetReviewsByAuthorHandler{
		log:                log,
		courseRepository:   courseRepository,
		grpcClient:         grpcClient,
		learningRepository: learningRepository,
	}
}

func (c *GetReviewsByAuthorHandler) Handle(ctx context.Context, command *GetReviewsByAuthor) (*dtos.GetReviewByAuthorResponseDto, error) {

	courseIDsByAuthor, err := c.courseRepository.FindIdsCourseAuthor(ctx, command.IdUser)

	if err != nil {
		return nil, err
	}

	var courseIDFilter []int
	if command.CourseID != nil {
		n, _ := strconv.Atoi(*command.CourseID)
		isExist := false
		for _, id := range courseIDsByAuthor {
			if id == n {
				isExist = true
				break
			}
		}

		if !isExist {
			return nil, customErrors.ErrorCannotPermission
		}

		courseIDFilter = append(courseIDFilter, n)
	} else {
		courseIDFilter = courseIDsByAuthor
	}
	fmt.Println("courseIDFilter: ", courseIDFilter)
	learnings, err := c.learningRepository.GetReviewsByCourseIDsPaginate(ctx, courseIDFilter, command.Sort, command.Reply, command.Rating, command.ListQuery)
	if err != nil {
		return nil, err
	}

	connUser, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}
	clientUser := courseservice.NewUsersServiceClient(connUser)
	var reviewsUser []*dtos.ReviewUser
	// fmt.Println("courses: ", len(courses.Items))
	for _, learning := range learnings.Items {

		//Get data user service
		dataUser, err := clientUser.GetUserByID(context.Background(), &courseservice.GetUserByIdReq{
			UserId: int32(learning.UserID),
		})
		if err != nil {
			return nil, err
		}

		// var averageReview float32
		// var totalReviewCount int

		// // var learningIDSameCourse []*models.Learning

		// // for _, item := range learnings.Items
		// count := 0
		// for i := index; i < len(learnings.Items); i++ {
		// 	if learnings.Items[i].CourseID == learning.CourseID {
		// 		totalReviewCount += *(learnings.Items[i]).StarCount
		// 		count++
		// 	}
		// }

		// averageReview = float32(totalReviewCount) / float32(count)

		reviewUser := &dtos.ReviewUser{
			Course: &dtos.CourseReviewUser{
				ID:    learning.Course.ID,
				Image: learning.Course.Image,
				Title: learning.Course.Title,
			},
			User: &dtos.UserReviewUser{
				ID:     int(dataUser.User.ID),
				Avatar: &dataUser.User.Avatar,
				Name:   dataUser.User.Name,
			},
			Comment:        learning.Comment,
			StarCount:      learning.StarCount,
			// AverageCurrent: averageReview,
			UpdatedAt:      learning.UpdatedAt,
			CreatedAt:      learning.CreatedAt,
		}

		reviewsUser = append(reviewsUser, reviewUser)
	}
	fmt.Println("learnings: ", learnings)
	result := &utils.ListResult[*dtos.ReviewUser]{
		Size:       learnings.Size,
		Page:       learnings.Page,
		TotalItems: learnings.TotalItems,
		TotalPage:  learnings.TotalPage,
		Items:      reviewsUser,
	}

	return &dtos.GetReviewByAuthorResponseDto{
		Message: "Get reviews successfully!",
		Result:  result,
	}, nil

}
