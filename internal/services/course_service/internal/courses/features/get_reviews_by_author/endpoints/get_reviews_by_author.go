package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/params"
	coursesError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_reviews_by_author/queries"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_reviews_by_author/dtos"
)

type getReviewsByAuthorEndpoint struct {
	params.CoursesRouteParams
}

func NewGetReviewsByAuthorEndpoint(
	params params.CoursesRouteParams,
) route.Endpoint {
	return &getReviewsByAuthorEndpoint{
		CoursesRouteParams: params,
	}
}

func (ep *getReviewsByAuthorEndpoint) MapEndpoint() {
	ep.CoursesGroup.GET("/reviews/author", ep.handler())
}

func (ep *getReviewsByAuthorEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		listQuery, err := utils.GetListQueryFromCtx(c)
		fmt.Println("Query: ", listQuery)
		
		if err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[getReviewsByAuthor.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getReviewsByAuthorEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())
		}
		
		request := &dtos.GetReviewByAuthorRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[getReviewsByAuthor.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getReviewsByAuthorEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}
		// fmt.Println("request level: ", request.Level)
		// fmt.Println("request duration: ", request.Duration)
		// fmt.Println("request rating: ", request.Rating)
		userId := utils.GetUserId(c)
		query, errValidate := queries.NewGetReviewsByAuthor(
			userId,
			request.Reply,
			request.Sort,
			request.Rating,
			request.CourseID,
			listQuery,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[getReviewsByAuthor_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getReviewsByAuthor_handler.StructCtx] err: {%v}", validationErr),
			)

			return customErrors.NewBadRequestErrorHttp(c, validationErr.Error())

		}

		result, err := mediatr.Send[*queries.GetReviewsByAuthor, *dtos.GetReviewByAuthorResponseDto](
			ctx,
			query,
		)

		if err != nil {
			if err.Error() == coursesError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[getReviewsByAuthor_handler.Send] error",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[getReviewsByAuthor.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			if err.Error() == coursesError.WrapError(customErrors.ErrorCannotPermission).Error() {
				errCustom := errors.WithMessage(
					err,
					"[getReviewsByAuthor_handler.Send] error",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[getReviewsByAuthor.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewForbiddenErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[getReviewsByAuthorEndpoint_handler.Send] error",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[getReviewsByAuthor.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
