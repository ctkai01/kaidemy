package dtos

//https://echo.labstack.com/guide/response/

type GetReviewByAuthorRequestDto struct {
	CourseID *string  `query:"course_id"`
	Sort     *string  `query:"sort"`
	Reply    []string `query:"reply"`
	Rating   []string `query:"rating"`
}
