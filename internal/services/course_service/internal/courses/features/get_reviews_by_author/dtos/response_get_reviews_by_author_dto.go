package dtos

// import "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"
import (
	"time"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
)

type ReviewUser struct {
	Course         *CourseReviewUser `json:"course"`
	User           *UserReviewUser   `json:"user"`
	Comment        *string           `json:"comment"`
	StarCount      *int              `json:"star_count"`
	// AverageCurrent float32           `json:"average_current"`
	UpdatedAt      *time.Time        `json:"updated_at"`
	CreatedAt      *time.Time        `json:"created_at"`
}

type CourseReviewUser struct {
	ID    int     `json:"id"`
	Image *string `json:"image"`
	Title string  `json:"title"`
}

type UserReviewUser struct {
	ID     int     `json:"id"`
	Avatar *string `json:"avatar"`
	Name   string  `json:"name"`
}

type GetReviewByAuthorResponseDto struct {
	Message string `json:"message"`

	Result *utils.ListResult[*ReviewUser] `json:"result"`
}
