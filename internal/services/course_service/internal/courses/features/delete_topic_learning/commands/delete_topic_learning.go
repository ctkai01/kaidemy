package commands

import (
	"fmt"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type DeleteTopicLearning struct {
	IdUser          int
	TopicLearningID int     `validate:"required,numeric"`
}

func NewDeleteTopicLearning(
	idUser int,
	topicLearningID int,
) (*DeleteTopicLearning, *validator.ValidateError) {
	command := &DeleteTopicLearning{
		idUser,
		topicLearningID,
	}
	errValidate := validator.Validate(command)
	fmt.Println("Err: ", errValidate)
	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
