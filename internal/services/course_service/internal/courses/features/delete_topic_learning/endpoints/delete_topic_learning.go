package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/params"
	coursesError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_topic_learning/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_topic_learning/dtos"
)

type deleteTopicLearningEndpoint struct {
	params.TopicLearningsRouteParams
}

func NewDeleteTopicLearningEndpoint(
	params params.TopicLearningsRouteParams,
) route.Endpoint {
	return &deleteTopicLearningEndpoint{
		TopicLearningsRouteParams: params,
	}
}

func (ep *deleteTopicLearningEndpoint) MapEndpoint() {
	ep.TopicLearningsGroup.DELETE("/:id", ep.handler())
}

func (ep *deleteTopicLearningEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.DeleteTopicLearningRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[DeleteTopicLearning.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[DeleteTopicLearningEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())
		}

		userId := utils.GetUserId(c)

		command, errValidate := commands.NewDeleteTopicLearning(
			userId,
			request.TopicLearningID,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[DeleteTopicLearning_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[DeleteTopicLearning_handler.StructCtx] err: {%v}", validationErr),
			)

			return customErrors.NewBadRequestErrorHttp(c, validationErr.Error())

		}

		result, err := mediatr.Send[*commands.DeleteTopicLearning, *dtos.DeleteTopicLearningResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == coursesError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"DeleteTopicLearningEndpoint_handler.Send] error in sending add course to topic learning",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"DeleteLearningTopicLearning.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			// if err.Error() == coursesError.WrapError(customErrors.ErrorCannotPermission).Error() {
			// 	errCustom := errors.WithMessage(
			// 		err,
			// 		"[CreateTopicLeaningEndpoint_handler.Send] error in sending create Answer",
			// 	)
			// 	ep.Logger.Error(
			// 		fmt.Sprintf(
			// 			"[CreateTopicLeaning.Send], err: %v",
			// 			errCustom,
			// 		),
			// 	)
			// 	return customErrors.NewForbiddenErrorHttp(c, err.Error())
			// }

			err = errors.WithMessage(
				err,
				"DeleteLearningTopicLearningEndpoint_handler.Send] error in sending Create Topic Leaning",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"DeleteLearningTopicLearning.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
