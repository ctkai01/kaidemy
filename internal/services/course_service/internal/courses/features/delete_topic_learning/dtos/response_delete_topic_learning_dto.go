package dtos


//https://echo.labstack.com/guide/response/

type DeleteTopicLearningResponseDto struct {
	Message       string                `json:"message"`
}
