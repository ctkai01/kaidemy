package dtos

//https://echo.labstack.com/guide/response/

type DeleteTopicLearningRequestDto struct {
	TopicLearningID int  `param:"id"`
}
