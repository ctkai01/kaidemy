package dtos

//https://echo.labstack.com/guide/response/

type CreateLectureRequestDto struct {
	UserID       int    
	CurriculumID int    `form:"curriculum_id"`
	Title        string `form:"title"`
}
