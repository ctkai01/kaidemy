package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type CreateLecture struct {
	IdUser       int
	Title        string `validate:"required,gte=1,lte=80"`
	CurriculumID int    `validate:"required,numeric"`
}

func NewCreateLecture(idUser int, title string, curriculumID int) (*CreateLecture, *validator.ValidateError) {
	command := &CreateLecture{idUser, title, curriculumID}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
