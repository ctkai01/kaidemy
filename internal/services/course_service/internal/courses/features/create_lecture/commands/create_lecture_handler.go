package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_lecture/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type CreateLectureHandler struct {
	log                  logger.Logger
	curriculumRepository data.CurriculumRepository
	lectureRepository    data.LectureRepository
	courseRepository    data.CourseRepository
	grpcClient           grpc.GrpcClient
}

func NewCreateLectureHandler(
	log logger.Logger,
	curriculumRepository data.CurriculumRepository,
	lectureRepository data.LectureRepository,
	courseRepository    data.CourseRepository,
	grpcClient grpc.GrpcClient,

) *CreateLectureHandler {
	return &CreateLectureHandler{log: log, curriculumRepository: curriculumRepository, lectureRepository: lectureRepository, grpcClient: grpcClient, courseRepository: courseRepository}
}

func (c *CreateLectureHandler) Handle(ctx context.Context, command *CreateLecture) (*dtos.CreateLectureResponseDto, error) {
	curriculum, err := c.curriculumRepository.GetCurriculumByIDRelation(ctx, []string{"Course"}, command.CurriculumID)

	if err != nil {
		return nil, err
	}

	if curriculum == nil {
		return nil, customErrors.ErrCurriculumNotFound
	}

	if curriculum.Course.UserID != command.IdUser {
		return nil, customErrors.ErrorCannotPermission
	}

	lectureCreate := &models.Lecture{
		Title:        command.Title,
		CurriculumID: command.CurriculumID,
		Type:         constants.LECTURE_TYPE,
	}
	if err := c.lectureRepository.CreateLecture(ctx, lectureCreate); err != nil {
		return nil, err
	}
	c.log.Info("Create lecture success: ")

	course := &curriculum.Course
	course.ReviewStatus = constants.REVIEW_INIT_STATUS
	if err := c.courseRepository.UpdateCourse(ctx, course); err != nil {
		return nil, err
	}

	return &dtos.CreateLectureResponseDto{
		Message: "Create lecture successfully!",
		Lecture: *lectureCreate,
	}, nil
}
