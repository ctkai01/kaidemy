package queries

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// "fmt"
	// "math"

	// "fmt"
	// "strings"

	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_reviews_by_course_id/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	courseservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
)

type GetReviewsByCourseIDHandler struct {
	log                logger.Logger
	learningRepository data.LearningRepository
	courseRepository   data.CourseRepository
	grpcClient         grpc.GrpcClient
}

func NewGetReviewsByCourseIDHandler(
	log logger.Logger,
	learningRepository data.LearningRepository,
	courseRepository data.CourseRepository,
	grpcClient grpc.GrpcClient,

) *GetReviewsByCourseIDHandler {
	return &GetReviewsByCourseIDHandler{
		log:                log,
		learningRepository: learningRepository,
		courseRepository:   courseRepository,
		grpcClient:         grpcClient,
	}
}

func (c *GetReviewsByCourseIDHandler) Handle(ctx context.Context, command *GetReviewsByCourseID) (*dtos.GetReviewsByCourseIDResponseDto, error) {

	_, err := c.courseRepository.GetCourseByID(ctx, command.IdCourse)
	if err != nil {
		return nil, err
	}

	learnings, err := c.learningRepository.GetReviewsCourseIDPaginate(ctx, command.IdCourse, command.ListQuery)

	if err != nil {
		return nil, err
	}

	var learningReviews = make([]*models.LearningReview, 0)
	connUser, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}
	clientUser := courseservice.NewUsersServiceClient(connUser)
	// var totalReviewCount int

	// var fiveStarCount int
	// var fourStarCount int
	// var threeStarCount int
	// var twoStarCount int
	// var oneStarCount int

	if learnings.Items != nil {
		for _, learning := range learnings.Items {
			dataUser, err := clientUser.GetUserByID(context.Background(), &courseservice.GetUserByIdReq{
				UserId: int32(learning.UserID),
			})
			if err != nil {
				return nil, err
			}

			// if learning.StarCount != nil {
			// 	totalReviewCount += *learning.StarCount

			// 	if *learning.StarCount == 5 {
			// 		fiveStarCount++
			// 	} else if *learning.StarCount == 4 {
			// 		fourStarCount++

			// 	} else if *learning.StarCount == 3 {
			// 		threeStarCount++

			// 	} else if *learning.StarCount == 2 {
			// 		twoStarCount++

			// 	} else {
			// 		oneStarCount++
			// 	}
			// }

			learningReviews = append(learningReviews, &models.LearningReview{
				ID:        learning.ID,
				Type:      learning.Type,
				StarCount: learning.StarCount,
				Comment:   learning.Comment,
				User: &models.UserReview{
					ID:     int(dataUser.User.ID),
					Name:   dataUser.User.Name,
					Avatar: dataUser.User.Avatar,
				},
				UpdatedAt: learning.UpdatedAt,
				CreatedAt: learning.UpdatedAt,
			})
		}
	}

	// fiveStarCount
	// fourStarCount
	// threeStarCount
	// twoReviewCount
	// oneReviewCount

	learningResult := &utils.ListResult[*models.LearningReview]{
		Size:       learnings.Size,
		Page:       learnings.Page,
		TotalItems: learnings.TotalItems,
		TotalPage:  learnings.TotalPage,
		Items:      learningReviews,
	}

	// var averageReview float32

	// if learnings.TotalItems != 0 {
	// 	averageReview = float32(totalReviewCount) / float32(learnings.TotalItems)
	// 	fiveStarCount = int(math.Round((float64(fiveStarCount) / float64(learnings.TotalItems)) * 100))
	// 	fourStarCount = int(math.Round((float64(fourStarCount) / float64(learnings.TotalItems)) * 100))
	// 	threeStarCount = int(math.Round((float64(threeStarCount) / float64(learnings.TotalItems)) * 100))
	// 	twoStarCount = int(math.Round((float64(twoStarCount) / float64(learnings.TotalItems)) * 100))
	// 	oneStarCount = int(math.Round((float64(oneStarCount) / float64(learnings.TotalItems)) * 100))
	// }

	// result := &dtos.OverallReviewsByCourseID{
	// 	Learnings:     learningResult,
	// 	AverageReview: averageReview,
	// 	TotalReview:   int(learnings.TotalItems),
	// 	Overall: &dtos.OverallStar{
	// 		FiveStar:  fiveStarCount,
	// 		FourStar:  fourStarCount,
	// 		ThreeStar: threeStarCount,
	// 		TwoStar:   twoStarCount,
	// 		OneStar:   oneStarCount,
	// 	},
	// }
	// fmt.Println("HHh: ", result)
	return &dtos.GetReviewsByCourseIDResponseDto{
		Message: "Get learnings successfully!",
		Learnings: learningResult,
	}, nil
}
