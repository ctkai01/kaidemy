package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetReviewsByCourseID struct {
	IdCourse int
	*utils.ListQuery
}

func NewGetReviewsByCourseID(
	idCourse int,
	query *utils.ListQuery,

) (*GetReviewsByCourseID, *validator.ValidateError) {
	q := &GetReviewsByCourseID{
		idCourse,
		query,
	}
	
	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
