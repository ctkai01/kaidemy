package dtos

//https://echo.labstack.com/guide/response/

type GetReviewsByCourseIDRequestDto struct {
	ID int `param:"id"`
}
