package dtos

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"
)

// type OverallReviewsByCourseID struct {
// 	Learnings     *utils.ListResult[*models.LearningReview] `json:"learnings"`
// 	AverageReview float32                                   `json:"average_review"`
// 	TotalReview   int                                       `json:"total_review"`
// 	Overall       *OverallStar                              `json:"overall"`
// }

// type OverallStar struct {
// 	FiveStar  int `json:"five_star"`
// 	FourStar  int `json:"four_star"`
// 	ThreeStar int `json:"three_star"`
// 	TwoStar   int `json:"two_star"`
// 	OneStar   int `json:"one_star"`
// }

type GetReviewsByCourseIDResponseDto struct {
	Message   string                                    `json:"message"`
	Learnings *utils.ListResult[*models.LearningReview] `json:"learnings"`
}
