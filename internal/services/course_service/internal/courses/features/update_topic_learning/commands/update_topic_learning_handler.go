package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_topic_learning/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type UpdateTopicLearningHandler struct {
	log                  logger.Logger
	topicLearningRepository data.TopicLearningRepository
	
	grpcClient           grpc.GrpcClient
}

func NewUpdateTopicLearningHandler(
	log logger.Logger,
	topicLearningRepository data.TopicLearningRepository,
	grpcClient grpc.GrpcClient,

) *UpdateTopicLearningHandler {
	return &UpdateTopicLearningHandler{
		log:                  log,
		topicLearningRepository: topicLearningRepository,
		grpcClient:           grpcClient,
	}
}

func (c *UpdateTopicLearningHandler) Handle(ctx context.Context, command *UpdateTopicLeaning) (*dtos.UpdateTopicLearningResponseDto, error) {
	topicLearning, err := c.topicLearningRepository.GetTopicLearningByID(ctx, command.TopicLearningID)
	
	if err != nil {
		return nil, err
	}

	if topicLearning.UserID != command.IdUser {
		return nil, customErrors.ErrTopicLearningNotBelongToUser
	}

	topicLearning.Title = command.Title

	if command.Description != nil {
		topicLearning.Description = command.Description
	}

	if err := c.topicLearningRepository.UpdateTopicLearning(ctx, topicLearning); err != nil {
		return nil, err
	}

	return &dtos.UpdateTopicLearningResponseDto{
		Message: "update topic learning successfully!",
		TopicLearning: topicLearning,
	}, nil
}
