package commands

import (
	"fmt"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type UpdateTopicLeaning struct {
	IdUser          int
	TopicLearningID int     `validate:"required,numeric"`
	Title           string  `validate:"required,gte=1,lte=100"`
	Description     *string `validate:"omitempty,gte=1,lte=200"`
}

func NewUpdateTopicLearning(
	idUser int,
	topicLearningID int,
	title string,
	description *string,

) (*UpdateTopicLeaning, *validator.ValidateError) {
	command := &UpdateTopicLeaning{
		idUser,
		topicLearningID,
		title,
		description,
	}
	errValidate := validator.Validate(command)
	fmt.Println("Err: ", errValidate)
	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
