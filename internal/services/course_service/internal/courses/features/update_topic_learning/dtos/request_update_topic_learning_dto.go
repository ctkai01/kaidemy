package dtos

//https://echo.labstack.com/guide/response/

type UpdateTopicLearningRequestDto struct {
	TopicLearningID int  `param:"id"`
	Title           string  `form:"title"`
	Description     *string `form:"description"`
}
