package dtos

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"
)

type CourseAuthorReview struct{
	ID int `json:"id"`
	Title string `json:"title"`

}

type GetCoursesByAuthorResponseDto struct {
	Message string                            `json:"message"`
	Courses *utils.ListResult[*CourseAuthorReview] `json:"courses"`
}
