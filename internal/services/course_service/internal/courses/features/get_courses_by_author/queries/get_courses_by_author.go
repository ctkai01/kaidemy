package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetCoursesByAuthor struct {
	IdUser int
	*utils.ListQuery
}

func NewGetCoursesByAuthor(
	idUser int,
	query *utils.ListQuery,

) (*GetCoursesByAuthor, *validator.ValidateError) {
	q := &GetCoursesByAuthor{
		idUser,
		query,
	}
	
	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
