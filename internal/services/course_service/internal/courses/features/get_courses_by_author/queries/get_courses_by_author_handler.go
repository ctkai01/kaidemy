package queries

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"fmt"
	// "strings"

	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"

	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_courses_by_author/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	// courseservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type GetCoursesByUserIDHandler struct {
	log              logger.Logger
	courseRepository data.CourseRepository
	grpcClient       grpc.GrpcClient
}

func NewGetCoursesByUserIDHandler(
	log logger.Logger,
	courseRepository data.CourseRepository,
	grpcClient grpc.GrpcClient,

) *GetCoursesByUserIDHandler {
	return &GetCoursesByUserIDHandler{
		log:              log,
		courseRepository: courseRepository,
		grpcClient:       grpcClient,
	}
}

func (c *GetCoursesByUserIDHandler) Handle(ctx context.Context, command *GetCoursesByAuthor) (*dtos.GetCoursesByAuthorResponseDto, error) {
	// if command.IdUser != command.IdUserCall {
	// 	return nil, customErrors.ErrorCannotPermission
	// }
	courses, err := c.courseRepository.GetCoursesAuthor(ctx, command.IdUser, command.ListQuery)
	
	if err != nil {
		return nil, err
	}

	var courseReview []*dtos.CourseAuthorReview

	for _, course := range courses.Items {
		courseReview = append(courseReview, &dtos.CourseAuthorReview{
			ID: course.ID,
			Title: course.Title,
		})
	}
	fmt.Println("courses.Page: ", courses.Page)
	result := &utils.ListResult[*dtos.CourseAuthorReview]{
		Size:       courses.Size,
		Page:       courses.Page,
		TotalItems: courses.TotalItems,
		TotalPage:  courses.TotalPage,
		Items:      courseReview,
	}
	// CourseResponse
	return &dtos.GetCoursesByAuthorResponseDto{
		Message: "Get courses successfully!",
		Courses: result,
	}, nil
}
