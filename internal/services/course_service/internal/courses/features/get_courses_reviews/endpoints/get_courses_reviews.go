package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/params"
	coursesError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_courses_reviews/queries"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_courses_reviews/dtos"
)

type getCoursesReviewsEndpoint struct {
	params.CoursesNoAuthRouteParams
}

func NewGetCourseReviewsEndpoint(
	params params.CoursesNoAuthRouteParams,
) route.Endpoint {
	return &getCoursesReviewsEndpoint{
		CoursesNoAuthRouteParams: params,
	}
}

func (ep *getCoursesReviewsEndpoint) MapEndpoint() {
	ep.CoursesGroup.GET("/reviews", ep.handler())
}

func (ep *getCoursesReviewsEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		listQuery, err := utils.GetListQueryFromCtx(c)
		fmt.Println("Query: ", listQuery)
		
		if err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[getCoursesReviews.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getCoursesReviewsEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())
		}
		
		// request := &dtos.GetCoursesReviewsRequestDto{}
		// if err := c.Bind(request); err != nil {
		// 	badRequestErr := customErrors.NewBadRequestErrorWrap(
		// 		err,
		// 		"[getCoursesReviews.Bind] error in the binding request",
		// 	)
		// 	ep.Logger.Errorf(
		// 		fmt.Sprintf("[getCoursesReviewsEndpoint_handler.Bind] err: %v", badRequestErr),
		// 	)
		// 	return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		// }

		// userId := utils.GetUserId(c)

		query, errValidate := queries.NewGetCoursesReviews(
			listQuery,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"getCoursesReviews_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("getCoursesReviews_handler.StructCtx] err: {%v}", validationErr),
			)

			return customErrors.NewBadRequestErrorHttp(c, validationErr.Error())

		}

		result, err := mediatr.Send[*queries.GetCoursesReviews, *dtos.GetCoursesReviewsResponseDto](
			ctx,
			query,
		)

		if err != nil {
			if err.Error() == coursesError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"getCoursesReviews_handler.Send] error in sending create Answer",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"getCoursesReviews.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"getCoursesReviewsEndpoint_handler.Send] error in sending get topic learnings",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"getCoursesReviews.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
