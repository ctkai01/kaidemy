package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetCoursesReviews struct {
	*utils.ListQuery
}

func NewGetCoursesReviews(
	query *utils.ListQuery,

) (*GetCoursesReviews, *validator.ValidateError) {
	q := &GetCoursesReviews{
		query,
	}
	
	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
