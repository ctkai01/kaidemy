package queries

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"fmt"

	// // "io"
	// "strings"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_courses_reviews/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type GetCoursesReviewsHandler struct {
	log              logger.Logger
	courseRepository data.CourseRepository
	grpcClient       grpc.GrpcClient
}

func NewGetCoursesReviewsHandler(
	log logger.Logger,
	courseRepository data.CourseRepository,
	grpcClient grpc.GrpcClient,

) *GetCoursesReviewsHandler {
	return &GetCoursesReviewsHandler{
		log:              log,
		courseRepository: courseRepository,
		grpcClient:       grpcClient,
	}
}

func (c *GetCoursesReviewsHandler) Handle(ctx context.Context, command *GetCoursesReviews) (*dtos.GetCoursesReviewsResponseDto, error) {
	// if command.Content == 1 {
	// 	if command.IdUser != command.IdUserCall {
	// 		return nil, customErrors.ErrorCannotPermission
	// 	}
		
	// } else {
	// 	strUserID := fmt.Sprintf("%d", command.IdUser)
	// 	command.Filters = append(command.Filters, &utils.FilterModel{
	// 		Field: "user_id",
	// 		Value: &strUserID,
	// 		Comparison: "equals",
	// 	})
	// 	courses, err := c.courseRepository.GetAllCourses(ctx, command.ListQuery)
	// 	if err != nil {
	// 		return nil, err
	// 	}

	// 	return &dtos.GetCoursesByUserIDResponseDto{
	// 		Message: "Get courses successfully!",
	// 		Courses: courses,
	// 	}, nil
	// }
	pendingStatusStr := fmt.Sprintf("%d", constants.REVIEW_PENDING_STATUS)
	command.Filters = append(command.Filters, &utils.FilterModel{
		Field: "review_status",
		Value: &pendingStatusStr,
		Comparison: "equals",
	})
	courses, err := c.courseRepository.FindCourseRelationPaginate(ctx, []string{"Curriculums"}, command.ListQuery)
	if err != nil {
		return nil, err
	}

	return &dtos.GetCoursesReviewsResponseDto{
		Message: "Get courses successfully!",
		Courses: courses,
	}, nil

}
