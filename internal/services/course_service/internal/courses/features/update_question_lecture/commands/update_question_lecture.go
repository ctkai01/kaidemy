package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type UpdateQuestionLecture struct {
	IdUser            int
	Title             string  `validate:"required,gte=1,lte=150"`
	Description       *string `validate:"omitempty,gte=1,lte=500"`
	QuestionLectureID int     `validate:"required,numeric"`
}

func NewUpdateQuestionLecture(idUser int, title string, description *string, questionLectureID int) (*UpdateQuestionLecture, *validator.ValidateError) {
	command := &UpdateQuestionLecture{idUser, title, description, questionLectureID}

	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}


	return command, nil
}