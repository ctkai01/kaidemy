package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"

	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_question_lecture/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	courseservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
)

type UpdateQuestionLectureHandler struct {
	log                       logger.Logger
	questionLectureRepository data.QuestionLectureRepository
	grpcClient                grpc.GrpcClient
}

func NewUpdateQuestionLectureHandler(
	log logger.Logger,
	questionLectureRepository data.QuestionLectureRepository,
	grpcClient grpc.GrpcClient,

) *UpdateQuestionLectureHandler {
	return &UpdateQuestionLectureHandler{log: log, questionLectureRepository: questionLectureRepository, grpcClient: grpcClient}
}

func (c *UpdateQuestionLectureHandler) Handle(ctx context.Context, command *UpdateQuestionLecture) (*dtos.UpdateQuestionLectureResponseDto, error) {
	questionLecture, err := c.questionLectureRepository.GetQuestionLectureByID(ctx, command.QuestionLectureID)

	if err != nil {
		return nil, err
	}

	if questionLecture.UserID != command.IdUser {
		return nil, customErrors.ErrorCannotPermission
	}

	questionLecture.Title = command.Title

	questionLecture.Description = command.Description

	err = c.questionLectureRepository.UpdateQuestionLecture(ctx, questionLecture)

	if err != nil {
		return nil, err
	}

	connUser, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}
	clientUser := courseservice.NewUsersServiceClient(connUser)

	dataUser, err := clientUser.GetUserByID(context.Background(), &courseservice.GetUserByIdReq{
		UserId: int32(questionLecture.UserID),
	})

	questionLectureShow := &models.QuestionLectureShow{
		ID: questionLecture.ID,
				User: &models.UserQuestionLecture{
					ID: int(dataUser.User.ID),
					Avatar: &dataUser.User.Avatar,
					Name: dataUser.User.Name,
				},
				TotalAnswer: len(questionLecture.AnswerLectures),
				CourseID: questionLecture.CourseID,
				LectureID: questionLecture.LectureID,
				Title: questionLecture.Title,
				Description: questionLecture.Description,
				UpdatedAt: questionLecture.UpdatedAt,
				CreatedAt: questionLecture.CreatedAt,
	}
	if err != nil {
		return nil, err
	}

	return &dtos.UpdateQuestionLectureResponseDto{
		Message:         "Update question lecture successfully!",
		QuestionLecture: questionLectureShow,
	}, nil
}
