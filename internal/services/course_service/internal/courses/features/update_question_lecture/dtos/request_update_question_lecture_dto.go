package dtos

//https://echo.labstack.com/guide/response/

type UpdateQuestionLectureRequestDto struct {
	QuestionLectureID int     `json:"-" param:"id"`
	Title             string  `form:"title"` //80
	Description       *string `form:"description"`
}
