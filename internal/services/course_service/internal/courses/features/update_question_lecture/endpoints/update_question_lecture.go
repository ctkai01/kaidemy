package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/params"
	coursesError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_question_lecture/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_question_lecture/dtos"
)

type updateQuestionLectureEndpoint struct {
	params.QuestionLectureRouteParams
}

func NewQuestionLectureEndpoint(
	params params.QuestionLectureRouteParams,
) route.Endpoint {
	return &updateQuestionLectureEndpoint{
		QuestionLectureRouteParams: params,
	}
}

func (ep *updateQuestionLectureEndpoint) MapEndpoint() {
	ep.QuestionLecturesGroup.PUT("/:id", ep.handler())
}

func (ep *updateQuestionLectureEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.UpdateQuestionLectureRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[UpdateQuestionLecture.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[UpdateQuestionLectureEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}

		userId := utils.GetUserId(c)


		command, errValidate := commands.NewUpdateQuestionLecture(
			userId,
			request.Title,
			request.Description,
			request.QuestionLectureID,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[UpdateQuestionLecture_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[UpdateQuestionLecture_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.UpdateQuestionLecture, *dtos.UpdateQuestionLectureResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == coursesError.WrapError(customErrors.ErrorCannotPermission).Error() {
				errCustom := errors.WithMessage(
					err,
					"[UpdateQuestionLectureEndpoint_handler.Send] error in sending Update Lecture",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[UpdateQuestionLecture.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewForbiddenErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"UpdateQuestionLectureEndpoint_handler.Send] error in sending Update Lecture",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"UpdateQuestionLecture.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
