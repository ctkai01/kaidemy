package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/params"
	coursesError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_answer/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_answer/dtos"
)

type updateAnswerEndpoint struct {
	params.AnswersRouteParams
}

func NewUpdateAnswerEndpoint(
	params params.AnswersRouteParams,
) route.Endpoint {
	return &updateAnswerEndpoint{
		AnswersRouteParams: params,
	}
}

func (ep *updateAnswerEndpoint) MapEndpoint() {
	ep.AnswersGroup.PUT("/:id", ep.handler())
}

func (ep *updateAnswerEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.UpdateAnswerRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[UpdateAnswer.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[UpdateAnswerEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}

		userId := utils.GetUserId(c)

		command, errValidate := commands.NewUpdateAnswer(
			userId,
			request.AnswerText,
			request.AnswerID,
			request.IsCorrect,
			request.Explain,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[UpdateAnswerEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[UpdateAnswerEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.UpdateAnswer, *dtos.UpdateAnswerResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == coursesError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[UpdateAnswerEndpoint_handler.Send] error in sending Update Answer",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[UpdateAnswer.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			if err.Error() == coursesError.WrapError(customErrors.ErrorCannotPermission).Error() {
				errCustom := errors.WithMessage(
					err,
					"[UpdateAnswerEndpoint_handler.Send] error in sending Update Answer",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[UpdateAnswer.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewForbiddenErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[UpdateAnswerEndpoint_handler.Send] error in sending Update Answer",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[UpdateAnswer.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
