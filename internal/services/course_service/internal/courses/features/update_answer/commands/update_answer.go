package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type UpdateAnswer struct {
	IdUser     int
	AnswerText string `validate:"required,gte=1,lte=120"`
	AnswerID   int    `validate:"required,numeric"`
	IsCorrect  bool
	Explain    *string `validate:"omitempty,gte=1,lte=100"`
}

func NewUpdateAnswer(
	idUser int,
	answerText string,
	answerID int,
	isCorrect bool,
	explain *string,
) (*UpdateAnswer, *validator.ValidateError) {
	command := &UpdateAnswer{
		idUser,
		answerText,
		answerID,
		isCorrect,
		explain,
	}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
