package dtos

//https://echo.labstack.com/guide/response/

type UpdateAnswerRequestDto struct {
	UserID     int
	AnswerID   int     `param:"id"`
	AnswerText string  `form:"answer_text"`
	IsCorrect  bool    `form:"is_correct"`
	Explain    *string `form:"explain"`
}
