package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/params"
	coursesError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_users_course_by_author/queries"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_users_course_by_author/dtos"
)

type getUsersCourseAuthorEndpoint struct {
	params.CoursesRouteParams
}

func NewUsersCourseAuthorEndpoint(
	params params.CoursesRouteParams,
) route.Endpoint {
	return &getUsersCourseAuthorEndpoint{
		CoursesRouteParams: params,
	}
}

func (ep *getUsersCourseAuthorEndpoint) MapEndpoint() {
	ep.CoursesGroup.GET("/users/author", ep.handler())
}

func (ep *getUsersCourseAuthorEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		listQuery, err := utils.GetListQueryFromCtx(c)
		fmt.Println("Query: ", listQuery)
		
		if err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[getUsersCourseAuthor.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getUsersCourseAuthorEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())
		}

		request := &dtos.GetUsersCourseByAuthorRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[getUsersCourseAuthor.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getUsersCourseAuthorEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}

		userId := utils.GetUserId(c)

		query, errValidate := queries.NewGetUsersCourseAuthor(
			userId,
			request.Search,
			listQuery,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[getUsersCourseAuthor_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getUsersCourseAuthor_handler.StructCtx] err: {%v}", validationErr),
			)

			return customErrors.NewBadRequestErrorHttp(c, validationErr.Error())

		}

		result, err := mediatr.Send[*queries.GetUsersCourseAuthor, *dtos.GetUsersCourseByAuthorResponseDto](
			ctx,
			query,
		)

		if err != nil {
			if err.Error() == coursesError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[getUsersCourseAuthor_handler.Send] error",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[getUsersCourseAuthor.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[getUsersCourseAuthorEndpoint_handler.Send] error",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[getUsersCourseAuthor.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
