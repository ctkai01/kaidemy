package dtos

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"
)

type UserCourseAuthorReview struct{
	ID int `json:"id"`
	Name string `json:"name"`
	Avatar *string `json:"avatar"` 

}

type GetUsersCourseByAuthorResponseDto struct {
	Message string                            `json:"message"`
	Users *utils.ListResult[*UserCourseAuthorReview] `json:"users"`
}
