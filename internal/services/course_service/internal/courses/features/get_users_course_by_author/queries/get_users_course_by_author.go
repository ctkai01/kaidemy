package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetUsersCourseAuthor struct {
	IdUser int
	Search string
	*utils.ListQuery
}

func NewGetUsersCourseAuthor(
	idUser int,
	search string,
	query *utils.ListQuery,

) (*GetUsersCourseAuthor, *validator.ValidateError) {
	q := &GetUsersCourseAuthor{
		idUser,
		search,
		query,
	}
	
	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
