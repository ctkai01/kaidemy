package queries

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"math"
	"strings"

	// "strings"

	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"

	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_users_course_by_author/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	courseservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type GetUsersCourseByAuthorHandler struct {
	log                logger.Logger
	courseRepository   data.CourseRepository
	learningRepository data.LearningRepository
	grpcClient         grpc.GrpcClient
}

func NewGetUsersCourseByAuthorHandler(
	log logger.Logger,
	courseRepository data.CourseRepository,
	learningRepository data.LearningRepository,
	grpcClient grpc.GrpcClient,

) *GetUsersCourseByAuthorHandler {
	return &GetUsersCourseByAuthorHandler{
		log:                log,
		courseRepository:   courseRepository,
		learningRepository: learningRepository,
		grpcClient:         grpcClient,
	}
}

func (c *GetUsersCourseByAuthorHandler) Handle(ctx context.Context, command *GetUsersCourseAuthor) (*dtos.GetUsersCourseByAuthorResponseDto, error) {
	// if command.IdUser != command.IdUserCall {
	// 	return nil, customErrors.ErrorCannotPermission
	// }
	coursesID, err := c.courseRepository.FindIdsCourseAuthor(ctx, command.IdUser)

	if err != nil {
		return nil, err
	}

	learnings, err := c.learningRepository.GetEnrollsByCourseIDs(ctx, coursesID)

	if err != nil {
		return nil, err
	}
	var usersCourse []*dtos.UserCourseAuthorReview

	connUser, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}
	clientUser := courseservice.NewUsersServiceClient(connUser)

	for _, learning := range learnings {
		//Get data user service
		dataUser, err := clientUser.GetUserByID(context.Background(), &courseservice.GetUserByIdReq{
			UserId: int32(learning.Course.UserID),
		})
		if err != nil {
			return nil, err
		}

		if strings.Contains(dataUser.User.Name, command.Search) {
			usersCourse = append(usersCourse, &dtos.UserCourseAuthorReview{
				ID:     int(dataUser.User.ID),
				Name:   dataUser.User.Name,
				Avatar: &dataUser.User.Avatar,
			})
		}
	}

	totalPage := 0
	totalItem := int64(len(usersCourse))
	if len(usersCourse) > 0 {
		//Define Offset
		offset := 0

		if command.ListQuery.Page == 0 {
			offset = 0
		} else {
			offset = (command.ListQuery.Page - 1) * command.ListQuery.Size
		}

		if offset >= len(usersCourse) {
			offset = len(usersCourse) - 1
		}

		limit := command.ListQuery.Size

		endIndex := offset + limit

		if endIndex > len(usersCourse) {
			endIndex = len(usersCourse)
		}

		usersCourse = usersCourse[offset:endIndex]
		d := float64(totalItem) / float64(command.ListQuery.Size)

		totalPage = int(math.Ceil(d))

		result := &utils.ListResult[*dtos.UserCourseAuthorReview]{
			Size:       command.ListQuery.Size,
			Page:       command.ListQuery.Page,
			TotalItems: totalItem,
			TotalPage:  totalPage,
			Items:      usersCourse,
		}

		// fmt.Println("totalItem: ", totalItem)
		// fmt.Println("command.ListQuery.Size: ", command.ListQuery.Size)
		// fmt.Println("totalPage: ", totalPage)

		return &dtos.GetUsersCourseByAuthorResponseDto{
			Message: "Get users successfully!",
			Users:   result,
		}, nil
	}

	result := &utils.ListResult[*dtos.UserCourseAuthorReview]{
		Size:       command.ListQuery.Size,
		Page:       command.ListQuery.Page,
		TotalItems: totalItem,
		TotalPage:  totalPage,
		Items:      usersCourse,
	}
	
	return &dtos.GetUsersCourseByAuthorResponseDto{
		Message: "Get users successfully!",
		Users:   result,
	}, nil
}
