package dtos

//https://echo.labstack.com/guide/response/

type DeleteQuizResponseDto struct {
	Message string `json:"message"`
}
