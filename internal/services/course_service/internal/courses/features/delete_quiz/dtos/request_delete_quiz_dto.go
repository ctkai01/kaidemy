package dtos

//https://echo.labstack.com/guide/response/

type DeleteQuizRequestDto struct {
	QuizID int `json:"-" param:"id"`
}
