package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_quiz/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type DeleteQuizHandler struct {
	log                  logger.Logger
	curriculumRepository data.CurriculumRepository
	lectureRepository    data.LectureRepository
	courseRepository     data.CourseRepository
	grpcClient           grpc.GrpcClient
}

func NewDeleteQuizHandler(
	log logger.Logger,
	curriculumRepository data.CurriculumRepository,
	lectureRepository data.LectureRepository,
	courseRepository data.CourseRepository,
	grpcClient grpc.GrpcClient,

) *DeleteQuizHandler {
	return &DeleteQuizHandler{log: log, courseRepository: courseRepository, curriculumRepository: curriculumRepository, lectureRepository: lectureRepository, grpcClient: grpcClient}

}

func (c *DeleteQuizHandler) Handle(ctx context.Context, command *DeleteQuiz) (*dtos.DeleteQuizResponseDto, error) {
	lecture, err := c.lectureRepository.GetLectureByID(ctx, command.QuizID)

	if err != nil {
		return nil, err
	}

	if lecture == nil {
		return nil, customErrors.ErrLectureNotFound
	}

	if lecture.Type != constants.QUIZ_TYPE {
		return nil, customErrors.ErrMustQuiz
	}

	curriculum, err := c.curriculumRepository.GetCurriculumByIDRelation(ctx, []string{"Course"}, lecture.CurriculumID)

	if err != nil {
		return nil, err
	}

	if curriculum == nil {
		return nil, customErrors.ErrCurriculumNotFound
	}

	if curriculum.Course.UserID != command.IdUser {
		return nil, customErrors.ErrorCannotPermission
	}

	if err := c.lectureRepository.DeleteLectureByID(ctx, command.QuizID); err != nil {
		return nil, err
	}
	c.log.Info("Delete quiz success: ")

	course := curriculum.Course
	course.ReviewStatus = constants.REVIEW_INIT_STATUS
	if err := c.courseRepository.UpdateCourse(ctx, &course); err != nil {
		return nil, err
	}
	return &dtos.DeleteQuizResponseDto{
		Message: "Delete quiz successfully!",
	}, nil
}
