package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type DeleteQuiz struct {
	IdUser int
	QuizID int `validate:"required,numeric"`
}

func NewDeleteQuiz(idUser int, quizID int) (*DeleteQuiz, *validator.ValidateError) {
	command := &DeleteQuiz{idUser, quizID}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
