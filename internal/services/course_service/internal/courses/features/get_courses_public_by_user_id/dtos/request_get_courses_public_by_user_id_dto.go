package dtos

//https://echo.labstack.com/guide/response/

type GetCoursePublicByUserIDRequestDto struct {
	UserID  int    `param:"id"`
	// Content int    `query:"content"`
	// Search  string `query:"search"`
}
