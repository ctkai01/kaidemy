package dtos

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"
)

type GetCoursesPublicByUserIDResponseDto struct {
	Message string                            `json:"message"`
	Learnings *utils.ListResult[*models.LearningShow] `json:"learnings"`

	// Courses *utils.ListResult[*models.Course] `json:"courses"`
}
