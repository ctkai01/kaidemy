package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetCoursesPublicByUserId struct {
	IdUser int
	*utils.ListQuery
}

func NewGetCoursesPublicByUserId(
	idUser int,
	query *utils.ListQuery,

) (*GetCoursesPublicByUserId, *validator.ValidateError) {
	q := &GetCoursesPublicByUserId{
		idUser,
		query,
	}
	
	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
