package queries

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"fmt"
	"strings"

	// "fmt"

	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"

	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_courses_public_by_user_id/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	courseservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
)

type GetCoursesPublicByUserIDHandler struct {
	log                logger.Logger
	learningRepository data.LearningRepository

	grpcClient grpc.GrpcClient
}

func NewGetCoursesPublicByUserIDHandler(
	log logger.Logger,
	learningRepository data.LearningRepository,
	grpcClient grpc.GrpcClient,

) *GetCoursesPublicByUserIDHandler {
	return &GetCoursesPublicByUserIDHandler{
		log:                log,
		learningRepository: learningRepository,
		grpcClient:         grpcClient,
	}
}

func (c *GetCoursesPublicByUserIDHandler) Handle(ctx context.Context, command *GetCoursesPublicByUserId) (*dtos.GetCoursesPublicByUserIDResponseDto, error) {
	// if command.Content == 1 {
	// 	if command.IdUser != command.IdUserCall {
	// 		return nil, customErrors.ErrorCannotPermission
	// 	}
	strUserID := fmt.Sprintf("%d", command.IdUser)
	command.Filters = append(command.Filters, &utils.FilterModel{
		Field:      "user_id",
		Value:      &strUserID,
		Comparison: "equals",
	})
	var learnings *utils.ListResult[*models.Learning]

	dataLearnings, err := c.learningRepository.FindLearningsRelationPaginate(ctx, []string{"Course.Curriculums.Lectures.Assets"}, command.ListQuery)
	learnings = dataLearnings
	if err != nil {
		return nil, err
	}
	connUser, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}
	clientUser := courseservice.NewUsersServiceClient(connUser)

	conn, err := c.grpcClient.GetGrpcConnection("catalog_service")
	if err != nil {
		return nil, err
	}
	client := courseservice.NewCatalogsServiceClient(conn)

	var learningShows = make([]*models.LearningShow, 0)

	if learnings.Items != nil {

		for _, learning := range learnings.Items {
			var levelID int

			if learning.Course.LevelID == nil {
				levelID = -1
			} else {
				levelID = *learning.Course.LevelID
			}

			dataLevel, err := client.GetLevelByID(context.Background(), &courseservice.GetLevelByIdReq{
				// UserId:  int32(command.IdUser),
				LevelId: int32(levelID),
			})

			if err != nil {
				if !strings.Contains(err.Error(), "level not found") {
					return nil, err

				}
			}

			//Get price
			var priceID int

			if learning.Course.PriceID == nil {
				priceID = -1
			} else {
				priceID = *learning.Course.PriceID
			}

			dataPrice, err := client.GetPriceByID(context.Background(), &courseservice.GetPriceByIdReq{
				PriceId: int32(priceID),
			})

			if err != nil {
				if !strings.Contains(err.Error(), "price not found") {
					return nil, err
				}
			}

			//Get data user service
			dataUser, err := clientUser.GetUserByID(context.Background(), &courseservice.GetUserByIdReq{
				UserId: int32(learning.Course.UserID),
			})
			if err != nil {
				return nil, err
			}

			var levelRes *models.LevelResponse

			if dataLevel == nil {
				levelRes = nil
			} else {
				levelRes = &models.LevelResponse{
					ID:   int(dataLevel.Level.ID),
					Name: dataLevel.Level.Name,
				}
			}

			var priceRes *models.PriceResponse

			if dataPrice == nil {
				priceRes = nil
			} else {
				priceRes = &models.PriceResponse{
					ID:    int(dataPrice.Price.ID),
					Tier:  dataPrice.Price.Tier,
					Value: int(dataPrice.Price.Value),
				}
			}

			learningShows = append(learningShows, &models.LearningShow{
				ID:        learning.ID,
				UserID:    learning.UserID,
				CourseID:  learning.CourseID,
				Process:   learning.Process,
				Type:      learning.Type,
				StarCount: learning.StarCount,
				Comment:   learning.Comment,
				Course: models.CourseLearning{
					ID:           learning.Course.ID,
					Title:        learning.Course.Title,
					Status:       learning.Course.Status,
					ReviewStatus: learning.Course.ReviewStatus,
					Image:        learning.Course.Image,
					Curriculums:  learning.Course.Curriculums,
					Level:        levelRes,
					Price:        priceRes,
					User: models.UserResponse{
						ID:   int(dataUser.User.ID),
						Name: dataUser.User.Name,
					},
					UpdatedAt: learning.Course.UpdatedAt,
					CreatedAt: learning.Course.CreatedAt,
				},
				UpdatedAt: learning.UpdatedAt,
				CreatedAt: learning.CreatedAt,
			})

		}
	}
	learningResult := &utils.ListResult[*models.LearningShow]{
		Size:       learnings.Size,
		Page:       learnings.Page,
		TotalItems: learnings.TotalItems,
		TotalPage:  learnings.TotalPage,
		Items:      learningShows,
	}

	return &dtos.GetCoursesPublicByUserIDResponseDto{
		Message:   "Get courses successfully!",
		Learnings: learningResult,
	}, nil
	// }

}
