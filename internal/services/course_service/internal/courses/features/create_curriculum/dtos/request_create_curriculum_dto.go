package dtos

//https://echo.labstack.com/guide/response/

type CreateCurriculumRequestDto struct {
	Title       string  `form:"title"` //80
	CourseID    int     `form:"course_id"`
	Description *string `form:"description"` //200
}
