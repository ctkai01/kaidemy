package dtos

import "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

//https://echo.labstack.com/guide/response/

type CreateCurriculumResponseDto struct {
	Message    string            `json:"message"`
	Curriculum models.Curriculum `json:"curriculum"`
}
