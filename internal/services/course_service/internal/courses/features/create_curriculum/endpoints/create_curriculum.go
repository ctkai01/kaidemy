package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/params"
	coursesError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_curriculum/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_curriculum/dtos"
)

type createCurriculumEndpoint struct {
	params.CurriculumsRouteParams
}

func NewCurriculumEndpoint(
	params params.CurriculumsRouteParams,
) route.Endpoint {
	return &createCurriculumEndpoint{
		CurriculumsRouteParams: params,
	}
}

func (ep *createCurriculumEndpoint) MapEndpoint() {
	ep.CurriculumsGroup.POST("", ep.handler())
}

func (ep *createCurriculumEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.CreateCurriculumRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[createCurriculum.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[createCurriculumEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}

		userId := utils.GetUserId(c)

		command, errValidate := commands.NewCreateCurriculum(
			userId,
			request.Title,
			request.Description,
			request.CourseID,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[createCurriculumEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[createCurriculumEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.CreateCurriculum, *dtos.CreateCurriculumResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == coursesError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[createCurriculumEndpoint_handler.Send] error in sending create Curriculum",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[createCurriculum.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[createCurriculumEndpoint_handler.Send] error in sending create Curriculum",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[createCurriculum.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusCreated, result)
	}
}
