package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type CreateCurriculum struct {
	IdUser      int
	Title       string  `validate:"required,gte=1,lte=80"`
	CourseID    int     `validate:"required,numeric"`
	Description *string `validate:"omitempty,gte=1,lte=200"`
}

func NewCreateCurriculum(idUser int, title string, description *string, courseID int) (*CreateCurriculum, *validator.ValidateError) {
	command := &CreateCurriculum{idUser, title, courseID, description}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
