package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_curriculum/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type CreateCurriculumHandler struct {
	log                  logger.Logger
	curriculumRepository data.CurriculumRepository
	courseRepository     data.CourseRepository
	grpcClient           grpc.GrpcClient
}

func NewCreateCurriculumHandler(
	log logger.Logger,
	curriculumRepository data.CurriculumRepository,
	courseRepository data.CourseRepository,
	grpcClient grpc.GrpcClient,

) *CreateCurriculumHandler {
	return &CreateCurriculumHandler{log: log, curriculumRepository: curriculumRepository, courseRepository: courseRepository, grpcClient: grpcClient}
}

func (c *CreateCurriculumHandler) Handle(ctx context.Context, command *CreateCurriculum) (*dtos.CreateCurriculumResponseDto, error) {
	course, err := c.courseRepository.GetCourseByID(ctx, command.CourseID)

	if err != nil {
		return nil, err
	}

	if course == nil {
		return nil, customErrors.ErrCourseNotFound
	}

	curriculumCreate := &models.Curriculum{
		Title:       command.Title,
		Description: command.Description,
		CourseID:    command.CourseID,
	}
	if err := c.curriculumRepository.CreateCurriculum(ctx, curriculumCreate); err != nil {
		return nil, err
	}
	c.log.Info("Create curriculum success: ")
	course.ReviewStatus = constants.REVIEW_INIT_STATUS
	if err := c.courseRepository.UpdateCourse(ctx, course); err != nil {
		return nil, err
	}
	return &dtos.CreateCurriculumResponseDto{
		Message:    "Create curriculum successfully!",
		Curriculum: *curriculumCreate,
	}, nil
}
