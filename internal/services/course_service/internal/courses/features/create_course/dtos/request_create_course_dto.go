package dtos

//https://echo.labstack.com/guide/response/

type CreateCourseRequestDto struct {
	Title         string `form:"title"`
	CategoryID    int    `form:"category_id"`
	SubCategoryID int    `form:"sub_category_id"`
}
