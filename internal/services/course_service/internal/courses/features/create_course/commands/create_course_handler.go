package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// "io"
	"strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	stripe "github.com/stripe/stripe-go/v76"
	stripeProduct "github.com/stripe/stripe-go/v76/product"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type CreateCourseHandler struct {
	log              logger.Logger
	courseRepository data.CourseRepository
	learningRepository data.LearningRepository
	grpcClient       grpc.GrpcClient
}

func NewCreateCourseHandler(
	log logger.Logger,
	courseRepository data.CourseRepository,
	learningRepository data.LearningRepository,
	grpcClient grpc.GrpcClient,

) *CreateCourseHandler {
	return &CreateCourseHandler{log: log, learningRepository: learningRepository, courseRepository: courseRepository, grpcClient: grpcClient}
}

func (c *CreateCourseHandler) Handle(ctx context.Context, command *CreateCourse) (*dtos.CreateCourseResponseDto, error) {
	conn, err := c.grpcClient.GetGrpcConnection("catalog_service")
	if err != nil {
		return nil, err
	}

	client := catalogservice.NewCatalogsServiceClient(conn)
	dataCategory, err := client.GetCategoryByID(context.Background(), &catalogservice.GetCategoryByIdReq{
		CategoryId: int32(command.SubCategoryID),
	})
	c.log.Info("err: ", err)

	if err != nil {
		if strings.Contains(err.Error(), customErrors.ErrCategoryNotFound.Error()) {
			return nil, customErrors.ErrCategoryNotFound

		}
		return nil, err
	}

	if dataCategory.Category.ParentID != int32(command.CategoryID) {
		return nil, customErrors.ErrParentCategoryNotCorrect
	}

	//Upload GRPC
	// connUpload, err := c.grpcClient.GetGrpcConnection("upload_service")
	// if err != nil {
	// 	return nil, err
	// }

	// clientUpload := catalogservice.NewUploadsServiceClient(connUpload)
	// stream, err := clientUpload.UploadVideo(ctx)

	// if err != nil {
	// 	return nil, err
	// }
	// file, err := command.Video.Open()

	// if err != nil {
	// 	return nil, err
	// }

	// buf := make([]byte, 1048576)
	// batchNumber := 1
	// for {
	// 	num, err := file.Read(buf)
	// 	if err == io.EOF {
	// 		break
	// 	}
	// 	if err != nil {
	// 		return nil, err
	// 	}
	// 	chunk := buf[:num]
	// 	if err := stream.Send(&catalogservice.UploadVideoReq{
	// 		Title:     command.Title,
	// 		Data:      chunk,
	// 		Extension: command.Video.Header["Content-Type"][0],
	// 		Size:      int64(len(chunk)),
	// 	}); err != nil {
	// 		return nil, err
	// 	}
	// 	c.log.Info("Sent - batch #%v - size - %v\n", batchNumber, len(chunk))
	// 	batchNumber += 1

	// }

	// res, err := stream.CloseAndRecv()
	// if err != nil {
	// 	return nil, err
	// }
	// c.log.Info("URL: ", res.GetUrl())

	//Create product stripe
	stripe.Key = "sk_test_51NUmJOFEsDSfMpHunO8Zscoo1KwMISvWpj00QwhHnLTjYQVkQF1uH0WBgCd8pWgfd5gAKBguz9MfszFPlQwOVPXl00ZUdeBaS5"
	productStripeParams := &stripe.ProductParams{
		Name: stripe.String(command.Title),
	}

	p, err := stripeProduct.New(productStripeParams)

	if err != nil {
		return nil, err
	}

	courseCreate := &models.Course{
		Title:         command.Title,
		CategoryID:    command.CategoryID,
		SubCategoryID: command.SubCategoryID,
		UserID:        command.IdUser,
		ProductIdStripe: p.ID,
	}
	if err := c.courseRepository.CreateCourse(ctx, courseCreate); err != nil {
		return nil, err
	}

	createLearning := &models.Learning{
		UserID: command.IdUser,
		CourseID: courseCreate.ID,
		Type: constants.STANDARD_TYPE,
	}
	
	err = c.learningRepository.CreateLearning(ctx, createLearning)

	if err != nil {
		return nil, err
	}
	c.log.Info("Create course success: ")

	return &dtos.CreateCourseResponseDto{
		Message: "Create course successfully!",
		Course:  *courseCreate,
	}, nil
}
