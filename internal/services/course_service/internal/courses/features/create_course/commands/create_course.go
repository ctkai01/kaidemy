package commands

import (
	"mime/multipart"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type CreateCourse struct {
	IdUser        int
	Title         string `validate:"required,gte=1,lte=60"`
	CategoryID    int    `validate:"required,numeric"`
	SubCategoryID int    `validate:"required,numeric"`
	Video         *multipart.FileHeader
}

func NewCreateCourse(idUser int, title string, categoryID int, subCategoryID int, video *multipart.FileHeader) (*CreateCourse, *validator.ValidateError) {
	command := &CreateCourse{idUser, title, categoryID, subCategoryID, video}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
