package dtos

//https://echo.labstack.com/guide/response/

type SubmitReviewCourseRequestDto struct {
	CourseID int `json:"-" param:"id"`
}
