package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/submit_review_course/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"

	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type SubmitReviewCourseHandler struct {
	log                  logger.Logger
	courseRepository data.CourseRepository
}

func NewSubmitReviewCourseHandler(
	log logger.Logger,
	courseRepository data.CourseRepository,
) *SubmitReviewCourseHandler {
	return &SubmitReviewCourseHandler{
		log:                  log,
		courseRepository: courseRepository,
	}
}

func (c *SubmitReviewCourseHandler) Handle(ctx context.Context, command *SubmitReviewCourse) (*dtos.SubmitReviewCourseResponseDto, error) {
	course, err := c.courseRepository.GetCourseByIDRelation(ctx, []string{"Curriculums"}, command.CourseID)

	if err != nil {
		return nil, err
	}

	if course.UserID != command.IdUser {
		return nil, customErrors.ErrorCannotPermission
	}

	if course.ReviewStatus != constants.REVIEW_INIT_STATUS {
		return nil, customErrors.ErrorCannotPermission
	}

	course.ReviewStatus = constants.REVIEW_PENDING_STATUS
	if err := c.courseRepository.UpdateCourse(ctx, course); err != nil {
		return nil, err
	}

	return &dtos.SubmitReviewCourseResponseDto{
		Message: "Submit review course successfully!",
		Course: course,
	}, nil
}
