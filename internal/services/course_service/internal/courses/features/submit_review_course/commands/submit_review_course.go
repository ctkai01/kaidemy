package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type SubmitReviewCourse struct {
	IdUser   int
	CourseID int `validate:"required,numeric"`
}

func NewSubmitReviewCourse(idUser int, courseID int) (*SubmitReviewCourse, *validator.ValidateError) {
	command := &SubmitReviewCourse{idUser, courseID}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
