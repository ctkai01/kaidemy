package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetTopicLearning struct {
	IdUser int
	*utils.ListQuery
}

func NewGetTopicLearning(
	idUser int,
	query *utils.ListQuery,

) (*GetTopicLearning, *validator.ValidateError) {
	q := &GetTopicLearning{
		idUser,
		query,
	}
	
	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
