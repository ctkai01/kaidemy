package dtos

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"
)

type GetTopicLearningResponseDto struct {
	Message        string                              `json:"message"`
	TopicLearnings *utils.ListResult[*models.TopicLearningShow] `json:"topic_learnings"`
}
