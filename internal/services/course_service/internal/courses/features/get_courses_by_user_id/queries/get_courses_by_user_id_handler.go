package queries

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"fmt"
	"strings"

	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"

	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_courses_by_user_id/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	courseservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type GetCoursesByUserIDHandler struct {
	log              logger.Logger
	courseRepository data.CourseRepository
	grpcClient       grpc.GrpcClient
}

func NewGetTopicLearningHandler(
	log logger.Logger,
	courseRepository data.CourseRepository,
	grpcClient grpc.GrpcClient,

) *GetCoursesByUserIDHandler {
	return &GetCoursesByUserIDHandler{
		log:              log,
		courseRepository: courseRepository,
		grpcClient:       grpcClient,
	}
}

func (c *GetCoursesByUserIDHandler) Handle(ctx context.Context, command *GetCoursesByUserId) (*dtos.GetCoursesByUserIDResponseDto, error) {
	// if command.IdUser != command.IdUserCall {
	// 	return nil, customErrors.ErrorCannotPermission
	// }
	stringUserID := fmt.Sprintf("%d", command.IdUser)
	command.Filters = append(command.Filters, &utils.FilterModel{
		Field:      "user_id",
		Value:      &stringUserID,
		Comparison: "equals",
	})

	for _, x := range command.Filters {
		fmt.Println("X: ", x)
	}

	courses, err := c.courseRepository.FindCourseRelationPaginate(ctx, []string{"Curriculums.Lectures.Assets"}, command.ListQuery)
	if err != nil {
		return nil, err
	}

	conn, err := c.grpcClient.GetGrpcConnection("catalog_service")
	if err != nil {
		return nil, err
	}
	client := courseservice.NewCatalogsServiceClient(conn)

	connUser, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}
	clientUser := courseservice.NewUsersServiceClient(connUser)
	var coursesResponse []*models.CourseResponse

	for _, course := range courses.Items {
		dataCategory, err := client.GetCategoryByID(context.Background(), &courseservice.GetCategoryByIdReq{
			CategoryId: int32(course.CategoryID),
		})

		if err != nil {
			return nil, err
		}

		//Get data category
		dataSubCategory, err := client.GetCategoryByID(context.Background(), &courseservice.GetCategoryByIdReq{
			// UserId:     int32(command.IdUser),
			CategoryId: int32(course.SubCategoryID),
		})

		if err != nil {
			return nil, err
		}

		//Get data Level
		var levelID int

		if course.LevelID == nil {
			levelID = -1
		} else {
			levelID = *course.LevelID
		}

		dataLevel, err := client.GetLevelByID(context.Background(), &courseservice.GetLevelByIdReq{
			// UserId:  int32(command.IdUser),
			LevelId: int32(levelID),
		})

		if err != nil {
			if !strings.Contains(err.Error(), "level not found") {
				return nil, err

			}
		}

		//Get Language

		var languageID int

		if course.LanguageID == nil {
			languageID = -1
		} else {
			languageID = *course.LanguageID
		}

		dataLanguage, err := client.GetLanguageByID(context.Background(), &courseservice.GetLanguageByIdReq{
			LanguageId: int32(languageID),
		})

		if err != nil {
			if !strings.Contains(err.Error(), "language not found") {
				return nil, err
			}
		}

		//Get price
		var priceID int

		if course.PriceID == nil {
			priceID = -1
		} else {
			priceID = *course.PriceID
		}

		dataPrice, err := client.GetPriceByID(context.Background(), &courseservice.GetPriceByIdReq{
			PriceId: int32(priceID),
		})

		if err != nil {
			if !strings.Contains(err.Error(), "price not found") {
				return nil, err
			}
		}

		//Get data user service
		dataUser, err := clientUser.GetUserByID(context.Background(), &courseservice.GetUserByIdReq{
			UserId: int32(course.UserID),
		})
		if err != nil {
			return nil, err
		}

		var parentIDSubCategory *int

		if dataSubCategory.Category.ParentID == 0 {
			parentIDSubCategory = nil
		} else {
			parentId := int(dataSubCategory.Category.ParentID)
			parentIDSubCategory = &parentId
		}
		var levelRes *models.LevelResponse

		if dataLevel == nil {
			levelRes = nil
		} else {
			levelRes = &models.LevelResponse{
				ID:   int(dataLevel.Level.ID),
				Name: dataLevel.Level.Name,
			}
		}

		var categoryRes *models.CategoryResponse

		if dataCategory == nil {
			categoryRes = nil
		} else {
			categoryRes = &models.CategoryResponse{
				ID:       int(dataCategory.Category.ID),
				Name:     dataCategory.Category.Name,
				ParentID: nil,
			}
		}

		//
		var subCategoryRes *models.CategoryResponse

		if dataSubCategory == nil {
			subCategoryRes = nil
		} else {
			subCategoryRes = &models.CategoryResponse{
				ID:       int(dataSubCategory.Category.ID),
				Name:     dataSubCategory.Category.Name,
				ParentID: parentIDSubCategory,
			}
		}

		//
		var languageRes *models.LanguageResponse

		if dataLanguage == nil {
			languageRes = nil
		} else {
			languageRes = &models.LanguageResponse{
				ID:   int(dataLanguage.Language.ID),
				Name: dataLanguage.Language.Name,
			}
		}

		//
		var priceRes *models.PriceResponse

		if dataPrice == nil {
			priceRes = nil
		} else {
			priceRes = &models.PriceResponse{
				ID:    int(dataPrice.Price.ID),
				Tier:  dataPrice.Price.Tier,
				Value: int(dataPrice.Price.Value),
			}
		}

		courseResponse := &models.CourseResponse{
			ID:                     course.ID,
			OutComes:               course.OutComes,
			IntendedFor:            course.IntendedFor,
			Requirement:            course.Requirement,
			ProductIdStripe:        course.ProductIdStripe,
			Level:                  levelRes,
			Category:               categoryRes,
			SubCategory:            subCategoryRes,
			Title:                  course.Title,
			WelcomeMessage:         course.WelcomeMessage,
			CongratulationsMessage: course.WelcomeMessage,
			Subtitle:               course.Subtitle,
			PrimarilyTeach:         course.PrimarilyTeach,
			Description:            course.Description,
			Status:                 course.Status,
			Language:               languageRes,
			Price:                  priceRes,
			ReviewStatus:           course.ReviewStatus,
			User: models.UserResponse{
				ID:   int(dataUser.User.ID),
				Name: dataUser.User.Name,
			},
			PromotionalVideo: course.PromotionalVideo,
			Image:            course.Image,
			Curriculums:      course.Curriculums,
			UpdatedAt:        course.UpdatedAt,
			CreatedAt:        course.CreatedAt,
		}

		coursesResponse = append(coursesResponse, courseResponse)
	}
	result := &utils.ListResult[*models.CourseResponse]{
		Size:       courses.Size,
		Page:       courses.Page,
		TotalItems: courses.TotalItems,
		TotalPage:  courses.TotalPage,
		Items:      coursesResponse,
	}
	// CourseResponse
	return &dtos.GetCoursesByUserIDResponseDto{
		Message: "Get courses successfully!",
		Courses: result,
	}, nil
}
