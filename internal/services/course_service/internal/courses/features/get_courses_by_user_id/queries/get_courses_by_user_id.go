package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetCoursesByUserId struct {
	IdUser int
	IdUserCall int
	Content int
	*utils.ListQuery
}

func NewGetCoursesByUserId(
	idUser int,
	idUserCall int,
	content int,
	query *utils.ListQuery,

) (*GetCoursesByUserId, *validator.ValidateError) {
	q := &GetCoursesByUserId{
		idUser,
		idUserCall,
		content,
		query,
	}
	
	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
