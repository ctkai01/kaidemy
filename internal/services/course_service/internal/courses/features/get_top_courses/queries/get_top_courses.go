package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetTopCourses struct {
	// UserID int
	*utils.ListQuery
}

func NewGetTopCourses(
	// userID int,
	query *utils.ListQuery,

) (*GetTopCourses, *validator.ValidateError) {
	q := &GetTopCourses{
		// userID,
		query,
	}
	
	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
