package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_answer_lecture/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type deleteAnswerLectureHandler struct {
	log                  logger.Logger
	answerLectureRepository    data.AnswerLectureRepository
	grpcClient           grpc.GrpcClient
}

func NewDeleteAnswerLectureHandler(
	log logger.Logger,
	answerLectureRepository    data.AnswerLectureRepository,
	grpcClient grpc.GrpcClient,

) *deleteAnswerLectureHandler {
	return &deleteAnswerLectureHandler{log: log, answerLectureRepository: answerLectureRepository, grpcClient: grpcClient}
}

func (c *deleteAnswerLectureHandler) Handle(ctx context.Context, command *DeleteAnswerLecture) (*dtos.DeleteAnswerLectureResponseDto, error) {
	answerLecture, err := c.answerLectureRepository.GetAnswerLectureByID(ctx, command.AnswerLectureID)

	if err != nil {
		return nil, err
	}


	if answerLecture.UserID != command.IdUser {
		return nil, customErrors.ErrorCannotPermission
	}

	err = c.answerLectureRepository.DeleteAnswerLectureByID(ctx, answerLecture.ID)

	if err != nil {
		return nil, err
	}


	return &dtos.DeleteAnswerLectureResponseDto{
		Message: "Delete answer lecture successfully!",
	}, nil
}
