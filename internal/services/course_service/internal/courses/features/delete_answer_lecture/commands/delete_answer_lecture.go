package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type DeleteAnswerLecture struct {
	IdUser            int `validate:"required,numeric"`
	AnswerLectureID int `validate:"required,numeric"`
}

func NewDeleteAnswerLecture(idUser int, lectureID int) (*DeleteAnswerLecture, *validator.ValidateError) {
	command := &DeleteAnswerLecture{idUser, lectureID}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
