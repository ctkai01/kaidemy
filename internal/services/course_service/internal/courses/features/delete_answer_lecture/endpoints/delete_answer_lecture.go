package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/params"
	coursesError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_answer_lecture/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_answer_lecture/dtos"
)

type deleteAnswerLectureEndpoint struct {
	params.AnswerLectureRouteParams
}

func NewDeleteAnswerLectureEndpoint(
	params params.AnswerLectureRouteParams,
) route.Endpoint {
	return &deleteAnswerLectureEndpoint{
		AnswerLectureRouteParams: params,
	}
}

func (ep *deleteAnswerLectureEndpoint) MapEndpoint() {
	ep.AnswerLecturesGroup.DELETE("/:id", ep.handler())
}

func (ep *deleteAnswerLectureEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.DeleteAnswerLectureRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[DeleteAnswerLecture.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[DeleteAnswerLectureEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}

		userId := utils.GetUserId(c)

		command, errValidate := commands.NewDeleteAnswerLecture(
			userId,
			request.AnswerLectureID,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[DeleteAnswerLectureEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[DeleteAnswerLectureEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.DeleteAnswerLecture, *dtos.DeleteAnswerLectureResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == coursesError.WrapError(customErrors.ErrorCannotPermission).Error() {
				errCustom := errors.WithMessage(
					err,
					"[DeleteAnswerLectureEndpoint_handler.Send] error in sending Delete Lecture",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[DeleteAnswerLecture.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewForbiddenErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[DeleteAnswerLectureEndpoint_handler.Send] error in sending Delete Lecture",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[DeleteAnswerLecture.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
