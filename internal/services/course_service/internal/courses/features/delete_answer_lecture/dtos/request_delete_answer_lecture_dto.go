package dtos

//https://echo.labstack.com/guide/response/

type DeleteAnswerLectureRequestDto struct {
	AnswerLectureID int `json:"-" param:"id"`
}
