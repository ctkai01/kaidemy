package dtos

//https://echo.labstack.com/guide/response/

type DeleteAnswerLectureResponseDto struct {
	Message string `json:"message"`
}
