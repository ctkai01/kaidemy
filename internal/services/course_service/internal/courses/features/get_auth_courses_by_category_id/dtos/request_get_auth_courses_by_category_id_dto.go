package dtos

//https://echo.labstack.com/guide/response/

type GetAuthCourseByCategoryIDRequestDto struct {
	CategoryID int      `param:"id"`
	Rating     *string   `query:"rating"`
	Sort       *string   `query:"sort"`
	Duration   []string `query:"duration"`
	Level      []string `query:"level"`
}

