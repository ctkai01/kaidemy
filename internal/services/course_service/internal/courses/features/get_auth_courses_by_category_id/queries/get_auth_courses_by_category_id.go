package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetAuthCoursesByCategoryId struct {
	
	IdCategory int
	Level []string
	Duration []string
	Sort *string
	Rating *string
	UserID int
	// IdUserCall int
	// Content int
	*utils.ListQuery
}

func NewGetAuthCoursesByCategoryId(
	idCategory int,
	level []string,
	duration []string,
	sort *string,
	rating *string,
	userID int,
	// idUserCall int,
	// content int,
	query *utils.ListQuery,

) (*GetAuthCoursesByCategoryId, *validator.ValidateError) {
	q := &GetAuthCoursesByCategoryId{
		// idUser,
		// idUserCall,
		// content,
		idCategory,
		level,
		duration,
		sort,
		rating,
		userID,
		query,
	}
	
	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
