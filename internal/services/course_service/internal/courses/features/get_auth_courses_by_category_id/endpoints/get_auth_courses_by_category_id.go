package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/params"
	coursesError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_auth_courses_by_category_id/queries"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_auth_courses_by_category_id/dtos"
)

type getAuthCourseByCategoryIDEndpoint struct {
	params.CoursesRouteParams
}

func NewGetAuthCourseByCategoryIDEndpoint(
	params params.CoursesRouteParams,
) route.Endpoint {
	return &getAuthCourseByCategoryIDEndpoint{
		CoursesRouteParams: params,
	}
}

func (ep *getAuthCourseByCategoryIDEndpoint) MapEndpoint() {
	ep.CoursesGroup.GET("/category-auth/:id", ep.handler())
}

func (ep *getAuthCourseByCategoryIDEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		listQuery, err := utils.GetListQueryFromCtx(c)
		fmt.Println("Query: ", listQuery)
		
		if err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[getAuthCourseByCategoryId.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getAuthCourseByCategoryIdEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())
		}
		
		request := &dtos.GetAuthCourseByCategoryIDRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[getAuthCourseByCategory.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getAuthCourseByCategoryEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}
		// fmt.Println("request level: ", request.Level)
		// fmt.Println("request duration: ", request.Duration)
		// fmt.Println("request rating: ", request.Rating)
		userId := utils.GetUserId(c)

		query, errValidate := queries.NewGetAuthCoursesByCategoryId(
			request.CategoryID,
			request.Level,
			request.Duration,
			request.Sort,
			request.Rating,
			userId,
			listQuery,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[getAuthCourseByCategory_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getAuthCourseByCategory_handler.StructCtx] err: {%v}", validationErr),
			)

			return customErrors.NewBadRequestErrorHttp(c, validationErr.Error())

		}

		result, err := mediatr.Send[*queries.GetAuthCoursesByCategoryId, *dtos.GetAuthCoursesByCategoryIDResponseDto](
			ctx,
			query,
		)

		if err != nil {
			if err.Error() == coursesError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[getAuthCourseByCategory_handler.Send] error in sending create Answer",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[getAuthCourseByCategory.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[getAuthCourseByCategoryEndpoint_handler.Send] error in sending get topic learnings",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[getAuthCourseByCategory.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
