package dtos

import "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

//https://echo.labstack.com/guide/response/

type UpdateLectureResponseDto struct {
	Message string         `json:"message"`
	Lecture models.Lecture `json:"lecture"`
}
