package dtos

//https://echo.labstack.com/guide/response/

type UpdateLectureRequestDto struct {
	LectureID     int     `json:"-" param:"id"`
	Type          int     `form:"type"` //80
	AssetType     int     `form:"asset_type"`
	AssetID       *int    `form:"asset_id"`
	IsPromotional *bool    `form:"is_promotional"`
	Title         *string `form:"title"`
	Description   *string `form:"description"`
	Article       *string `form:"article"` //200
}
