package commands

import (
	"mime/multipart"

	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
	courseConstants "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/constants"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"
)

type UpdateLecture struct {
	IdUser        int
	Article       *string `validate:"omitempty,gte=1,lte=2000"`
	TypeUpdate    int     `validate:"omitempty,gte=1,lte=3"`
	AssetType     int     `validate:"omitempty,gte=1,lte=2"`
	Title         *string `validate:"omitempty,gte=1,lte=80"`
	Description   *string `validate:"omitempty,gte=1,lte=2000"`
	Asset         *multipart.FileHeader
	LectureID     int   `validate:"required,numeric"`
	AssetID       *int  `validate:"omitempty,numeric"`
	IsPromotional *bool `validate:"omitempty"`
}

func NewUpdateLecture(idUser int, article *string, typeUpdate int, asset *multipart.FileHeader, lectureID int, videoType int, title *string, description *string, assetID *int, isPromotional *bool) (*UpdateLecture, *validator.ValidateError) {
	command := &UpdateLecture{idUser, article, typeUpdate, videoType, title, description, asset, lectureID, assetID, isPromotional}

	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	// Check Upload Video Type
	if command.TypeUpdate == courseConstants.UPLOAD_VIDEO_TYPE {
		if command.Asset == nil {
			valdiateErr := validator.ValidateError{
				ErrorValue: []*validator.CustomValidationError{
					{
						Field:   "asset",
						Message: customErrors.ErrAssetRequire.Error(),
					},
				},
				ErrorType: customErrors.ErrAssetRequire,
			}
			return nil, &valdiateErr
		}

		if command.AssetType != models.ResourceType && command.AssetType != models.WatchType {
			valdiateErr := validator.ValidateError{
				ErrorValue: []*validator.CustomValidationError{
					{
						Field:   "asset",
						Message: customErrors.ErrAssetTypeNotCorrect.Error(),
					},
				},
				ErrorType: customErrors.ErrAssetTypeNotCorrect,
			}
			return nil, &valdiateErr
		}

		if command.AssetType == models.WatchType {
			errValidate = checkVideoUpload(command.Asset)

			if errValidate != nil && errValidate.ErrorType != nil {
				return nil, errValidate
			}
		}
	}

	return command, nil
}

func checkVideoUpload(video *multipart.FileHeader) *validator.ValidateError {
	allowFileType := []string{"video/mp4"}
	if video.Size > 1048576*100 {
		valdiateErr := validator.ValidateError{
			ErrorValue: []*validator.CustomValidationError{
				{
					Field:   "video",
					Message: customErrors.ErrVideoToMax.Error(),
				},
			},
			ErrorType: customErrors.ErrVideoToMax,
		}
		return &valdiateErr
	}

	isSupportType := false
	for _, v := range allowFileType {
		if v == video.Header["Content-Type"][0] {
			isSupportType = true
		}
	}
	if !isSupportType {
		valdiateErr := validator.ValidateError{
			ErrorValue: []*validator.CustomValidationError{
				{
					Field:   "video",
					Message: customErrors.ErrNotVideoType.Error(),
				},
			},
			ErrorType: customErrors.ErrNotVideoType,
		}
		return &valdiateErr
	}
	return nil
}
