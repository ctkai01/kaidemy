package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"io"

	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_lecture/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	courseConstants "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/constants"
	grpcService "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
)

type UpdateLectureHandler struct {
	log                  logger.Logger
	curriculumRepository data.CurriculumRepository
	lectureRepository    data.LectureRepository
	assetRepository      data.AssetRepository
	courseRepository     data.CourseRepository
	grpcClient           grpc.GrpcClient
}

func NewUpdateLectureHandler(
	log logger.Logger,
	curriculumRepository data.CurriculumRepository,
	lectureRepository data.LectureRepository,
	assetRepository data.AssetRepository,
	courseRepository data.CourseRepository,
	grpcClient grpc.GrpcClient,

) *UpdateLectureHandler {
	return &UpdateLectureHandler{log: log, courseRepository: courseRepository, curriculumRepository: curriculumRepository, lectureRepository: lectureRepository, assetRepository: assetRepository, grpcClient: grpcClient}
}

func (c *UpdateLectureHandler) Handle(ctx context.Context, command *UpdateLecture) (*dtos.UpdateLectureResponseDto, error) {
	lecture, err := c.lectureRepository.GetLectureByIDRelation(ctx, []string{"Assets"}, command.LectureID)

	if err != nil {
		return nil, err
	}

	if lecture == nil {
		return nil, customErrors.ErrLectureNotFound
	}

	if lecture.Type != constants.LECTURE_TYPE {
		return nil, customErrors.ErrMustLecture
	}

	curriculum, err := c.curriculumRepository.GetCurriculumByIDRelation(ctx, []string{"Course"}, lecture.CurriculumID)

	if err != nil {
		return nil, err
	}

	if curriculum == nil {
		return nil, customErrors.ErrCurriculumNotFound
	}

	if curriculum.Course.UserID != command.IdUser {
		return nil, customErrors.ErrorCannotPermission
	}

	if command.TypeUpdate == courseConstants.REMOVE_VIDEO_TYPE {
		if command.AssetType == models.WatchType {
			var assetWatchType *models.Asset

			for _, lectureItem := range lecture.Assets {
				if lectureItem.Type == models.WatchType {
					assetWatchType = lectureItem
				}
			}

			if assetWatchType == nil {
				return nil, customErrors.ErrorWatchVideoNotExist
			}

			connUpload, err := c.grpcClient.GetGrpcConnection("upload_service")
			if err != nil {
				return nil, err
			}

			clientUpload := grpcService.NewUploadsServiceClient(connUpload)

			if _, err := clientUpload.DeleteVideo(ctx, &grpcService.DeleteVideoReq{
				VideoID: assetWatchType.BunnyID,
			}); err != nil {
				return nil, err
			}

			if err := c.assetRepository.DeleteAssetByID(ctx, assetWatchType.ID); err != nil {
				return nil, err
			}
		}

		if command.AssetType == models.ResourceType {
			var assetResourceType *models.Asset

			for _, lectureItem := range lecture.Assets {
				if lectureItem.Type == models.ResourceType && lectureItem.ID == *command.AssetID {
					assetResourceType = lectureItem
				}
			}

			if assetResourceType == nil {
				return nil, customErrors.ErrorResourceAssetNotExist
			}

			connUpload, err := c.grpcClient.GetGrpcConnection("upload_service")
			if err != nil {
				return nil, err
			}

			clientUpload := grpcService.NewUploadsServiceClient(connUpload)

			if _, err := clientUpload.DeleteResource(ctx, &grpcService.DeleteResourceReq{
				Url: assetResourceType.URL,
			}); err != nil {
				return nil, err
			}

			if err := c.assetRepository.DeleteAssetByID(ctx, assetResourceType.ID); err != nil {
				return nil, err
			}
		}
	}

	if command.TypeUpdate == courseConstants.UPLOAD_ARTICLE_TYPE {
		lecture.Article = command.Article

		if command.Title != nil {
			lecture.Title = *command.Title
		}

		if command.IsPromotional != nil {
			lecture.IsPromotional = *command.IsPromotional
		}
		lecture.Description = command.Description

		if err := c.lectureRepository.UpdateLecture(ctx, lecture); err != nil {
			return nil, err
		}
		c.log.Info("Update lecture success: ")
	}

	if command.TypeUpdate == courseConstants.UPLOAD_VIDEO_TYPE {
		if command.AssetType == models.WatchType {
			var assetWatchType *models.Asset

			for _, lectureItem := range lecture.Assets {
				if lectureItem.Type == models.WatchType {
					assetWatchType = lectureItem
				}
			}

			if assetWatchType != nil {
				return nil, customErrors.ErrorLimitWatchVideo
			}

			//Upload GRPC
			connUpload, err := c.grpcClient.GetGrpcConnection("upload_service")
			if err != nil {
				return nil, err
			}

			clientUpload := grpcService.NewUploadsServiceClient(connUpload)
			stream, err := clientUpload.UploadVideo(ctx)

			if err != nil {
				return nil, err
			}
			file, err := command.Asset.Open()

			if err != nil {
				return nil, err
			}

			buf := make([]byte, 1048576)
			batchNumber := 1
			for {
				num, err := file.Read(buf)
				if err == io.EOF {
					break
				}
				if err != nil {
					return nil, err
				}
				chunk := buf[:num]
				if err := stream.Send(&grpcService.UploadVideoReq{
					Title:     lecture.Title,
					Data:      chunk,
					Extension: command.Asset.Header["Content-Type"][0],
					Size:      int64(len(chunk)),
				}); err != nil {
					return nil, err
				}
				c.log.Info("Sent - batch #%v - size - %v\n", batchNumber, len(chunk))
				batchNumber += 1

			}

			res, err := stream.CloseAndRecv()
			if err != nil {
				return nil, err
			}

			c.log.Info("URL: ", res.GetUrl())

			assetCreate := &models.Asset{
				URL:       res.GetUrl(),
				Type:      models.WatchType,
				LectureID: command.LectureID,
				BunnyID:   res.GetVideoID(),
				Size:      res.GetSize(),
				Duration:  res.GetDuration(),
				Name:      command.Asset.Filename,
			}

			if err := c.assetRepository.CreateAsset(ctx, assetCreate); err != nil {
				return nil, err
			}
		}

		if command.AssetType == models.ResourceType {
			countAssetResourceType := 0

			for _, lectureItem := range lecture.Assets {
				if lectureItem.Type == models.ResourceType {
					countAssetResourceType++
				}
			}

			if countAssetResourceType >= 2 {
				return nil, customErrors.ErrorLimitResourceAsset
			}

			//Upload GRPC
			connUpload, err := c.grpcClient.GetGrpcConnection("upload_service")
			if err != nil {
				return nil, err
			}

			clientUpload := grpcService.NewUploadsServiceClient(connUpload)
			stream, err := clientUpload.UploadResource(ctx)

			if err != nil {
				return nil, err
			}
			file, err := command.Asset.Open()

			if err != nil {
				return nil, err
			}

			var sizeFile int64
			buf := make([]byte, 1048576)
			batchNumber := 1
			for {
				num, err := file.Read(buf)
				if err == io.EOF {
					break
				}
				if err != nil {
					return nil, err
				}
				chunk := buf[:num]
				// fileNameTemp := strings.Split(command.Asset.Filename, "/")
				// fileName := fileNameTemp[len(fileNameTemp)-1]
				// fmt.Println("File name 1: ", fileName)
				// fmt.Println("File name: ", command.Asset.Filename)
				sizeFile = int64(len(chunk))
				if err := stream.Send(&grpcService.UploadResourceReq{
					Title:     command.Asset.Filename,
					Data:      chunk,
					Type:      int32(constants.ResourceType),
					Extension: command.Asset.Header["Content-Type"][0],
					Size:      int64(len(chunk)),
					Name:      command.Asset.Filename,
				}); err != nil {
					return nil, err
				}
				c.log.Info("Sent - batch #%v - size - %v\n", batchNumber, len(chunk))
				batchNumber += 1

			}

			res, err := stream.CloseAndRecv()
			c.log.Info("Err: ", err)

			if err != nil {
				return nil, err
			}
			c.log.Info("URL: ", res.GetUrl())

			assetCreate := &models.Asset{
				URL:       res.GetUrl(),
				Type:      models.ResourceType,
				LectureID: command.LectureID,
				Size:      sizeFile,
				Name:      command.Asset.Filename,
			}

			if err := c.assetRepository.CreateAsset(ctx, assetCreate); err != nil {
				return nil, err
			}
		}
	}

	lecture, err = c.lectureRepository.GetLectureByIDRelation(ctx, []string{"Assets"}, command.LectureID)

	if err != nil {
		return nil, err
	}

	course := curriculum.Course
	course.ReviewStatus = constants.REVIEW_INIT_STATUS
	if err := c.courseRepository.UpdateCourse(ctx, &course); err != nil {
		return nil, err
	}

	return &dtos.UpdateLectureResponseDto{
		Message: "Update lecture successfully!",
		Lecture: *lecture,
	}, nil
}
