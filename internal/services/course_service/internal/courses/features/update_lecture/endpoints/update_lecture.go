package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/params"
	coursesError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_lecture/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_lecture/dtos"
)

type updateLectureEndpoint struct {
	params.LecturesRouteParams
}

func NewUpdateLectureEndpoint(
	params params.LecturesRouteParams,
) route.Endpoint {
	return &updateLectureEndpoint{
		LecturesRouteParams: params,
	}
}

func (ep *updateLectureEndpoint) MapEndpoint() {
	ep.LecturesGroup.PUT("/:id", ep.handler())
}

func (ep *updateLectureEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.UpdateLectureRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[UpdateLecture.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[UpdateLectureEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}
		userId := utils.GetUserId(c)
	
		file, _ := c.FormFile("asset")

		// if err != nil {
		// 	return customErrors.NewInternalServerErrorHttp(c, err.Error())
		// }

		command, errValidate := commands.NewUpdateLecture(
			userId,
			request.Article,
			request.Type,
			file,
			request.LectureID,
			request.AssetType,
			request.Title,
			request.Description,
			request.AssetID,
			request.IsPromotional,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[UpdateLectureEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[UpdateLectureEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.UpdateLecture, *dtos.UpdateLectureResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == coursesError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[UpdateLectureEndpoint_handler.Send] error in sending Update Lecture",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[UpdateLecture.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[UpdateLectureEndpoint_handler.Send] error in sending Update Lecture",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[UpdateLecture.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
