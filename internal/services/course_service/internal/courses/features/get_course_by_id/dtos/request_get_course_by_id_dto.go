package dtos

//https://echo.labstack.com/guide/response/

type GetCourseByIdRequestDto struct {
	CourseID int `json:"-" param:"id"`
	TypeCourse *int `json:"-" query:"type"`
}
