package dtos

import "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

//https://echo.labstack.com/guide/response/

type GetCourseByIdResponseDto struct {
	Message string         `json:"message"`
	Course  *models.Course `json:"course"`
}
