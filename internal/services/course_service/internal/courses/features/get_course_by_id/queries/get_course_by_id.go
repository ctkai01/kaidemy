package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetCourseById struct {
	IdCourse int `validate:"required,numeric"`
	TypeCourse *int `validate:"omitempty,numeric,oneof=1 2"`
}

func NewGetCourseById(idCourse int, typeCourse *int) (*GetCourseById, *validator.ValidateError) {
	query := &GetCourseById{idCourse, typeCourse}
	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return query, nil
}
