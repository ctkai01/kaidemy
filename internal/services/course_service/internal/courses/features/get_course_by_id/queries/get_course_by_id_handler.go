package queries

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_course_by_id/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/course/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// userservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type GetCourseByIdHandler struct {
	log              logger.Logger
	courseRepository data.CourseRepository
	grpcClient       grpc.GrpcClient
}

func NewGetCourseByIdHandler(
	log logger.Logger,
	courseRepository data.CourseRepository,
	grpcClient grpc.GrpcClient,

) *GetCourseByIdHandler {
	return &GetCourseByIdHandler{log: log, courseRepository: courseRepository, grpcClient: grpcClient}
}

func (c *GetCourseByIdHandler) Handle(ctx context.Context, command *GetCourseById) (*dtos.GetCourseByIdResponseDto, error) {
	if command.TypeCourse == nil {
		command.TypeCourse = &constants.GET_DETAIL_TITLE_TYPE
	}
	var course *models.Course
	var err error
	if *command.TypeCourse == constants.GET_DETAIL_TITLE_TYPE {
		course, err = c.courseRepository.GetCourseByIDRelation(ctx, []string{"Curriculums", "Curriculums.Lectures"}, command.IdCourse)
	}

	if *command.TypeCourse == constants.GET_DETAIL_TYPE {
		course, err = c.courseRepository.GetCourseByID(ctx, command.IdCourse)
	}

	if err != nil {
		return nil, err
	}

	if course == nil {
		return nil, customErrors.ErrCourseNotFound
	}

	return &dtos.GetCourseByIdResponseDto{
		Message: "Get course successfully",
		Course:  course,
	}, nil

}
