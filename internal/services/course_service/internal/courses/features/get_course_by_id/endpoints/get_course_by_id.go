package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/params"
	coursesError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_course_by_id/dtos"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_course_by_id/queries"
)

type getCourseByIdEndpoint struct {
	params.CoursesRouteParams
}

func NewGetCourseByIdEndpoint(
	params params.CoursesRouteParams,
) route.Endpoint {
	return &getCourseByIdEndpoint{
		CoursesRouteParams: params,
	}
}

func (ep *getCourseByIdEndpoint) MapEndpoint() {
	ep.CoursesGroup.GET("/:id", ep.handler())
}

func (ep *getCourseByIdEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.GetCourseByIdRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[getCourseById.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getCourseByIdEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())
		}

		// userId := utils.GetUserId(c)

		command, errValidate := queries.NewGetCourseById(
			request.CourseID,
			request.TypeCourse,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[getCourseByIdEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getCourseByIdEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*queries.GetCourseById, *dtos.GetCourseByIdResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == coursesError.WrapError(customErrors.ErrCourseNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[getCourseEndpoint_handler.Send] error in sending get Course",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[getCourse.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[getCourseByIdEndpoint_handler.Send] error in sending get category by id",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[getCourseById.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
