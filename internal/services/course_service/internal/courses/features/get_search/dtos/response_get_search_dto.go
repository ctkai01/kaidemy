package dtos

// import "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"
import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"
)
type CourseSearch struct {
	ID int `json:"id"`
	Title string `json:"title"`
	Image *string `json:"image"`
	NameAuthor string `json:"name_author"` 
}

type UserSearch struct {
	ID int `json:"id"`
	Name string `json:"name"`
	Avatar string `json:"avatar"`

}

type SearchItem struct {
	Course *CourseSearch `json:"course"`
	Teacher *UserSearch `json:"teacher"`
}


type GetSearchResponseDto struct {
	Message string `json:"message"`
	Result *utils.ListResult[*SearchItem] `json:"result"`
}
