package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/params"
	coursesError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_search/queries"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_search/dtos"
)

type getSearchEndpoint struct {
	params.CoursesNoAuthRouteParams
}

func NewGetSearchEndpoint(
	params params.CoursesNoAuthRouteParams,
) route.Endpoint {
	return &getSearchEndpoint{
		CoursesNoAuthRouteParams: params,
	}
}

func (ep *getSearchEndpoint) MapEndpoint() {
	ep.CoursesGroup.GET("/search-global", ep.handler())
}

func (ep *getSearchEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		listQuery, err := utils.GetListQueryFromCtx(c)
		fmt.Println("Query: ", listQuery)
		
		if err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[getSearch.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getSearchEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())
		}
		
		request := &dtos.GetSearchRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[getSearch.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getSearchEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}
		// fmt.Println("request level: ", request.Level)
		// fmt.Println("request duration: ", request.Duration)
		// fmt.Println("request rating: ", request.Rating)
		// userId := utils.GetUserId(c)

		query, errValidate := queries.NewGetSearch(
			request.Search,
			listQuery,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[getSearch_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getSearch_handler.StructCtx] err: {%v}", validationErr),
			)

			return customErrors.NewBadRequestErrorHttp(c, validationErr.Error())

		}

		result, err := mediatr.Send[*queries.GetSearch, *dtos.GetSearchResponseDto](
			ctx,
			query,
		)

		if err != nil {
			if err.Error() == coursesError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[getSearch_handler.Send] error in sending create Answer",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[getSearch.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[getSearchEndpoint_handler.Send] error in sending get topic learnings",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[getSearch.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
