package queries

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// "fmt"
	"math"

	// "fmt"
	// "strings"

	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"

	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_search/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	courseservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type GetSearchHandler struct {
	log              logger.Logger
	courseRepository data.CourseRepository
	grpcClient       grpc.GrpcClient
}

func NewGetSearchHandler(
	log logger.Logger,
	courseRepository data.CourseRepository,
	grpcClient grpc.GrpcClient,

) *GetSearchHandler {
	return &GetSearchHandler{
		log:              log,
		courseRepository: courseRepository,
		grpcClient:       grpcClient,
	}
}

func (c *GetSearchHandler) Handle(ctx context.Context, command *GetSearch) (*dtos.GetSearchResponseDto, error) {

	// conn, err := c.grpcClient.GetGrpcConnection("catalog_service")
	// if err != nil {
	// 	return nil, err
	// }
	// client := courseservice.NewCatalogsServiceClient(conn)

	// category, err := client.GetCategoryByID(context.Background(), &courseservice.GetCategoryByIdReq{
	// 	CategoryId: int32(command.IdCategory),
	// })

	// if err != nil {
	// 	return nil, err
	// }

	// categoryResponse:= &models.CategoryShow{
	// 	ID: int(category.Category.ID),
	// 	Name: category.Category.Name,
	// }

	courses, err := c.courseRepository.GetCoursesBySearchNormalPaginate(ctx, command.search, command.ListQuery)
	if err != nil {
		return nil, err
	}

	connUser, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}
	clientUser := courseservice.NewUsersServiceClient(connUser)
	var searchData []*dtos.SearchItem
	// fmt.Println("courses: ", len(courses.Items))
	for _, course := range courses.Items {
		//Get data user service
		dataUser, err := clientUser.GetUserByID(context.Background(), &courseservice.GetUserByIdReq{
			UserId: int32(course.UserID),
		})
		if err != nil {
			return nil, err
		}

		searchItem := &dtos.SearchItem{
			Course: &dtos.CourseSearch{
				ID:         course.ID,
				Title:      course.Title,
				NameAuthor: dataUser.User.Name,
				Image: course.Image,
			},
		}
		searchData = append(searchData, searchItem)
	}

	usersSearch, err := clientUser.GetTeachers(context.Background(), &courseservice.GetTeachersReq{
		Search: command.search,
	})
	if err != nil {
		return nil, err
	}

	for _, user := range usersSearch.Users {
		searchItem := &dtos.SearchItem{
			Teacher: &dtos.UserSearch{
				ID:   int(user.ID),
				Name: user.Name,
				Avatar: user.Avatar,
			},
		}
		searchData = append(searchData, searchItem)
	}

	totalItem := int64(len(searchData))
	
	totalPage := 0
	if len(searchData) > 0 {
		//Define Offset
		offset := 0

		if command.ListQuery.Page == 0 {
			offset = 0
		} else {
			offset = (command.ListQuery.Page - 1) * command.ListQuery.Size
		}

		if offset >= len(searchData) {
			offset = len(searchData) - 1
		}

		limit := command.ListQuery.Size

		endIndex := offset + limit

		if endIndex > len(searchData) {
			endIndex = len(searchData)
		}

		searchData = searchData[offset:endIndex]
		d := float64(totalItem) / float64(command.ListQuery.Size)

		totalPage = int(math.Ceil(d))

		result := &utils.ListResult[*dtos.SearchItem]{
			Size:       courses.Size,
			Page:       command.ListQuery.Page,
			TotalItems: totalItem,
			TotalPage:  totalPage,
			Items:      searchData,
		}
		
	
		return &dtos.GetSearchResponseDto{
			Message:       "Get data successfully!",
			Result:       result,
		}, nil
	}
	result := &utils.ListResult[*dtos.SearchItem]{
		Size:       courses.Size,
		Page:       command.ListQuery.Page,
		TotalItems: totalItem,
		TotalPage:  totalPage,
		Items:      searchData,
	}
	// CourseResponse
	return &dtos.GetSearchResponseDto{
		Message: "Get data successfully!",
		Result: result,
	}, nil
}
