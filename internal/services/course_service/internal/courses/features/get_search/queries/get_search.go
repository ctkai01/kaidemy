package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetSearch struct {
	
	search string `validate:"required"`
	// IdUserCall int
	// Content int
	*utils.ListQuery
}

func NewGetSearch(
	search string,
	// idUserCall int,
	// content int,
	query *utils.ListQuery,

) (*GetSearch, *validator.ValidateError) {
	q := &GetSearch{
		// idUser,
		// idUserCall,
		// content,
		search,
		query,
	}
	
	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
