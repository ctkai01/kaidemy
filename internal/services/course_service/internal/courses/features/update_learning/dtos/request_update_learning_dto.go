package dtos

//https://echo.labstack.com/guide/response/

type UpdateLearningRequestDto struct {
	LearningID int `json:"-" param:"id"`

	Process   *int    `form:"process"`
	Type      int     `form:"type"`
	StarCount *int    `form:"star_count"`
	Comment   *string `form:"comment"`
}
