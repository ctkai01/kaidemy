package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/params"
	coursesError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_learning/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_learning/dtos"
)

type updateLeaningEndpoint struct {
	params.LearningsRouteParams
}

func NewUpdateLearningEndpoint(
	params params.LearningsRouteParams,
) route.Endpoint {
	return &updateLeaningEndpoint{
		LearningsRouteParams: params,
	}
}

func (ep *updateLeaningEndpoint) MapEndpoint() {
	ep.LearningsGroup.PUT("/:id", ep.handler())
}

func (ep *updateLeaningEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.UpdateLearningRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[UpdateLearning.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[UpdateLearningEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}

		userId := utils.GetUserId(c)

		command, errValidate := commands.NewUpdateLearning(
			userId,
			request.LearningID,
			request.Process,
			request.Type,
			request.StarCount,
			request.Comment,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[UpdateLearningEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[UpdateLearningEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.UpdateLearning, *dtos.UpdateLearningResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == coursesError.WrapError(customErrors.ErrorCannotPermission).Error() {
				errCustom := errors.WithMessage(
					err,
					"[UpdateLearningEndpoint_handler.Send] error in sending Update Course User",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[UpdateLearning.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewForbiddenErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[UpdateLearningEndpoint_handler.Send] error in sending Update Answer",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[UpdateLearning.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
