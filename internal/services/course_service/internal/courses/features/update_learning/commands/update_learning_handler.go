package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_learning/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type UpdateLearningHandler struct {
	log logger.Logger
	// curriculumRepository data.CurriculumRepository
	// quizRepository       data.QuizRepository
	// questionRepository   data.QuestionRepository
	// answerRepository     data.AnswerRepository
	learningRepository data.LearningRepository
	grpcClient           grpc.GrpcClient
}

func NewUpdateLearningHandler(
	log logger.Logger,
	learningRepository data.LearningRepository,
	grpcClient grpc.GrpcClient,

) *UpdateLearningHandler {
	return &UpdateLearningHandler{
		log: log,
		grpcClient:           grpcClient,
		learningRepository: learningRepository,
	}
}

func (c *UpdateLearningHandler) Handle(ctx context.Context, command *UpdateLearning) (*dtos.UpdateLearningResponseDto, error) {
	learning, err := c.learningRepository.GetLearningByID(ctx, command.LearningID)

	if err != nil {
		return nil, err
	}

	if learning.UserID != command.IdUser {
		return nil, customErrors.ErrorCannotPermission
	}

	if command.Process != nil {
		learning.Process = command.Process
	}

	if command.StarCount != nil {
		learning.StarCount = command.StarCount
	}

	if command.Comment != nil {
		if *command.Comment == "" {
			learning.Comment = nil
		} else {
			learning.Comment = command.Comment
		}
	}
	
	if learning.Type == constants.WISH_LIST_TYPE {
		return nil, customErrors.ErrCannotUpdateToWishList
	}

	learning.Type = command.TypeLearning

	if err := c.learningRepository.UpdateLearning(ctx, learning); err != nil {
		return nil, err
	}

	return &dtos.UpdateLearningResponseDto{
		Message: "Update learning successfully!",
		Learning: learning,
	}, nil
}
