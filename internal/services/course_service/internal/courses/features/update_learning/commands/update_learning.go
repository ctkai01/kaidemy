package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type UpdateLearning struct {
	IdUser     int
	LearningID int `validate:"required,numeric"`

	Process      *int `validate:"omitempty,gte=1,lte=100"`
	TypeLearning int  `validate:"required,gte=1,lte=3"`
	StarCount    *int `validate:"omitempty,gte=1,lte=5"`

	Comment *string `validate:"omitempty,gte=0,lte=200"`
}

func NewUpdateLearning(
	idUser int,
	learningID int,
	process *int,
	typeLearning int,
	starCount *int,
	comment *string,
) (*UpdateLearning, *validator.ValidateError) {
	command := &UpdateLearning{
		idUser,
		learningID,
		process,
		typeLearning,
		starCount,
		comment,
	}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
