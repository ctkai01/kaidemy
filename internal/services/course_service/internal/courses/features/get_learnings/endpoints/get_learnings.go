package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/params"
	// coursesError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_learnings/queries"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_learnings/dtos"
)

type getLearningsEndpoint struct {
	params.LearningsRouteParams
}

func NewGetLearningsEndpoint(
	params params.LearningsRouteParams,
) route.Endpoint {
	return &getLearningsEndpoint{
		LearningsRouteParams: params,
	}
}

func (ep *getLearningsEndpoint) MapEndpoint() {
	ep.LearningsGroup.GET("", ep.handler())
}

func (ep *getLearningsEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		listQuery, err := utils.GetListQueryFromCtx(c)
		fmt.Println("Query: ", listQuery)
		
		if err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[getLearnings.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getLearningsEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())
		}
		fmt.Printf("Filters: %v\n", listQuery.Filters)

		userId := utils.GetUserId(c)
		// userIdStr := fmt.Sprintf("%d", userId)
	
		// listQuery.Filters = append(listQuery.Filters, &utils.FilterModel {
		// 	Field: "user_id",
		// 	Value: &userIdStr,
		// 	Comparison: "equals",
		// })

		query, errValidate := queries.NewGetLearnings(
			userId,
			listQuery,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[getLearnings_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getLearnings_handler.StructCtx] err: {%v}", validationErr),
			)

			return customErrors.NewBadRequestErrorHttp(c, validationErr.Error())

		}

		result, err := mediatr.Send[*queries.GetLearnings, *dtos.GetLearningsResponseDto](
			ctx,
			query,
		)

		if err != nil {
			// if err.Error() == coursesError.WrapError(customErrors.ErrUserNotFound).Error() {
			// 	errCustom := errors.WithMessage(
			// 		err,
			// 		"[RegisterNotificationToken_handler.Send] error in sending create Answer",
			// 	)
			// 	ep.Logger.Error(
			// 		fmt.Sprintf(
			// 			"[createAnswer.Send], err: %v",
			// 			errCustom,
			// 		),
			// 	)
			// 	return customErrors.NewBadRequestErrorHttp(c, err.Error())
			// }

			err = errors.WithMessage(
				err,
				"[GetLearningsEndpoint_handler.Send] error in sending get learnings",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[GetLearnings.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
