package dtos

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"
)

type GetLearningsResponseDto struct {
	Message   string                                  `json:"message"`
	Learnings *utils.ListResult[*models.LearningShow] `json:"learnings"`
}
