package queries

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"fmt"
	"strings"

	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_learnings/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"

	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	courseservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
)

type GetLearningsHandler struct {
	log                logger.Logger
	learningRepository data.LearningRepository
	grpcClient         grpc.GrpcClient
}

func NewGetLearningsHandler(
	log logger.Logger,
	learningRepository data.LearningRepository,
	grpcClient grpc.GrpcClient,

) *GetLearningsHandler {
	return &GetLearningsHandler{
		log:                log,
		learningRepository: learningRepository,
		grpcClient:         grpcClient,
	}
}

func (c *GetLearningsHandler) Handle(ctx context.Context, command *GetLearnings) (*dtos.GetLearningsResponseDto, error) {
	isWishList := false
	for _, filter := range command.Filters {
		if filter.Field == "type" && *filter.Value == fmt.Sprintf("%d", constants.WISH_LIST_TYPE) {
			isWishList = true
		}

	}
	fmt.Println("isWishList: ", isWishList)
	var learnings *utils.ListResult[*models.Learning]
	// if isWishList {
	dataLearnings, err := c.learningRepository.FindLearningsRelationPaginate(ctx, []string{"Course.Curriculums.Lectures.Assets", "Course.Learnings"}, command.ListQuery)
	learnings = dataLearnings
	if err != nil {
		return nil, err
	}

	connUser, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}
	clientUser := courseservice.NewUsersServiceClient(connUser)

	conn, err := c.grpcClient.GetGrpcConnection("catalog_service")
	if err != nil {
		return nil, err
	}
	client := courseservice.NewCatalogsServiceClient(conn)

	var learningShows = make([]*models.LearningShow, 0)

	if learnings.Items != nil {

		for _, learning := range learnings.Items {
			var levelID int
			fmt.Println("L;earnings course: ", learning.Course.Learnings)

			var totalReviewCountStar int
			var totalReviewCount int
			var totalStudent int

			for _, learningCourse := range learning.Course.Learnings {
				if learningCourse.StarCount != nil {
					totalReviewCountStar += *learningCourse.StarCount
					totalReviewCount++
				}

				if learningCourse.Type == constants.STANDARD_TYPE || learningCourse.Type == constants.ARCHIE {
					totalStudent++
				}
			}

			var averageReview float32

			if totalReviewCount != 0 {
				averageReview = float32(totalReviewCountStar) / float32(totalReviewCount)

			}

			if learning.Course.LevelID == nil {
				levelID = -1
			} else {
				levelID = *learning.Course.LevelID
			}

			dataLevel, err := client.GetLevelByID(context.Background(), &courseservice.GetLevelByIdReq{
				// UserId:  int32(command.IdUser),
				LevelId: int32(levelID),
			})

			if err != nil {
				if !strings.Contains(err.Error(), "level not found") {
					return nil, err

				}
			}

			//Get price
			var priceID int

			if learning.Course.PriceID == nil {
				priceID = -1
			} else {
				priceID = *learning.Course.PriceID
			}

			dataPrice, err := client.GetPriceByID(context.Background(), &courseservice.GetPriceByIdReq{
				PriceId: int32(priceID),
			})

			if err != nil {
				if !strings.Contains(err.Error(), "price not found") {
					return nil, err
				}
			}

			//Get data user service
			dataUser, err := clientUser.GetUserByID(context.Background(), &courseservice.GetUserByIdReq{
				UserId: int32(learning.Course.UserID),
			})
			if err != nil {
				return nil, err
			}

			var levelRes *models.LevelResponse

			if dataLevel == nil {
				levelRes = nil
			} else {
				levelRes = &models.LevelResponse{
					ID:   int(dataLevel.Level.ID),
					Name: dataLevel.Level.Name,
				}
			}

			var priceRes *models.PriceResponse

			if dataPrice == nil {
				priceRes = nil
			} else {
				priceRes = &models.PriceResponse{
					ID:    int(dataPrice.Price.ID),
					Tier:  dataPrice.Price.Tier,
					Value: int(dataPrice.Price.Value),
				}
			}
			// if !isWishList {
			var percent int
			markCount := 0
			totalLecture := 0
			for _, curriculum := range learning.Course.Curriculums {
				totalLecture += len(curriculum.Lectures)
				for _, lecture := range curriculum.Lectures {
					count, err := c.learningRepository.CountAssociationLectures(ctx, learning, &lecture)

					if err != nil {
						return nil, err
					}

					if count == 1 {
						markCount++
					}
				}
			}

			if totalLecture != 0 {
				percent = int(float64(markCount) / float64(totalLecture) * 100)
			}

			learningShows = append(learningShows, &models.LearningShow{
				ID:        learning.ID,
				UserID:    learning.UserID,
				CourseID:  learning.CourseID,
				Process:   &percent,
				Type:      learning.Type,
				StarCount: learning.StarCount,
				Comment:   learning.Comment,
				Course: models.CourseLearning{
					ID:           learning.Course.ID,
					Title:        learning.Course.Title,
					Status:       learning.Course.Status,
					ReviewStatus: learning.Course.ReviewStatus,
					Image:        learning.Course.Image,
					Curriculums:  learning.Course.Curriculums,
					Level:        levelRes,
					Price:        priceRes,
					Subtitle:     learning.Course.Subtitle,
					OutComes:     learning.Course.OutComes,
					User: models.UserResponse{
						ID:   int(dataUser.User.ID),
						Name: dataUser.User.Name,
					},
					UpdatedAt: learning.Course.UpdatedAt,
					CreatedAt: learning.Course.CreatedAt,
				},
				AverageReview: &averageReview,
				CountReview:   &totalReviewCount,
				UpdatedAt:     learning.UpdatedAt,
				CreatedAt:     learning.CreatedAt,
			})

		}
	}
	// learnings.Items = learningShows
	learningResult := &utils.ListResult[*models.LearningShow]{
		Size:       learnings.Size,
		Page:       learnings.Page,
		TotalItems: learnings.TotalItems,
		TotalPage:  learnings.TotalPage,
		Items:      learningShows,
	}

	// listResultDto, err := utils.ListResultToListResultDto[*models.LearningShow](learningResult)

	// if err != nil {
	// 	return nil, err
	// }

	return &dtos.GetLearningsResponseDto{
		Message:   "Get learnings successfully!",
		Learnings: learningResult,
	}, nil
}
