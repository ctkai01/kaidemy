package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetLearnings struct {
	IdUser int
	*utils.ListQuery
}

func NewGetLearnings(
	idUser int,
	query *utils.ListQuery,

) (*GetLearnings, *validator.ValidateError) {
	q := &GetLearnings{
		idUser,
		query,
	}
	
	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
