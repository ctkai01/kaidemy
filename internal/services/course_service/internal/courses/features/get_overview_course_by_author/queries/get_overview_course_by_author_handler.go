package queries

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"fmt"
	"math"
	"time"

	// "math"
	"strconv"
	// "strings"

	// "strings"

	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"

	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"
	stripe "github.com/stripe/stripe-go/v76"
	stripeBalanceTransaction "github.com/stripe/stripe-go/v76/balancetransaction"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_overview_course_by_author/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	courseservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type GetOverviewCourseByAuthorHandler struct {
	log                logger.Logger
	courseRepository   data.CourseRepository
	learningRepository data.LearningRepository
	grpcClient         grpc.GrpcClient
}

func NewGetOverviewCourseByAuthorHandler(
	log logger.Logger,
	courseRepository data.CourseRepository,
	learningRepository data.LearningRepository,
	grpcClient grpc.GrpcClient,

) *GetOverviewCourseByAuthorHandler {
	return &GetOverviewCourseByAuthorHandler{
		log:                log,
		courseRepository:   courseRepository,
		learningRepository: learningRepository,
		grpcClient:         grpcClient,
	}
}

func (c *GetOverviewCourseByAuthorHandler) Handle(ctx context.Context, command *GetOverviewCourseByAuthor) (*dtos.GetOverviewCourseByAuthorResponseDto, error) {
	// if command.IdUser != command.IdUserCall {
	// 	return nil, customErrors.ErrorCannotPermission
	// }
	currentMonth := time.Now().Month()

	userConn, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}

	clientConn := courseservice.NewUsersServiceClient(userConn)

	userData, err := clientConn.GetUserByID(ctx, &courseservice.GetUserByIdReq{
		UserId: int32(command.IdUser),
	})
	if err != nil {
		return nil, err
	}
	stripe.Key = "sk_test_51NUmJOFEsDSfMpHunO8Zscoo1KwMISvWpj00QwhHnLTjYQVkQF1uH0WBgCd8pWgfd5gAKBguz9MfszFPlQwOVPXl00ZUdeBaS5"

	// productStripeParams := &stripe.BalanceTransactionListParams{
	// 	// CreatedRange: &stripe.RangeQueryParams{},
	// 	Source:       &userData.User.AccountStripeID,

	// }
	typeTransaction := "payment"

	// Get the Unix timestamp
	startYear := time.Date(time.Now().Year(), 1, 1, 0, 0, 0, 0, time.Local)

	params := &stripe.BalanceTransactionListParams{
		ListParams: stripe.ListParams{
			StripeAccount: &userData.User.AccountStripeID,
			Single:        true,
		},
		CreatedRange: &stripe.RangeQueryParams{
			LesserThanOrEqual: time.Now().Unix(),
			GreaterThan:       startYear.Unix(),
		},
		Type: &typeTransaction,
	}
	resultTx := stripeBalanceTransaction.List(params)

	monthlyNetCount := make([]float32, 12)
	monthlyFeeCount := make([]float32, 12)
	var totalNetThisMonth float32
	var totalNet float32
	var totalFeeThisMonth float32
	var totalFee float32

	var totalRevenue float32
	currency := ""
	for resultTx.Next() {
		monthCreated := time.Unix(resultTx.BalanceTransaction().Created, 0).Month()
		netValue := float32(resultTx.BalanceTransaction().Net) / float32(100)
		feeValue := float32(resultTx.BalanceTransaction().Fee) / float32(100)

		
		if monthCreated == currentMonth {
			totalNetThisMonth += netValue
			totalFeeThisMonth += feeValue
		}
		totalRevenue = totalRevenue + netValue
		totalNet +=  netValue
		totalFee +=  feeValue

		if currency == "" {
			currency = string(resultTx.BalanceTransaction().Currency)
		}
		monthlyNetCount[monthCreated-1] = monthlyNetCount[monthCreated-1] + netValue
		monthlyFeeCount[monthCreated-1] = monthlyFeeCount[monthCreated-1] + feeValue
	}

	for i, item := range  monthlyNetCount {
		monthlyNetCount[i] = float32(math.Ceil(float64(item) * 100) / 100)
	}

	for i, item := range  monthlyFeeCount {
		monthlyFeeCount[i] = float32(math.Ceil(float64(item) * 100) / 100)
	}

	revenue := &dtos.Revenue{
		Total: float32(math.Ceil(float64(totalRevenue) * 100) / 100),
		Fee:   &dtos.RevenueStats{
			Total: float32(math.Ceil(float64(totalFee) * 100) / 100),
			TotalThisMonth: float32(math.Ceil(float64(totalFeeThisMonth) * 100) / 100),
			DetailStats: monthlyFeeCount,
		},
		Net: &dtos.RevenueStats{
			Total: float32(math.Ceil(float64(totalNet) * 100) / 100),
			TotalThisMonth: float32(math.Ceil(float64(totalNetThisMonth) * 100) / 100),
			DetailStats: monthlyNetCount,
		},
		Currency: currency,
	}
	
	courseIDsByAuthor, err := c.courseRepository.FindIdsCourseAuthor(ctx, command.IdUser)

	if err != nil {
		return nil, err
	}

	var courseIDFilter []int
	if command.CourseID != nil {
		n, _ := strconv.Atoi(*command.CourseID)
		isExist := false
		for _, id := range courseIDsByAuthor {
			if id == n {
				isExist = true
				break
			}
		}

		if !isExist {
			return nil, customErrors.ErrorCannotPermission
		}

		courseIDFilter = append(courseIDFilter, n)
	} else {
		courseIDFilter = courseIDsByAuthor
	}

	learnings, err := c.learningRepository.GetEnrollsByCourseIDsAuthorThisYear(ctx, courseIDFilter, command.IdUser)

	if err != nil {
		return nil, err
	}

	// var usersCourse []*dtos.UserCourseAuthorReview

	// connUser, err := c.grpcClient.GetGrpcConnection("user_service")
	// if err != nil {
	// 	return nil, err
	// }
	// clientUser := courseservice.NewUsersServiceClient(connUser)
	monthlyEnrollmentCount := make([]int, 12)
	monthlyRatingCount := make([]int, 12)
	totalRatings := 0
	totalStarRatings := 0
	totalRatingsThisMonth := 0
	totalEnrollmentThisMonth := 0
	for _, learning := range learnings {
		if learning.CreatedAt != nil {
			month := learning.CreatedAt.Month()
			if month == currentMonth {
				totalEnrollmentThisMonth++
			}
			monthlyEnrollmentCount[month-1]++ // Adjusting month index to start from 0
		}

		if learning.StarCount != nil && learning.UpdatedAt != nil {
			month := learning.UpdatedAt.Month()
			if month == currentMonth {
				totalRatingsThisMonth++
			}
			monthlyRatingCount[month-1]++ // Adjusting month index to start from 0
			totalRatings++
			totalStarRatings += *learning.StarCount
		}
	}

	var averageRating float32
	if totalRatings != 0 {
		averageRating = float32(totalStarRatings) / float32(totalRatings)

	}

	result := &dtos.OverviewCourseAuthor{
		Enrollments: &dtos.EnrollmentStats{
			Total:          len(learnings),
			TotalThisMonth: totalEnrollmentThisMonth,
			DetailStats:    monthlyEnrollmentCount,
		},
		Ratings: &dtos.RatingStats{
			Total:          roundFloat32(averageRating, 2),
			TotalThisMonth: totalRatingsThisMonth,
			DetailStats:    monthlyRatingCount,
		},
		Revenue: revenue,
	}
	// 	if strings.Contains(dataUser.User.Name, command.Search) {
	// 		usersCourse = append(usersCourse, &dtos.UserCourseAuthorReview{
	// 			ID:     int(dataUser.User.ID),
	// 			Name:   dataUser.User.Name,
	// 			Avatar: &dataUser.User.Avatar,
	// 		})
	// 	}
	// }

	// totalPage := 0
	// totalItem := int64(len(usersCourse))
	// if len(usersCourse) > 0 {
	// 	//Define Offset
	// 	offset := 0

	// 	if command.ListQuery.Page == 0 {
	// 		offset = 0
	// 	} else {
	// 		offset = (command.ListQuery.Page - 1) * command.ListQuery.Size
	// 	}

	// 	if offset >= len(usersCourse) {
	// 		offset = len(usersCourse) - 1
	// 	}

	// 	limit := command.ListQuery.Size

	// 	endIndex := offset + limit

	// 	if endIndex > len(usersCourse) {
	// 		endIndex = len(usersCourse)
	// 	}

	// 	usersCourse = usersCourse[offset:endIndex]
	// 	d := float64(totalItem) / float64(command.ListQuery.Size)

	// 	totalPage = int(math.Ceil(d))

	// 	result := &utils.ListResult[*dtos.UserCourseAuthorReview]{
	// 		Size:       command.ListQuery.Size,
	// 		Page:       command.ListQuery.Page,
	// 		TotalItems: totalItem,
	// 		TotalPage:  totalPage,
	// 		Items:      usersCourse,
	// 	}

	// 	// fmt.Println("totalItem: ", totalItem)
	// 	// fmt.Println("command.ListQuery.Size: ", command.ListQuery.Size)
	// 	// fmt.Println("totalPage: ", totalPage)

	// 	return &dtos.GetUsersCourseByAuthorResponseDto{
	// 		Message: "Get users successfully!",
	// 		Users:   result,
	// 	}, nil
	// }

	// result := &utils.ListResult[*dtos.UserCourseAuthorReview]{
	// 	Size:       command.ListQuery.Size,
	// 	Page:       command.ListQuery.Page,
	// 	TotalItems: totalItem,
	// 	TotalPage:  totalPage,
	// 	Items:      usersCourse,
	// }
	fmt.Println("Hey 1: ", result)

	return &dtos.GetOverviewCourseByAuthorResponseDto{
		Message: "Get data successfully!",
		// Users:   result,
		Result: result,

	}, nil
}

func roundFloat32(num float32, precision int) float32 {
	output := fmt.Sprintf("%.*f", precision, num)
	result, _ := strconv.ParseFloat(output, 32)
	return float32(result)
}
