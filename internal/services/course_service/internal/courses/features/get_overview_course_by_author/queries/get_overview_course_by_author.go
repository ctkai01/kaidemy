package queries

import (
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetOverviewCourseByAuthor struct {
	IdUser int
	CourseID *string
	// *utils.ListQuery
}

func NewGetOverviewCourseByAuthor(
	idUser int,
	courseID *string,
	// search string,
	// query *utils.ListQuery,

) (*GetOverviewCourseByAuthor, *validator.ValidateError) {
	q := &GetOverviewCourseByAuthor{
		idUser,
		courseID,
		// query,
	}
	
	errValidate := validator.Validate(q)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
