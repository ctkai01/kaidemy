package dtos

// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

type OverviewCourseAuthor struct {
	Enrollments *EnrollmentStats `json:"enrollments"`
	Ratings     *RatingStats     `json:"ratings"`
	Revenue     *Revenue         `json:"revenues"`
}

type EnrollmentStats struct {
	Total          int   `json:"total"`
	TotalThisMonth int   `json:"total_this_month"`
	DetailStats    []int `json:"detail_stats"`
}

type RatingStats struct {
	Total          float32 `json:"total"`
	TotalThisMonth int     `json:"total_this_month"`
	DetailStats    []int   `json:"detail_stats"`
}

type RevenueStats struct {
	Total          float32   `json:"total"`
	TotalThisMonth float32   `json:"total_this_month"`
	DetailStats    []float32 `json:"detail_stats"`
}

type Revenue struct {
	Total    float32       `json:"total"`
	Fee      *RevenueStats `json:"fee"`
	Net      *RevenueStats `json:"net"`
	Currency string        `json:"currency"`
}

type GetOverviewCourseByAuthorResponseDto struct {
	Message string                `json:"message"`
	Result  *OverviewCourseAuthor `json:"result"`
}
