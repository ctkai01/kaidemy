package dtos

//https://echo.labstack.com/guide/response/

type GetOverviewCourseByAuthorRequestDto struct {
	CourseID  *string `query:"course_id"`
}
