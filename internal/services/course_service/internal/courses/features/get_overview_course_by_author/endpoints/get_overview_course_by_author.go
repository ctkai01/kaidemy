package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/params"
	coursesError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_overview_course_by_author/queries"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_overview_course_by_author/dtos"
)

type getOverviewCourseByAuthorEndpoint struct {
	params.CoursesRouteParams
}

func NewOverviewCourseByAuthorEndpoint(
	params params.CoursesRouteParams,
) route.Endpoint {
	return &getOverviewCourseByAuthorEndpoint{
		CoursesRouteParams: params,
	}
}

func (ep *getOverviewCourseByAuthorEndpoint) MapEndpoint() {
	ep.CoursesGroup.GET("/overview/author", ep.handler())
}

func (ep *getOverviewCourseByAuthorEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		// listQuery, err := utils.GetListQueryFromCtx(c)
		// fmt.Println("Query: ", listQuery)
		
		// if err != nil {
		// 	badRequestErr := customErrors.NewBadRequestErrorWrap(
		// 		err,
		// 		"[getOverviewCourseByAuthor.Bind] error in the binding request",
		// 	)
		// 	ep.Logger.Errorf(
		// 		fmt.Sprintf("[getOverviewCourseByAuthorEndpoint_handler.Bind] err: %v", badRequestErr),
		// 	)
		// 	return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())
		// }

		request := &dtos.GetOverviewCourseByAuthorRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[getOverviewCourseByAuthor.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getOverviewCourseByAuthorEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}

		userId := utils.GetUserId(c)

		query, errValidate := queries.NewGetOverviewCourseByAuthor(
			userId,
			request.CourseID,
			// request.Search,
			// listQuery,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[getOverviewCourseByAuthor_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getOverviewCourseByAuthor_handler.StructCtx] err: {%v}", validationErr),
			)

			return customErrors.NewBadRequestErrorHttp(c, validationErr.Error())

		}

		result, err := mediatr.Send[*queries.GetOverviewCourseByAuthor, *dtos.GetOverviewCourseByAuthorResponseDto](
			ctx,
			query,
		)

		if err != nil {
			if err.Error() == coursesError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[getOverviewCourseByAuthor_handler.Send] error",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[getOverviewCourseByAuthor.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			if err.Error() == coursesError.WrapError(customErrors.ErrorCannotPermission).Error() {
				errCustom := errors.WithMessage(
					err,
					"[getOverviewCourseByAuthor_handler.Send] error",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[getOverviewCourseByAuthor.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewForbiddenErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[getOverviewCourseByAuthorEndpoint_handler.Send] error",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[getOverviewCourseByAuthor.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
