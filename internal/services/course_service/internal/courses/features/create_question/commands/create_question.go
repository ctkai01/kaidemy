package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type CreateQuestion struct {
	IdUser    int
	Title     string `validate:"required,gte=1,lte=600"`
	LectureID int    `validate:"required,numeric"`
}

func NewCreateQuestion(idUser int, title string, lectureID int) (*CreateQuestion, *validator.ValidateError) {
	command := &CreateQuestion{idUser, title, lectureID}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
