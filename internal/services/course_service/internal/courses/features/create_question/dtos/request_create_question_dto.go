package dtos

//https://echo.labstack.com/guide/response/

type CreateQuestionRequestDto struct {
	UserID    int
	LectureID int    `form:"lecture_id"`
	Title     string `form:"title"`
}
