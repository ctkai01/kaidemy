package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"

	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_lecture/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	grpcService "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
)

type DeleteLectureHandler struct {
	log                  logger.Logger
	curriculumRepository data.CurriculumRepository
	lectureRepository    data.LectureRepository
	courseRepository     data.CourseRepository
	grpcClient           grpc.GrpcClient
}

func NewDeleteLectureHandler(
	log logger.Logger,
	curriculumRepository data.CurriculumRepository,
	lectureRepository data.LectureRepository,
	courseRepository data.CourseRepository,
	grpcClient grpc.GrpcClient,

) *DeleteLectureHandler {
	return &DeleteLectureHandler{log: log, courseRepository: courseRepository, curriculumRepository: curriculumRepository, lectureRepository: lectureRepository, grpcClient: grpcClient}
}

func (c *DeleteLectureHandler) Handle(ctx context.Context, command *DeleteLecture) (*dtos.DeleteLectureResponseDto, error) {
	// lecture, err := c.lectureRepository.GetLectureByID(ctx, command.LectureID)
	lecture, err := c.lectureRepository.GetLectureByIDRelation(ctx, []string{"Assets"}, command.LectureID)

	if err != nil {
		return nil, err
	}

	if lecture == nil {
		return nil, customErrors.ErrLectureNotFound
	}

	if lecture.Type != constants.LECTURE_TYPE {
		return nil, customErrors.ErrMustLecture
	}

	curriculum, err := c.curriculumRepository.GetCurriculumByIDRelation(ctx, []string{"Course"}, lecture.CurriculumID)

	if err != nil {
		return nil, err
	}

	if curriculum == nil {
		return nil, customErrors.ErrCurriculumNotFound
	}

	if curriculum.Course.UserID != command.IdUser {
		return nil, customErrors.ErrorCannotPermission
	}

	for _, asset := range lecture.Assets {
		connUpload, err := c.grpcClient.GetGrpcConnection("upload_service")
		if err != nil {
			return nil, err
		}
		clientUpload := grpcService.NewUploadsServiceClient(connUpload)

		if asset.Type == models.WatchType {
			//Delete video

			if _, err := clientUpload.DeleteVideo(ctx, &grpcService.DeleteVideoReq{
				VideoID: asset.BunnyID,
			}); err != nil {
				return nil, err
			}
		} else {
			if _, err := clientUpload.DeleteResource(ctx, &grpcService.DeleteResourceReq{
				Url: asset.URL,
			}); err != nil {
				return nil, err
			}
		}
	}

	if err := c.lectureRepository.DeleteLectureByID(ctx, command.LectureID); err != nil {
		return nil, err
	}

	c.log.Info("Delete lecture success: ")
	course := curriculum.Course
	course.ReviewStatus = constants.REVIEW_INIT_STATUS
	if err := c.courseRepository.UpdateCourse(ctx, &course); err != nil {
		return nil, err
	}
	return &dtos.DeleteLectureResponseDto{
		Message: "Delete lecture successfully!",
	}, nil
}
