package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type DeleteLecture struct {
	IdUser    int
	LectureID int `validate:"required,numeric"`
}

func NewDeleteLecture(idUser int, lectureID int) (*DeleteLecture, *validator.ValidateError) {
	command := &DeleteLecture{idUser, lectureID}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
