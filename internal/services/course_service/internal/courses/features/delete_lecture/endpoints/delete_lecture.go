package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/params"
	coursesError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_lecture/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_lecture/dtos"
)

type deleteLectureEndpoint struct {
	params.LecturesRouteParams
}

func NewDeleteLectureEndpoint(
	params params.LecturesRouteParams,
) route.Endpoint {
	return &deleteLectureEndpoint{
		LecturesRouteParams: params,
	}
}

func (ep *deleteLectureEndpoint) MapEndpoint() {
	ep.LecturesGroup.DELETE("/:id", ep.handler())
}

func (ep *deleteLectureEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.DeleteLectureRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[DeleteLecture.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[DeleteLectureEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}

		userId := utils.GetUserId(c)

		command, errValidate := commands.NewDeleteLecture(
			userId,
			request.LectureID,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[DeleteLectureEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[DeleteLectureEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.DeleteLecture, *dtos.DeleteLectureResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == coursesError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[DeleteLectureEndpoint_handler.Send] error in sending Delete Lecture",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[DeleteLecture.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[DeleteLectureEndpoint_handler.Send] error in sending Delete Lecture",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[DeleteLecture.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
