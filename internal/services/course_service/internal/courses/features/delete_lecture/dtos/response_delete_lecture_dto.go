package dtos

//https://echo.labstack.com/guide/response/

type DeleteLectureResponseDto struct {
	Message    string            `json:"message"`
}
