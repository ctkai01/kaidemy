package dtos

//https://echo.labstack.com/guide/response/

type DeleteLectureRequestDto struct {
	LectureID int `json:"-" param:"id"`
}
