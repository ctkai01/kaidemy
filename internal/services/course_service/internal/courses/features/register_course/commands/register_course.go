package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type RegisterCourse struct {
	UserID   int
	CourseID int `validate:"required,numeric"`
}

func NewDeleteAnswer(userID int, courseID int) (*RegisterCourse, *validator.ValidateError) {
	command := &RegisterCourse{userID, courseID}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
