package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/register_course/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
)

type RegisterCourseHandler struct {
	log                  logger.Logger
	learningRepository data.LearningRepository
	grpcClient           grpc.GrpcClient
}

func NewRegisterCourseHandler(
	log logger.Logger,
	learningRepository data.LearningRepository,
	grpcClient grpc.GrpcClient,
) *RegisterCourseHandler {
	return &RegisterCourseHandler{
		log:                  log,
		learningRepository: learningRepository,
		grpcClient:           grpcClient,
	}
}

func (c *RegisterCourseHandler) Handle(ctx context.Context, command *RegisterCourse) (*dtos.RegisterCourseResponseDto, error) {
	createLearning := &models.Learning{
		UserID: command.UserID,
		CourseID: command.CourseID,
		Type: constants.STANDARD_TYPE,
	}
	err := c.learningRepository.CreateLearning(ctx, createLearning)

	if err != nil {
		return nil, err
	}
	return &dtos.RegisterCourseResponseDto{
		Message: "Create learning successfully!",
	}, nil
}
