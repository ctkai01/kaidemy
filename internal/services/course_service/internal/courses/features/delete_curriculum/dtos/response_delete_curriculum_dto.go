package dtos

//https://echo.labstack.com/guide/response/

type DeleteCurriculumResponseDto struct {
	Message    string            `json:"message"`
}
