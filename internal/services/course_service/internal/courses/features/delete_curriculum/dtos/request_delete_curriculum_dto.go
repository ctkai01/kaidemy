package dtos

//https://echo.labstack.com/guide/response/

type DeleteCurriculumRequestDto struct {
	CurriculumID int     `json:"-" param:"id"`
}
