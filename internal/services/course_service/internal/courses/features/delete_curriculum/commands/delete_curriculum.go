package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type DeleteCurriculum struct {
	IdUser       int
	CurriculumID int `validate:"required,numeric"`
}

func NewDeleteCurriculum(idUser int, curriculumID int) (*DeleteCurriculum, *validator.ValidateError) {
	command := &DeleteCurriculum{idUser, curriculumID}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
