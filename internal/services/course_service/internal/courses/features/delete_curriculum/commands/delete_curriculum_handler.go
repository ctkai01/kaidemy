package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"

	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_curriculum/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	grpcService "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
)

type DeleteCurriculumHandler struct {
	log                  logger.Logger
	curriculumRepository data.CurriculumRepository
	courseRepository     data.CourseRepository
	grpcClient           grpc.GrpcClient
}

func NewDeleteCurriculumHandler(
	log logger.Logger,
	curriculumRepository data.CurriculumRepository,
	courseRepository data.CourseRepository,
	grpcClient grpc.GrpcClient,

) *DeleteCurriculumHandler {
	return &DeleteCurriculumHandler{log: log, curriculumRepository: curriculumRepository, courseRepository: courseRepository, grpcClient: grpcClient}
}

func (c *DeleteCurriculumHandler) Handle(ctx context.Context, command *DeleteCurriculum) (*dtos.DeleteCurriculumResponseDto, error) {
	curriculumDelete, err := c.curriculumRepository.GetCurriculumByIDRelation(ctx, []string{"Course", "Lectures", "Lectures.Assets"}, command.CurriculumID)

	if err != nil {
		return nil, err
	}

	if curriculumDelete == nil {
		return nil, customErrors.ErrCurriculumNotFound
	}

	if curriculumDelete.Course.UserID != command.IdUser {
		return nil, customErrors.ErrorCannotPermission
	}

	//Delete all assets

	for _, lecture := range curriculumDelete.Lectures {
		for _, asset := range lecture.Assets {
			connUpload, err := c.grpcClient.GetGrpcConnection("upload_service")
			if err != nil {
				return nil, err
			}
			clientUpload := grpcService.NewUploadsServiceClient(connUpload)

			if asset.Type == models.WatchType {
				//Delete video

				if _, err := clientUpload.DeleteVideo(ctx, &grpcService.DeleteVideoReq{
					VideoID: asset.BunnyID,
				}); err != nil {
					return nil, err
				}
			} else {
				if _, err := clientUpload.DeleteResource(ctx, &grpcService.DeleteResourceReq{
					Url: asset.URL,
				}); err != nil {
					return nil, err
				}
			}
		}
	}

	//Delete
	course := curriculumDelete.Course
	course.ReviewStatus = constants.REVIEW_INIT_STATUS
	if err := c.courseRepository.UpdateCourse(ctx, &course); err != nil {
		return nil, err
	}

	if err := c.curriculumRepository.DeleteCurriculumByID(ctx, command.CurriculumID); err != nil {
		return nil, err
	}
	c.log.Info("Delete curriculum success: ")

	return &dtos.DeleteCurriculumResponseDto{
		Message: "Delete curriculum successfully!",
	}, nil
}
