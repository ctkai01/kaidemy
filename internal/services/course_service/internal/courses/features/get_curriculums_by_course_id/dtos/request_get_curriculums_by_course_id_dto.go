package dtos

//https://echo.labstack.com/guide/response/

type GetCurriculumsByCourseIDRequestDto struct {
	CourseID int  `param:"id"`
}
