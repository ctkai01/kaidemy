package dtos

import "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

//https://echo.labstack.com/guide/response/

type GetCurriculumsByCourseIDResponseDto struct {
	Message string        `json:"message"`
	Course  models.CourseResponse `json:"course"`
	// TopicLearning *models.TopicLearning `json:"topic_learning"`
}
