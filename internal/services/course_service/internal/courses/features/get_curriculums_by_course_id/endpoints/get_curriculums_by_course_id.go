package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/params"
	coursesError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_curriculums_by_course_id/dtos"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_curriculums_by_course_id/queries"
)

type getCurriculumsByCourseIDEndpoint struct {
	params.CoursesNoAuthRouteParams
}

func NewGetCurriculumsByCourseIDEndpoint(
	params params.CoursesNoAuthRouteParams,
) route.Endpoint {
	return &getCurriculumsByCourseIDEndpoint{
		CoursesNoAuthRouteParams: params,
	}
}

func (ep *getCurriculumsByCourseIDEndpoint) MapEndpoint() {
	ep.CoursesGroup.GET("/:id/curriculums", ep.handler())
}

func (ep *getCurriculumsByCourseIDEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.GetCurriculumsByCourseIDRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[GetCurriculumsByCourseID.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[GetCurriculumsByCourseIDEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())
		}

		// userId := utils.GetUserId(c)
		// userRole := utils.GetUserRole(c)
		// fmt.Println("userRole: ", userRole)
		command, errValidate := queries.NewGetCurriculumsByCourseID(
			// userId,
			request.CourseID,
			// userRole,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[GetCurriculumsByCourseID_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[GetCurriculumsByCourseID_handler.StructCtx] err: {%v}", validationErr),
			)

			return customErrors.NewBadRequestErrorHttp(c, validationErr.Error())

		}

		result, err := mediatr.Send[*queries.GetCurriculumsByCourseID, *dtos.GetCurriculumsByCourseIDResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == coursesError.WrapError(customErrors.ErrUserNotFound).Error() {

				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			ep.Logger.Error(
				fmt.Sprintf(
					"[GetCurriculumsByCourseID.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
