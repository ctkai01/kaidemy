package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/params"
	coursesError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_courses_by_search/queries"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_courses_by_search/dtos"
)

type getCourseBySearchEndpoint struct {
	params.CoursesNoAuthRouteParams
}

func NewGetCourseBySearchEndpoint(
	params params.CoursesNoAuthRouteParams,
) route.Endpoint {
	return &getCourseBySearchEndpoint{
		CoursesNoAuthRouteParams: params,
	}
}

func (ep *getCourseBySearchEndpoint) MapEndpoint() {
	ep.CoursesGroup.GET("/search", ep.handler())
}

func (ep *getCourseBySearchEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		listQuery, err := utils.GetListQueryFromCtx(c)
		fmt.Println("Query: ", listQuery)
		
		if err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[getCourseBySearch.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getCourseBySearchEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())
		}
		
		request := &dtos.GetCourseBySearchRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[getCourseBySearch.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getCourseBySearchEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}
		// fmt.Println("request level: ", request.Level)
		// fmt.Println("request duration: ", request.Duration)
		// fmt.Println("request rating: ", request.Rating)
		// userId := utils.GetUserId(c)

		query, errValidate := queries.NewGetCoursesBySearch(
			request.Search,
			request.Level,
			request.Duration,
			request.Sort,
			request.Rating,
			// userId,
			listQuery,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[getCourseBySearch_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getCourseBySearch_handler.StructCtx] err: {%v}", validationErr),
			)

			return customErrors.NewBadRequestErrorHttp(c, validationErr.Error())

		}

		result, err := mediatr.Send[*queries.GetCoursesBySearch, *dtos.GetCoursesBySearchResponseDto](
			ctx,
			query,
		)

		if err != nil {
			if err.Error() == coursesError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[getCourseBySearch_handler.Send] error in sending create Answer",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[getCourseBySearch.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[getCourseBySearchEndpoint_handler.Send] error in sending get topic learnings",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[getCourseBySearch.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
