package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetCoursesBySearch struct {
	
	search string `validate:"required"`
	Level []string
	Duration []string
	Sort *string
	Rating *string
	// IdUserCall int
	// Content int
	*utils.ListQuery
}

func NewGetCoursesBySearch(
	search string,
	level []string,
	duration []string,
	sort *string,
	rating *string,
	// idUserCall int,
	// content int,
	query *utils.ListQuery,

) (*GetCoursesBySearch, *validator.ValidateError) {
	q := &GetCoursesBySearch{
		// idUser,
		// idUserCall,
		// content,
		search,
		level,
		duration,
		sort,
		rating,
		query,
	}
	
	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
