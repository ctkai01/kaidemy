package queries

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"fmt"
	"strconv"

	// "fmt"
	// "math"

	// "fmt"
	// "strings"

	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_lecture_questions_author/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"

	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	courseservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
)

type GetLectureQuestionsAuthorHandler struct {
	log                logger.Logger
	questionLectureRepository data.QuestionLectureRepository
	courseRepository   data.CourseRepository
	grpcClient         grpc.GrpcClient
}

func NewGetLectureQuestionsAuthorHandler(
	log logger.Logger,
	questionLectureRepository data.QuestionLectureRepository,
	courseRepository data.CourseRepository,
	grpcClient grpc.GrpcClient,

) *GetLectureQuestionsAuthorHandler {
	return &GetLectureQuestionsAuthorHandler{
		log:                log,
		questionLectureRepository: questionLectureRepository,
		courseRepository:   courseRepository,
		grpcClient:         grpcClient,
	}
}

func (c *GetLectureQuestionsAuthorHandler) Handle(ctx context.Context, command *GetLectureQuestionsAuthor) (*dtos.GetLectureQuestionsAuthorResponseDto, error) {
	courseIDsByAuthor, err := c.courseRepository.FindIdsCourseAuthor(ctx, command.UserID)

	if err != nil {
		return nil, err
	}

	var courseIDFilter []int
	if command.CourseID != nil {
		n, _ := strconv.Atoi(*command.CourseID)
		isExist := false
		for _, id := range courseIDsByAuthor {
			if id == n {
				isExist = true
				break
			}
		}

		if !isExist {
			return nil, customErrors.ErrorCannotPermission
		}

		courseIDFilter = append(courseIDFilter, n)
	} else {
		courseIDFilter = courseIDsByAuthor
	}
	
	fmt.Println("courseIDFilter: ", command)
	questionLections, err := c.questionLectureRepository.FindQuestionLecturesAuthorRelationPaginate(ctx, courseIDFilter, command.Sort, command.Search, command.ListQuery)
	
	if err != nil {
		return nil, err
	}

	fmt.Println("questionLections: ", questionLections)
	// _, err := c.courseRepository.GetCourseByID(ctx, command.IdCourse)
	

	// learnings, err := c.learningRepository.GetReviewsCourseIDPaginate(ctx, command.IdCourse, command.ListQuery)

	// if err != nil {
	// 	return nil, err
	// }

	var questionLectureShows = make([]*models.QuestionLectureAuthorShow, 0)
	connUser, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}
	clientUser := courseservice.NewUsersServiceClient(connUser)
	

	if questionLections.Items != nil {
		for _, questionLecture := range questionLections.Items {
			dataUser, err := clientUser.GetUserByID(context.Background(), &courseservice.GetUserByIdReq{
				UserId: int32(questionLecture.UserID),
			})
			if err != nil {
				return nil, err
			}

			questionLectureShows = append(questionLectureShows, &models.QuestionLectureAuthorShow{
				ID: questionLecture.ID,
				User: &models.UserQuestionLecture{
					ID: int(dataUser.User.ID),
					Avatar: &dataUser.User.Avatar,
					Name: dataUser.User.Name,
				},
				TotalAnswer: len(questionLecture.AnswerLectures),
				Course: &models.CourseQuestionLecture{
					ID: questionLecture.Course.ID,
					Image: questionLecture.Course.Image,
					Title: questionLecture.Course.Title,
				},
				LectureID: questionLecture.LectureID,
				Title: questionLecture.Title,
				Description: questionLecture.Description,
				UpdatedAt: questionLecture.UpdatedAt,
				CreatedAt: questionLecture.CreatedAt,
			})
		}
	}

	result := &utils.ListResult[*models.QuestionLectureAuthorShow]{
		Size:       questionLections.Size,
		Page:       questionLections.Page,
		TotalItems: questionLections.TotalItems,
		TotalPage:  questionLections.TotalPage,
		Items:      questionLectureShows,
	}

	// var averageReview float32

	// if learnings.TotalItems != 0 {
	// 	averageReview = float32(totalReviewCount) / float32(learnings.TotalItems)
	// 	fiveStarCount = int(math.Round((float64(fiveStarCount) / float64(learnings.TotalItems)) * 100))
	// 	fourStarCount = int(math.Round((float64(fourStarCount) / float64(learnings.TotalItems)) * 100))
	// 	threeStarCount = int(math.Round((float64(threeStarCount) / float64(learnings.TotalItems)) * 100))
	// 	twoStarCount = int(math.Round((float64(twoStarCount) / float64(learnings.TotalItems)) * 100))
	// 	oneStarCount = int(math.Round((float64(oneStarCount) / float64(learnings.TotalItems)) * 100))
	// }

	// result := &dtos.OverallReviewsByCourseID{
	// 	Learnings:     learningResult,
	// 	AverageReview: averageReview,
	// 	TotalReview:   int(learnings.TotalItems),
	// 	Overall: &dtos.OverallStar{
	// 		FiveStar:  fiveStarCount,
	// 		FourStar:  fourStarCount,
	// 		ThreeStar: threeStarCount,
	// 		TwoStar:   twoStarCount,
	// 		OneStar:   oneStarCount,
	// 	},
	// }
	// fmt.Println("HHh: ", result)
	return &dtos.GetLectureQuestionsAuthorResponseDto{
		Message: "Get lecture question successfully!",
		QuestionLectures: result,
		// Learnings: learningResult,
	}, nil
}
