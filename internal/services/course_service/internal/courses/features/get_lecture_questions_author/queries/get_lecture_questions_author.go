package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetLectureQuestionsAuthor struct {
	UserID int
	Sort *string
	Search *string
	CourseID *string
	*utils.ListQuery
}

func NewGetLectureQuestionsAuthor(
	idUser int,
	sort *string,
	search *string,
	courseID *string,
	query *utils.ListQuery,

) (*GetLectureQuestionsAuthor, *validator.ValidateError) {
	q := &GetLectureQuestionsAuthor{
		idUser,
		sort, 
		search,
		courseID,
		query,
	}
	
	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
