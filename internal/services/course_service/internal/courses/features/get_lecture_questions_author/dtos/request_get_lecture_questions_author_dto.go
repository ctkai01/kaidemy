package dtos

//https://echo.labstack.com/guide/response/

type GetLectureQuestionsAuthorRequestDto struct {
	Sort     *string `query:"sort"`
	Search   *string `query:"search"`
	CourseID *string `query:"course_id"`
}
