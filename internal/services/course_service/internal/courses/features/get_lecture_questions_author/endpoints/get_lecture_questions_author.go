package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/params"
	// coursesError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_lecture_questions_author/queries"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_lecture_questions_author/dtos"
)

type getLectureQuestionAuthorEndpoint struct {
	params.QuestionLectureRouteParams
}

func NewGetLectureQuestionAuthorEndpoint(
	params params.QuestionLectureRouteParams,
) route.Endpoint {
	return &getLectureQuestionAuthorEndpoint{
		QuestionLectureRouteParams: params,
	}
}

func (ep *getLectureQuestionAuthorEndpoint) MapEndpoint() {
	ep.QuestionLecturesGroup.GET("/author", ep.handler())
}

func (ep *getLectureQuestionAuthorEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		listQuery, err := utils.GetListQueryFromCtx(c)
		fmt.Println("Query: ", listQuery)
		
		if err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[getLectureQuestionsAuthorEndpoint.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getLectureQuesAuthortionsIDEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())
		}
		fmt.Printf("Filters: %v\n", listQuery.Filters)

		request := &dtos.GetLectureQuestionsAuthorRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[getReviewsByCourseIDEndpoint.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getReviewsByCourseIDEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())
		}

		userId := utils.GetUserId(c)
		// userIdStr := fmt.Sprintf("%d", userId)
	
		// listQuery.Filters = append(listQuery.Filters, &utils.FilterModel {
		// 	Field: "user_id",
		// 	Value: &userIdStr,
		// 	Comparison: "equals",
		// })
			fmt.Println("request", request)
		query, errValidate := queries.NewGetLectureQuestionsAuthor(
			userId,
			request.Sort,
			request.Search,
			request.CourseID,
			listQuery,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[getLectureQuestionsAuthorEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getLectureQuestionsAuthorEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return customErrors.NewBadRequestErrorHttp(c, validationErr.Error())

		}

		result, err := mediatr.Send[*queries.GetLectureQuestionsAuthor, *dtos.GetLectureQuestionsAuthorResponseDto](
			ctx,
			query,
		)

		if err != nil {
			// if err.Error() == coursesError.WrapError(customErrors.ErrUserNotFound).Error() {
			// 	errCustom := errors.WithMessage(
			// 		err,
			// 		"[RegisterNotificationToken_handler.Send] error in sending create Answer",
			// 	)
			// 	ep.Logger.Error(
			// 		fmt.Sprintf(
			// 			"[createAnswer.Send], err: %v",
			// 			errCustom,
			// 		),
			// 	)
			// 	return customErrors.NewBadRequestErrorHttp(c, err.Error())
			// }

			err = errors.WithMessage(
				err,
				"[getLectureQuestionsAuthorEndpoint_handler.Send] error in sending get learnings",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[getLectureQuestionsAuthor.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
