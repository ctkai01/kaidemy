package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/params"
	coursesError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_reports/dtos"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_reports/queries"
)

type getReportsEndpoint struct {
	params.ReportAdminRouteParams
}

func NewGetReportsEndpoint(
	params params.ReportAdminRouteParams,
) route.Endpoint {
	return &getReportsEndpoint{
		ReportAdminRouteParams: params,
	}
}

func (ep *getReportsEndpoint) MapEndpoint() {
	ep.ReportsGroup.GET("", ep.handler())
}

func (ep *getReportsEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		listQuery, err := utils.GetListQueryFromCtx(c)

		if err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[getReports.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getReportsEndpoint_handler.Bind] err: %v", badRequestErr),
			)

			return c.JSON(http.StatusBadRequest, badRequestErr)

		}
		fmt.Printf("Filters: %v\n", listQuery.Filters)

		userId := utils.GetUserId(c)
		query, errValidate := queries.NewGetReports(
			userId,
			listQuery,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[getReportsEndpoint_handler.StructCtx] query validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getReportsEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*queries.GetReports, *dtos.GetReportsResponseDto](
			ctx,
			query,
		)

		if err != nil {
			if err.Error() == coursesError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[getReportsEndpoint_handler.Send] error in sending get Prices",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[getReports.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[getReportsEndpoint_handler.Send] error in sending get Reports",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[getReports.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
