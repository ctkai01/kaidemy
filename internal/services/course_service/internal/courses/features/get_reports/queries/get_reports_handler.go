package queries

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"

	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_reports/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/models"
	// "golang.org/x/oauth2"
	courseservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// userservice "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type GetReportsHandler struct {
	log              logger.Logger
	reportRepository data.ReportRepository
	courseRepository data.CourseRepository
	grpcClient       grpc.GrpcClient
}

func NewGetReportsHandler(
	log logger.Logger,
	reportRepository data.ReportRepository,
	courseRepository data.CourseRepository,
	grpcClient grpc.GrpcClient,

) *GetReportsHandler {
	return &GetReportsHandler{log: log, courseRepository: courseRepository, reportRepository: reportRepository, grpcClient: grpcClient}
}

func (c *GetReportsHandler) Handle(ctx context.Context, query *GetReports) (*dtos.GetReportsResponseDto, error) {
	reports, err := c.reportRepository.GetAllReports(ctx, query.ListQuery)
	if err != nil {
		return nil, err
	}

	connUser, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}
	clientUser := courseservice.NewUsersServiceClient(connUser)

	conn, err := c.grpcClient.GetGrpcConnection("catalog_service")
	if err != nil {
		return nil, err
	}

	client := courseservice.NewCatalogsServiceClient(conn)

	var reportShow []*dtos.ReportShow

	for _, report := range reports.Items {
		dataUser, err := clientUser.GetUserByID(context.Background(), &courseservice.GetUserByIdReq{
			UserId: int32(report.UserID),
		})
		if err != nil {
			return nil, err
		}

		dataIssueType, err := client.GetIssueTypeByID(context.Background(), &courseservice.GetIssueTypeByIdReq{
			IssueTypeId: int32(report.IssueTypeID),
		})

		if err != nil {
			if !strings.Contains(err.Error(), "issue") {
				return nil, err
			}
		}

		reportShow = append(reportShow, &dtos.ReportShow{
			ID: report.ID,
			User: &dtos.UserReport{
				ID: int(dataUser.User.ID),
				Name: dataUser.User.Name,
				Avatar: &dataUser.User.Avatar,
			},
			Course: &dtos.CourseReport{
				ID: report.Course.ID,
				Title: report.Course.Title,
				Image: report.Course.Image,
			},
			IssueType: &dtos.IssueType{
				ID: int(dataIssueType.IssueType.ID),
				Name: dataIssueType.IssueType.Name,
			},
			IssueDetail: report.IssueDetail,
			UpdatedAt: report.UpdatedAt,
			CreatedAt: report.CreatedAt,
		})
	}
	// listResultDto, err := utils.ListResultToListResultDto[*models.Report](reports)
	result := &utils.ListResult[*dtos.ReportShow] {
		Size: reports.Size,
		Page: reports.Page,
		TotalItems: reports.TotalItems,
		TotalPage: reports.TotalPage,
		Items: reportShow,
	}
	if err != nil {
		return nil, err
	}
	return &dtos.GetReportsResponseDto{Reports: result}, nil

}
