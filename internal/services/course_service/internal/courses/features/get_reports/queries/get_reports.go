package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetReports struct {
	IdUser int
	*utils.ListQuery
}

func NewGetReports(idUser int, query *utils.ListQuery) (*GetReports, *validator.ValidateError) {
	q := &GetReports{IdUser: idUser, ListQuery: query}

	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
