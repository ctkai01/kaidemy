package dtos

import (
	"time"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"
)

//https://echo.labstack.com/guide/response/

type ReportShow struct {
	ID          int           `json:"id"`
	User        *UserReport   `json:"user"`
	Course      *CourseReport `json:"course"`
	IssueType   *IssueType    `json:"issue_type"`
	IssueDetail string    `json:"issue_detail"`
	UpdatedAt   *time.Time    `json:"updated_at"`
	CreatedAt   *time.Time    `json:"created_at"`
}

type UserReport struct {
	ID     int     `json:"id"`
	Name   string  `json:"name"`
	Avatar *string `json:"avatar"`
}

type IssueType struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

type CourseReport struct {
	ID    int     `json:"id"`
	Title string  `json:"title"`
	Image *string `json:"image"`
}

type GetReportsResponseDto struct {
	Reports *utils.ListResult[*ReportShow] `json:"reports"`
}
