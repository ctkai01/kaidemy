package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/params"
	coursesError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/remove_learning_topic_learning/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/remove_learning_topic_learning/dtos"
)

type removeLearningTopicLearningEndpoint struct {
	params.TopicLearningsRouteParams
}

func NewRemoveLearningTopicLearningEndpoint(
	params params.TopicLearningsRouteParams,
) route.Endpoint {
	return &removeLearningTopicLearningEndpoint{
		TopicLearningsRouteParams: params,
	}
}

func (ep *removeLearningTopicLearningEndpoint) MapEndpoint() {
	ep.TopicLearningsGroup.DELETE("/course/:id/:learningId", ep.handler())
}

func (ep *removeLearningTopicLearningEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.RemoveLearningTopicLeaningRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[RemoveLearningTopicLearning.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[RemoveLearningTopicLearningEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())
		}

		userId := utils.GetUserId(c)

		command, errValidate := commands.NewRemoveLearningTopicLeaning(
			userId,
			request.TopicLearningID,
			request.LearningID,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[RemoveLearningTopicLearning_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[RemoveLearningTopicLearning_handler.StructCtx] err: {%v}", validationErr),
			)

			return customErrors.NewBadRequestErrorHttp(c, validationErr.Error())

		}

		result, err := mediatr.Send[*commands.RemoveLearningTopicLeaning, *dtos.RemoveLearningTopicLeaningResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == coursesError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[RermoveLearningTopicLearningEndpoint_handler.Send] error in sending Rermove course to topic learning",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[RermoveLearningTopicLearning.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			// if err.Error() == coursesError.WrapError(customErrors.ErrorCannotPermission).Error() {
			// 	errCustom := errors.WithMessage(
			// 		err,
			// 		"[CreateTopicLeaningEndpoint_handler.Send] error in sending create Answer",
			// 	)
			// 	ep.Logger.Error(
			// 		fmt.Sprintf(
			// 			"[CreateTopicLeaning.Send], err: %v",
			// 			errCustom,
			// 		),
			// 	)
			// 	return customErrors.NewForbiddenErrorHttp(c, err.Error())
			// }

			err = errors.WithMessage(
				err,
				"[RemoveLearningTopicLearningEndpoint_handler.Send] error in sending Create Topic Leaning",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[RemoveLearningTopicLearning.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
