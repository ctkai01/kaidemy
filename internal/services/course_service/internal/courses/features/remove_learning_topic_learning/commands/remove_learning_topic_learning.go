package commands

import (
	"fmt"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type RemoveLearningTopicLeaning struct {
	IdUser          int
	TopicLearningID int `validate:"required,numeric"`
	LearningID        int `validate:"required,numeric"`
}

func NewRemoveLearningTopicLeaning(
	idUser int,
	topicLearningID int,
	learningID int,

) (*RemoveLearningTopicLeaning, *validator.ValidateError) {
	command := &RemoveLearningTopicLeaning{
		idUser,
		topicLearningID,
		learningID,
	}
	errValidate := validator.Validate(command)
	fmt.Println("Err: ", errValidate)
	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
