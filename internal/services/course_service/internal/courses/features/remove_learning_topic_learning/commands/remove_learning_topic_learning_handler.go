package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/remove_learning_topic_learning/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type RemoveLearningTopicLearningHandler struct {
	log                     logger.Logger
	topicLearningRepository data.TopicLearningRepository
	learningRepository      data.LearningRepository

	grpcClient grpc.GrpcClient
}

func NewRemoveLearningTopicLearningHandler(
	log logger.Logger,
	topicLearningRepository data.TopicLearningRepository,
	learningRepository data.LearningRepository,
	grpcClient grpc.GrpcClient,

) *RemoveLearningTopicLearningHandler {
	return &RemoveLearningTopicLearningHandler{
		log:                     log,
		topicLearningRepository: topicLearningRepository,
		learningRepository:      learningRepository,
		grpcClient:              grpcClient,
	}
}

func (c *RemoveLearningTopicLearningHandler) Handle(ctx context.Context, command *RemoveLearningTopicLeaning) (*dtos.RemoveLearningTopicLeaningResponseDto, error) {
	// Check course whether belong to user
	learning, err := c.learningRepository.GetLearningByID(ctx, command.LearningID)

	if err != nil {
		return nil, err
	}

	if learning.UserID != command.IdUser {
		return nil, customErrors.ErrCourseNotBelongToUser
	}

	// Check course belong Standard type
	if learning.Type != constants.STANDARD_TYPE {
		return nil, customErrors.ErrCannotAddCourseTopicLearning
	}

	// Check whether topic learning ID exist
	topicLearning, err := c.topicLearningRepository.GetTopicLearningByID(ctx, command.TopicLearningID)

	if err != nil {
		return nil, err
	}

	if topicLearning.UserID != command.IdUser {
		return nil, customErrors.ErrTopicLearningNotBelongToUser
	}

	err = c.topicLearningRepository.FindAssociationLearningExist(ctx, topicLearning, learning)
		
	if err == customErrors.ErrCourseHasBeenExistTopicLearning {
		//Remove

		if err := c.topicLearningRepository.RemoveAssociationLearning(ctx, topicLearning, learning); err != nil {
			return nil, err
		}

		return &dtos.RemoveLearningTopicLeaningResponseDto{
			Message: "Remove learning topic learning successfully!",
		}, nil
	}

	if err != nil {
		return nil, err
	} else {
		return nil, customErrors.ErrCourseNotHasBeenExistTopicLearning
	}
}
