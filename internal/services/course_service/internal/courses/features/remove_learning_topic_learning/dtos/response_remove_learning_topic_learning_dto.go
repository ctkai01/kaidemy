package dtos


//https://echo.labstack.com/guide/response/

type RemoveLearningTopicLeaningResponseDto struct {
	Message string        `json:"message"`
}
