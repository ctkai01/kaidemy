package dtos

//https://echo.labstack.com/guide/response/

type RemoveLearningTopicLeaningRequestDto struct {
	TopicLearningID int `param:"id"`
	LearningID      int `param:"learningId"`
}
