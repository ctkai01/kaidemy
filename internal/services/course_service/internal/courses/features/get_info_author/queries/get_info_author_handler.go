package queries

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"fmt"

	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/core/data/specification"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_info_author/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	courseservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type GetInfoAuthorHandler struct {
	log                logger.Logger
	courseRepository   data.CourseRepository
	learningRepository data.LearningRepository
	grpcClient         grpc.GrpcClient
}

func NewGetInfoAuthorHandler(
	log logger.Logger,
	courseRepository data.CourseRepository,
	learningRepository data.LearningRepository,

	grpcClient grpc.GrpcClient,

) *GetInfoAuthorHandler {
	return &GetInfoAuthorHandler{
		log:                log,
		courseRepository:   courseRepository,
		learningRepository: learningRepository,
		grpcClient:         grpcClient,
	}
}

func (c *GetInfoAuthorHandler) Handle(ctx context.Context, command *GetInfoAuthor) (*dtos.GetInfoAuthorResponseDto, error) {
	// spec := specification.Equal("user_id", command.idUser)

	courseIDs, err := c.courseRepository.FindIdsCourseAuthor(ctx, command.idUser)
	if err != nil {
		return nil, err
	}

	countLearningPurchase, err := c.learningRepository.GetCountLearningForCourseIDs(ctx, courseIDs)

	if err != nil {
		return nil, err
	}

	reviews, err := c.learningRepository.GetReviewsCourseIDs(ctx, courseIDs)
	if err != nil {
		return nil, err
	}
	var totalReviewCount int

	for _, review := range reviews {
		if review.StarCount != nil {
			totalReviewCount += *review.StarCount
		}
	}
	var averageReview float32
	if len(reviews) != 0 {
		averageReview = float32(totalReviewCount) / float32(len(reviews))

	}
	fmt.Println("Count: ", len(courseIDs))
	fmt.Println("courseIDs: ", courseIDs)
	fmt.Println("countLearningPurchase: ", countLearningPurchase)
	fmt.Println("reviews: ", len(reviews))
	fmt.Println("totalReviewCount: ", totalReviewCount)
	fmt.Println("average: ", averageReview)

	connUser, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}
	clientUser := courseservice.NewUsersServiceClient(connUser)

	dataUser, err := clientUser.GetUserByID(context.Background(), &courseservice.GetUserByIdReq{
		UserId: int32(command.idUser),
	})

	if err != nil {
		return nil, err
	}
	authorStatisticInfo := &models.AuthorStatisticInfo{
		User: &models.AuthorInfo{
			ID:        int(dataUser.User.ID),
			Avatar:    &dataUser.User.Avatar,
			Name:      dataUser.User.Name,
			Biography: &dataUser.User.Biography,
		},
		AverageReview: averageReview,
		CountReview:   len(reviews),
		CountStudent:  countLearningPurchase,
		TotalCourse:   len(courseIDs),
	}
	return &dtos.GetInfoAuthorResponseDto{
		Message:             "Get courses successfully!",
		AuthorStatisticInfo: authorStatisticInfo,
	}, nil

}
