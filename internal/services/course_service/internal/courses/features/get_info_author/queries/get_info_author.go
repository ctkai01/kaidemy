package queries

import (
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetInfoAuthor struct {
	idUser int
}

func NewGetInfoAuthor(
	idUser int,

) (*GetInfoAuthor, *validator.ValidateError) {
	q := &GetInfoAuthor{
		idUser,
	}
	
	errValidate := validator.Validate(q)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
