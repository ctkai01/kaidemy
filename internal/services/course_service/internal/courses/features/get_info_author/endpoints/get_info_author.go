package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/params"
	coursesError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_info_author/queries"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_info_author/dtos"
)

type getInfoAuthorEndpoint struct {
	params.CoursesNoAuthRouteParams
}

func NewGetInfoAuthorEndpoint(
	params params.CoursesNoAuthRouteParams,
) route.Endpoint {
	return &getInfoAuthorEndpoint{
		CoursesNoAuthRouteParams: params,
	}
}

func (ep *getInfoAuthorEndpoint) MapEndpoint() {
	ep.CoursesGroup.GET("/user/:id/important", ep.handler())
}

func (ep *getInfoAuthorEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()


		request := &dtos.GetInfoAuthorRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[getInfoAuthor.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getInfoAuthorEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}
		

		query, errValidate := queries.NewGetInfoAuthor(
			request.UserID,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"getInfoAuthor_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("getInfoAuthor_handler.StructCtx] err: {%v}", validationErr),
			)

			return customErrors.NewBadRequestErrorHttp(c, validationErr.Error())

		}

		result, err := mediatr.Send[*queries.GetInfoAuthor, *dtos.GetInfoAuthorResponseDto](
			ctx,
			query,
		)

		if err != nil {
			if err.Error() == coursesError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"getInfoAuthor_handler.Send] error in sending create Answer",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"getInfoAuthor.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"getInfoAuthorEndpoint_handler.Send] error in sending get topic learnings",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"getInfoAuthor.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
