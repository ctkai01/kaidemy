package dtos

import "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"
type GetInfoAuthorResponseDto struct {
	Message string                            `json:"message"`
	AuthorStatisticInfo *models.AuthorStatisticInfo `json:"statisticAuthor"`
}
