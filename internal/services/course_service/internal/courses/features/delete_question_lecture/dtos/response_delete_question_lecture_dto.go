package dtos

//https://echo.labstack.com/guide/response/

type DeleteQuestionLectureResponseDto struct {
	Message string `json:"message"`
}
