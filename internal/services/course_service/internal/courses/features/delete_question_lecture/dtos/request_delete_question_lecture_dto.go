package dtos

//https://echo.labstack.com/guide/response/

type DeleteQuestionLectureRequestDto struct {
	QuestionLectureID int `json:"-" param:"id"`
}
