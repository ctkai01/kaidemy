package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type DeleteQuestionLecture struct {
	IdUser            int `validate:"required,numeric"`
	QuestionLectureID int `validate:"required,numeric"`
}

func NewDeleteQuestionLecture(idUser int, lectureID int) (*DeleteQuestionLecture, *validator.ValidateError) {
	command := &DeleteQuestionLecture{idUser, lectureID}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
