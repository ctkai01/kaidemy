package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_question_lecture/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type DeleteQuestionLectureHandler struct {
	log                  logger.Logger
	questionLectureRepository    data.QuestionLectureRepository
	grpcClient           grpc.GrpcClient
}

func NewDeleteQuestionLectureHandler(
	log logger.Logger,
	questionLectureRepository    data.QuestionLectureRepository,
	grpcClient grpc.GrpcClient,

) *DeleteQuestionLectureHandler {
	return &DeleteQuestionLectureHandler{log: log, questionLectureRepository: questionLectureRepository, grpcClient: grpcClient}
}

func (c *DeleteQuestionLectureHandler) Handle(ctx context.Context, command *DeleteQuestionLecture) (*dtos.DeleteQuestionLectureResponseDto, error) {
	questionLecture, err := c.questionLectureRepository.GetQuestionLectureByID(ctx, command.QuestionLectureID)

	if err != nil {
		return nil, err
	}


	if questionLecture.UserID != command.IdUser {
		return nil, customErrors.ErrorCannotPermission
	}

	err = c.questionLectureRepository.DeleteQuestionLectureByID(ctx, questionLecture.ID)

	if err != nil {
		return nil, err
	}


	return &dtos.DeleteQuestionLectureResponseDto{
		Message: "Delete question lecture successfully!",
	}, nil
}
