package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/params"
	coursesError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/approval_review_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/approval_review_course/dtos"
)

type approvalReviewCourseEndpoint struct {
	params.CoursesAdminRouteParams
}

func NewApprovalReviewCourseEndpoint(
	params params.CoursesAdminRouteParams,
) route.Endpoint {
	return &approvalReviewCourseEndpoint{
		CoursesAdminRouteParams: params,
	}
}

func (ep *approvalReviewCourseEndpoint) MapEndpoint() {
	ep.CoursesGroup.POST("/:id/approval-review", ep.handler())
}

func (ep *approvalReviewCourseEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.ApprovalReviewCourseRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[ApprovalReviewCourse.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[ApprovalReviewCourseEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}

		userId := utils.GetUserId(c)

		command, errValidate := commands.NewSubmitReviewCourse(
			userId,
			request.CourseID,
			request.Approval,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[ApprovalReviewCourse_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[ApprovalReviewCourse_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.ApprovalReviewCourse, *dtos.ApprovalReviewCourseResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == coursesError.WrapError(customErrors.ErrorCannotPermission).Error() {
				errCustom := errors.WithMessage(
					err,
					"[ApprovalReviewCourse_handler.Send] error in sending",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[ApprovalReviewCourse.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewForbiddenErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[ApprovalReviewCourse_handler.Send] error in sending",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[ApprovalReviewCourse.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
