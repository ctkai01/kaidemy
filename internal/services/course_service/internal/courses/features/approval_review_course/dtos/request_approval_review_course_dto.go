package dtos

//https://echo.labstack.com/guide/response/

type ApprovalReviewCourseRequestDto struct {
	CourseID int `json:"-" param:"id"`
	Approval int `form:"approval"`
}
