package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type ApprovalReviewCourse struct {
	IdUser   int
	CourseID int `validate:"required,numeric"`
	Approval int `validate:"numeric"`
}

func NewSubmitReviewCourse(idUser int, courseID int, approval int) (*ApprovalReviewCourse, *validator.ValidateError) {
	command := &ApprovalReviewCourse{idUser, courseID, approval}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
