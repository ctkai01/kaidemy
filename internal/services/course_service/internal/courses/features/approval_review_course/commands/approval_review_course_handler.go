package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/approval_review_course/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"

	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type ApprovalReviewCourseHandler struct {
	log                  logger.Logger
	courseRepository data.CourseRepository
}

func NewApprovalReviewCourseHandler(
	log logger.Logger,
	courseRepository data.CourseRepository,
) *ApprovalReviewCourseHandler {
	return &ApprovalReviewCourseHandler{
		log:                  log,
		courseRepository: courseRepository,
	}
}

func (c *ApprovalReviewCourseHandler) Handle(ctx context.Context, command *ApprovalReviewCourse) (*dtos.ApprovalReviewCourseResponseDto, error) {
	course, err := c.courseRepository.GetCourseByIDRelation(ctx, []string{"Curriculums"}, command.CourseID)

	if err != nil {
		return nil, err
	}

	if course.ReviewStatus != constants.REVIEW_PENDING_STATUS {
		return nil, customErrors.ErrNeedPendingReviewStatus
	}

	if command.Approval == constants.REVIEW_VERIFY_STATUS {
		course.ReviewStatus = constants.REVIEW_VERIFY_STATUS
	} else {
		course.ReviewStatus = constants.REVIEW_INIT_STATUS
	}

	if err := c.courseRepository.UpdateCourse(ctx, course); err != nil {
		return nil, err
	}

	return &dtos.ApprovalReviewCourseResponseDto{
		Message: "Approval review course successfully!",
		Course: course,
	}, nil
}
