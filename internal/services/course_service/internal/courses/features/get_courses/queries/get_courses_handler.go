package queries

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"

	// // "io"
	// "strings"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_courses/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type GetCoursesHandler struct {
	log              logger.Logger
	courseRepository data.CourseRepository
	grpcClient       grpc.GrpcClient
}

func NewGetCoursesHandler(
	log logger.Logger,
	courseRepository data.CourseRepository,
	grpcClient grpc.GrpcClient,

) *GetCoursesHandler {
	return &GetCoursesHandler{
		log:              log,
		courseRepository: courseRepository,
		grpcClient:       grpcClient,
	}
}

func (c *GetCoursesHandler) Handle(ctx context.Context, command *GetCourses) (*dtos.GetCoursesResponseDto, error) {

	courses, err := c.courseRepository.FindCourseRelationPaginate(ctx, []string{"Curriculums"}, command.ListQuery)
	if err != nil {
		return nil, err
	}

	return &dtos.GetCoursesResponseDto{
		Message: "Get courses successfully!",
		Courses: courses,
	}, nil

}
