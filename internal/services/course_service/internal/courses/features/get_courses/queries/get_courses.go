package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetCourses struct {
	*utils.ListQuery
}

func NewGetCourses(
	query *utils.ListQuery,

) (*GetCourses, *validator.ValidateError) {
	q := &GetCourses{
		query,
	}
	
	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
