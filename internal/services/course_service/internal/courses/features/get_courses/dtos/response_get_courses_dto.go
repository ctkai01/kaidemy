package dtos

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"
)

type GetCoursesResponseDto struct {
	Message string                            `json:"message"`
	Courses *utils.ListResult[*models.Course] `json:"courses"`
}
