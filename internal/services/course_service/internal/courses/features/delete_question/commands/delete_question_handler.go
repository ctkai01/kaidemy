package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/delete_question/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type DeleteQuestionHandler struct {
	log                  logger.Logger
	curriculumRepository data.CurriculumRepository
	lectureRepository       data.LectureRepository
	questionRepository   data.QuestionRepository
	courseRepository   data.CourseRepository
	grpcClient           grpc.GrpcClient
}

func NewDeleteQuestionHandler(
	log logger.Logger,
	curriculumRepository data.CurriculumRepository,
	lectureRepository data.LectureRepository,
	questionRepository data.QuestionRepository,
	courseRepository   data.CourseRepository,
	grpcClient grpc.GrpcClient,

) *DeleteQuestionHandler {
	return &DeleteQuestionHandler{log: log, courseRepository: courseRepository, curriculumRepository: curriculumRepository, lectureRepository: lectureRepository, questionRepository: questionRepository, grpcClient: grpcClient}
}

func (c *DeleteQuestionHandler) Handle(ctx context.Context, command *DeleteQuestion) (*dtos.DeleteQuestionResponseDto, error) {
	question, err := c.questionRepository.GetQuestionByID(ctx, command.QuestionID)

	if err != nil {
		return nil, err
	}

	quiz, err := c.lectureRepository.GetLectureByID(ctx, question.LectureID)

	if err != nil {
		return nil, err
	}

	if quiz == nil {
		return nil, customErrors.ErrLectureNotFound
	}

	curriculum, err := c.curriculumRepository.GetCurriculumByIDRelation(ctx, []string{"Course"}, quiz.CurriculumID)

	if err != nil {
		return nil, err
	}

	if curriculum == nil {
		return nil, customErrors.ErrCurriculumNotFound
	}

	if curriculum.Course.UserID != command.IdUser {
		return nil, customErrors.ErrorCannotPermission
	}

	if err := c.questionRepository.DeleteQuestionByID(ctx, command.QuestionID); err != nil {
		return nil, err
	}
	c.log.Info("Delete Question success: ")

	course := curriculum.Course
	course.ReviewStatus = constants.REVIEW_INIT_STATUS
	if err := c.courseRepository.UpdateCourse(ctx, &course); err != nil {
		return nil, err
	}

	return &dtos.DeleteQuestionResponseDto{
		Message: "Delete Question successfully!",
	}, nil
}
