package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type DeleteQuestion struct {
	IdUser     int
	QuestionID int `validate:"required,numeric"`
}

func NewDeleteQuestion(idUser int, questionID int) (*DeleteQuestion, *validator.ValidateError) {
	command := &DeleteQuestion{idUser, questionID}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
