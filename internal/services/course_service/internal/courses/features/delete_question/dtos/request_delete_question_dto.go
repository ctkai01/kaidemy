package dtos

//https://echo.labstack.com/guide/response/

type DeleteQuestionRequestDto struct {
	QuestionID int `json:"-" param:"id"`
}
