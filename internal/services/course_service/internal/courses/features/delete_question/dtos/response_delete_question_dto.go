package dtos

//https://echo.labstack.com/guide/response/

type DeleteQuestionResponseDto struct {
	Message string `json:"message"`
}
