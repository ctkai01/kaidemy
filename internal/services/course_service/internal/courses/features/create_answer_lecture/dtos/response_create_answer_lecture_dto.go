package dtos

import "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

//https://echo.labstack.com/guide/response/

type CreateAnswerLectureResponseDto struct {
	Message       string                `json:"message"`
	AnswerLecture *models.AnswerLectureShow `json:"answer_lecture"`
}
