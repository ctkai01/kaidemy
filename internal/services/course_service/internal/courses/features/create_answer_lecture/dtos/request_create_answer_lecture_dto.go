package dtos

//https://echo.labstack.com/guide/response/

type CreateAnswerLectureRequestDto struct {
	QuestionLectureID int    `form:"question_lecture_id"`
	Answer            string `form:"answer"`
}
