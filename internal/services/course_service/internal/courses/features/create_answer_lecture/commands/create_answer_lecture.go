package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type CreateAnswerLecture struct {
	IdUser            int    `validate:"required,numeric"`
	QuestionLectureID int    `validate:"required,numeric"`
	Answer            string `validate:"required,gte=1,lte=500"`
}

func NewCreateAnswerLecture(
	idUser int,
	questionLectureID int,
	answer string,

) (*CreateAnswerLecture, *validator.ValidateError) {
	command := &CreateAnswerLecture{idUser, questionLectureID, answer}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
