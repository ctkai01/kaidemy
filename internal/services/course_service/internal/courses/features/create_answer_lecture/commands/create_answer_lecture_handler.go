package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_answer_lecture/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	courseservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
)

type createAnswerLectureHandler struct {
	log                       logger.Logger
	questionLectureRepository data.QuestionLectureRepository
	learningRepository        data.LearningRepository
	answerLectureRepository   data.AnswerLectureRepository
	grpcClient                grpc.GrpcClient
}

func NewCreateAnswerLectureHandler(
	log logger.Logger,
	questionLectureRepository data.QuestionLectureRepository,
	learningRepository data.LearningRepository,
	answerLectureRepository data.AnswerLectureRepository,
	grpcClient grpc.GrpcClient,

) *createAnswerLectureHandler {
	return &createAnswerLectureHandler{log: log,
		questionLectureRepository: questionLectureRepository,
		answerLectureRepository:   answerLectureRepository,
		learningRepository:        learningRepository,
		grpcClient:                grpcClient,
	}
}

func (c *createAnswerLectureHandler) Handle(ctx context.Context, command *CreateAnswerLecture) (*dtos.CreateAnswerLectureResponseDto, error) {
	questionLecture, err := c.questionLectureRepository.GetQuestionLectureByID(ctx, command.QuestionLectureID)

	if err != nil {
		return nil, err
	}

	// questionLecture.

	//Check whether user has this course ?
	_, err = c.learningRepository.GetLearningByUserCourseID(ctx, command.IdUser, questionLecture.CourseID)

	if err != nil {
		return nil, err
	}

	createAnswerLecture := &models.AnswerLecture{
		UserID:            command.IdUser,
		QuestionLectureID: command.QuestionLectureID,
		Answer:            command.Answer,
	}

	if err := c.answerLectureRepository.CreateAnswerLecture(ctx, createAnswerLecture); err != nil {
		return nil, err
	}

	connUser, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}
	clientUser := courseservice.NewUsersServiceClient(connUser)

	dataUser, err := clientUser.GetUserByID(context.Background(), &courseservice.GetUserByIdReq{
		UserId: int32(createAnswerLecture.UserID),
	})
	if err != nil {
		return nil, err
	}
	answerLectureShow := &models.AnswerLectureShow{
		ID: createAnswerLecture.ID,
		User: &models.UserAnswerLecture{
			ID:     int(dataUser.User.ID),
			Avatar: &dataUser.User.Avatar,
			Name:   dataUser.User.Name,
		},
		QuestionLectureID: createAnswerLecture.QuestionLectureID,
		Answer:            createAnswerLecture.Answer,
		UpdatedAt:         createAnswerLecture.UpdatedAt,
		CreatedAt:         createAnswerLecture.CreatedAt,
	}

	return &dtos.CreateAnswerLectureResponseDto{
		Message:       "Create answer lecture successfully!",
		AnswerLecture: answerLectureShow,
	}, nil
}
