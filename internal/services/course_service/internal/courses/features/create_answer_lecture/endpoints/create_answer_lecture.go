package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/params"
	// coursesError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_answer_lecture/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_answer_lecture/dtos"
)

type createAnswerLectureEndpoint struct {
	params.AnswerLectureRouteParams
}

func NewCreateAnswerLectureEndpoint(
	params params.AnswerLectureRouteParams,
) route.Endpoint {
	return &createAnswerLectureEndpoint{
		AnswerLectureRouteParams: params,
	}
}

func (ep *createAnswerLectureEndpoint) MapEndpoint() {
	ep.AnswerLecturesGroup.POST("", ep.handler())
}

func (ep *createAnswerLectureEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.CreateAnswerLectureRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[createAnswerLecture.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[createAnswerLectureEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}

		userId := utils.GetUserId(c)

		command, errValidate := commands.NewCreateAnswerLecture(
			userId,
			request.QuestionLectureID,
			request.Answer,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[createAnswerLectureEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[createAnswerLectureEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.CreateAnswerLecture, *dtos.CreateAnswerLectureResponseDto](
			ctx,
			command,
		)

		if err != nil {
			// if err.Error() == coursesError.WrapError(customErrors.ErrorCannotPermission).Error() {
			// 	errCustom := errors.WithMessage(
			// 		err,
			// 		"[createQuestionEndpoint_handler.Send] error in sending create Question",
			// 	)
			// 	ep.Logger.Error(
			// 		fmt.Sprintf(
			// 			"[createQuestion.Send], err: %v",
			// 			errCustom,
			// 		),
			// 	)
			// 	return customErrors.NewForbiddenErrorHttp(c, err.Error())
			// }

			err = errors.WithMessage(
				err,
				"[createAnswerLectureEndpoint_handler.Send] error in sending create answer lecture",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[createAnswerLecture.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusCreated, result)
	}
}
