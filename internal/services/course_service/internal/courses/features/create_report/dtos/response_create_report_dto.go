package dtos

import "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

//https://echo.labstack.com/guide/response/

type CreateReportResponseDto struct {
	Message string        `json:"message"`
	Report  *models.Report `json:"report"`
}
