package dtos

//https://echo.labstack.com/guide/response/

type CreateReportRequestDto struct {
	CourseID    int    `form:"course_id"`
	IssueTypeID int    `form:"issue_type_id"`
	IssueDetail string `form:"issue_detail"`
}
