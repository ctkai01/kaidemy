package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type CreateReport struct {
	IdUser      int
	CourseID    int    `validate:"required,numeric"`
	IssueTypeID int    `validate:"required,numeric"`
	IssueDetail string `validate:"required,gte=1,lte=100"`
}

func NewCreateReport(idUser int, courseID int, issueTypeID int, issueDetail string) (*CreateReport, *validator.ValidateError) {
	command := &CreateReport{idUser, courseID, issueTypeID, issueDetail}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
