package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_report/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	courseservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type CreateReportHandler struct {
	log              logger.Logger
	courseRepository data.CourseRepository
	reportRepository data.ReportRepository
	grpcClient       grpc.GrpcClient
}

func NewCreateReportHandler(
	log logger.Logger,
	courseRepository data.CourseRepository,
	reportRepository data.ReportRepository,
	grpcClient grpc.GrpcClient,

) *CreateReportHandler {
	return &CreateReportHandler{log: log, courseRepository: courseRepository, reportRepository: reportRepository, grpcClient: grpcClient}
}

func (c *CreateReportHandler) Handle(ctx context.Context, command *CreateReport) (*dtos.CreateReportResponseDto, error) {
	// curriculum, err := c.curriculumRepository.GetCurriculumByIDRelation(ctx, []string{"Course"}, command.CurriculumID)

	conn, err := c.grpcClient.GetGrpcConnection("catalog_service")
	if err != nil {
		return nil, err
	}

	client := courseservice.NewCatalogsServiceClient(conn)
	dataIssueType, err := client.GetIssueTypeByID(context.Background(), &courseservice.GetIssueTypeByIdReq{
		IssueTypeId: int32(command.IssueTypeID),
	})

	if err != nil {
		return nil, err
	}

	if dataIssueType.IssueType == nil {
		return nil, customErrors.ErrIssueTypeNotFound
	}

	//Check exist course

	_, err = c.courseRepository.GetCourseByID(ctx, command.CourseID)

	if err != nil {
		return nil, err
	}

	createReport := &models.Report{
		UserID: command.IdUser,
		CourseID: command.CourseID,
		IssueTypeID: command.IssueTypeID,
		IssueDetail: command.IssueDetail,
	}
	
	if err := c.reportRepository.CreateReport(ctx, createReport); err != nil {
		return nil, err
	}

	return &dtos.CreateReportResponseDto{
		Message: "Create report successfully!",
		Report:    createReport,
	}, nil
}
