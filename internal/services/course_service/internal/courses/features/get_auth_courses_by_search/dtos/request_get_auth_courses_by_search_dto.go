package dtos

//https://echo.labstack.com/guide/response/

type GetAuthCourseBySearchRequestDto struct {
	// CategoryID int      `param:"id"`
	Rating     *string   `query:"rating"`
	Sort       *string   `query:"sort"`
	Search       string   `query:"search"`
	Duration   []string `query:"duration"`
	Level      []string `query:"level"`
}

