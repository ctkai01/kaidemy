package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetAuthCoursesBySearch struct {
	
	search string `validate:"required"`
	Level []string
	Duration []string
	Sort *string
	Rating *string
	UserID int
	// IdUserCall int
	// Content int
	*utils.ListQuery
}

func NewGetAuthCoursesBySearch(
	search string,
	level []string,
	duration []string,
	sort *string,
	rating *string,
	userID int,
	// idUserCall int,
	// content int,
	query *utils.ListQuery,

) (*GetAuthCoursesBySearch, *validator.ValidateError) {
	q := &GetAuthCoursesBySearch{
		// idUser,
		// idUserCall,
		// content,
		search,
		level,
		duration,
		sort,
		rating,
		userID,
		query,
	}
	
	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
