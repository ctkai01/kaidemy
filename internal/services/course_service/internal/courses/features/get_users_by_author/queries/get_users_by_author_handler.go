package queries

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"strconv"

	// "strings"

	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"

	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_users_by_author/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	courseservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	// courseservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type GetUsersByAuthorHandler struct {
	log              logger.Logger
	courseRepository data.CourseRepository
	learningRepository data.LearningRepository
	grpcClient       grpc.GrpcClient
}

func NewGetUsersByAuthorHandler(
	log logger.Logger,
	courseRepository data.CourseRepository,
	learningRepository data.LearningRepository,
	grpcClient grpc.GrpcClient,

) *GetUsersByAuthorHandler {
	return &GetUsersByAuthorHandler{
		log:              log,
		courseRepository: courseRepository,
		learningRepository: learningRepository,
		grpcClient:       grpcClient,
	}
}

func (c *GetUsersByAuthorHandler) Handle(ctx context.Context, command *GetUsersByAuthor) (*dtos.GetUsersByAuthorResponseDto, error) {
	// if command.IdUser != command.IdUserCall {
	// 	return nil, customErrors.ErrorCannotPermission
	// }
	courseIDsByAuthor, err := c.courseRepository.FindIdsCourseAuthor(ctx, command.IdUser)

	if err != nil {
		return nil, err
	}

	var courseIDFilter []int
	if command.CourseID != nil {
		n, _ := strconv.Atoi(*command.CourseID)
		isExist := false
		for _, id := range courseIDsByAuthor {
			if id == n {
				isExist = true
				break
			}
		}

		if !isExist {
			return nil, customErrors.ErrorCannotPermission
		}

		courseIDFilter = append(courseIDFilter, n)
	} else {
		courseIDFilter = courseIDsByAuthor
	}
	connUser, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}
	clientUser := courseservice.NewUsersServiceClient(connUser)
	learnings, totalUnique, err := c.learningRepository.GetEnrollsByCourseIDsPaginate(ctx, courseIDFilter, command.ListQuery)
	if err != nil {
		return nil, err
	}

	var usersRegister []*dtos.UserRegister

	for _, learning := range learnings.Items {
		//Get data user service
		dataUser, err := clientUser.GetUserByID(context.Background(), &courseservice.GetUserByIdReq{
			UserId: int32(learning.UserID),
		})
		if err != nil {
			return nil, err
		}

		usersRegister = append(usersRegister, &dtos.UserRegister{
			ID: int(dataUser.User.ID),
			Name: dataUser.User.Name,
			Avatar: &dataUser.User.Avatar,
			Course: &dtos.CourseRegister{
				ID: learning.Course.ID,
				Title: learning.Course.Title,
				Image: learning.Course.Image,
			},
			RegisterDate: learning.CreatedAt,
		})
	}
	
	result := &utils.ListResult[*dtos.UserRegister]{
		Size:       learnings.Size,
		Page:       learnings.Page,
		TotalItems: learnings.TotalItems,
		TotalPage:  learnings.TotalPage,
		Items:      usersRegister,
	}
	// CourseResponse
	return &dtos.GetUsersByAuthorResponseDto{
		Message: "Get users successfully!",
		Users: result,
		TotalUnique: totalUnique,
	}, nil
}
