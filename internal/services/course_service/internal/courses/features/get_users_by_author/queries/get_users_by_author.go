package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetUsersByAuthor struct {
	IdUser int
	CourseID *string
	*utils.ListQuery
}

func NewGetUsersByAuthor(
	idUser int,
	courseID *string,
	query *utils.ListQuery,

) (*GetUsersByAuthor, *validator.ValidateError) {
	q := &GetUsersByAuthor{
		idUser,
		courseID,
		query,
	}
	
	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
