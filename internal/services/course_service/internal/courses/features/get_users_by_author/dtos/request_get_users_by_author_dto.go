package dtos

//https://echo.labstack.com/guide/response/

type GetUsersByAuthorRequestDto struct {
	UserID   int     `param:"id"`
	CourseID *string `query:"course_id"`
}
