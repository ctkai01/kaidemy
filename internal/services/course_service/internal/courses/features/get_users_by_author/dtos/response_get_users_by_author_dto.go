package dtos

import (
	"time"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"
)

type UserRegister struct{
	ID int `json:"id"`
	Name string `json:"name"`
	Avatar	*string `json:"avatar"`
	Course *CourseRegister `json:"course"`
	RegisterDate *time.Time `json:"register_date"` 
}

type CourseRegister struct {
	ID int `json:"id"`
	Title string `json:"title"`
	Image *string `json:"image"`
}
type GetUsersByAuthorResponseDto struct {
	Message string                            `json:"message"`
	Users *utils.ListResult[*UserRegister] `json:"users"`
	TotalUnique int `json:"total_unique"`
}
