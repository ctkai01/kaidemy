package dtos

//https://echo.labstack.com/guide/response/

type UpdateQuizRequestDto struct {
	QuizID      int     `json:"-" param:"id"`
	Title       *string `form:"title"`       //80
	Description *string `form:"description"` //200
}
