package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type UpdateQuiz struct {
	IdUser      int
	Title       *string `validate:"omitempty,gte=1,lte=80"`
	Description *string `validate:"omitempty,gte=0,lte=500"`
	QuizID      int     `validate:"required,numeric"`
}

func NewUpdateQuiz(idUser int, title *string, description *string, quizID int) (*UpdateQuiz, *validator.ValidateError) {
	command := &UpdateQuiz{idUser, title, description, quizID}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
