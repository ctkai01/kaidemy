package commands

import (
	"fmt"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type CreateTopicLeaning struct {
	IdUser      int
	Title       string  `validate:"required,gte=1,lte=100"`
	Description *string `validate:"omitempty,gte=1,lte=200"`
}

func NewCreateTopicLeaning(
	idUser int,
	title string,
	description *string,
	
) (*CreateTopicLeaning, *validator.ValidateError) {
	command := &CreateTopicLeaning{
		idUser,
		title,
		description,
	}
	errValidate := validator.Validate(command)
	fmt.Println("Err: ", errValidate)
	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
