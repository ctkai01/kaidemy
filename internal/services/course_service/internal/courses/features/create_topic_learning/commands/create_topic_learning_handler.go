package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_topic_learning/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type CreateTopicLearningHandler struct {
	log                  logger.Logger
	topicLearningRepository data.TopicLearningRepository
	
	grpcClient           grpc.GrpcClient
}

func NewCreateTopicLearningHandler(
	log logger.Logger,
	topicLearningRepository data.TopicLearningRepository,
	grpcClient grpc.GrpcClient,

) *CreateTopicLearningHandler {
	return &CreateTopicLearningHandler{
		log:                  log,
		topicLearningRepository: topicLearningRepository,
		grpcClient:           grpcClient,
	}
}

func (c *CreateTopicLearningHandler) Handle(ctx context.Context, command *CreateTopicLeaning) (*dtos.CreateTopicLearningResponseDto, error) {
	createTopicLearning := &models.TopicLearning{
		Title: command.Title,
		Description: command.Description,
		UserID: command.IdUser,
	}

	if err := c.topicLearningRepository.CreateTopicLearning(ctx, createTopicLearning); err != nil {
		return nil, err
	}

	return &dtos.CreateTopicLearningResponseDto{
		Message: "Create topic learning successfully!",
		TopicLearning:  *createTopicLearning,
	}, nil
}
