package dtos

//https://echo.labstack.com/guide/response/

type CreateTopicLeaningRequestDto struct {
	Title string     `form:"title"`
	Description *string  `form:"description"`
}
