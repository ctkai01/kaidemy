package dtos

import "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

//https://echo.labstack.com/guide/response/

type CreateTopicLearningResponseDto struct {
	Message string        `json:"message"`
	TopicLearning  models.TopicLearning `json:"topic_learning"`
}
