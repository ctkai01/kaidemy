package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/params"
	coursesError "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_topic_learning/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_topic_learning/dtos"
)

type createTopicLearningEndpoint struct {
	params.TopicLearningsRouteParams
}

func NewCreateTopicLearningEndpoint(
	params params.TopicLearningsRouteParams,
) route.Endpoint {
	return &createTopicLearningEndpoint{
		TopicLearningsRouteParams: params,
	}
}

func (ep *createTopicLearningEndpoint) MapEndpoint() {
	ep.TopicLearningsGroup.POST("", ep.handler())
}

func (ep *createTopicLearningEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.CreateTopicLeaningRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[CreateTopicLeaning.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[CreateTopicLeaningEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())
		}

		userId := utils.GetUserId(c)

		command, errValidate := commands.NewCreateTopicLeaning(
			userId,
			request.Title,
			request.Description,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[CreateTopicLeaningEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[CreateTopicLeaningEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return customErrors.NewBadRequestErrorHttp(c, validationErr.Error())

		}

		result, err := mediatr.Send[*commands.CreateTopicLeaning, *dtos.CreateTopicLearningResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == coursesError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[createAnswerEndpoint_handler.Send] error in sending create Answer",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[createAnswer.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			// if err.Error() == coursesError.WrapError(customErrors.ErrorCannotPermission).Error() {
			// 	errCustom := errors.WithMessage(
			// 		err,
			// 		"[CreateTopicLeaningEndpoint_handler.Send] error in sending create Answer",
			// 	)
			// 	ep.Logger.Error(
			// 		fmt.Sprintf(
			// 			"[CreateTopicLeaning.Send], err: %v",
			// 			errCustom,
			// 		),
			// 	)
			// 	return customErrors.NewForbiddenErrorHttp(c, err.Error())
			// }

			err = errors.WithMessage(
				err,
				"[CreateTopicLeaningEndpoint_handler.Send] error in sending Create Topic Leaning",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[CreateTopicLeaning.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusCreated, result)
	}
}
