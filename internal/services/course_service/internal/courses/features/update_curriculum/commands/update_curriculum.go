package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type UpdateCurriculum struct {
	IdUser       int
	Title        *string `validate:"omitempty,gte=1,lte=80"`
	Description  *string `validate:"omitempty,gte=1,lte=200"`
	CurriculumID int     `validate:"required,numeric"`
}

func NewUpdateCurriculum(idUser int, title *string, description *string, curriculumID int) (*UpdateCurriculum, *validator.ValidateError) {
	command := &UpdateCurriculum{idUser, title, description, curriculumID}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
