package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/update_curriculum/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type UpdateCurriculumHandler struct {
	log                  logger.Logger
	curriculumRepository data.CurriculumRepository
	courseRepository     data.CourseRepository
	grpcClient           grpc.GrpcClient
}

func NewUpdateCurriculumHandler(
	log logger.Logger,
	curriculumRepository data.CurriculumRepository,
	courseRepository data.CourseRepository,
	grpcClient grpc.GrpcClient,

) *UpdateCurriculumHandler {
	return &UpdateCurriculumHandler{log: log, curriculumRepository: curriculumRepository, courseRepository: courseRepository, grpcClient: grpcClient}
}

func (c *UpdateCurriculumHandler) Handle(ctx context.Context, command *UpdateCurriculum) (*dtos.UpdateCurriculumResponseDto, error) {
	curriculumUpdate, err := c.curriculumRepository.GetCurriculumByIDRelation(ctx, []string{"Course"}, command.CurriculumID)

	if err != nil {
		return nil, err
	}

	if curriculumUpdate == nil {
		return nil, customErrors.ErrCurriculumNotFound
	}

	if curriculumUpdate.Course.UserID != command.IdUser {
		return nil, customErrors.ErrorCannotPermission
	}

	if command.Title != nil {
		curriculumUpdate.Title = *command.Title
	}

	if command.Description != nil {
		curriculumUpdate.Description = command.Description
	}
	if err := c.curriculumRepository.UpdateCurriculum(ctx, curriculumUpdate); err != nil {
		return nil, err
	}
	c.log.Info("Update curriculum success: ")

	course := &curriculumUpdate.Course
	course.ReviewStatus = constants.REVIEW_INIT_STATUS
	if err := c.courseRepository.UpdateCourse(ctx, course); err != nil {
		return nil, err
	}
	return &dtos.UpdateCurriculumResponseDto{
		Message:    "Update curriculum successfully!",
		Curriculum: *curriculumUpdate,
	}, nil
}
