package dtos

//https://echo.labstack.com/guide/response/

type UpdateCurriculumRequestDto struct {
	Title        *string `form:"title"` //80
	CurriculumID int     `json:"-" param:"id"`

	Description *string `form:"description"` //200
}
