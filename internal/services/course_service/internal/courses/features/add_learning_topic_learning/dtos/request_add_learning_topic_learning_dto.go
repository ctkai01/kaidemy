package dtos

//https://echo.labstack.com/guide/response/

type AddLearningTopicLeaningRequestDto struct {
	TopicLearningID int `form:"topic_learning_id"`
	LearningID      int `form:"learning_id"`
}
