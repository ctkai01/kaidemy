package dtos

import "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

//https://echo.labstack.com/guide/response/

type AddLearningTopicLeaningResponseDto struct {
	Message string        `json:"message"`
	TopicLearning *models.TopicLearningShow `json:"topic_learning"`
}
