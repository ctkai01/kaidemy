package commands

import (
	"fmt"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type AddLearningTopicLeaning struct {
	IdUser          int
	TopicLearningID int `validate:"required,numeric"`
	LearningID        int `validate:"required,numeric"`
}

func NewAddLearningTopicLeaning(
	idUser int,
	topicLearningID int,
	learningID int,

) (*AddLearningTopicLeaning, *validator.ValidateError) {
	command := &AddLearningTopicLeaning{
		idUser,
		topicLearningID,
		learningID,
	}
	errValidate := validator.Validate(command)
	fmt.Println("Err: ", errValidate)
	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
