package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"strings"
	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/add_learning_topic_learning/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	courseservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"

)

type AddLearningTopicLearningHandler struct {
	log                     logger.Logger
	topicLearningRepository data.TopicLearningRepository
	learningRepository      data.LearningRepository

	grpcClient grpc.GrpcClient
}

func NewAddLearningTopicLearningHandler(
	log logger.Logger,
	topicLearningRepository data.TopicLearningRepository,
	learningRepository data.LearningRepository,
	grpcClient grpc.GrpcClient,

) *AddLearningTopicLearningHandler {
	return &AddLearningTopicLearningHandler{
		log:                     log,
		topicLearningRepository: topicLearningRepository,
		learningRepository:      learningRepository,
		grpcClient:              grpcClient,
	}
}

func (c *AddLearningTopicLearningHandler) Handle(ctx context.Context, command *AddLearningTopicLeaning) (*dtos.AddLearningTopicLeaningResponseDto, error) {
	// Check course whether belong to user
	learning, err := c.learningRepository.GetLearningByID(ctx, command.LearningID)

	if err != nil {
		return nil, err
	}

	if learning.UserID != command.IdUser {
		return nil, customErrors.ErrCourseNotBelongToUser
	}

	// Check course belong Standard type
	if learning.Type != constants.STANDARD_TYPE {
		return nil, customErrors.ErrCannotAddCourseTopicLearning
	}

	// Check whether topic learning ID exist
	topicLearning, err := c.topicLearningRepository.GetTopicLearningByID(ctx, command.TopicLearningID)

	if err != nil {
		return nil, err
	}

	if topicLearning.UserID != command.IdUser {
		return nil, customErrors.ErrTopicLearningNotBelongToUser
	}

	// Check whether add learning to Topic Learning
	if err := c.topicLearningRepository.FindAssociationLearningExist(ctx, topicLearning, learning); err != nil {
		return nil, err
	}

	if err := c.topicLearningRepository.AppendAssociationLearning(ctx, topicLearning, learning); err != nil {
		return nil, err
	}

	// topicLearning, err := c.topicLearningRepository.GetTopicLearningByID(ctx, command.TopicLearningID)

	topicLearningRelation, err := c.topicLearningRepository.GetTopicLearningByIDRelation(ctx, []string{"Learnings", "Learnings.Course"}, command.TopicLearningID)

	if err != nil {
		return nil, err
	}


	connUser, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}
	clientUser := courseservice.NewUsersServiceClient(connUser)

	conn, err := c.grpcClient.GetGrpcConnection("catalog_service")
	if err != nil {
		return nil, err
	}
	client := courseservice.NewCatalogsServiceClient(conn)
	// var topicLearningShow *modecls.TopicLearningShow
	
	var learningShows = make([]*models.LearningShow, 0)
	for _, learning := range topicLearningRelation.Learnings {
		var levelID int

		if learning.Course.LevelID == nil {
			levelID = -1
		} else {
			levelID = *learning.Course.LevelID
		}

		dataLevel, err := client.GetLevelByID(context.Background(), &courseservice.GetLevelByIdReq{
			// UserId:  int32(command.IdUser),
			LevelId: int32(levelID),
		})

		if err != nil {
			if !strings.Contains(err.Error(), "level not found") {
				return nil, err

			}
		}

		//Get price
		var priceID int

		if learning.Course.PriceID == nil {
			priceID = -1
		} else {
			priceID = *learning.Course.PriceID
		}

		dataPrice, err := client.GetPriceByID(context.Background(), &courseservice.GetPriceByIdReq{
			PriceId: int32(priceID),
		})

		if err != nil {
			if !strings.Contains(err.Error(), "price not found") {
				return nil, err
			}
		}

		//Get data user service
		dataUser, err := clientUser.GetUserByID(context.Background(), &courseservice.GetUserByIdReq{
			UserId: int32(learning.Course.UserID),
		})
		if err != nil {
			return nil, err
		}

		var levelRes *models.LevelResponse

		if dataLevel == nil {
			levelRes = nil
		} else {
			levelRes = &models.LevelResponse{
				ID:   int(dataLevel.Level.ID),
				Name: dataLevel.Level.Name,
			}
		}

		var priceRes *models.PriceResponse

		if dataPrice == nil {
			priceRes = nil
		} else {
			priceRes = &models.PriceResponse{
				ID:    int(dataPrice.Price.ID),
				Tier:  dataPrice.Price.Tier,
				Value: int(dataPrice.Price.Value),
			}
		}

		learningShows = append(learningShows, &models.LearningShow{
			ID:        learning.ID,
			UserID:    learning.UserID,
			CourseID:  learning.CourseID,
			Process:   learning.Process,
			Type:      learning.Type,
			StarCount: learning.StarCount,
			Comment:   learning.Comment,
			Course: models.CourseLearning{
				ID:           learning.Course.ID,
				Title:        learning.Course.Title,
				Status:       learning.Course.Status,
				ReviewStatus: learning.Course.ReviewStatus,
				Image:        learning.Course.Image,
				Curriculums:  make([]models.Curriculum, 0),
				Level:        levelRes,
				Price:        priceRes,
				User: models.UserResponse{
					ID:   int(dataUser.User.ID),
					Name: dataUser.User.Name,
				},
				UpdatedAt: learning.Course.UpdatedAt,
				CreatedAt: learning.Course.CreatedAt,
			},
			UpdatedAt: learning.UpdatedAt,
			CreatedAt: learning.CreatedAt,
		})
	}

	topicLearningShow := &models.TopicLearningShow{
		ID: topicLearning.ID,
		Title: topicLearning.Title,
		Description: topicLearning.Description,
		UserID: topicLearning.UserID,
		Learnings: learningShows,
		UpdatedAt: topicLearning.UpdatedAt,
		CreatedAt: topicLearning.CreatedAt,
	}

	return &dtos.AddLearningTopicLeaningResponseDto{
		Message: "Add learning topic learning successfully!",
		TopicLearning: topicLearningShow,
	}, nil
}
