package dtos

import "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

//https://echo.labstack.com/guide/response/

type CreateQuizResponseDto struct {
	Message string      `json:"message"`
	Quiz    models.Lecture `json:"quiz"`
}
