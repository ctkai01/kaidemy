package dtos

//https://echo.labstack.com/guide/response/

type CreateQuizRequestDto struct {
	UserID       int
	CurriculumID int    `form:"curriculum_id"`
	Title        string `form:"title"`
	Description  *string `form:"description"`
}
