package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type CreateQuiz struct {
	IdUser       int
	Title        string  `validate:"required,gte=1,lte=80"`
	Description  *string `validate:"omitempty,gte=1,lte=150"`
	CurriculumID int     `validate:"required,numeric"`
}

func NewCreateQuiz(idUser int, title string, curriculumID int, description *string) (*CreateQuiz, *validator.ValidateError) {
	command := &CreateQuiz{idUser, title, description, curriculumID}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
