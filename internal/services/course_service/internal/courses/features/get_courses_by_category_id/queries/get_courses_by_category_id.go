package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetCoursesByCategoryId struct {
	IdCategory int
	Level []string
	Duration []string
	Sort *string
	Rating *string
	
	// IdUserCall int
	// Content int
	*utils.ListQuery
}

func NewGetCoursesByCategoryId(
	idCategory int,
	level []string,
	duration []string,
	sort *string,
	rating *string,
	// idUserCall int,
	// content int,
	query *utils.ListQuery,

) (*GetCoursesByCategoryId, *validator.ValidateError) {
	q := &GetCoursesByCategoryId{
		// idUser,
		// idUserCall,
		// content,
		idCategory,
		level,
		duration,
		sort,
		rating,
		query,
	}
	
	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
