package queries

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"fmt"
	"math"
	"sort"
	"strconv"
	"strings"

	// "fmt"
	// "strings"

	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"

	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/get_courses_by_category_id/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	courseservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type GetCoursesByCategoryIDHandler struct {
	log              logger.Logger
	courseRepository data.CourseRepository
	grpcClient       grpc.GrpcClient
}

func NewGetCoursesByCategoryIDHandler(
	log logger.Logger,
	courseRepository data.CourseRepository,
	grpcClient grpc.GrpcClient,

) *GetCoursesByCategoryIDHandler {
	return &GetCoursesByCategoryIDHandler{
		log:              log,
		courseRepository: courseRepository,
		grpcClient:       grpcClient,
	}
}

func (c *GetCoursesByCategoryIDHandler) Handle(ctx context.Context, command *GetCoursesByCategoryId) (*dtos.GetCoursesByCategoryIDResponseDto, error) {
	
	conn, err := c.grpcClient.GetGrpcConnection("catalog_service")
	if err != nil {
		return nil, err
	}
	client := courseservice.NewCatalogsServiceClient(conn)

	category, err := client.GetCategoryByID(context.Background(), &courseservice.GetCategoryByIdReq{
		CategoryId: int32(command.IdCategory),
	})
	
	if err != nil {
		return nil, err
	}

	categoryResponse:= &models.CategoryShow{
		ID: int(category.Category.ID),
		Name: category.Category.Name,
	}

	courses, err := c.courseRepository.GetCoursesCategoryIDPaginate(ctx, command.IdCategory, command.Level, command.Sort, command.ListQuery)
	if err != nil {
		return nil, err
	}

	connUser, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}
	clientUser := courseservice.NewUsersServiceClient(connUser)
	var coursesCategory []*models.CourseCategory
	// fmt.Println("courses: ", len(courses.Items))
	for _, course := range courses.Items {
		// dataCategory, err := client.GetCategoryByID(context.Background(), &courseservice.GetCategoryByIdReq{
		// 	CategoryId: int32(course.CategoryID),
		// })

		// if err != nil {
		// 	return nil, err
		// }

		// //Get data category
		// dataSubCategory, err := client.GetCategoryByID(context.Background(), &courseservice.GetCategoryByIdReq{
		// 	// UserId:     int32(command.IdUser),
		// 	CategoryId: int32(course.SubCategoryID),
		// })

		// if err != nil {
		// 	return nil, err
		// }

		//Get data Level
		var levelID int

		if course.LevelID == nil {
			levelID = -1
		} else {
			levelID = *course.LevelID
		}

		dataLevel, err := client.GetLevelByID(context.Background(), &courseservice.GetLevelByIdReq{
			// UserId:  int32(command.IdUser),
			LevelId: int32(levelID),
		})

		if err != nil {
			if !strings.Contains(err.Error(), "level not found") {
				return nil, err

			}
		}

		//Get Language

		// var languageID int

		// if course.LanguageID == nil {
		// 	languageID = -1
		// } else {
		// 	languageID = *course.LanguageID
		// }

		// dataLanguage, err := client.GetLanguageByID(context.Background(), &courseservice.GetLanguageByIdReq{
		// 	LanguageId: int32(languageID),
		// })

		// if err != nil {
		// 	if !strings.Contains(err.Error(), "language not found") {
		// 		return nil, err
		// 	}
		// }

		//Get price
		var priceID int

		if course.PriceID == nil {
			priceID = -1
		} else {
			priceID = *course.PriceID
		}

		dataPrice, err := client.GetPriceByID(context.Background(), &courseservice.GetPriceByIdReq{
			PriceId: int32(priceID),
		})

		if err != nil {
			if !strings.Contains(err.Error(), "price not found") {
				return nil, err
			}
		}

		//Get data user service
		dataUser, err := clientUser.GetUserByID(context.Background(), &courseservice.GetUserByIdReq{
			UserId: int32(course.UserID),
		})
		if err != nil {
			return nil, err
		}

		// var parentIDSubCategory *int

		// if dataSubCategory.Category.ParentID == 0 {
		// 	parentIDSubCategory = nil
		// } else {
		// 	parentId := int(dataSubCategory.Category.ParentID)
		// 	parentIDSubCategory = &parentId
		// }
		var levelRes *models.LevelResponse

		if dataLevel == nil {
			levelRes = nil
		} else {
			levelRes = &models.LevelResponse{
				ID:   int(dataLevel.Level.ID),
				Name: dataLevel.Level.Name,
			}
		}

		// var categoryRes *models.CategoryResponse

		// if dataCategory == nil {
		// 	categoryRes = nil
		// } else {
		// 	categoryRes = &models.CategoryResponse{
		// 		ID:       int(dataCategory.Category.ID),
		// 		Name:     dataCategory.Category.Name,
		// 		ParentID: nil,
		// 	}
		// }

		//
		// var subCategoryRes *models.CategoryResponse

		// if dataSubCategory == nil {
		// 	subCategoryRes = nil
		// } else {
		// 	subCategoryRes = &models.CategoryResponse{
		// 		ID:       int(dataSubCategory.Category.ID),
		// 		Name:     dataSubCategory.Category.Name,
		// 		ParentID: parentIDSubCategory,
		// 	}
		// }

		//
		// var languageRes *models.LanguageResponse

		// if dataLanguage == nil {
		// 	languageRes = nil
		// } else {
		// 	languageRes = &models.LanguageResponse{
		// 		ID:   int(dataLanguage.Language.ID),
		// 		Name: dataLanguage.Language.Name,
		// 	}
		// }

		//
		var priceRes *models.PriceResponse

		if dataPrice == nil {
			priceRes = nil
		} else {
			priceRes = &models.PriceResponse{
				ID:    int(dataPrice.Price.ID),
				Tier:  dataPrice.Price.Tier,
				Value: int(dataPrice.Price.Value),
			}
		}
		totalDuration := 0
		for _, curriculum := range course.Curriculums {
			for _, lecture := range curriculum.Lectures {
				for _, asset := range lecture.Assets {
					if lecture.Type == constants.LECTURE_TYPE && asset.Type == models.WatchType {
						totalDuration += int(asset.Duration)
					}
				}
			}
		}

		var totalReviewCount int
		var averageReview float32
		for _, learning := range course.Learnings {
			if learning.StarCount != nil {
				totalReviewCount += *learning.StarCount
			}
		}

		if len(course.Learnings) != 0 {
			averageReview = float32(totalReviewCount) / float32(len(course.Learnings))

		}

		courseCategory := &models.CourseCategory{
			ID:           course.ID,
			Level:        levelRes,
			Title:        course.Title,
			Subtitle:     course.Subtitle,
			Price:        priceRes,
			ReviewStatus: course.ReviewStatus,
			User: models.UserResponse{
				ID:   int(dataUser.User.ID),
				Name: dataUser.User.Name,
			},
			IsPurchased: false,
			OutComes: course.OutComes,
			TotalLecture:  len(course.Curriculums),
			Duration:      int64(totalDuration),
			AverageRating: averageReview,
			TotalRating:   int(len(course.Learnings)),
			CategoryID: course.CategoryID,
			SubCategoryID: course.SubCategoryID,
			Image:         course.Image,
			UpdatedAt:     course.UpdatedAt,
			CreatedAt:     course.CreatedAt,
		}

		coursesCategory = append(coursesCategory, courseCategory)
	}

	// coursesCategory.
	if command.Rating != nil {
		var filteredRatingCourses []*models.CourseCategory
		rating, _ := strconv.ParseFloat(*command.Rating, 32)
		fmt.Println("Rating: ", rating)

		for _, course := range coursesCategory {
			if course.AverageRating >= float32(rating) {
				filteredRatingCourses = append(filteredRatingCourses, course)
			}
		}
		coursesCategory = filteredRatingCourses
	}

	HOUR := 3600
	if command.Duration != nil {
		var filteredDurationCourses []*models.CourseCategory
		for _, course := range coursesCategory {
			for _, itemDuration := range command.Duration {
				if itemDuration == constants.SHORT_DURATION {
					if course.Duration <= int64(HOUR) {
						filteredDurationCourses = append(filteredDurationCourses, course)
					}
				}

				if itemDuration == constants.EXTRA_SHORT_DURATION {
					if course.Duration > int64(HOUR) {
						filteredDurationCourses = append(filteredDurationCourses, course)
					}
				}
			}

		}
		coursesCategory = filteredDurationCourses
	}

	if command.Sort != nil {
		if *command.Sort == constants.MOST_REVIEW_SORT {
			//count
			less := func(i, j int) bool {
				return coursesCategory[i].TotalRating > coursesCategory[j].TotalRating // sort DESC
			}

			// Use sort.Slice with the custom sorting function
			sort.Slice(coursesCategory, less)
		}

		if *command.Sort == constants.HIGHEST_SORT {
			less := func(i, j int) bool {
				return coursesCategory[i].AverageRating > coursesCategory[j].AverageRating // sort DESC
			}

			// Use sort.Slice with the custom sorting function
			sort.Slice(coursesCategory, less)
		}

	}
	totalItem := int64(len(coursesCategory))
	// fmt.Println("coursesCategory: ", len(coursesCategory))
	tierOneRatingCount := 0
	tierTwoRatingCount := 0
	tierThreeRatingCount := 0
	tieFourRatingCount := 0
	tierOneDurationCount := 0
	tierTwoDurationCount := 0
	allLevelCount := 0
	beginnerLevelCount := 0
	intermediateLevelCount := 0
	expertLevelCount := 0

	for _, item := range coursesCategory {
		fmt.Println("item.AverageRating: ", item.AverageRating)
		if item.AverageRating > 4.5 {
			tierOneRatingCount++
		}

		if item.AverageRating > 4.0 {
			tierTwoRatingCount++
		}

		if item.AverageRating > 3.5 {
			tierThreeRatingCount++
		}

		if item.AverageRating > 3.0 {
			tieFourRatingCount++
		}

		if item.Duration < int64(HOUR) {
			tierOneDurationCount++
		} else {
			tierTwoDurationCount++
		}

		if item.Level.Name == "Người bắt đầu" {
			beginnerLevelCount++
		}

		if item.Level.Name == "Trung cấp" {
			intermediateLevelCount++
		}

		if item.Level.Name == "Chuyên gia" {
			expertLevelCount++
		}

		if item.Level.Name == "Tất cả" {
			allLevelCount++
		}

	}
	totalPage := 0
	if len(coursesCategory) > 0 {
		//Define Offset
		offset := 0

		if command.ListQuery.Page == 0 {
			offset = 0
		} else {
			offset = (command.ListQuery.Page - 1) * command.ListQuery.Size
		}

		if offset >= len(coursesCategory) {
			offset = len(coursesCategory) - 1
		}

		limit := command.ListQuery.Size

		endIndex := offset + limit

		if endIndex > len(coursesCategory) {
			endIndex = len(coursesCategory)
		}

		coursesCategory = coursesCategory[offset:endIndex]
		d := float64(totalItem) / float64(command.ListQuery.Size)

		totalPage = int(math.Ceil(d))

		result := &utils.ListResult[*models.CourseCategory]{
			Size:       courses.Size,
			Page:       command.ListQuery.Page,
			TotalItems: totalItem,
			TotalPage:  totalPage,
			Items:      coursesCategory,
		}
		overallFilter := &dtos.OverallCourse{
			TierOneRatingCount:     tierOneRatingCount,
			TierTwoRatingCount:     tierTwoRatingCount,
			TierThreeRatingCount:   tierThreeRatingCount,
			TieFourRatingCount:     tieFourRatingCount,
			TierOneDurationCount:   tierOneDurationCount,
			TierTwoDurationCount:   tierTwoDurationCount,
			AllLevelCount:          allLevelCount,
			BeginnerLevelCount:     beginnerLevelCount,
			IntermediateLevelCount: intermediateLevelCount,
			ExpertLevelCount:       expertLevelCount,
		}
		// fmt.Println("totalItem: ", totalItem)
		// fmt.Println("command.ListQuery.Size: ", command.ListQuery.Size)
		// fmt.Println("totalPage: ", totalPage)

		return &dtos.GetCoursesByCategoryIDResponseDto{
			Message:       "Get courses successfully!",
			Courses:       result,
			OverallCourse: overallFilter,
			Category: categoryResponse,
		}, nil
	}
	result := &utils.ListResult[*models.CourseCategory]{
		Size:       courses.Size,
		Page:       command.ListQuery.Page,
		TotalItems: totalItem,
		TotalPage:  totalPage,
		Items:      coursesCategory,
	}
	// CourseResponse
	return &dtos.GetCoursesByCategoryIDResponseDto{
		Message: "Get courses successfully!",
		Courses: result,
		Category: categoryResponse,
	}, nil
}
