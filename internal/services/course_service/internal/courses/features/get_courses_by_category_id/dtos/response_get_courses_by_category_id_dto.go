package dtos

// import "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"
import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"
)

type OverallCourse struct {
	TierOneRatingCount     int `json:"tier_one_rating_count"`
	TierTwoRatingCount     int `json:"tier_two_rating_count"`
	TierThreeRatingCount   int `json:"tier_three_rating_count"`
	TieFourRatingCount     int `json:"tier_four_rating_count"`
	TierOneDurationCount   int `json:"tier_one_duration_count"`
	TierTwoDurationCount   int `json:"tier_two_duration_count"`
	AllLevelCount          int `json:"all_level_count"`
	BeginnerLevelCount     int `json:"beginner_level_count"`
	IntermediateLevelCount int `json:"intermediate_level_count"`
	ExpertLevelCount       int `json:"expert_level_count"`
}

type GetCoursesByCategoryIDResponseDto struct {
	Message string `json:"message"`
	Category *models.CategoryShow `json:"category"`
	// Courses *utils.ListResult[*models.CourseResponse] `json:"courses"`
	Courses *utils.ListResult[*models.CourseCategory] `json:"courses"`
	OverallCourse *OverallCourse `json:"overall_filter"`
	// Course []*models.Course `json:"courses"`
}
