package consumer_rabbitmq

import (
	"context"
	"encoding/json"
	"fmt"
	"log"

	"github.com/mehdihadeli/go-mediatr"
	"github.com/rabbitmq/amqp091-go"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	registerCourseCommand "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/register_course/commands"
	registerCourseDto "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/register_course/dtos"

	
)

func RegisterCourseHandler(d amqp091.Delivery) {
	log.Printf("Received a message : %s", d.Body)
	
	var registerCourse constants.RegisterCourse
	err := json.Unmarshal(d.Body, &registerCourse)

	if err != nil {
		log.Println("[registerCourse]: ", err)
		d.Reject(true)
	}
	
	// fmt.Println("dataSendNotificationPurchaseCourse : ", sendNotificationPurchaseCourse)

	command, _ := registerCourseCommand.NewDeleteAnswer(
		int(registerCourse.IdUser),
		int(registerCourse.IdCourse),
	)

	_, err = mediatr.Send[*registerCourseCommand.RegisterCourse, *registerCourseDto.RegisterCourseResponseDto](
		context.Background(),
		command,
	)
	fmt.Println("Err 1: ", err)
	if err != nil {
		d.Reject(true)
	} else {
		d.Ack(false)
	}
}