package courses

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/configurations"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/consumer"
	appconfig "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/config"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/consumer_rabbitmq"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/configurations/courses/infrastructure"
	"go.uber.org/fx"
	// internal/shared/configurations/users/infrastructure
)

var CoursesServiceModule = fx.Module(
	"coursesfx",
	// Shared Modules
	appconfig.Module,
	infrastructure.Module,

	// Features Modules
	courses.Module,

	// Other provides
	// fx.Provide(provideCatalogsMetrics),
	fx.Invoke(func(rabbitmqBuilder configurations.RabbitMQConfigurationBuilder) {
		rabbitmqBuilder.AddConsumerToQueue(&consumer.RabbitMQConsumerToQueue{
			QueueName:   constants.REGISTER_COURSE,
			HandlerFunc: consumer_rabbitmq.RegisterCourseHandler,
		})

	}),
)
