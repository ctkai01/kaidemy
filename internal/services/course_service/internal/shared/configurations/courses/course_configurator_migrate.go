package courses

import (
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"
	"gorm.io/gorm"
)

func (ic *CoursesServiceConfigurator) migrateCourses(gorm *gorm.DB) error {
	// or we could use `gorm.Migrate()`
	err := gorm.AutoMigrate(
		&models.Course{}, &models.Curriculum{},
		&models.Lecture{},
		 &models.Question{},
		 &models.Answer{},
		// &models.Quiz{},
		&models.Asset{}, &models.Learning{}, &models.TopicLearning{},
		&models.LearningTopicLearning{},
		&models.Report{},
		&models.QuestionLecture{},
		&models.AnswerLecture{},
		&models.LearningLecture{},
	)
	if err != nil {
		return err
	}

	return nil
}
