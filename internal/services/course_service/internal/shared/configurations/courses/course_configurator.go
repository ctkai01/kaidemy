package courses

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	"gorm.io/gorm"

	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/config"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/configurations/infrastructure"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/configurations"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/configurations"
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/configurations/courses/infrastructure"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp/contracts"
	customEcho "gitlab.com/ctkai01/kaidemy/internal/pkg/http/custom_echo"
)

type CoursesServiceConfigurator struct {
	contracts.Application
	infrastructureConfigurator *infrastructure.InfrastructureConfigurator
	coursesModuleConfigurator  *configurations.CoursesModuleConfigurator
}

func NewCoursesServiceConfigurator(app contracts.Application) *CoursesServiceConfigurator {
	infraConfigurator := infrastructure.NewInfrastructureConfigurator(app)
	courseModuleConfigurator := configurations.NewCoursesModuleConfigurator(app)

	return &CoursesServiceConfigurator{
		Application:                app,
		infrastructureConfigurator: infraConfigurator,
		coursesModuleConfigurator:  courseModuleConfigurator,
	}
}

func (ic *CoursesServiceConfigurator) ConfigureCourses() {
	// Shared
	// Infrastructure
	ic.infrastructureConfigurator.ConfigInfrastructures()
	// Shared
	// Catalogs configurations
	ic.ResolveFunc(func(gorm *gorm.DB) error {
		err := ic.migrateCourses(gorm)
		if err != nil {
			return err
		}
		return nil
	})
	// Modules
	// Product module
	ic.coursesModuleConfigurator.ConfigureCoursesModule()
}

func (ic *CoursesServiceConfigurator) MapCoursesEndpoints() {
	// Shared
	ic.ResolveFunc(
		func(coursesServer customEcho.EchoHttpServer, cfg *config.Config) error {
			coursesServer.SetupDefaultMiddlewares()

			// Config catalogs root endpoint
			coursesServer.RouteBuilder().
				RegisterRoutes(func(e *echo.Echo) {
					e.GET("", func(ec echo.Context) error {
						return ec.String(
							http.StatusOK,
							fmt.Sprintf(
								"%s is running...",
								cfg.AppOptions.GetMicroserviceNameUpper(),
							),
						)
					})
				})

			// Config catalogs swagger
			// ic.configSwagger(catalogsServer.RouteBuilder())

			return nil
		},
	)

	// Modules
	// Products CatalogsServiceModule endpoints
	ic.coursesModuleConfigurator.MapCoursesEndpoints()
}
