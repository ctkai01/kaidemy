package utils

// import (
// 	"fmt"
// 	"math/rand"
// 	"strconv"
// 	"time"

// 	"github.com/dgrijalva/jwt-go"
// 	constantsCommon "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
// 	"golang.org/x/crypto/bcrypt"
// )

// func Encode(id int, role int) (string, error) {
// 	claims := jwt.MapClaims{
// 		"sub":  id,
// 		"role": role,
// 		"exp":  time.Now().Add(time.Hour * 72).Unix(),
// 	}

// 	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
// 	t, err := token.SignedString(constantsCommon.JwtSecret)
// 	if err != nil {
// 		return "", err
// 	}
// 	return t, nil
// }

// func HashPassword(password string) (string, error) {
// 	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
// 	if err != nil {
// 		return "", err
// 	}

// 	return string(hashedPassword), nil
// }

// func ComparePassword(password string, passwordHash string) bool {
// 	err := bcrypt.CompareHashAndPassword([]byte(passwordHash), []byte(password))

// 	if err != nil {
// 		return false
// 	} else {
// 		return true
// 	}
// }

// func EncodeEmail(email string) (string, error) {
// 	claims := jwt.MapClaims{
// 		"sub": email,
// 		"exp": time.Now().Add(time.Hour * 24).Unix(),
// 	}

// 	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
// 	t, err := token.SignedString(constantsCommon.JwtSecret)
// 	if err != nil {
// 		return "", err
// 	}
// 	return t, nil
// }

// func ParseClaimsEmailToken(emailToken string) (*jwt.Token, error) {
// 	claims := &jwt.StandardClaims{}

// 	token, err := jwt.ParseWithClaims(emailToken, claims, func(token *jwt.Token) (interface{}, error) {
// 		return constantsCommon.JwtSecret, nil
// 	})

// 	return token, err
// }

// func GenerateUploadLink(nameFile string) string {
// 	uploadLink := fmt.Sprint(time.Now().UnixMilli()) + strconv.Itoa(rand.Int()) + nameFile
// 	return uploadLink
// }
