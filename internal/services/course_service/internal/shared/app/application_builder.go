package app

import (
	"os"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/config/environment"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp/contracts"
)

type CoursesApplicationBuilder struct {
	contracts.ApplicationBuilder
}

func NewCourseApplicationBuilder() *CoursesApplicationBuilder {
	var envs []environment.Environment 

	if os.Getenv("ENV") == string(environment.Production) {
		envs = append(envs, environment.Production)
	} else {
		envs = append(envs, environment.Development)
	}
	builder := &CoursesApplicationBuilder{fxapp.NewApplicationBuilder(envs...)}

	return builder
}

func (a *CoursesApplicationBuilder) Build() *CoursesApplication {

	return NewCoursesApplication(
		a.GetProvides(),
		a.GetDecorates(),
		a.Options(),
		a.Logger(),
		a.Environment(),
	)
}
