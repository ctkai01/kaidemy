package app

import (
	"go.uber.org/fx"

	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/configurations/courses"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/config/environment"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
)

type CoursesApplication struct {
	*courses.CoursesServiceConfigurator
}

func NewCoursesApplication(
	providers []interface{},
	decorates []interface{},
	options []fx.Option,
	logger logger.Logger,
	environment environment.Environment,
) *CoursesApplication {
	app := fxapp.NewApplication(providers, decorates, options, logger, environment)
	return &CoursesApplication{
		CoursesServiceConfigurator: courses.NewCoursesServiceConfigurator(app),
	}
}
