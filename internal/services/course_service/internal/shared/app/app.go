package app

import (
	"gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/configurations/courses"
)

type App struct{}

func NewApp() *App {
	return &App{}
}

func (a *App) Run() {
	// configure dependencies
	appBuilder := NewCourseApplicationBuilder()
	appBuilder.ProvideModule(courses.CoursesServiceModule)

	app := appBuilder.Build()
	// // configure application
	app.ConfigureCourses()
	app.MapCoursesEndpoints()

	app.Logger().Info("Starting course service application")
	app.Run()
}
