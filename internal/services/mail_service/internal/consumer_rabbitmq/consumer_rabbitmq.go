package consumer_rabbitmq

import (
	"encoding/json"
	"fmt"
	"log"
	"os"

	"github.com/rabbitmq/amqp091-go"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/services/mailservice/internal/mail/service"
)

func ForgotPasswordHandler(d amqp091.Delivery) {
	log.Printf("Received a message 1: %s", d.Body)

	var forgotPassword constants.MailForgotPassword

	err := json.Unmarshal(d.Body, &forgotPassword)
	if err != nil {
		fmt.Println("Err Unmarshal: ", err)
		d.Reject(true)
	}
	data := service.MailInfoForgotPassword{
		FullName:    forgotPassword.Name,
		URLRedirect: fmt.Sprintf("%s/%s?email=%s", os.Getenv("APP_URL"), "email-password-change", forgotPassword.Token),
	}

	mailTo := "quangnam11032001@gmail.com"
	mailForgotPassword := service.NewMailForgotPassword([]string{mailTo}, data)

	if err := mailForgotPassword.SendMail(); err != nil {
		fmt.Println("ERR: ", err)
		d.Reject(true)
	} else {
		d.Ack(false)
	}
}
