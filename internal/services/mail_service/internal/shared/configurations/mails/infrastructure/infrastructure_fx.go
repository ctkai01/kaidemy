package infrastructure

import (
	// "github.com/go-playground/validator"

	"go.uber.org/fx"

	// rabbitmq2 "gitlab.com/ctkai01/kaidemy/internal/services/catalogreadservice/internal/products/configurations/rabbitmq"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/core"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/configurations"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/consumer"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	customEcho "gitlab.com/ctkai01/kaidemy/internal/pkg/http/custom_echo"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/mongodb"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/otel/tracing"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/configurations"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/redis"

	"gitlab.com/ctkai01/kaidemy/internal/services/mailservice/internal/consumer_rabbitmq"
)

// https://pmihaylov.com/shared-components-go-microservices/
var Module = fx.Module(
	"infrastructurefx",
	// Modules
	core.Module,
	customEcho.Module,
	// grpc.Module,
	// mongodb.Module,
	// redis.Module,
	rabbitmq.Module,

	fx.Invoke(func(rabbitmqBuilder configurations.RabbitMQConfigurationBuilder) {
		rabbitmqBuilder.AddConsumerToQueue(&consumer.RabbitMQConsumerToQueue{
			QueueName:   constants.SEND_MAIL_FORGOT_PASSWORD,
			HandlerFunc: consumer_rabbitmq.ForgotPasswordHandler,
		})

	}),

	// func(v *validator.Validate, l logger.Logger, tracer tracing.AppTracer) configurations.RabbitMQConfigurationBuilderFuc {
	// 	return func(builder configurations.RabbitMQConfigurationBuilder) {
	// 		// rabbitmq2.ConfigProductsRabbitMQ(builder, l, v, tracer)
	// 	}
	// },
	// func() {
	// 	fmt.Println("Rabbitmq 123")

	// Other provides
	// fx.Provide(validator.New),
)
