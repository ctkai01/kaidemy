package mails

import (
	// "fmt"
	// "github.com/labstack/echo/v4"
	// "net/http"

	// "gitlab.com/ctkai01/kaidemy/internal/services/mailservice/config"
	// "gitlab.com/ctkai01/kaidemy/internal/services/mailservice/internal/shared/configurations/infrastructure"
	// "gitlab.com/ctkai01/kaidemy/internal/services/mailservice/internal/users/configurations"
	"gitlab.com/ctkai01/kaidemy/internal/services/mailservice/internal/shared/configurations/mails/infrastructure"
	// "gitlab.com/ctkai01/kaidemy/internal/services/mailservice/internal/mails/configurations"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp/contracts"
	// customEcho "gitlab.com/ctkai01/kaidemy/internal/pkg/http/custom_echo"
)

type MailsServiceConfigurator struct {
	contracts.Application
	infrastructureConfigurator *infrastructure.InfrastructureConfigurator
	// usersModuleConfigurator    *configurations.UsersModuleConfigurator
}

func NewMailsServiceConfigurator(app contracts.Application) *MailsServiceConfigurator {
	infraConfigurator := infrastructure.NewInfrastructureConfigurator(app)
	// userModuleConfigurator := configurations.NewUsersModuleConfigurator(app)

	return &MailsServiceConfigurator{
		Application:                app,
		infrastructureConfigurator: infraConfigurator,
		// usersModuleConfigurator:    userModuleConfigurator,
	}
}

func (ic *MailsServiceConfigurator) ConfigureMails() {
	// Shared
	// Infrastructure
	ic.infrastructureConfigurator.ConfigInfrastructures()
	// Shared
	// Catalogs configurations

	// Modules
	// Product module
	// ic.usersModuleConfigurator.ConfigureUsersModule()
}

// func (ic *MailsServiceConfigurator) MapUsersEndpoints() {
// 	// Shared
// 	ic.ResolveFunc(
// 		func(usersServer customEcho.EchoHttpServer, cfg *config.Config) error {
// 			usersServer.SetupDefaultMiddlewares()

// 			// Config catalogs root endpoint
// 			usersServer.RouteBuilder().
// 				RegisterRoutes(func(e *echo.Echo) {
// 					e.GET("", func(ec echo.Context) error {
// 						return ec.String(
// 							http.StatusOK,
// 							fmt.Sprintf(
// 								"%s is running...",
// 								cfg.AppOptions.GetMicroserviceNameUpper(),
// 							),
// 						)
// 					})
// 				})

// 			// Config catalogs swagger
// 			// ic.configSwagger(catalogsServer.RouteBuilder())

// 			return nil
// 		},
// 	)

// 	// Modules
// 	// Products CatalogsServiceModule endpoints
// 	ic.usersModuleConfigurator.MapUsersEndpoints()
// 	ic.usersModuleConfigurator.MapAuthEndpoints()
// }
