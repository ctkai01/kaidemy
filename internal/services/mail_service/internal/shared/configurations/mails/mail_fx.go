package mails

import (
	appconfig "gitlab.com/ctkai01/kaidemy/internal/services/mailservice/config"
	"gitlab.com/ctkai01/kaidemy/internal/services/mailservice/internal/shared/configurations/mails/infrastructure"
	"go.uber.org/fx"
	// "gitlab.com/ctkai01/kaidemy/internal/services/mailservice/internal/mails"
	// internal/shared/configurations/users/infrastructure
)

var MailsServiceModule = fx.Module(
	"mailsfx",
	// Shared Modules
	appconfig.Module,
	infrastructure.Module,

	// Features Modules
	// users.Module,

	// Other provides
	// fx.Provide(provideCatalogsMetrics),
)
