package app

import (
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/shared/configurations/"
	"gitlab.com/ctkai01/kaidemy/internal/services/mailservice/internal/shared/configurations/mails"
)

type App struct{}

func NewApp() *App {
	return &App{}
}

func (a *App) Run() {
	// configure dependencies
	appBuilder := NewMailsApplicationBuilder()
	appBuilder.ProvideModule(mails.MailsServiceModule)

	app := appBuilder.Build()
	// // configure application
	// app.ConfigureMails()
	// app.MapUsersEndpoints()

	app.Logger().Info("Starting mail service application")
	app.Run()
}
