package app

import (
	"go.uber.org/fx"

	"gitlab.com/ctkai01/kaidemy/internal/services/mailservice/internal/shared/configurations/mails"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/config/environment"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
)

type MailsApplication struct {
	*mails.MailsServiceConfigurator
}

func NewMailsApplication(
	providers []interface{},
	decorates []interface{},
	options []fx.Option,
	logger logger.Logger,
	environment environment.Environment,
) *MailsApplication {
	app := fxapp.NewApplication(providers, decorates, options, logger, environment)
	return &MailsApplication{

		MailsServiceConfigurator: mails.NewMailsServiceConfigurator(app),
	}
}
