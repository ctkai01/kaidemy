package service

import (
	"fmt"
	"html/template"
	"net/smtp"
	"strings"
)

type MailInfoForgotPassword struct {
	FullName    string
	URLRedirect string
}

var (
	SUBJECT_RESET_PASSWORD = "Reset Password"
)

type MailForgotPassword struct {
	to   []string
	data MailInfoForgotPassword
}

func NewMailForgotPassword(to []string, data MailInfoForgotPassword) EmailService {
	return &MailForgotPassword{
		to,
		data,
	}
}

func (mail *MailForgotPassword) SendMail() error {
	from := "kaidemy2023@gmail.com"

	password := "ybbecfbciohbhied"
	address :=  fmt.Sprintf("%s:%s", "smtp.gmail.com", "587")
	
	subject := SUBJECT_RESET_PASSWORD

	var messageTemplate *template.Template

	template, err := template.ParseFiles("internal/mail/template/forgot-password.html")
	
	messageTemplate = template
	if err != nil {
		return err
	}

	var messageText strings.Builder

	if err := messageTemplate.Execute(&messageText, mail.data); err != nil {
		return err
	}

	message := []byte(fmt.Sprintf("To: %s\r\nSubject: %s\r\nContent-Type: text/html; charset=UTF-8\r\n\r\n%s", strings.Join([]string{from}, ","), subject, messageText.String()))
	auth := smtp.PlainAuth("", from, password, "smtp.gmail.com")

	if err := smtp.SendMail(address, auth, from, mail.to, message); err != nil {
		return err
	}

	return nil
}