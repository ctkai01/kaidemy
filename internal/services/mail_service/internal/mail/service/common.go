package service

type EmailService interface {
	SendMail() error
}