module gitlab.com/ctkai01/kaidemy/internal/services/userservice

go 1.19

require github.com/go-playground/validator v9.31.0+incompatible

require (
	github.com/ahmetb/go-linq/v3 v3.2.0 // indirect
	github.com/goccy/go-reflect v1.2.0 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/stripe/stripe-go/v76 v76.6.0 // indirect
	golang.org/x/net v0.14.0 // indirect
	golang.org/x/oauth2 v0.11.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
)

require (
	github.com/go-playground/locales v0.14.1 // indirect
	github.com/go-playground/universal-translator v0.18.1 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/leodido/go-urn v1.2.4 // indirect
	github.com/mehdihadeli/go-mediatr v1.1.10
	github.com/spf13/cobra v1.7.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
)
