package mappings

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/mapper"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/dto"
	usersService "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/grpc/proto/service_clients"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	"google.golang.org/protobuf/types/known/timestamppb"
)

// "github.com/mehdihadeli/go-ecommerce-microservices/internal/services/catalogreadservice/internal/products/models"
// "gitlab.com/ctkai01/kaidemy/internal/pkg/mapper"

func ConfigureUsersMappings() error {
	err := mapper.CreateMap[*models.User, *dto.UserDto]()
	if err != nil {
		return err
	}

	err = mapper.CreateMap[*models.User, *models.User]()
	if err != nil {
		return err
	}

	err = mapper.CreateCustomMap[*dto.UserDto, *usersService.User](
		func(user *dto.UserDto) *usersService.User {
			if user == nil {
				return nil
			}
			var avatar string
			var headline string
			var biography string
			var websiteURL string
			var twitterURL string
			var facebookURL string
			var linkedInURL string
			var youtubeURL string
			var accountStripeID string

			if user.Avatar != nil {
				avatar = *user.Avatar
			}

			if user.AccountStripeID != nil {
				accountStripeID = *user.AccountStripeID
			}

			if user.Headline != nil {
				headline = *user.Headline
			}

			if user.Biography != nil {
				biography = *user.Biography
			}

			if user.WebsiteURL != nil {
				websiteURL = *user.WebsiteURL
			}

			if user.TwitterURL != nil {
				twitterURL = *user.TwitterURL
			}

			if user.LinkedInURL != nil {
				linkedInURL = *user.LinkedInURL
			}

			if user.FacebookURL != nil {
				facebookURL = *user.FacebookURL
			}

			if user.YoutubeURL != nil {
				youtubeURL = *user.YoutubeURL
			}

			return &usersService.User{
				ID:          int32(user.ID),
				Email:       user.Email,
				TypeAccount: int32(user.TypeAccount),
				Name: 		user.Name,
				Avatar:      avatar,
				Headline:    headline,
				Biography:   biography,
				WebsiteURL:  websiteURL,
				TwitterURL:  twitterURL,
				FacebookURL: facebookURL,
				LinkedInURL: linkedInURL,
				YoutubeURL:  youtubeURL,
				Role:        int32(user.Role),
				AccountStripeID: accountStripeID,
				CreatedAt:   timestamppb.New(*user.CreatedAt),
				UpdatedAt:   timestamppb.New(*user.UpdatedAt),
			}
		},
	)

	if err != nil {
		return err
	}

	// err = mapper.CreateMap[*models.Product, *models.Product]()
	// if err != nil {
	// 	return err
	// }

	return nil
}
