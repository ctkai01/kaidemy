package configurations

import (
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/configurations/mappings"
	"golang.org/x/oauth2"

	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/configurations/mediator"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/data"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp/contracts"
	logger2 "gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/configurations"
	googleGrpc "google.golang.org/grpc"

	grpcServer "gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/grpc"

	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/configurations/mediator"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/data"
	usersservice "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/grpc/proto/service_clients"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	// grpcServer "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/pkg/grpc"

)

type UsersModuleConfigurator struct {
	contracts.Application
}

func NewUsersModuleConfigurator(
	app contracts.Application,
) *UsersModuleConfigurator {
	return &UsersModuleConfigurator{
		Application: app,
	}
}

func (c *UsersModuleConfigurator) ConfigureUsersModule() {
	c.ResolveFunc(
		func(logger logger2.Logger, userRepository data.UserRepository, googleOauth *oauth2.Config, rabbitmqBuilder configurations.RabbitMQConfigurationBuilder, grpcClient grpcServer.GrpcClient) error {
			// Config Products Mediators
			err := mediator.ConfigUsersMediator(logger, userRepository, googleOauth, rabbitmqBuilder, grpcClient)
			if err != nil {
				return err
			}

			// Config Products Mappings
			err = mappings.ConfigureUsersMappings()
			if err != nil {
				return err
			}
			return nil
		},
	)
}

func (c *UsersModuleConfigurator) MapAuthEndpoints() {
	// Config Users Http Endpoints
	c.ResolveFuncWithParamTag(func(endpoints []route.Endpoint) {
		for _, endpoint := range endpoints {
			endpoint.MapEndpoint()
		}
	}, `group:"auth-routes"`,
	)
}

func (c *UsersModuleConfigurator) MapUsersEndpoints() {
	// Config Users Http Endpoints
	c.ResolveFuncWithParamTag(func(endpoints []route.Endpoint) {
		for _, endpoint := range endpoints {
			endpoint.MapEndpoint()
		}
	}, `group:"users-routes"`,
	)

	c.ResolveFunc(
		func(usersGrpcServer grpcServer.GrpcServer, grpcService *grpc.UserGrpcServiceServer) error {
			usersGrpcServer.GrpcServiceBuilder().RegisterRoutes(func(server *googleGrpc.Server) {
				usersservice.RegisterUsersServiceServer(server, grpcService)
			})

			return nil
		},
	)
}
