package mediator

import (
	// "emperror.dev/errors"
	// "github.com/mehdihadeli/go-mediatr"

	"github.com/mehdihadeli/go-mediatr"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/data"
	forgotPasswordCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/forgot_password/commands"
	forgotPasswordDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/forgot_password/dtos"
	loginCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login/commands"
	loginDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login/dtos"
	loginGoogleCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login_by_google/commands"
	loginGoogleDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login_by_google/dtos"

	// loginGoogleQuery "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login_by_google/queries"
	registerCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/register/commands"
	registerDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/register/dtos"
	requestLoginGoogleCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/commands"
	requestLoginGoogleDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	resetPasswordCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/reset_password/commands"
	resetPasswordDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/reset_password/dtos"

	changePasswordCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/change_password/commands"
	changePasswordDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/change_password/dtos"

	changeProfileCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/change_profile/commands"
	changeProfileDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/change_profile/dtos"

	registerTeacherCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/register_teacher/commands"
	registerTeacherDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/register_teacher/dtos"

	profileByMeCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/profile_by_me/commands"
	profileByMeDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/profile_by_me/dtos"

	createNormalAdminCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/create_normal_admin/commands"
	createNormalAdminDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/create_normal_admin/dtos"

	updateNormalAdminCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/update_normal_admin/commands"
	updateNormalAdminDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/update_normal_admin/dtos"

	deleteNormalAdminCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/delete_normal_admin/commands"
	deleteNormalAdminDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/delete_normal_admin/dtos"

	getNormalAdminDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_normal_admins/dtos"
	getNormalAdminQueries "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_normal_admins/queries"

	requestRegisterTeacherCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/request_register_teacher/commands"
	requestRegisterTeacherDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/request_register_teacher/dtos"

	verifyRegisterTeacherCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/verify_register_teacher/commands"
	verifyRegisterTeacherDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/verify_register_teacher/dtos"

	getUsersDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_users/dtos"
	getUsersQueries "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_users/queries"

	blockUserCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/block_user/commands"
	blockUserDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/block_user/dtos"

	getUserPublicByIDDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_user_public_by_id/dtos"
	getUserPublicByIDQueries "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_user_public_by_id/queries"

	getSearchDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_teachers_search/dtos"
	getSearchQueries "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_teachers_search/queries"

	getOverviewStudentDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_overview_student/dtos"
	getOverviewStudentQueries "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_overview_student/queries"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	"golang.org/x/oauth2"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/configurations"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_user_by_id/dtos"
	getUserIdQuery "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_user_by_id/queries"
)

// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"

func ConfigUsersMediator(
	logger logger.Logger,
	userRepository data.UserRepository,
	googleOauth *oauth2.Config,
	rabbitmqBuilder configurations.RabbitMQConfigurationBuilder,
	grpcClient grpc.GrpcClient,

) error {
	err := mediatr.RegisterRequestHandler[*any, *requestLoginGoogleDtos.RequestLoginGoogleResponseDto](
		requestLoginGoogleCommand.NewRequestLoginGoogleHandler(logger, googleOauth),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*loginGoogleCommand.UserCreateByLoginGoogle, *loginGoogleDtos.ResponseLoginGoogleDto](
		loginGoogleCommand.NewLoginGoogleHandler(logger, googleOauth, userRepository),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*registerCommand.UserCreateByNormal, *registerDtos.RegisterResponseDto](
		registerCommand.NewRegisterHandler(logger, googleOauth, userRepository),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*loginCommand.UserLoginStandard, *loginDtos.LoginResponseDto](
		loginCommand.NewLoginHandler(logger, googleOauth, userRepository),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*forgotPasswordCommand.ForgotPassword, *forgotPasswordDtos.ForgotPasswordResponseDto](
		forgotPasswordCommand.NewForgotPasswordHandler(logger, userRepository, rabbitmqBuilder),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*resetPasswordCommand.ResetPassword, *resetPasswordDtos.ResetPassResponseDto](
		resetPasswordCommand.NewResetPasswordHandler(logger, userRepository),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*changePasswordCommand.ChangePassword, *changePasswordDtos.ChangePasswordResponseDto](
		changePasswordCommand.NewChangePasswordHandler(logger, userRepository),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*changeProfileCommand.ChangeProfile, *changeProfileDtos.ChangeProfileResponseDto](
		changeProfileCommand.NewChangeProfileHandler(logger, userRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*registerTeacherCommand.RegisterTeacher, *registerTeacherDtos.RegisterTeacherResponseDto](
		registerTeacherCommand.NewRegisterTeacherHandler(logger, userRepository),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*profileByMeCommand.ProfileByMe, *profileByMeDtos.ProfileByMeResponseDto](
		profileByMeCommand.NewProfileByMeHandler(logger, userRepository),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getUserIdQuery.GetUserById, *dtos.GetUserByIdResponseDto](
		getUserIdQuery.NewGetUserByIdHandler(logger, userRepository),
	)
	if err != nil {
		return err
	}

	//Admin
	err = mediatr.RegisterRequestHandler[*createNormalAdminCommand.CreateNormalAdmin, *createNormalAdminDtos.CreateNormalAdminResponseDto](
		createNormalAdminCommand.NewCreateNormalAdminHandler(logger, userRepository),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*updateNormalAdminCommand.UpdateNormalAdmin, *updateNormalAdminDtos.UpdateNormalAdminResponseDto](
		updateNormalAdminCommand.NewUpdateNormalAdminHandler(logger, userRepository),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*deleteNormalAdminCommand.DeleteNormalAdmin, *deleteNormalAdminDtos.DeleteNormalAdminResponseDto](
		deleteNormalAdminCommand.NewDeleteNormalAdminHandler(logger, userRepository),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getNormalAdminQueries.GetNormalAdmins, *getNormalAdminDtos.GetNormalAdminsResponseDto](
		getNormalAdminQueries.NewGetNormalAdminsHandler(logger, userRepository),
	)
	if err != nil {
		return err
	}

	//User
	err = mediatr.RegisterRequestHandler[*requestRegisterTeacherCommand.RequestRegisterTeacher, *requestRegisterTeacherDtos.RequestRegisterTeacherResponseDto](
		requestRegisterTeacherCommand.NewRequestRegisterTeacherHandler(logger, userRepository),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*verifyRegisterTeacherCommand.VerifyRegisterTeacher, *verifyRegisterTeacherDtos.VerifyRegisterTeacherResponseDto](
		verifyRegisterTeacherCommand.NewVerifyRegisterTeacherHandler(logger, userRepository),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getUsersQueries.GetUsers, *getUsersDtos.GetUsersResponseDto](
		getUsersQueries.NewGetUsersHandler(logger, userRepository),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*blockUserCommand.BlockUser, *blockUserDtos.BlockUserResponseDto](
		blockUserCommand.NewBlockUserHandler(logger, userRepository),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getUserPublicByIDQueries.GetUserPublicById, *getUserPublicByIDDtos.GetUserPublicByIdResponseDto](
		getUserPublicByIDQueries.NewGetUserPublicByIdHandler(logger, userRepository),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getSearchQueries.GetTeachersSearch, *getSearchDtos.GetTeachersSearchResponseDto](
		getSearchQueries.NewGetTeachersSearchHandler(logger, userRepository),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getOverviewStudentQueries.GetOverviewStudent, *getOverviewStudentDtos.GetOverviewStudentResponseDto](
		getOverviewStudentQueries.NewGetOverviewStudentHandler(logger, userRepository),
	)
	if err != nil {
		return err
	}

	// err = mediatr.RegisterRequestHandler[*getUserByIDQueries.GetUserById, *getUserByIDDtos.GetUserByIdResponseDto](
	// 	getUserByIDQueries.NewGetUserByIdHandler(logger, userRepository),
	// )
	// if err != nil {
	// 	return err
	// }

	// err = mediatr.RegisterRequestHandler[*loginGoogleQuery.LoginGoogle, *loginGoogleDtos.LoginGoogleResponseDto](
	// 	loginGoogleCommand.NewLoginGoogleHandler(logger, googleOauth, userRepository),
	// )
	// if err != nil {
	// 	return err
	// }

	// err := mediatr.RegisterRequestHandler[*createProductCommandV1.CreateProduct, *createProductDtosV1.CreateProductResponseDto](
	// 	createProductCommandV1.NewCreateProductHandler(
	// 		logger,
	// 		mongoProductRepository,
	// 		cacheProductRepository,
	// 		tracer,
	// 	),
	// )
	// if err != nil {
	// 	return errors.WrapIf(err, "error while registering handlers in the mediator")
	// }

	// err = mediatr.RegisterRequestHandler[*deleteProductCommandV1.DeleteProduct, *mediatr.Unit](
	// 	deleteProductCommandV1.NewDeleteProductHandler(
	// 		logger,
	// 		mongoProductRepository,
	// 		cacheProductRepository,
	// 		tracer,
	// 	),
	// )
	// if err != nil {
	// 	return errors.WrapIf(err, "error while registering handlers in the mediator")
	// }

	// err = mediatr.RegisterRequestHandler[*updateProductCommandV1.UpdateProduct, *mediatr.Unit](
	// 	updateProductCommandV1.NewUpdateProductHandler(
	// 		logger,
	// 		mongoProductRepository,
	// 		cacheProductRepository,
	// 		tracer,
	// 	),
	// )
	// if err != nil {
	// 	return errors.WrapIf(err, "error while registering handlers in the mediator")
	// }

	// err = mediatr.RegisterRequestHandler[*getProductsQueryV1.GetProducts, *getProductsDtoV1.GetProductsResponseDto](
	// 	getProductsQueryV1.NewGetProductsHandler(logger, mongoProductRepository, tracer),
	// )
	// if err != nil {
	// 	return errors.WrapIf(err, "error while registering handlers in the mediator")
	// }

	// err = mediatr.RegisterRequestHandler[*searchProductsQueryV1.SearchProducts, *searchProductsDtosV1.SearchProductsResponseDto](
	// 	searchProductsQueryV1.NewSearchProductsHandler(
	// 		logger,
	// 		mongoProductRepository,
	// 		tracer,
	// 	),
	// )
	// if err != nil {
	// 	return errors.WrapIf(err, "error while registering handlers in the mediator")
	// }

	// err = mediatr.RegisterRequestHandler[*getProductByIdQueryV1.GetProductById, *getProductByIdDtosV1.GetProductByIdResponseDto](
	// 	getProductByIdQueryV1.NewGetProductByIdHandler(
	// 		logger,
	// 		mongoProductRepository,
	// 		cacheProductRepository,
	// 		tracer,
	// 	),
	// )
	// if err != nil {
	// 	return errors.WrapIf(err, "error while registering handlers in the mediator")
	// }

	return nil
}
