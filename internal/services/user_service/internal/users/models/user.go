package models

import (
	"time"
)

var (
	AccountNormal   = 0
	AccountGoogle   = 1
	AccountFacebook = 2
)

type User struct {
	ID                  int        `json:"id" gorm:"primary_key"`
	Email               string     `json:"email" gorm:"type:varchar(255);not null;unique"`
	Name                string     `json:"name" gorm:"type:varchar(255);not null"`
	Password            string     `json:"-" gorm:"type:varchar(255);not null"`
	TypeAccount         int        `json:"type_account" gorm:"not null"`
	Avatar              *string    `json:"avatar" gorm:"type:varchar(255)"`
	Headline            *string    `json:"headline" gorm:"type:varchar(255)"`
	Biography           *string    `json:"biography" gorm:"type:varchar(255)"`
	WebsiteURL          *string    `json:"website_url" gorm:"type:varchar(255)"`
	TwitterURL          *string    `json:"twitter_url" gorm:"type:varchar(255)"`
	FacebookURL         *string    `json:"facebook_url" gorm:"type:varchar(255)"`
	LinkedInURL         *string    `json:"linkedin_url" gorm:"type:varchar(255)"`
	YoutubeURL          *string    `json:"youtube_url" gorm:"type:varchar(255)"`
	Role                int        `json:"role" gorm:"not null"`
	EmailToken          *string    `json:"-" gorm:"type:varchar(255)"`
	AccountStripeID     *string    `json:"account_stripe_id"`
	AccountStripeStatus *int       `json:"account_stripe_status"`
	KeyAccountStripe    *string    `json:"key_account_stripe"`
	IsBlock             bool       `json:"is_block" gorm:"default:false"`
	UpdatedAt           *time.Time `json:"updated_at" gorm:"column:updated_at"`
	CreatedAt           *time.Time `json:"created_at" gorm:"column:created_at"`
}

func (User) TableName() string { return "users" }

// type ProductsList struct {
// 	TotalCount int64      `json:"totalCount" bson:"totalCount"`
// 	TotalPages int64      `json:"totalPages" bson:"totalPages"`
// 	Page       int64      `json:"page" bson:"page"`
// 	Size       int64      `json:"size" bson:"size"`
// 	Products   []*User `json:"products" bson:"products"`
// }
