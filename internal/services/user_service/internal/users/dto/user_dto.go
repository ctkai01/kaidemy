package dto

import "time"

type UserDto struct {
	ID          int        `json:"id"`
	Email       string     `json:"email"`
	Name        string     `json:"name"`
	TypeAccount int        `json:"type_account"`
	Avatar      *string    `json:"avatar"`
	Headline    *string    `json:"headline"`
	Biography   *string    `json:"biography"`
	WebsiteURL  *string    `json:"website_url"`
	TwitterURL  *string    `json:"twitter_url"`
	FacebookURL *string    `json:"facebook_url"`
	LinkedInURL *string    `json:"linkedin_url"`
	YoutubeURL  *string    `json:"youtube_url"`
	AccountStripeID *string    `json:"account_stripe_id"`
	Role        int        `json:"role"`
	UpdatedAt   *time.Time `json:"updated_at"`
	CreatedAt   *time.Time `json:"created_at"`
}
