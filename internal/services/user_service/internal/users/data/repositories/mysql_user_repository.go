package repositories

// https://github.com/Kamva/mgm
// https://github.com/mongodb/mongo-go-driver
// https://blog.logrocket.com/how-to-use-mongodb-with-go/
// https://www.mongodb.com/docs/drivers/go/current/quick-reference/
// https://www.mongodb.com/docs/drivers/go/current/fundamentals/bson/
// https://www.mongodb.com/docs

import (
	"context"
	"fmt"
	"time"

	// "errors"

	// "fmt"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"

	"emperror.dev/errors"
	// uuid2 "github.com/satori/go.uuid"
	"gorm.io/gorm"

	// "go.mongodb.org/mongo-driver/mongo"
	// attribute2 "go.opentelemetry.io/otel/attribute"

	data2 "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/core/data"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/core/data"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/gorm_mysql/repository"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/mongodb"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/mongodb/repository"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"

	userErrors "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/data/errors"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/otel/tracing/attribute"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
)

// const (
// 	productCollection = "products"
// )

type mysqlUserRepository struct {
	log                   logger.Logger
	gormGenericRepository data.GenericRepository[*models.User]
}

func NewMySqlUserRepository(
	log logger.Logger,
	db *gorm.DB,
) data2.UserRepository {
	gormRepository := repository.NewGenericGormRepository[*models.User](db)
	return &mysqlUserRepository{
		log:                   log,
		gormGenericRepository: gormRepository,
	}
}

func (p *mysqlUserRepository) CreateUser(
	ctx context.Context,
	user *models.User,
) error {
	err := p.gormGenericRepository.Add(ctx, user)

	if err != nil {
		if userErrors.CheckDuplicateEmail(err) {
			return userErrors.ErrAlreadyExistEmail
		}
		return err
	}

	return nil
}

func (p *mysqlUserRepository) GetUserByEmail(
	ctx context.Context,
	email string,
) (*models.User, error) {
	user, err := p.gormGenericRepository.GetByEmail(ctx, email)

	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, customErrors.ErrUserNotFound
		}
		return nil, err
	}

	return user, nil
}

func (p *mysqlUserRepository) GetUserByEmailToken(
	ctx context.Context,
	emailToken string,
) (*models.User, error) {
	user, err := p.gormGenericRepository.GetByEmailToken(ctx, emailToken)

	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, customErrors.ErrUserNotFound
		}
		return nil, err
	}

	return user, nil
}

func (p *mysqlUserRepository) GetUserByID(
	ctx context.Context,
	id int,
) (*models.User, error) {
	user, err := p.gormGenericRepository.GetById(ctx, id)

	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, customErrors.ErrUserNotFound
		}
		return nil, err
	}

	return user, nil
}

func (p *mysqlUserRepository) UpdateUser(ctx context.Context, user *models.User) error {
	err := p.gormGenericRepository.Update(ctx, user)

	if err != nil {
		return err
	}

	return nil
}

func (p *mysqlUserRepository) DeleteUserByID(ctx context.Context, id int) error {

	err := p.gormGenericRepository.Delete(ctx, id)
	if err != nil {
		return err
	}

	return nil
}

func (p *mysqlUserRepository) GetAllNormalAdmins(
	ctx context.Context,
	listQuery *utils.ListQuery,
) (*utils.ListResult[*models.User], error) {

	role := fmt.Sprintf("%d", constants.ADMIN)
	listQuery.Filters = append(listQuery.Filters, &utils.FilterModel{
		Field:      "role",
		Value:      &role,
		Comparison: "equals",
	})

	result, err := p.gormGenericRepository.GetAll(ctx, listQuery)
	if err != nil {
		return nil, errors.WrapIf(
			err,
			"[mysqlNormalAdminRepository_GetAllNormalAdmins.Paginate] error in the paginate",
		)
	}

	p.log.Infow(
		"[mysqlNormalAdminRepository_GetAllNormalAdmins] NormalAdmins loaded",
		logger.Fields{"NormalAdminsResult": result},
	)

	return result, nil
}

func (p *mysqlUserRepository) GetAllUsers(
	ctx context.Context,
	listQuery *utils.ListQuery,
) (*utils.ListResult[*models.User], error) {

	result, err := p.gormGenericRepository.GetAll(ctx, listQuery)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (p *mysqlUserRepository) GetTeachersSearch(
	ctx context.Context,
	search string,
) ([]*models.User, error) {
	// p.gormGenericRepository
	var users []*models.User

	db := p.gormGenericRepository.CustomDB(ctx).Model(&models.User{}).Where("(name LIKE ?) AND role = ? AND is_block = 0", "%"+search+"%", constants.TEACHER)

	if err := db.Find(&users).Error; err != nil {
		return nil, err
	}
	
	return users, nil
}
func (p *mysqlUserRepository) GetAllStudentInThisYear(ctx context.Context) ([]*models.User, error) {
	var user []*models.User
	// var learningsUnique []*models.Learning
	currentYear := time.Now().Year()
	db := p.gormGenericRepository.CustomDB(ctx).Model(&models.User{}).Where("EXTRACT(YEAR FROM created_at) = ?", currentYear).Where("role IN ?", []int{constants.TEACHER, constants.NORMAL_USER})
	// uniqueStudent := make(map[int]bool)

	if err := db.Find(&user).Error; err != nil {
		return nil, err
	}

	// for _, learning := range learnings {
	// 	if _, exists := uniqueStudent[learning.UserID]; exists {
	// 		uniqueStudent[learning.UserID] = true
	// 		learningsUnique = append(learningsUnique, learning)
	// 	}
	// }

	return user, nil
}

// if err := mysqlRepo.db.Table(user.TableName()).Where("email = ?", email).First(&user).Error; err != nil {
// 	fmt.Println(err)
// 	if err == gorm.ErrRecordNotFound {
// 		return nil, models.ErrUserNotFound
// 	}
// 	return nil, err
// }
// return user, nil
// ctx, span := p.tracer.Start(ctx, "postgresProductRepository.CreateProduct")
// defer span.End()

// err := p.gormGenericRepository.Add(ctx, product)
// if err != nil {
// 	return nil, tracing.TraceErrFromSpan(
// 		span,
// 		errors.WrapIf(
// 			err,
// 			"[postgresProductRepository_CreateProduct.Create] error in the inserting product into the database.",
// 		),
// 	)
// }

// return nil, nil
// }
// func (p *mysqlUserRepository) GetAllProducts(
// 	ctx context.Context,
// 	listQuery *utils.ListQuery,
// ) (*utils.ListResult[*models.User], error) {
// 	ctx, span := p.tracer.Start(ctx, "postgresProductRepository.GetAllProducts")
// 	defer span.End()

// 	result, err := p.gormGenericRepository.GetAll(ctx, listQuery)
// 	if err != nil {
// 		return nil, tracing.TraceErrFromContext(
// 			ctx,
// 			errors.WrapIf(
// 				err,
// 				"[postgresProductRepository_GetAllProducts.Paginate] error in the paginate",
// 			),
// 		)
// 	}

// 	p.log.Infow(
// 		"[postgresProductRepository.GetAllProducts] products loaded",
// 		logger.Fields{"ProductsResult": result},
// 	)
// 	span.SetAttributes(attribute.Object("ProductsResult", result))

// 	return result, nil
// }

// func (p *mysqlUserRepository) SearchProducts(
// 	ctx context.Context,
// 	searchText string,
// 	listQuery *utils.ListQuery,
// ) (*utils.ListResult[*models.User], error) {
// 	ctx, span := p.tracer.Start(ctx, "postgresProductRepository.SearchProducts")
// 	span.SetAttributes(attribute2.String("SearchText", searchText))
// 	defer span.End()

// 	result, err := p.gormGenericRepository.Search(ctx, searchText, listQuery)
// 	if err != nil {
// 		return nil, tracing.TraceErrFromContext(
// 			ctx,
// 			errors.WrapIf(
// 				err,
// 				"[postgresProductRepository_SearchProducts.Paginate] error in the paginate",
// 			),
// 		)
// 	}

// 	p.log.Infow(
// 		fmt.Sprintf(
// 			"[postgresProductRepository.SearchProducts] products loaded for search term '%s'",
// 			searchText,
// 		),
// 		logger.Fields{"ProductsResult": result},
// 	)
// 	span.SetAttributes(attribute.Object("ProductsResult", result))

// 	return result, nil
// }

// func (p *mysqlUserRepository) GetProductById(
// 	ctx context.Context,
// 	uuid uuid2.UUID,
// ) (*models.User, error) {
// 	ctx, span := p.tracer.Start(ctx, "postgresProductRepository.GetProductById")
// 	span.SetAttributes(attribute2.String("ProductId", uuid.String()))
// 	defer span.End()

// 	product, err := p.gormGenericRepository.GetById(ctx, uuid)
// 	if err != nil {
// 		return nil, tracing.TraceErrFromSpan(
// 			span,
// 			errors.WrapIf(
// 				err,
// 				fmt.Sprintf(
// 					"[postgresProductRepository_GetProductById.First] can't find the product with id %s into the database.",
// 					uuid,
// 				),
// 			),
// 		)
// 	}

// 	span.SetAttributes(attribute.Object("Product", product))
// 	p.log.Infow(
// 		fmt.Sprintf(
// 			"[postgresProductRepository.GetProductById] product with id %s laoded",
// 			uuid.String(),
// 		),
// 		logger.Fields{"Product": product, "ProductId": uuid},
// 	)

// 	return product, nil
// }

// 	span.SetAttributes(attribute.Object("Product", product))
// 	p.log.Infow(
// 		fmt.Sprintf(
// 			"[postgresProductRepository.CreateProduct] product with id '%s' created",
// 			product.ProductId,
// 		),
// 		logger.Fields{"Product": product, "ProductId": product.ProductId},
// 	)

// 	return product, nil
// }

// func (p *mysqlUserRepository) UpdateProduct(
// 	ctx context.Context,
// 	updateProduct *models.User,
// ) (*models.User, error) {
// 	ctx, span := p.tracer.Start(ctx, "postgresProductRepository.UpdateProduct")
// 	defer span.End()

// 	err := p.gormGenericRepository.Update(ctx, updateProduct)
// 	if err != nil {
// 		return nil, tracing.TraceErrFromSpan(
// 			span,
// 			errors.WrapIf(
// 				err,
// 				fmt.Sprintf(
// 					"[postgresProductRepository_UpdateProduct.Save] error in updating product with id %s into the database.",
// 					updateProduct.ProductId,
// 				),
// 			),
// 		)
// 	}

// 	span.SetAttributes(attribute.Object("Product", updateProduct))
// 	p.log.Infow(
// 		fmt.Sprintf(
// 			"[postgresProductRepository.UpdateProduct] product with id '%s' updated",
// 			updateProduct.ProductId,
// 		),
// 		logger.Fields{"Product": updateProduct, "ProductId": updateProduct.ProductId},
// 	)

// 	return updateProduct, nil
// }

// func (p *mysqlUserRepository) DeleteProductByID(ctx context.Context, uuid uuid2.UUID) error {
// 	ctx, span := p.tracer.Start(ctx, "postgresProductRepository.UpdateProduct")
// 	span.SetAttributes(attribute2.String("ProductId", uuid.String()))
// 	defer span.End()

// 	err := p.gormGenericRepository.Delete(ctx, uuid)
// 	if err != nil {
// 		return tracing.TraceErrFromSpan(span, errors.WrapIf(err, fmt.Sprintf(
// 			"[postgresProductRepository_DeleteProductByID.Delete] error in the deleting product with id %s into the database.",
// 			uuid,
// 		)))
// 	}

// 	p.log.Infow(
// 		fmt.Sprintf(
// 			"[postgresProductRepository.DeleteProductByID] product with id %s deleted",
// 			uuid,
// 		),
// 		logger.Fields{"Product": uuid},
// 	)

// 	return nil
// }
