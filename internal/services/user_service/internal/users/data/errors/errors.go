package errors

import (
	"strings"

	"emperror.dev/errors"
)

func CheckDuplicateEmail(err error) bool {
	return strings.Contains(err.Error(), "Duplicate")
}

var (
	ErrAlreadyExistEmail = errors.New("email already existed")
	ErrAlreadyTeacher = errors.New("user already teacher")

)

func WrapError(err error) error {
	return errors.Wrap(err, "error handling request")
}
