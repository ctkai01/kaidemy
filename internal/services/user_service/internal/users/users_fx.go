package users

import (
	// "fmt"

	// customEcho "gitlab.com/ctkai01/kaidemy/internal/pkg/http/custom_echo"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "fmt"

	"github.com/labstack/echo/v4"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/data/repositories"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/grpc"
	"go.uber.org/fx"

	// "github.com/mehdihadeli/go-ecommerce-microservices/internal/services/catalogreadservice/internal/products/data/repositories"
	// getProductByIdV1 "github.com/mehdihadeli/go-ecommerce-microservices/internal/services/catalogreadservice/internal/products/features/get_product_by_id/v1/endpoints"
	changePassword "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/change_password/endpoints"
	changeProfile "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/change_profile/endpoints"
	forgotPassword "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/forgot_password/endpoints"
	loginStandard "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login/endpoints"
	loginByGoogle "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login_by_google/endpoints"
	profileByMe "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/profile_by_me/endpoints"
	register "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/register/endpoints"
	registerTeacher "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/register_teacher/endpoints"
	requestLoginByGoogle "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/endpoints"
	resetPassword "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/reset_password/endpoints"
	getUserPublicByID "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_user_public_by_id/endpoints"

	createNormalAdmin "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/create_normal_admin/endpoints"
	deleteNormalAdmin "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/delete_normal_admin/endpoints"
	getNormalAdmins "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_normal_admins/endpoints"
	updateNormalAdmin "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/update_normal_admin/endpoints"
	
	requestRegisterTeacher "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/request_register_teacher/endpoints"
	verifyRegisterTeacher "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/verify_register_teacher/endpoints"
	
	getUsers "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_users/endpoints"
	blockUser "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/block_user/endpoints"

	// searchProductV1 "github.com/mehdihadeli/go-ecommerce-microservices/internal/services/catalogreadservice/internal/products/features/searching_products/v1/endpoints"
	customEcho "gitlab.com/ctkai01/kaidemy/internal/pkg/http/custom_echo"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/http/middlewares"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	// googleOauth "gitlab.com/ctkai01/kaidemy/internal/pkg/oauth/google"
)

// func NewAnimal(log logger.Logger) *Animal {
// 	log.Info("Hello world")
// 	return &Animal{
// 		name: "Tiger",
// 	}
// }

var Module = fx.Module(
	"usersfx",

	// Other provides
	// fx.Provide(repositories.NewRedisProductRepository),
	// fx.Provide(NewHTTPServer),

	fx.Provide(repositories.NewMySqlUserRepository),
	fx.Provide(grpc.NewUserGrpcService),

	fx.Provide(fx.Annotate(func(catalogsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		catalogsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			group := v1.Group("/auth")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"auth-echo-group"`))),

	fx.Provide(fx.Annotate(func(catalogsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		catalogsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			v1.Use(middlewares.AuthMiddleware)
			group := v1.Group("/users")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"users-echo-group"`))),

	fx.Provide(fx.Annotate(func(catalogsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		catalogsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			v1.Use(middlewares.AuthMiddleware)
			v1.Use(middlewares.AdminMiddleware)
			group := v1.Group("/users")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"users-admin-echo-group"`))),

	fx.Provide(fx.Annotate(func(catalogsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		catalogsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			v1.Use(middlewares.AuthMiddleware)
			v1.Use(middlewares.SuperAdminMiddleware)
			group := v1.Group("/admins")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"admins-echo-group"`))),

	fx.Provide(
		// fx.Annotate(
		// 	loginByGoogle.NewLoginByGoogleEndpoint,
		// 	fx.As(new(route.Endpoint)),
		// 	fx.ResultTags(fmt.Sprintf(`group:"%s"`, "product-routes")),
		// ),

		//Auth route
		route.AsRoute(requestLoginByGoogle.NewRequestLoginByGoogleEndpoint, "auth-routes"),
		route.AsRoute(loginByGoogle.NewLoginByGoogleEndpoint, "auth-routes"),
		route.AsRoute(register.NewRegisterEndpoint, "auth-routes"),
		route.AsRoute(register.NewRegisterEndpoint, "auth-routes"),
		route.AsRoute(loginStandard.NewLoginEndpoint, "auth-routes"),
		route.AsRoute(forgotPassword.NewForgotPasswordEndpoint, "auth-routes"),
		route.AsRoute(resetPassword.NewResetPasswordEndpoint, "auth-routes"),

		//Users route
		route.AsRoute(changePassword.NewChangePasswordEndpoint, "users-routes"),
		route.AsRoute(changeProfile.NewChangeProfileEndpoint, "users-routes"),
		route.AsRoute(registerTeacher.NewRegisterTeacherEndpoint, "users-routes"),
		route.AsRoute(profileByMe.NewProfileByMeEndpoint, "users-routes"),
		route.AsRoute(requestRegisterTeacher.NewRequestRegisterTeacherEndpoint, "users-routes"),
		route.AsRoute(verifyRegisterTeacher.NewVerifyRegisterTeacherEndpoint, "users-routes"),
		route.AsRoute(getUsers.NewGetUsersEndpoint, "users-routes"),
		route.AsRoute(blockUser.NewBlockUserEndpoint, "users-routes"),
		route.AsRoute(getUserPublicByID.NewGetUserPublicIDEndpoint, "users-routes"),
		
		//Admins route
		route.AsRoute(createNormalAdmin.NewCreateNormalAdminEndpoint, "users-routes"),
		route.AsRoute(updateNormalAdmin.NewUpdateNormalAdminEndpoint, "users-routes"),
		route.AsRoute(deleteNormalAdmin.NewDeleteNormalAdminEndpoint, "users-routes"),
		route.AsRoute(getNormalAdmins.NewGetNormalAdminsEndpoint, "users-routes"),
	
		// route.AsRoute(changeProfile.NewChangeProfileEndpoint, "users-routes"),
		// route.AsRoute(registerTeacher.NewRegisterTeacherEndpoint, "users-routes"),
		// route.AsRoute(profileByMe.NewProfileByMeEndpoint, "users-routes"),
	),
)

//   lc.Append(fx.Hook{
//     OnStart: func(ctx context.Context) error {
//       ln, err := net.Listen("tcp", srv.Addr)
//       if err != nil {
//         return err
//       }
//       fmt.Println("Starting HTTP server at", srv.Addr)
//       go srv.Serve(ln)
//       return nil
//     },
//     OnStop: func(ctx context.Context) error {
//       return srv.Shutdown(ctx)
//     },
//   })
