package params

import (
	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"go.uber.org/fx"

	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/data"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
)

type AdminsRouteParams struct {
	fx.In

	Logger         logger.Logger
	AdminsGroup     *echo.Group `name:"admins-echo-group"`
	Validator      *validator.Validate
	UserRepository data.UserRepository
}
