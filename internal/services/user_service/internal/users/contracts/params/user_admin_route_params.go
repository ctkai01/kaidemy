package params

import (
	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"go.uber.org/fx"

	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/data"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
)

type UsersAdminRouteParams struct {
	fx.In

	Logger         logger.Logger
	UsersGroup     *echo.Group `name:"users-admin-echo-group"`
	Validator      *validator.Validate
	UserRepository data.UserRepository
}
