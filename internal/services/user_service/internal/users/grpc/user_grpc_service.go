package grpc

import (
	"context"
	"fmt"

	// productsService
	"emperror.dev/errors"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/mapper"

	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_user_by_id/dtos"
	getUserByIdDto "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_user_by_id/dtos"
	getUserIdQuery "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_user_by_id/queries"

	getTeachersSearchDto "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_teachers_search/dtos"
	getTeachersSearchQuery "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_teachers_search/queries"

	getOverviewStudentDto "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_overview_student/dtos"
	getOverviewStudentQuery "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_overview_student/queries"

	usersService "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/grpc/proto/service_clients"
)

type UserGrpcServiceServer struct {
	logger logger.Logger
}

func NewUserGrpcService(
	logger logger.Logger,

) *UserGrpcServiceServer {

	return &UserGrpcServiceServer{
		logger,
	}
}

func (s *UserGrpcServiceServer) GetUserByID(ctx context.Context, req *usersService.GetUserByIdReq) (*usersService.GetUserByIdRes, error) {
	fmt.Println("req.UserId: ", req.UserId)
	query, errValidate := getUserIdQuery.NewGetUserById(
		int(req.UserId),
	)
	if errValidate != nil && errValidate.ErrorType != nil {

		validationErr := customErrors.NewValidationErrorWrap(
			errValidate.ErrorType,
			"[getUserById_handler.StructCtx] command validation failed",
		)
		s.logger.Errorf(
			fmt.Sprintf("[getUserById_handler.StructCtx] err: {%v}", validationErr),
		)

		return nil, errValidate.ErrorType
	}

	result, err := mediatr.Send[*getUserIdQuery.GetUserById, *getUserByIdDto.GetUserByIdResponseDto](
		ctx,
		query,
	)

	if err != nil {
		err = errors.WithMessage(
			err,
			"[UserGrpcServiceServer_GetUserById.Send] error in sending GetUserById",
		)
		s.logger.Errorw(
			fmt.Sprintf(
				"[UserGrpcServiceServer_GetUserById.Send] id: {%d}, err: %v",
				query.UserID,
				err,
			),
			logger.Fields{"UserId": query.UserID},
		)
		return nil, err
	}
	fmt.Println(result.User)
	user, err := mapper.Map[*usersService.User](result.User)
	if err != nil {
		err = errors.WithMessage(
			err,
			"[UserGrpcServiceServer_GetUserById.Map] error in mapping user",
		)
		return nil, err
	}

	return &usersService.GetUserByIdRes{
		User: user,
	}, nil
}

func (s *UserGrpcServiceServer) GetTeachers(ctx context.Context, req *usersService.GetTeachersReq) (*usersService.GetTeachersRes, error) {
	query, errValidate := getTeachersSearchQuery.NewGetTeachersSearch(
		req.Search,
	)
	if errValidate != nil && errValidate.ErrorType != nil {

		validationErr := customErrors.NewValidationErrorWrap(
			errValidate.ErrorType,
			"[getTeachersSearch_handler.StructCtx] command validation failed",
		)
		s.logger.Errorf(
			fmt.Sprintf("[getTeachersSearch_handler.StructCtx] err: {%v}", validationErr),
		)

		return nil, errValidate.ErrorType
	}

	result, err := mediatr.Send[*getTeachersSearchQuery.GetTeachersSearch, *getTeachersSearchDto.GetTeachersSearchResponseDto](
		ctx,
		query,
	)

	if err != nil {
		err = errors.WithMessage(
			err,
			"[UserGrpcServiceServer_getTeachersSearch.Send] error in sending getTeachersSearch",
		)

		return nil, err
	}
	users := make([]*usersService.User, 0)

	for _, user := range result.Users {
		userData, err := mapper.Map[*usersService.User](user)
		if err != nil {
			err = errors.WithMessage(
				err,
				"[UserGrpcServiceServer_GetUserById.Map] error in mapping user",
			)
			return nil, err
		}
		users = append(users, userData)
	}

	return &usersService.GetTeachersRes{
		Users: users,
	}, nil
}

func (s *UserGrpcServiceServer) GetOverViewStudents(ctx context.Context, req *usersService.GetOverViewStudentsReq) (*usersService.GetOverViewStudentsRes, error) {

	query, errValidate := getOverviewStudentQuery.NewGetTeachersSearch()
	if errValidate != nil && errValidate.ErrorType != nil {

		validationErr := customErrors.NewValidationErrorWrap(
			errValidate.ErrorType,
			"[getOverviewStudent_handler.StructCtx] command validation failed",
		)
		s.logger.Errorf(
			fmt.Sprintf("[OverviewStudent_handler.StructCtx] err: {%v}", validationErr),
		)

		return nil, errValidate.ErrorType
	}

	result, err := mediatr.Send[*getOverviewStudentQuery.GetOverviewStudent, *getOverviewStudentDto.GetOverviewStudentResponseDto](
		ctx,
		query,
	)

	if err != nil {
		err = errors.WithMessage(
			err,
			"[UserGrpcServiceServer_getOverviewStudent.Send] error in sending getTeachersSearch",
		)

		return nil, err
	}
	// users := make([]*usersService.User, 0)

	// for _, user := range result.Users {
	// 	userData, err := mapper.Map[*usersService.User](user)
	// 	if err != nil {
	// 		err = errors.WithMessage(
	// 			err,
	// 			"[UserGrpcServiceServer_GetUserById.Map] error in mapping user",
	// 		)
	// 		return nil, err
	// 	}
	// 	users = append(users, userData)
	// }
	var detailStat []int32
	for _, item := range result.Overview.DetailStats {
		detailStat = append(detailStat, int32(item))
	}
	dataResult := &usersService.GetOverViewStudentsRes{
		Total:          int32(result.Overview.Total),
		TotalThisMonth: int32(result.Overview.TotalThisMonth),
		MonthlyCount:   detailStat,
	}

	return dataResult, nil
}
