package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type DeleteNormalAdmin struct {
	UserID int `validate:"required,numeric"`
}

func NewDeleteNormalAdmin(userID int) (*DeleteNormalAdmin, *validator.ValidateError) {
	command := &DeleteNormalAdmin{UserID: userID}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
