package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"

	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/delete_normal_admin/dtos"
)

type DeleteNormalAdminHandler struct {
	log            logger.Logger
	userRepository data.UserRepository
}

func NewDeleteNormalAdminHandler(
	log logger.Logger,
	userRepository data.UserRepository,
) *DeleteNormalAdminHandler {
	return &DeleteNormalAdminHandler{log: log, userRepository: userRepository}
}

func (c *DeleteNormalAdminHandler) Handle(ctx context.Context, command *DeleteNormalAdmin) (*dtos.DeleteNormalAdminResponseDto, error) {

	user, err := c.userRepository.GetUserByID(ctx, command.UserID)

	if user == nil || err != nil {
		return nil, customErrors.ErrUserNotFound
	}


	err = c.userRepository.DeleteUserByID(ctx, command.UserID)

	if err != nil {
		return nil, err
	}

	return &dtos.DeleteNormalAdminResponseDto{
		Message: "Delete successfully!",
	}, nil
}
