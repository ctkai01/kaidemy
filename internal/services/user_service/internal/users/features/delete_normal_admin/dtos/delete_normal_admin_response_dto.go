package dtos

//https://echo.labstack.com/guide/response/

type DeleteNormalAdminResponseDto struct {
	Message  string `json:"message"`
}
