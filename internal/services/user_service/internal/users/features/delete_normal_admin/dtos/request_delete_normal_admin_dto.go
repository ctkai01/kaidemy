package dtos

//https://echo.labstack.com/guide/response/

type DeleteNormalAdminRequestDto struct {
	UserID int `json:"-" param:"id"`
}
