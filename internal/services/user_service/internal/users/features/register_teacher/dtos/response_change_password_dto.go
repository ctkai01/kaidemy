package dtos

//https://echo.labstack.com/guide/response/

type RegisterTeacherResponseDto struct {
	Message string `json:"message"`
}
