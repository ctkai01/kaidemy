package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/params"
	usersError "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/register_teacher/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/register_teacher/dtos"
)

type registerTeacherEndpoint struct {
	params.UsersRouteParams
}

func NewRegisterTeacherEndpoint(
	params params.UsersRouteParams,
) route.Endpoint {
	return &registerTeacherEndpoint{
		UsersRouteParams: params,
	}
}

func (ep *registerTeacherEndpoint) MapEndpoint() {
	ep.UsersGroup.POST("/register-teacher", ep.handler())
}

func (ep *registerTeacherEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		userId := utils.GetUserId(c)

		command := commands.NewRegisterTeacher(
			userId,
		)

		result, err := mediatr.Send[*commands.RegisterTeacher, *dtos.RegisterTeacherResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == usersError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[registerTeacherEndpoint_handler.Send] error in sending register teacher",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[registerTeacher.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			if err.Error() == usersError.WrapError(usersError.ErrAlreadyTeacher).Error() {
				errCustom := errors.WithMessage(
					err,
					"[registerTeacherEndpoint_handler.Send] error in sending register teacher",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[registerTeacher.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[registerTeacherEndpoint_handler.Send] error in sending change Password",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[registerTeacher.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
