package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/register_teacher/dtos"

	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type RegisterTeacherHandler struct {
	log            logger.Logger
	userRepository data.UserRepository
}

func NewRegisterTeacherHandler(
	log logger.Logger,
	userRepository data.UserRepository,

) *RegisterTeacherHandler {
	return &RegisterTeacherHandler{log: log, userRepository: userRepository}
}

func (c *RegisterTeacherHandler) Handle(ctx context.Context, command *RegisterTeacher) (*dtos.RegisterTeacherResponseDto, error) {
	user, err := c.userRepository.GetUserByID(ctx, command.IdUser)

	if user == nil || err != nil {
		return nil, customErrors.ErrUserNotFound
	}

	if user.Role == constants.TEACHER {
		return nil, errors.ErrAlreadyTeacher
	}

	if err != nil {
		return nil, err
	}
	user.Role = constants.TEACHER

	if err := c.userRepository.UpdateUser(ctx, user); err != nil {
		return nil, err
	}

	return &dtos.RegisterTeacherResponseDto{
		Message: "Register teacher successfully!",
	}, nil
}
