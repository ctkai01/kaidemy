package commands

import (
)

type RegisterTeacher struct {
	IdUser          int
}

func NewRegisterTeacher(idUser int) *RegisterTeacher {
	command := &RegisterTeacher{IdUser: idUser}

	return command
}
