package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// "github.com/dgrijalva/jwt-go"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/shared/utils"

	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/reset_password/dtos"

	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/register/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type ResetPasswordHandler struct {
	log            logger.Logger
	userRepository data.UserRepository
}

func NewResetPasswordHandler(
	log logger.Logger,
	userRepository data.UserRepository,
) *ResetPasswordHandler {
	return &ResetPasswordHandler{log: log, userRepository: userRepository}
}

func (c *ResetPasswordHandler) Handle(ctx context.Context, command *ResetPassword) (*dtos.ResetPassResponseDto, error) {
	user, err := c.userRepository.GetUserByEmailToken(ctx, command.EmailToken)

	if user == nil || err != nil {
		return nil, customErrors.ErrEmailTokenInvalid
	}

	if user.TypeAccount != constants.AccountNormal {
		return nil, customErrors.ErrOnlyForgotPasswordNormal
	}

	token, err := utils.ParseClaimsEmailToken(command.EmailToken)

	if err != nil {
		return nil, customErrors.ErrEmailTokenInvalid
	}

	if !token.Valid {
		return nil, customErrors.ErrEmailTokenInvalid
	}

	newPasswordHash, err := utils.HashPassword(command.Password)

	if err != nil {
		return nil, err
	}
	user.Password = newPasswordHash
	user.EmailToken = nil

	if err := c.userRepository.UpdateUser(ctx, user); err != nil {
		return nil, err
	}

	return &dtos.ResetPassResponseDto{
		Message: "Reset password successfully!",
	}, nil
}
