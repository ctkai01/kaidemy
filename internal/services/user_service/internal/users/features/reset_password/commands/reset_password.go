package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type ResetPassword struct {
	EmailToken string `validate:"required"`
	Password   string `validate:"required,gte=8,lte=30"`
}

func NewResetPassword(emailToken string, password string) (*ResetPassword, *validator.ValidateError) {
	command := &ResetPassword{EmailToken: emailToken, Password: password}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
