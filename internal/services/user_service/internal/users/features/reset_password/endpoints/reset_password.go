package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/params"
	usersError "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/reset_password/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/reset_password/dtos"
)

type resetPasswordEndpoint struct {
	params.AuthRouteParams
}

func NewResetPasswordEndpoint(
	params params.AuthRouteParams,
) route.Endpoint {
	return &resetPasswordEndpoint{
		AuthRouteParams: params,
	}
}

func (ep *resetPasswordEndpoint) MapEndpoint() {
	ep.AuthGroup.POST("/reset-password", ep.handler())
}

func (ep *resetPasswordEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.ResetPassRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[resetPasswordEndpoint_handler.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[resetPasswordEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}
		command, errValidate := commands.NewResetPassword(
			request.TokenEmail,
			request.Password,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[resetPasswordEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[resetPasswordEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.ResetPassword, *dtos.ResetPassResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == usersError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[resetPasswordEndpoint_handler.Send] error in sending reset Password",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[resetPassword.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			if err.Error() == usersError.WrapError(customErrors.ErrOnlyForgotPasswordNormal).Error() {
				errCustom := errors.WithMessage(
					err,
					"[resetPasswordEndpoint_handler.Send] error in sending reset Password",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[resetPassword.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			if err.Error() == usersError.WrapError(customErrors.ErrEmailTokenInvalid).Error() {
				errCustom := errors.WithMessage(
					err,
					"[resetPasswordEndpoint_handler.Send] error in sending reset Password",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[resetPassword.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}
			err = errors.WithMessage(
				err,
				"[resetPasswordEndpoint_handler.Send] error in sending reset Password",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[resetPassword.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
