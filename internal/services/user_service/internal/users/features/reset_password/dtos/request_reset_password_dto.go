package dtos

//https://echo.labstack.com/guide/response/

type ResetPassRequestDto struct {
	TokenEmail string `json:"email_token" form:"email_token"`
	Password   string `json:"password" form:"password"`
}
