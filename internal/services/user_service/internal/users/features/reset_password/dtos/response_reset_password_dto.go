package dtos

//https://echo.labstack.com/guide/response/

type ResetPassResponseDto struct {
	Message string `json:"message"`
}
