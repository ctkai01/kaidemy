package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/params"
	usersError "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/create_normal_admin/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/create_normal_admin/dtos"
)

type createNormalAdminEndpoint struct {
	params.AdminsRouteParams
}

func NewCreateNormalAdminEndpoint(
	params params.AdminsRouteParams,
) route.Endpoint {
	return &createNormalAdminEndpoint{
		AdminsRouteParams: params,
	}
}

func (ep *createNormalAdminEndpoint) MapEndpoint() {
	ep.AdminsGroup.POST("", ep.handler())
}

func (ep *createNormalAdminEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.CreateNormalAdminRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[createNormalAdminEndpoint_handler.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[createNormalAdminEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}
		command, errValidate := commands.NewCreateNormalAdmin(
			request.Name,
			request.Email,
			request.Password,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[createNormalAdminEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[createNormalAdminEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.CreateNormalAdmin, *dtos.CreateNormalAdminResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == usersError.WrapError(usersError.ErrAlreadyExistEmail).Error() {
				errCustom := errors.WithMessage(
					err,
					"[createNormalAdminEndpoint_handler.Send] error in sending register",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[createNormalAdmin.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}
			err = errors.WithMessage(
				err,
				"[createNormalAdminEndpoint_handler.Send] error in sending register",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[createNormalAdmin.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusCreated, result)
	}
}
