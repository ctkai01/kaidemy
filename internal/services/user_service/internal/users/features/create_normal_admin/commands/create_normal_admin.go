package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type CreateNormalAdmin struct {
	Name     string `validate:"required,gte=2,lte=100"`
	Email    string `validate:"required,email"`
	Password string `validate:"required,gte=8,lte=30"`
}

func NewCreateNormalAdmin(name string, email string, password string) (*CreateNormalAdmin, *validator.ValidateError) {
	command := &CreateNormalAdmin{Name: name, Email: email, Password: password}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
