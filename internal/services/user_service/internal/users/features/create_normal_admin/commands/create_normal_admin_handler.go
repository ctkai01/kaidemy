package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/create_normal_admin/dtos"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
)

type CreateNormalAdminHandler struct {
	log            logger.Logger
	userRepository data.UserRepository
}

func NewCreateNormalAdminHandler(
	log logger.Logger,
	userRepository data.UserRepository,
) *CreateNormalAdminHandler {
	return &CreateNormalAdminHandler{log: log, userRepository: userRepository}
}

func (c *CreateNormalAdminHandler) Handle(ctx context.Context, command *CreateNormalAdmin) (*dtos.CreateNormalAdminResponseDto, error) {

	userCreate := &models.User{
		Email:       command.Email,
		Name:        command.Name,
		TypeAccount: constants.AccountNormal,
		Role:        constants.ADMIN,
	}

	hashPassword, err := utils.HashPassword(command.Password)
	if err != nil {
		return nil, err
	}

	userCreate.Password = hashPassword

	if err := c.userRepository.CreateUser(ctx, userCreate); err != nil {
		return nil, err
	}

	user, err := c.userRepository.GetUserByID(ctx, userCreate.ID)

	if user == nil || err != nil {
		return nil, customErrors.ErrUnAuthorize
	}

	return &dtos.CreateNormalAdminResponseDto{
		User:  user,
	}, nil
}
