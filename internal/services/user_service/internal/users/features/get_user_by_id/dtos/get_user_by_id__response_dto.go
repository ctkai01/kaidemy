package dtos

import "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/dto"

//https://echo.labstack.com/guide/response/

type GetUserByIdResponseDto struct {
	User *dto.UserDto `json:"user"`
}
