package dtos

//https://echo.labstack.com/guide/response/

type GetUserByIDRequestDto struct {
	UserID int `param:"id"`
}
