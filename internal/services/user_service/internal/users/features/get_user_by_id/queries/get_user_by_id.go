package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

// https://echo.labstack.com/guide/request/
// https://github.com/go-playground/validator

type GetUserById struct {
	UserID int `validate:"required"`
}

func NewGetUserById(userId int) (*GetUserById, *validator.ValidateError) {
	query := &GetUserById{UserID: userId}

	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}
	return query, nil
}
