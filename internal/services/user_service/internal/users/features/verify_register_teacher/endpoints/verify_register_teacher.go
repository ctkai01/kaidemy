package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/params"
	usersError "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/verify_register_teacher/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/verify_register_teacher/dtos"
)

type verifyRegisterTeacherEndpoint struct {
	params.UsersRouteParams
}

func NewVerifyRegisterTeacherEndpoint(
	params params.UsersRouteParams,
) route.Endpoint {
	return &verifyRegisterTeacherEndpoint{
		UsersRouteParams: params,
	}
}

func (ep *verifyRegisterTeacherEndpoint) MapEndpoint() {
	ep.UsersGroup.POST("/verify-teacher", ep.handler())
}

func (ep *verifyRegisterTeacherEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.VerifyRegisterTeacherRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[verifyRegisterTeacherEndpoint_handler.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[verifyRegisterTeacherEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}
		userId := utils.GetUserId(c)

		command, errValidate := commands.NewVerifyRegisterTeacher(
			request.Key,
			userId,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[verifyRegisterTeacherEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[verifyRegisterTeacherEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.VerifyRegisterTeacher, *dtos.VerifyRegisterTeacherResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == usersError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[verifyRegisterTeacherEndpoint_handler.Send] error in sending reset Password",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[verifyRegisterTeacher.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[verifyRegisterTeacherEndpoint_handler.Send] error in sending reset Password",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[verifyRegisterTeacher.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
