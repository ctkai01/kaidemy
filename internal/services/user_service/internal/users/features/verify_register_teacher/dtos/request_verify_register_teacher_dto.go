package dtos

//https://echo.labstack.com/guide/response/

type VerifyRegisterTeacherRequestDto struct {
	Key string `json:"key" form:"key"`
}
