package dtos

import "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"

//https://echo.labstack.com/guide/response/

type VerifyRegisterTeacherResponseDto struct {
	Message string `json:"message"`
	Token string `json:"token"`
	User  *models.User `json:"user"`
}
