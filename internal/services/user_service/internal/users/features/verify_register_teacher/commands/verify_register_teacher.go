package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type VerifyRegisterTeacher struct {
	Key string `validate:"required"`
	IdUser int `validate:"required,numeric"`
}

func NewVerifyRegisterTeacher(key string, idUser int) (*VerifyRegisterTeacher, *validator.ValidateError) {
	command := &VerifyRegisterTeacher{Key: key, IdUser: idUser}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
