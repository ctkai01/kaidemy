package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// "github.com/dgrijalva/jwt-go"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"

	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/verify_register_teacher/dtos"

	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/register/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type VerifyRegisterTeacherHandler struct {
	log            logger.Logger
	userRepository data.UserRepository
}

func NewVerifyRegisterTeacherHandler(
	log logger.Logger,
	userRepository data.UserRepository,
) *VerifyRegisterTeacherHandler {
	return &VerifyRegisterTeacherHandler{log: log, userRepository: userRepository}
}

func (c *VerifyRegisterTeacherHandler) Handle(ctx context.Context, command *VerifyRegisterTeacher) (*dtos.VerifyRegisterTeacherResponseDto, error) {
	user, err := c.userRepository.GetUserByID(ctx, command.IdUser)

	if user == nil || err != nil {
		return nil, customErrors.ErrUserNotFound
	}

	if user.Role != constants.NORMAL_USER {
		return nil, customErrors.ErrOnlyRegisterTeacherForNormalUser
	}

	if user.AccountStripeStatus != nil {
		if *user.AccountStripeStatus == constants.ACCOUNT_STRIPE_VERIFY {
			return nil, customErrors.ErrAlreadyTeacher
		}
	}

	if user.KeyAccountStripe != nil {
		if *user.KeyAccountStripe != command.Key {
			return nil, customErrors.ErrKeyVerifyIncorrect
		}
	}
	
	user.KeyAccountStripe = nil
	user.AccountStripeStatus = &constants.ACCOUNT_STRIPE_VERIFY
	user.Role = constants.TEACHER
	
	tokenUser, err := utils.Encode(user.ID, user.Role)

	if err != nil {
		return nil, err
	}
	if err := c.userRepository.UpdateUser(ctx, user); err != nil {
		return nil, err
	}

	return &dtos.VerifyRegisterTeacherResponseDto{
		Message: "Verify register teacher successfully!",
		Token: tokenUser,
		User: user,
	}, nil
}
