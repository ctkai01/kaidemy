package dtos

//https://echo.labstack.com/guide/response/

type LoginRequestDto struct {
	Email    string `json:"email" form:"email"`
	Password string `json:"password" form:"password"`
}
