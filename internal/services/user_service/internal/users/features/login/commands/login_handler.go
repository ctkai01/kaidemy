package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// "encoding/json"
	// "io"

	// "emperror.dev/errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login/dtos"

	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login_by_google/queries"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"golang.org/x/oauth2"
)

type LoginHandler struct {
	log            logger.Logger
	googleOauth    *oauth2.Config
	userRepository data.UserRepository
}

func NewLoginHandler(
	log logger.Logger,
	googleOauth *oauth2.Config,
	userRepository data.UserRepository,
) *LoginHandler {
	return &LoginHandler{log: log, googleOauth: googleOauth, userRepository: userRepository}
}

func (c *LoginHandler) Handle(ctx context.Context, command *UserLoginStandard) (*dtos.LoginResponseDto, error) {
	user, err := c.userRepository.GetUserByEmail(ctx, command.Email)

	if user == nil || err != nil {
		return nil, customErrors.ErrUnAuthorize
	}

	if user.IsBlock {
		return nil, customErrors.ErrUserBeingBlocked
	}

	if !utils.ComparePassword(command.Password, user.Password) {
		return nil, customErrors.ErrUnAuthorize
	}

	if user.TypeAccount != constants.AccountNormal {
		return nil, customErrors.ErrUnAuthorize
	}

	token, err := utils.Encode(user.ID, user.Role)

	if err != nil {
		return nil, err
	}

	return &dtos.LoginResponseDto{
			Token: token,
			User:  user,
		},
		nil

}
