package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type UserLoginStandard struct {
	Email    string `validate:"required,email"`
	Password string `validate:"required,gte=8,lte=30"`
}

func NewLoginUserStandard(email string, password string) (*UserLoginStandard, *validator.ValidateError) {
	command := &UserLoginStandard{
		Email:    email,
		Password: password,
	}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}
	return command, nil
}
