package queries

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// "encoding/json"
	// "io"

	// "emperror.dev/errors"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/mapper"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/dto"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_teachers_search/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login_by_google/queries"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
)

type GetTeachersSearchHandler struct {
	log            logger.Logger
	userRepository data.UserRepository
}

func NewGetTeachersSearchHandler(
	log logger.Logger,
	userRepository data.UserRepository,
) *GetTeachersSearchHandler {
	return &GetTeachersSearchHandler{log: log, userRepository: userRepository}
}

func (c *GetTeachersSearchHandler) Handle(ctx context.Context, query *GetTeachersSearch) (*dtos.GetTeachersSearchResponseDto, error) {
	users, err := c.userRepository.GetTeachersSearch(ctx, query.Search)

	if err != nil {
		return nil, err
	}
	// if user == nil || err != nil {
	// 	return nil, customErrors.ErrUserNotFound
	// }

	usersDto := make([]*dto.UserDto, 0)

	for _, user := range users {
		userDto, err := mapper.Map[*dto.UserDto](user)
		if err != nil {
			return nil,
				customErrors.NewApplicationErrorWrap(
					err,
					"[GetUserByIdHandler_Handle.Map] error in the mapping user",
				)
		}
		usersDto = append(usersDto, userDto)
	}

	return &dtos.GetTeachersSearchResponseDto{
			Users: usersDto,
		},
		nil
}
