package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/params"
	usersError "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/forgot_password/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/forgot_password/dtos"
)

type forgotPasswordEndpoint struct {
	params.AuthRouteParams
}

func NewForgotPasswordEndpoint(
	params params.AuthRouteParams,
) route.Endpoint {
	return &forgotPasswordEndpoint{
		AuthRouteParams: params,
	}
}

func (ep *forgotPasswordEndpoint) MapEndpoint() {
	ep.AuthGroup.POST("/forgot-password", ep.handler())
}

func (ep *forgotPasswordEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.ForgotPasswordRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[forgotPasswordEndpoint_handler.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[forgotPasswordEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}
		command, errValidate := commands.NewForgotPassword(
			request.Email,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[forgotPasswordEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[forgotPasswordEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.ForgotPassword, *dtos.ForgotPasswordResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == usersError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[forgotPasswordEndpoint_handler.Send] error in sending forgot Password",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[forgotPassword.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			if err.Error() == usersError.WrapError(customErrors.ErrOnlyForgotPasswordNormal).Error() {
				errCustom := errors.WithMessage(
					err,
					"[forgotPasswordEndpoint_handler.Send] error in sending forgot Password",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[forgotPassword.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}
			err = errors.WithMessage(
				err,
				"[forgotPasswordEndpoint_handler.Send] error in sending forgot Password",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[forgotPassword.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
