package dtos

//https://echo.labstack.com/guide/response/

type ForgotPasswordRequestDto struct {
	Email string `json:"email" form:"email"`
}
