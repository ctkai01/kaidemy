package dtos

//https://echo.labstack.com/guide/response/

type ForgotPasswordResponseDto struct {
	Message string `json:"message"`
}
