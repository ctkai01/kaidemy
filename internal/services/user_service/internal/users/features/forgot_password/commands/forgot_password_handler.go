package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"encoding/json"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/configurations"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/producer"

	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/forgot_password/dtos"

	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type ForgotPasswordHandler struct {
	log             logger.Logger
	userRepository  data.UserRepository
	rabbitmqBuilder configurations.RabbitMQConfigurationBuilder
}

func NewForgotPasswordHandler(
	log logger.Logger,
	userRepository data.UserRepository,
	rabbitmqBuilder configurations.RabbitMQConfigurationBuilder,

) *ForgotPasswordHandler {
	return &ForgotPasswordHandler{log: log, userRepository: userRepository, rabbitmqBuilder: rabbitmqBuilder}
}

func (c *ForgotPasswordHandler) Handle(ctx context.Context, command *ForgotPassword) (*dtos.ForgotPasswordResponseDto, error) {
	user, err := c.userRepository.GetUserByEmail(ctx, command.Email)

	if user == nil || err != nil {
		return nil, customErrors.ErrUserNotFound
	}

	if user.TypeAccount != constants.AccountNormal {
		return nil, customErrors.ErrOnlyForgotPasswordNormal
	}

	emailToken, err := utils.EncodeEmail(command.Email)

	if err != nil {
		return nil, err
	}

	user.EmailToken = &emailToken

	if err := c.userRepository.UpdateUser(ctx, user); err != nil {
		return nil, err
	}

	body := constants.MailForgotPassword{
		Email: command.Email,
		Token: emailToken,
		Name: user.Name,
	}

	bodyByte, err := json.Marshal(body)

	if err != nil {
		return nil, err
	}

	c.rabbitmqBuilder.AddProducerOnQueue(&producer.RabbitMQProducerOnQueue{
		QueueName: constants.SEND_MAIL_FORGOT_PASSWORD,
		Data:      bodyByte,
	})

	// err = ch.PublishWithContext(ctx,
	// 	"",     // exchange
	// 	q.Name, // routing key
	// 	false,  // mandatory
	// 	false,  // immediate
	// 	amqp091.Publishing{
	// 		ContentType: "text/plain",
	// 		Body:        bodyByte,
	// 	})

	// if err != nil {
	// 	return nil, err
	// }
	// defer c.connRabbitMq.Close()

	return &dtos.ForgotPasswordResponseDto{
		Message: "Forgot password successfully!",
	}, nil
}
