package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type ForgotPassword struct {
	Email string `validate:"required,email"`
}

func NewForgotPassword(email string) (*ForgotPassword, *validator.ValidateError) {
	command := &ForgotPassword{Email: email}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
