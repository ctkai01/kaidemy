package dtos

import "mime/multipart"

//https://echo.labstack.com/guide/response/

type ChangeProfileRequestDto struct {
	Name        *string               `json:"name" form:"name"`
	Headline    *string               `json:"headline" form:"headline"`
	Biography   *string               `json:"biography" form:"biography"`
	WebsiteURL  *string               `json:"website_url" form:"website_url"`
	TwitterURL  *string               `json:"twitter_url" form:"twitter_url"`
	FacebookURL *string               `json:"facebook_url" form:"facebook_url"`
	LinkedInURL *string               `json:"linkedin_url" form:"linkedin_url"`
	YoutubeURL  *string               `json:"youtube_url" form:"youtube_url"`
	Avatar      *multipart.FileHeader `json:"avatar" form:"avatar"`
}
