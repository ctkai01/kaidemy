package dtos

import "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"

//https://echo.labstack.com/guide/response/

type ChangeProfileResponseDto struct {
	Message string       `json:"message"`
	User    *models.User `json:"user"`
}
