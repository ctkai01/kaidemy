package commands

import (
	"mime/multipart"

	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
	// errorUtils "gitlab.com/ctkai01/kaidemy/internal/pkg/utils/error_utils"
)

// Password string `validate:"required,gte=8,lte=30"`

type ChangeProfile struct {
	Id          int
	Name        *string `validate:"omitempty,gte=4,lte=30"`
	Headline    *string `validate:"omitempty,gte=0,lte=255"`
	Biography   *string `validate:"omitempty,gte=0,lte=600"`
	WebsiteURL  *string `validate:"omitempty,gte=0,lte=255"`
	TwitterURL  *string `validate:"omitempty,gte=0,lte=255"`
	FacebookURL *string `validate:"omitempty,gte=0,lte=255"`
	LinkedInURL *string `validate:"omitempty,gte=0,lte=255"`
	YoutubeURL  *string `validate:"omitempty,gte=0,lte=255"`
	Avatar      *multipart.FileHeader
}

func NewChangeProfile(
	id int,
	name *string,
	headline *string,
	biography *string,
	websiteURL *string,
	twitterURL *string,
	facebookURL *string,
	linkedInURL *string,
	youtubeURL *string,
	avatar *multipart.FileHeader,
) (*ChangeProfile, *validator.ValidateError) {
	command := &ChangeProfile{
		Id:          id,
		Name:        name,
		Headline:    headline,
		Biography:   biography,
		WebsiteURL:  websiteURL,
		TwitterURL:  twitterURL,
		FacebookURL: facebookURL,
		LinkedInURL: linkedInURL,
		YoutubeURL:  youtubeURL,
		Avatar:      avatar,
	}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	if command.Avatar != nil {
		errValidate := checkAvatar(command.Avatar)

		if errValidate != nil && errValidate.ErrorType != nil {
			return nil, errValidate
		}
	}

	return command, nil
}

func checkAvatar(avatar *multipart.FileHeader) *validator.ValidateError {
	allowFileType := []string{"image/png", "image/jpg", "image/jpeg"}
	if avatar.Size > 1048576*5 {
		valdiateErr := validator.ValidateError{
			ErrorValue: []*validator.CustomValidationError{
				{
					Field:   "avatar",
					Message: customErrors.ErrAvatarToMax.Error(),
				},
			},
			ErrorType: customErrors.ErrAvatarToMax,
		}
		return &valdiateErr
	}

	isSupportType := false
	for _, v := range allowFileType {
		if v == avatar.Header["Content-Type"][0] {
			isSupportType = true
		}
	}
	if !isSupportType {
		valdiateErr := validator.ValidateError{
			ErrorValue: []*validator.CustomValidationError{
				{
					Field:   "avatar",
					Message: customErrors.ErrAvatarNotSupport.Error(),
				},
			},
			ErrorType: customErrors.ErrAvatarNotSupport,
		}
		return &valdiateErr
	}
	return nil
}
