package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"fmt"
	"io"
	"strings"

	// "encoding/json"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/producer"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/shared/utils"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/change_profile/dtos"

	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"

	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	grpcService "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/grpc/proto/service_clients"
)

type ChangeProfileHandler struct {
	log                logger.Logger
	userRepository     data.UserRepository
	grpcClient grpc.GrpcClient

}

func NewChangeProfileHandler(
	log logger.Logger,
	userRepository data.UserRepository,
	grpcClient grpc.GrpcClient,

) *ChangeProfileHandler {
	return &ChangeProfileHandler{log: log, userRepository: userRepository, grpcClient: grpcClient}
}

func (c *ChangeProfileHandler) Handle(ctx context.Context, command *ChangeProfile) (*dtos.ChangeProfileResponseDto, error) {
	// user, err := c.userRepository.GetUserByEmail(ctx, command.Email)
	user, err := c.userRepository.GetUserByID(ctx, command.Id)

	if user == nil || err != nil {
		return nil, customErrors.ErrUserNotFound
	}


	if command.Avatar != nil {
		connUpload, err := c.grpcClient.GetGrpcConnection("upload_service")
		if err != nil {
			return nil, err
		}
		clientUpload := grpcService.NewUploadsServiceClient(connUpload)

		// Remove previous image
		if user.Avatar != nil {
			resourceFile := strings.Split(*user.Avatar, "/")
			resourceFile = resourceFile[len(resourceFile)-2:]
			resourceFileStr := strings.Join(resourceFile, "/")
			resourceFileUrl := fmt.Sprintf("https://sg.storage.bunnycdn.com/kaidemy/%s", resourceFileStr)

			fmt.Println("URL delete: ", resourceFileUrl)
			if _, err := clientUpload.DeleteResource(ctx, &grpcService.DeleteResourceReq{
				Url: resourceFileUrl,
			}); err != nil {
				c.log.Err("Error remove resource: ", err)
			}
		}

		//Upload GRPC

		stream, err := clientUpload.UploadResource(ctx)

		if err != nil {
			return nil, err
		}
		file, err := command.Avatar.Open()

		if err != nil {
			return nil, err
		}

		buf := make([]byte, 1048576)
		batchNumber := 1
		for {
			num, err := file.Read(buf)
			if err == io.EOF {
				break
			}
			if err != nil {
				return nil, err
			}
			chunk := buf[:num]

			if err := stream.Send(&grpcService.UploadResourceReq{
				Title:     "Image",
				Type:      int32(constants.MediaType),
				Data:      chunk,
				Extension: command.Avatar.Header["Content-Type"][0],
				Size:      int64(len(chunk)),
				Name:      command.Avatar.Filename,
			}); err != nil {
				return nil, err
			}
			c.log.Info("Sent - batch #%v - size - %v\n", batchNumber, len(chunk))
			batchNumber += 1

		}

		res, err := stream.CloseAndRecv()
		c.log.Info("Err: ", err)

		if err != nil {
			return nil, err
		}
		c.log.Info("URL: ", res.GetUrl())
		user.Avatar = &res.Url
	} 
	
	if command.Name != nil {
		user.Name = *command.Name
	}

	if command.Headline != nil {
		user.Headline = command.Headline
	}

	if command.Biography != nil {
		user.Biography = command.Biography
	}

	if command.WebsiteURL != nil {
		user.WebsiteURL = command.WebsiteURL
	}

	if command.TwitterURL != nil {
		user.TwitterURL = command.TwitterURL
	}

	if command.FacebookURL != nil {
		user.FacebookURL = command.FacebookURL
	}

	if command.LinkedInURL != nil {
		user.LinkedInURL = command.LinkedInURL
	}

	if command.YoutubeURL != nil {
		user.YoutubeURL = command.YoutubeURL
	}

	if err := c.userRepository.UpdateUser(ctx, user); err != nil {
		return nil, err
	}
	// if user == nil || err != nil {
	// 	return nil, customErrors.ErrUserNotFound
	// }

	// if user.TypeAccount != constants.AccountNormal {
	// 	return nil, customErrors.ErrOnlyForgotPasswordNormal
	// }

	// emailToken, err := utils.EncodeEmail(command.Email)

	// if err != nil {
	// 	return nil, err
	// }

	// user.EmailToken = &emailToken

	// if err := c.userRepository.UpdateUser(ctx, user); err != nil {
	// 	return nil, err
	// }

	// body := MailForgotPassword{
	// 	Email: command.Email,
	// 	Token: emailToken,
	// }

	// bodyByte, err := json.Marshal(body)

	// if err != nil {
	// 	return nil, err
	// }

	// c.rabbitmqBuilder.AddProducerOnQueue(&producer.RabbitMQProducerOnQueue{
	// 	QueueName: "SEND_MAIL_FORGOT_PASSWORD",
	// 	Data: bodyByte,
	// })

	// err = ch.PublishWithContext(ctx,
	// 	"",     // exchange
	// 	q.Name, // routing key
	// 	false,  // mandatory
	// 	false,  // immediate
	// 	amqp091.Publishing{
	// 		ContentType: "text/plain",
	// 		Body:        bodyByte,
	// 	})

	// if err != nil {
	// 	return nil, err
	// }
	// defer c.connRabbitMq.Close()

	return &dtos.ChangeProfileResponseDto{
		Message: "Update profile successfully!",
		User:    user,
	}, nil
}
