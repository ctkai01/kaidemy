package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"

	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"

	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/block_user/dtos"
)

type BlockUserHandler struct {
	log            logger.Logger
	userRepository data.UserRepository
}

func NewBlockUserHandler(
	log logger.Logger,
	userRepository data.UserRepository,
) *BlockUserHandler {
	return &BlockUserHandler{log: log, userRepository: userRepository}
}

func (c *BlockUserHandler) Handle(ctx context.Context, command *BlockUser) (*dtos.BlockUserResponseDto, error) {

	user, err := c.userRepository.GetUserByID(ctx, command.UserID)

	if user == nil || err != nil {
		return nil, customErrors.ErrUserNotFound
	}

	if user.Role == constants.SUPPER_ADMIN || user.Role == constants.ADMIN {
		return nil, customErrors.ErrCannotBlockAdmin
	}

	user.IsBlock = command.IsBlock

	err = c.userRepository.UpdateUser(ctx, user)

	if err != nil {
		return nil, err
	}

	return &dtos.BlockUserResponseDto{
		Message: "Update successfully!",
	}, nil
}
