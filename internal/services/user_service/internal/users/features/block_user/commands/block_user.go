package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type BlockUser struct {
	UserID        int  `validate:"required,numeric"`
	IsBlock bool `validate:"omitempty"`
}

func NewBlockUser(userID int, isBlock bool) (*BlockUser, *validator.ValidateError) {
	command := &BlockUser{UserID: userID, IsBlock: isBlock}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
