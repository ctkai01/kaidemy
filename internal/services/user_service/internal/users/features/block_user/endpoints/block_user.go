package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/params"
	usersError "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/block_user/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/block_user/dtos"
)

type blockUserEndpoint struct {
	params.UsersAdminRouteParams
}

func NewBlockUserEndpoint(
	params params.UsersAdminRouteParams,
) route.Endpoint {
	return &blockUserEndpoint{
		UsersAdminRouteParams: params,
	}
}

func (ep *blockUserEndpoint) MapEndpoint() {
	ep.UsersGroup.PUT("/:id/block", ep.handler())

}

func (ep *blockUserEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.BlockUserRequestDto{}
		fmt.Println("requestL: ", request)
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[BlockUserEndpoint_handler.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[BlockUserEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}

		command, errValidate := commands.NewBlockUser(
			request.UserID,
			request.IsBlock,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[BlockUserEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[BlockUserEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.BlockUser, *dtos.BlockUserResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == usersError.WrapError(usersError.ErrAlreadyExistEmail).Error() {
				errCustom := errors.WithMessage(
					err,
					"[BlockUserEndpoint_handler.Send] error",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[BlockUser.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}
			err = errors.WithMessage(
				err,
				"[BlockUserEndpoint_handler.Send] error",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[BlockUser.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
