package dtos

//https://echo.labstack.com/guide/response/

type BlockUserRequestDto struct {
	UserID  int  `json:"-" param:"id"`
	IsBlock bool `form:"is_block"`
}
