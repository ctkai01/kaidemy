package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"time"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	stripe "github.com/stripe/stripe-go/v76"
	stripeAccount "github.com/stripe/stripe-go/v76/account"
	stripeAccountLink "github.com/stripe/stripe-go/v76/accountlink"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/request_register_teacher/dtos"

	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"

	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type RequestRegisterTeacherHandler struct {
	log            logger.Logger
	userRepository data.UserRepository
}

func NewRequestRegisterTeacherHandler(
	log logger.Logger,
	userRepository data.UserRepository,

) *RequestRegisterTeacherHandler {
	return &RequestRegisterTeacherHandler{log: log, userRepository: userRepository}
}

func (c *RequestRegisterTeacherHandler) Handle(ctx context.Context, command *RequestRegisterTeacher) (*dtos.RequestRegisterTeacherResponseDto, error) {
	user, err := c.userRepository.GetUserByID(ctx, command.Id)

	if user == nil || err != nil {
		return nil, customErrors.ErrUserNotFound
	}

	if user.Role != constants.NORMAL_USER {
		return nil, customErrors.ErrOnlyRegisterTeacherForNormalUser
	}

	if user.AccountStripeStatus != nil {
		if *user.AccountStripeStatus == constants.ACCOUNT_STRIPE_VERIFY {
			return nil, customErrors.ErrAlreadyTeacher
		}
	}

	stripe.Key = "sk_test_51NUmJOFEsDSfMpHunO8Zscoo1KwMISvWpj00QwhHnLTjYQVkQF1uH0WBgCd8pWgfd5gAKBguz9MfszFPlQwOVPXl00ZUdeBaS5"

	accountStripeParams := &stripe.AccountParams{
		Type: stripe.String("express"),
		Capabilities: &stripe.AccountCapabilitiesParams{
			CardPayments: &stripe.AccountCapabilitiesCardPaymentsParams{
				Requested: stripe.Bool(true),
			},
			Transfers: &stripe.AccountCapabilitiesTransfersParams{
				Requested: stripe.Bool(true),
			},
		},
	}

	a, err := stripeAccount.New(accountStripeParams)

	if err != nil {
		return nil, err
	}
	//
	key := generateHashKey(fmt.Sprintf("%d", time.Now().Unix()))
	//
	accountLinkStripeParams := &stripe.AccountLinkParams{
		Account:    stripe.String(a.ID),
		RefreshURL: stripe.String("http://localhost:5173/register-teacher?refresh_url=true"),
		ReturnURL:  stripe.String(fmt.Sprintf("http://localhost:5173/register-teacher?return_url=true&key=%s", key)),
		Type:       stripe.String(string(stripe.AccountLinkTypeAccountOnboarding)),
	}

	accountLink, err := stripeAccountLink.New(accountLinkStripeParams)

	if err != nil {
		return nil, err
	}

	user.KeyAccountStripe = &key
	user.AccountStripeID = &a.ID
	user.AccountStripeStatus = &constants.ACCOUNT_STRIPE_PENDING

	if err := c.userRepository.UpdateUser(ctx, user); err != nil {
		return nil, err
	}

	return &dtos.RequestRegisterTeacherResponseDto{
		Message:     "Request register teacher successfully!",
		AccountLink: accountLink.URL,
	}, nil
}

func generateHashKey(input string) string {
	hash := sha256.New()
	hash.Write([]byte(input))
	hashed := hash.Sum(nil)
	return hex.EncodeToString(hashed)
}
