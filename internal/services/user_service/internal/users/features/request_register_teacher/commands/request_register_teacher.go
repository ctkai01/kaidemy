package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type RequestRegisterTeacher struct {
	Id   int `validate:"required,numeric"`
}

func NewRequestRegisterTeacher(id int) (*RequestRegisterTeacher, *validator.ValidateError) {
	command := &RequestRegisterTeacher{Id: id}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
