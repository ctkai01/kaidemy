package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/params"
	usersError "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/request_register_teacher/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/request_register_teacher/dtos"
)

type requestRegisterTeacherEndpoint struct {
	params.UsersRouteParams
}

func NewRequestRegisterTeacherEndpoint(
	params params.UsersRouteParams,
) route.Endpoint {
	return &requestRegisterTeacherEndpoint{
		UsersRouteParams: params,
	}
}

func (ep *requestRegisterTeacherEndpoint) MapEndpoint() {
	ep.UsersGroup.POST("/request-teacher", ep.handler())
}

func (ep *requestRegisterTeacherEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()


		userId := utils.GetUserId(c)

		command, errValidate := commands.NewRequestRegisterTeacher(
			userId,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[requestRegisterTeacherEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[requestRegisterTeacherEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.RequestRegisterTeacher, *dtos.RequestRegisterTeacherResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == usersError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[requestRegisterTeacher_handler.Send] error in sending change Password",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[requestRegisterTeacher.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}


			if err.Error() == usersError.WrapError(customErrors.ErrPasswordNotExact).Error() {
				errCustom := errors.WithMessage(
					err,
					"[requestRegisterTeacher_handler.Send] error in sending change Password",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[requestRegisterTeacher.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[requestRegisterTeacher_handler.Send] error in sending change Password",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[requestRegisterTeacher.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
