package dtos

//https://echo.labstack.com/guide/response/

type RequestRegisterTeacherResponseDto struct {
	AccountLink string `json:"account_link"`
	Message string `json:"message"`
}
