package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"

	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/update_normal_admin/dtos"
)

type UpdateNormalAdminHandler struct {
	log            logger.Logger
	userRepository data.UserRepository
}

func NewUpdateNormalAdminHandler(
	log logger.Logger,
	userRepository data.UserRepository,
) *UpdateNormalAdminHandler {
	return &UpdateNormalAdminHandler{log: log, userRepository: userRepository}
}

func (c *UpdateNormalAdminHandler) Handle(ctx context.Context, command *UpdateNormalAdmin) (*dtos.UpdateNormalAdminResponseDto, error) {

	user, err := c.userRepository.GetUserByID(ctx, command.UserID)

	if user == nil || err != nil {
		return nil, customErrors.ErrUserNotFound
	}

	if (command.Name != nil) {
		user.Name = *command.Name
	}

	if (command.Password != nil) {
		hashPassword, err := utils.HashPassword(*command.Password)
		if err != nil {
			return nil, err
		}
		user.Password = hashPassword
	}

	err = c.userRepository.UpdateUser(ctx, user)

	if err != nil {
		return nil, err
	}

	return &dtos.UpdateNormalAdminResponseDto{
		User:  user,
		Message: "Update successfully!",
	}, nil
}
