package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type UpdateNormalAdmin struct {
	UserID int `validate:"required,numeric"`
	Password *string `validate:"omitempty,gte=8,lte=30"`
	Name     *string `validate:"omitempty,gte=2,lte=100"`
}

func NewUpdateNormalAdmin(name *string, password *string, userID int) (*UpdateNormalAdmin, *validator.ValidateError) {
	command := &UpdateNormalAdmin{Name: name, Password: password, UserID: userID}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
