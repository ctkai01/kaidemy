package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/params"
	usersError "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/update_normal_admin/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/update_normal_admin/dtos"
)

type updateNormalAdminEndpoint struct {
	params.AdminsRouteParams
}

func NewUpdateNormalAdminEndpoint(
	params params.AdminsRouteParams,
) route.Endpoint {
	return &updateNormalAdminEndpoint{
		AdminsRouteParams: params,
	}
}

func (ep *updateNormalAdminEndpoint) MapEndpoint() {
	ep.AdminsGroup.PUT("/:id", ep.handler())

}

func (ep *updateNormalAdminEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.UpdateNormalAdminRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[updateNormalAdminEndpoint_handler.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[updateNormalAdminEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}

		command, errValidate := commands.NewUpdateNormalAdmin(
			request.Name,
			request.Password,
			request.UserID,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[updateNormalAdminEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[updateNormalAdminEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.UpdateNormalAdmin, *dtos.UpdateNormalAdminResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == usersError.WrapError(usersError.ErrAlreadyExistEmail).Error() {
				errCustom := errors.WithMessage(
					err,
					"[updateNormalAdminEndpoint_handler.Send] error in sending register",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[updateNormalAdmin.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}
			err = errors.WithMessage(
				err,
				"[updateNormalAdminEndpoint_handler.Send] error in sending register",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[updateNormalAdmin.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusCreated, result)
	}
}
