package dtos

//https://echo.labstack.com/guide/response/

type UpdateNormalAdminRequestDto struct {
	UserID int `json:"-" param:"id"`
	Password *string `json:"password" form:"password"`
	Name *string `json:"name" form:"name"`
}
