package endpoints

import (
	"fmt"
	"net/http"

	"emperror.dev/errors"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"

	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/params"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login_by_google/dtos"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login_by_google/commands"
)

type loginByGoogleEndpoint struct {
	params.AuthRouteParams
}

func NewLoginByGoogleEndpoint(
	params params.AuthRouteParams,
) route.Endpoint {
	return &loginByGoogleEndpoint{
		AuthRouteParams: params,
	}
}

func (ep *loginByGoogleEndpoint) MapEndpoint() {
	ep.AuthGroup.POST("/login-google", ep.handler())
}

func (ep *loginByGoogleEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()
		request := &dtos.RequestLoginGoogleDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[loginGoogleEndpoint_handler.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[loginGoogleEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}

		command, errValidate := commands.NewCreateUserByLoginGoogle(
			request.Token,
		)
		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[loginGoogleEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[loginGoogleEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.UserCreateByLoginGoogle, *dtos.ResponseLoginGoogleDto](
			ctx,
			command,
		)

		if err != nil {
			err = errors.WithMessage(
				err,
				"[loginGoogle_handler.Send] error in loginGoogle",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[loginGoogle.Send], err: %v",
					err,
				),
			)
			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}
		return c.JSON(http.StatusCreated, result)
	}
}
