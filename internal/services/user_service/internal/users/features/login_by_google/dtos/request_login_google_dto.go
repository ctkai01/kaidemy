package dtos

//https://echo.labstack.com/guide/response/

type RequestLoginGoogleDto struct {
	Token string `form:"token"`
}