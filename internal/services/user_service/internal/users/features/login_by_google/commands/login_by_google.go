package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type UserGetByLoginGoogle struct {
	Name  string `json:"name"`
	Email  string `json:"email"`
}


type UserCreateByLoginGoogle struct {
	Token  string `validate:"required"`
}

func NewCreateUserByLoginGoogle(token string) (*UserCreateByLoginGoogle, *validator.ValidateError) {
	command := &UserCreateByLoginGoogle{token}

	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}
	return command, nil
}
