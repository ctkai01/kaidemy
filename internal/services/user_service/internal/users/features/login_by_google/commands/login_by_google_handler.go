package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"encoding/json"
	"io"

	"emperror.dev/errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login_by_google/dtos"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	"golang.org/x/oauth2"
)

type LoginGoogleHandler struct {
	log            logger.Logger
	googleOauth    *oauth2.Config
	userRepository data.UserRepository
}

func NewLoginGoogleHandler(
	log logger.Logger,
	googleOauth *oauth2.Config,
	userRepository data.UserRepository,
) *LoginGoogleHandler {
	return &LoginGoogleHandler{log: log, googleOauth: googleOauth, userRepository: userRepository}
}

func (c *LoginGoogleHandler) Handle(ctx context.Context, command *UserCreateByLoginGoogle) (*dtos.ResponseLoginGoogleDto, error) {
	// token, err := c.googleOauth.Exchange(ctx, query.Code)

	// if err != nil {
	// 	c.log.Error("Error Google: ", err)
	// 	return nil, errors.New("Error exchanging token")
	// }

	client := c.googleOauth.Client(ctx, &oauth2.Token{
		AccessToken: command.Token,
	})
	// c.googleOauth.Client(ctx, &oauth2.Token{
	// 	AccessToken: ,
	// })
	response, err := client.Get("https://www.googleapis.com/oauth2/v2/userinfo")
	if err != nil {
		return nil, errors.New("Error fetching user info")

	}

	defer response.Body.Close()

	body, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, errors.New("Error reading response")

	}
	var userData *UserGetByLoginGoogle

	json.Unmarshal(body, &userData)
	c.log.Info(userData)
	// userData, errValidate := NewCreateUserByLoginGoogle(userData.Name, userData.Email)

	// if errValidate != nil {
	// 	return nil, err
	// }

	user, err := c.userRepository.GetUserByEmail(ctx, userData.Email)

	if err != nil {
		if err != customErrors.ErrUserNotFound {
			return nil, err
		}
	}

	if user == nil {
		user = &models.User{
			Email:       userData.Email,
			Name:        userData.Name,
			TypeAccount: constants.AccountGoogle,
			Role:        constants.DEFAULT_ROLE,
		}
		if err := c.userRepository.CreateUser(ctx, user); err != nil {
			return nil, err
		}
	} else {
		if user.IsBlock {
			return nil, customErrors.ErrUserBeingBlocked
		}
	}

	if user.TypeAccount != constants.AccountGoogle {
		return nil, customErrors.ErrThisEmailHasBeenUse
	}

	tokenUser, err := utils.Encode(user.ID, user.Role)

	if err != nil {
		return nil, err
	}

	return &dtos.ResponseLoginGoogleDto{
			Token: tokenUser,
			User:  user,
		},
		nil
}
