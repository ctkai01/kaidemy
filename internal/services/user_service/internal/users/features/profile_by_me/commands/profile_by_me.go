package commands

// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"

type ProfileByMe struct {
	IdUser int
}

func NewProfileByMe(idUser int) *ProfileByMe {
	command := &ProfileByMe{IdUser: idUser}

	return command
}
