package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/profile_by_me/dtos"

	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type ProfileByMeHandler struct {
	log            logger.Logger
	userRepository data.UserRepository
}

func NewProfileByMeHandler(
	log logger.Logger,
	userRepository data.UserRepository,

) *ProfileByMeHandler {
	return &ProfileByMeHandler{log: log, userRepository: userRepository}
}

func (c *ProfileByMeHandler) Handle(ctx context.Context, command *ProfileByMe) (*dtos.ProfileByMeResponseDto, error) {
	user, err := c.userRepository.GetUserByID(ctx, command.IdUser)

	if user == nil || err != nil {
		return nil, customErrors.ErrUserNotFound
	}

	return &dtos.ProfileByMeResponseDto{
		User: *user,
	}, nil
}
