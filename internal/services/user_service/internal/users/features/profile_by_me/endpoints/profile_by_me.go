package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/params"
	usersError "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/profile_by_me/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/profile_by_me/dtos"
)

type profileByMeEndpoint struct {
	params.UsersRouteParams
}

func NewProfileByMeEndpoint(
	params params.UsersRouteParams,
) route.Endpoint {
	return &profileByMeEndpoint{
		UsersRouteParams: params,
	}
}

func (ep *profileByMeEndpoint) MapEndpoint() {
	ep.UsersGroup.GET("/me", ep.handler())
}

func (ep *profileByMeEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		userId := utils.GetUserId(c)

		command := commands.NewProfileByMe(
			userId,
		)
		fmt.Println("RESSS")
		result, err := mediatr.Send[*commands.ProfileByMe, *dtos.ProfileByMeResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == usersError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[profileByMeEndpoint_handler.Send] error in sending profile by me",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[profileByMe.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[profileByMeEndpoint.Send] error in sending profile by me",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[profileByMe.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
