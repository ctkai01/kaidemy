package dtos

//https://echo.labstack.com/guide/response/

type GetUserPublicByIDRequestDto struct {
	UserID int `param:"id"`
}
