package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/params"
	usersError "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_user_public_by_id/dtos"
	getUserIdQuery "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_user_public_by_id/queries"
)

type getUserPublicByIDEndpoint struct {
	params.UsersRouteParams
}

func NewGetUserPublicIDEndpoint(
	params params.UsersRouteParams,
) route.Endpoint {
	return &getUserPublicByIDEndpoint{
		UsersRouteParams: params,
	}
}

func (ep *getUserPublicByIDEndpoint) MapEndpoint() {
	ep.UsersGroup.GET("/:id/public", ep.handler())
}

func (ep *getUserPublicByIDEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.GetUserPublicByIDRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[getUserById.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getUserByIdEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}
		query, errValidate := getUserIdQuery.NewGetUserById(
			request.UserID,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[getUserById_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getUserById_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*getUserIdQuery.GetUserPublicById, *dtos.GetUserPublicByIdResponseDto](
			ctx,
			query,
		)

		if err != nil {
			if err.Error() == usersError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[getUserByIdEndpoint_handler.Send] error in sending get User By Id",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[getUserById.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[getUserByIdEndpoint_handler.Send] error in sending reset Password",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[getUserById.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
