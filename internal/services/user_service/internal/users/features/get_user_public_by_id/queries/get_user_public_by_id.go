package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

// https://echo.labstack.com/guide/request/
// https://github.com/go-playground/validator

type GetUserPublicById struct {
	UserID int `validate:"required"`
}

func NewGetUserById(userId int) (*GetUserPublicById, *validator.ValidateError) {
	query := &GetUserPublicById{UserID: userId}

	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}
	return query, nil
}
