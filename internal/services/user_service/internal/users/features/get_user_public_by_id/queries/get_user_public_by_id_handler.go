package queries

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// "encoding/json"
	// "io"

	// "emperror.dev/errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/mapper"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/dto"
	getUserByIdDto "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_user_public_by_id/dtos"

	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login_by_google/queries"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type GetUserByIdHandler struct {
	log            logger.Logger
	userRepository data.UserRepository
}

func NewGetUserPublicByIdHandler(
	log logger.Logger,
	userRepository data.UserRepository,
) *GetUserByIdHandler {
	return &GetUserByIdHandler{log: log, userRepository: userRepository}
}

func (c *GetUserByIdHandler) Handle(ctx context.Context, query *GetUserPublicById) (*getUserByIdDto.GetUserPublicByIdResponseDto, error) {
	user, err := c.userRepository.GetUserByID(ctx, query.UserID)

	if user == nil || err != nil {
		return nil, customErrors.ErrUserNotFound
	}
	userDto, err := mapper.Map[*dto.UserDto](user)

	if err != nil {
		return nil,
			customErrors.NewApplicationErrorWrap(
				err,
				"[GetUserByIdHandler_Handle.Map] error in the mapping user",
			)
	}
	return &getUserByIdDto.GetUserPublicByIdResponseDto{
			User: userDto,
		},
		nil
}
