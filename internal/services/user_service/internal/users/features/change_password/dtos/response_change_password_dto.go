package dtos

//https://echo.labstack.com/guide/response/

type ChangePasswordResponseDto struct {
	Message string `json:"message"`
}
