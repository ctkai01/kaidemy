package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type ChangePassword struct {
	Id          int
	OldPassword string `validate:"required,gte=8,lte=30"`
	NewPassword string `validate:"required,gte=8,lte=30"`
}

func NewChangePassword(oldPassword string, newPassword string, id int) (*ChangePassword, *validator.ValidateError) {
	command := &ChangePassword{OldPassword: oldPassword, NewPassword: newPassword, Id: id}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
