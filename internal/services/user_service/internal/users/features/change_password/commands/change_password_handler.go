package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/change_password/dtos"

	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type ChangePasswordHandler struct {
	log            logger.Logger
	userRepository data.UserRepository
}

func NewChangePasswordHandler(
	log logger.Logger,
	userRepository data.UserRepository,

) *ChangePasswordHandler {
	return &ChangePasswordHandler{log: log, userRepository: userRepository}
}

func (c *ChangePasswordHandler) Handle(ctx context.Context, command *ChangePassword) (*dtos.ChangePasswordResponseDto, error) {
	user, err := c.userRepository.GetUserByID(ctx, command.Id)

	if user == nil || err != nil {
		return nil, customErrors.ErrUserNotFound
	}

	if user.TypeAccount != constants.AccountNormal {
		return nil, customErrors.ErrOnlyForgotPasswordNormal
	}

	if !utils.ComparePassword(command.OldPassword, user.Password) {
		return nil, customErrors.ErrPasswordNotExact
	}

	newPasswordHash, err := utils.HashPassword(command.NewPassword)

	if err != nil {
		return nil, err
	}
	user.Password = newPasswordHash

	if err := c.userRepository.UpdateUser(ctx, user); err != nil {
		return nil, err
	}

	return &dtos.ChangePasswordResponseDto{
		Message: "Change password successfully!",
	}, nil
}
