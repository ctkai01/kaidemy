package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/params"
	usersError "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/change_password/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/change_password/dtos"
)

type changePasswordEndpoint struct {
	params.UsersRouteParams
}

func NewChangePasswordEndpoint(
	params params.UsersRouteParams,
) route.Endpoint {
	return &changePasswordEndpoint{
		UsersRouteParams: params,
	}
}

func (ep *changePasswordEndpoint) MapEndpoint() {
	ep.UsersGroup.POST("/change-password", ep.handler())
}

func (ep *changePasswordEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.ChangePasswordRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[changePasswordEndpoint_handler.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[changePasswordEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}

		userId := utils.GetUserId(c)

		command, errValidate := commands.NewChangePassword(
			request.OldPassword,
			request.NewPassword,
			userId,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[changePasswordEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[changePasswordEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.ChangePassword, *dtos.ChangePasswordResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == usersError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[changePasswordEndpoint_handler.Send] error in sending change Password",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[changePassword.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			if err.Error() == usersError.WrapError(customErrors.ErrOnlyForgotPasswordNormal).Error() {
				errCustom := errors.WithMessage(
					err,
					"[changePasswordEndpoint_handler.Send] error in sending change Password",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[changePassword.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			if err.Error() == usersError.WrapError(customErrors.ErrPasswordNotExact).Error() {
				errCustom := errors.WithMessage(
					err,
					"[changePasswordEndpoint_handler.Send] error in sending change Password",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[changePassword.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[changePasswordEndpoint_handler.Send] error in sending change Password",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[changePassword.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
