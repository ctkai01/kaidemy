package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type UserCreateByNormal struct {
	Name     string `validate:"required,gte=2,lte=100"`
	Email    string `validate:"required,email"`
	Password string `validate:"required,gte=8,lte=30"`
}

func NewCreateUserByNormal(name string, email string, password string) (*UserCreateByNormal, *validator.ValidateError) {
	command := &UserCreateByNormal{Name: name, Email: email, Password: password}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
