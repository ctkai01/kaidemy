package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/register/dtos"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	"golang.org/x/oauth2"
)

type RegisterHandler struct {
	log            logger.Logger
	googleOauth    *oauth2.Config
	userRepository data.UserRepository
}

func NewRegisterHandler(
	log logger.Logger,
	googleOauth *oauth2.Config,
	userRepository data.UserRepository,
) *RegisterHandler {
	return &RegisterHandler{log: log, googleOauth: googleOauth, userRepository: userRepository}
}

func (c *RegisterHandler) Handle(ctx context.Context, command *UserCreateByNormal) (*dtos.RegisterResponseDto, error) {

	userCreate := &models.User{
		Email:       command.Email,
		Name:        command.Name,
		TypeAccount: constants.AccountNormal,
		Role:        constants.DEFAULT_ROLE,
	}

	hashPassword, err := utils.HashPassword(command.Password)
	if err != nil {
		return nil, err
	}

	userCreate.Password = hashPassword

	if err := c.userRepository.CreateUser(ctx, userCreate); err != nil {
		return nil, err
	}

	tokenUser, err := utils.Encode(userCreate.ID, userCreate.Role)

	if err != nil {
		return nil, err
	}

	user, err := c.userRepository.GetUserByID(ctx, userCreate.ID)

	if user == nil || err != nil {
		return nil, customErrors.ErrUnAuthorize
	}

	return &dtos.RegisterResponseDto{
		Token: tokenUser,
		User:  user,
	}, nil
}
