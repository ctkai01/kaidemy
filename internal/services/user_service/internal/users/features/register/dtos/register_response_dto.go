package dtos

//https://echo.labstack.com/guide/response/
import "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"

type RegisterResponseDto struct {
	Token string       `json:"token"`
	User  *models.User `json:"user"`
}
