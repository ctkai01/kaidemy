package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/params"
	usersError "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/register/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/register/dtos"
)

type registerEndpoint struct {
	params.AuthRouteParams
}

func NewRegisterEndpoint(
	params params.AuthRouteParams,
) route.Endpoint {
	return &registerEndpoint{
		AuthRouteParams: params,
	}
}

func (ep *registerEndpoint) MapEndpoint() {
	ep.AuthGroup.POST("/register", ep.handler())
}

func (ep *registerEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.RegisterRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[registerEndpoint_handler.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[registerEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}
		command, errValidate := commands.NewCreateUserByNormal(
			request.Name,
			request.Email,
			request.Password,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[registerEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[registerEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.UserCreateByNormal, *dtos.RegisterResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == usersError.WrapError(usersError.ErrAlreadyExistEmail).Error() {
				errCustom := errors.WithMessage(
					err,
					"[registerEndpoint_handler.Send] error in sending register",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[register.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}
			err = errors.WithMessage(
				err,
				"[registerEndpoint_handler.Send] error in sending register",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[register.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusCreated, result)
	}
}
