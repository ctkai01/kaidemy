package endpoints

import (
	"fmt"
	"net/http"

	"emperror.dev/errors"

	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/params"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
)

type requestLoginBytGoogleEndpoint struct {
	params.AuthRouteParams
}

func NewRequestLoginByGoogleEndpoint(
	params params.AuthRouteParams,
) route.Endpoint {
	return &requestLoginBytGoogleEndpoint{
		AuthRouteParams: params,
	}
}

func (ep *requestLoginBytGoogleEndpoint) MapEndpoint() {
	ep.AuthGroup.GET("/login-google", ep.handler())
}

func (ep *requestLoginBytGoogleEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()
		response, err := mediatr.Send[*any, *dtos.RequestLoginGoogleResponseDto](
			ctx,
			nil,
		)

		if err != nil {
			err = errors.WithMessage(
				err,
				"[requestLoginGoogle_handler.Send] error in sending requestLoginGoogle",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[requestLoginGoogle.Send], err: %v",
					err,
				),
			)
			return err
		}

		return c.Redirect(http.StatusMovedPermanently, response.URL)
	}
}
