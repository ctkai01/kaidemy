package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"golang.org/x/oauth2"
)

type RequestLoginGoogleHandler struct {
	log         logger.Logger
	googleOauth *oauth2.Config
}

func NewRequestLoginGoogleHandler(
	log logger.Logger,
	googleOauth *oauth2.Config,

) *RequestLoginGoogleHandler {
	return &RequestLoginGoogleHandler{log: log, googleOauth: googleOauth}
}

func (c *RequestLoginGoogleHandler) Handle(ctx context.Context, request *any) (*dtos.RequestLoginGoogleResponseDto, error) {
	url := c.googleOauth.AuthCodeURL("state")
	c.log.Info("URL: ", url)
	return &dtos.RequestLoginGoogleResponseDto{
			URL: url,
		},
		nil

}
