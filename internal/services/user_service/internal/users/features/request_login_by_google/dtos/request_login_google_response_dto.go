package dtos

//https://echo.labstack.com/guide/response/

type RequestLoginGoogleResponseDto struct {
	URL string `json:"url"`
}
