package dtos

// import "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/dto"

//https://echo.labstack.com/guide/response/


type Stats struct {
	Total          int   `json:"total"`
	TotalThisMonth int   `json:"total_this_month"`
	DetailStats    []int `json:"detail_stats"`
}

type GetOverviewStudentResponseDto struct {
	Overview *Stats `json:"overview"`
}
