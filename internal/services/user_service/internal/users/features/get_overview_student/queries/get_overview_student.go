package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

// https://echo.labstack.com/guide/request/
// https://github.com/go-playground/validator

type GetOverviewStudent struct {
	// Search string `validate:"required"`
}

func NewGetTeachersSearch() (*GetOverviewStudent, *validator.ValidateError) {
	query := &GetOverviewStudent{}

	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}
	return query, nil
}
