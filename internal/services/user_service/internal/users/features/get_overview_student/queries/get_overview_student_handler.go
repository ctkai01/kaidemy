package queries

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"time"

	// "encoding/json"
	// "io"

	// "emperror.dev/errors"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_overview_student/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login_by_google/queries"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
)

type GetOverviewStudentHandler struct {
	log            logger.Logger
	userRepository data.UserRepository
}

func NewGetOverviewStudentHandler(
	log logger.Logger,
	userRepository data.UserRepository,
) *GetOverviewStudentHandler {
	return &GetOverviewStudentHandler{log: log, userRepository: userRepository}
}

func (c *GetOverviewStudentHandler) Handle(ctx context.Context, query *GetOverviewStudent) (*dtos.GetOverviewStudentResponseDto, error) {
	users, err := c.userRepository.GetAllStudentInThisYear(ctx)

	if err != nil {
		return nil, err
	}
	// if user == nil || err != nil {
	// 	return nil, customErrors.ErrUserNotFound
	// }
	monthlyUserCount := make([]int, 12)
	totalUserThisMonth := 0

	currentMonth := time.Now().Month()
	for _, user := range users {
		if user.CreatedAt != nil {
			month := user.CreatedAt.Month()
			if month == currentMonth {
				totalUserThisMonth++
			}
			monthlyUserCount[month-1]++ // Adjusting month index to start from 0
		}
	}

	result := &dtos.Stats{
		Total:          len(users),
		TotalThisMonth: totalUserThisMonth,
		DetailStats:    monthlyUserCount,
	}

	return &dtos.GetOverviewStudentResponseDto{
			Overview: result,
		},
		nil
}
