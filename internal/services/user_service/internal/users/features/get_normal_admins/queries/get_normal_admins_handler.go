package queries

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_normal_admins/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	// userservice "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type GetNormalAdminsHandler struct {
	log                logger.Logger
	userRepository data.UserRepository
}

func NewGetNormalAdminsHandler(
	log logger.Logger,
	userRepository data.UserRepository,

) *GetNormalAdminsHandler {
	return &GetNormalAdminsHandler{log: log, userRepository: userRepository}
}

func (c *GetNormalAdminsHandler) Handle(ctx context.Context, query *GetNormalAdmins) (*dtos.GetNormalAdminsResponseDto, error) {

	normalAdmins, err := c.userRepository.GetAllNormalAdmins(ctx, query.ListQuery)
	if err != nil {
		return nil, err
	}

	listResultDto, err := utils.ListResultToListResultDto[*models.User](normalAdmins)

	if err != nil {
		return nil, err
	}
	return &dtos.GetNormalAdminsResponseDto{Users: listResultDto}, nil

}
