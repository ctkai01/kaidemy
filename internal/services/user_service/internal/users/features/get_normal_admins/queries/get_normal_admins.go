package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetNormalAdmins struct {
	*utils.ListQuery
}

func NewNormalAdmins(query *utils.ListQuery) (*GetNormalAdmins, *validator.ValidateError) {
	q := &GetNormalAdmins{ListQuery: query}

	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
