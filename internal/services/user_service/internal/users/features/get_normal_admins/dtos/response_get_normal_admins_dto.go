package dtos

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
)

//https://echo.labstack.com/guide/response/

type GetNormalAdminsResponseDto struct {
	Users *utils.ListResult[*models.User]
}
