package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/params"
	usersError "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_normal_admins/dtos"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_normal_admins/queries"
)

type getNormalAdminsEndpoint struct {
	params.AdminsRouteParams
}

func NewGetNormalAdminsEndpoint(
	params params.AdminsRouteParams,
) route.Endpoint {
	return &getNormalAdminsEndpoint{
		AdminsRouteParams: params,
	}
}

func (ep *getNormalAdminsEndpoint) MapEndpoint() {
	ep.AdminsGroup.GET("", ep.handler())
}

func (ep *getNormalAdminsEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		listQuery, err := utils.GetListQueryFromCtx(c)

		if err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[getNormalAdmins.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getNormalAdminsEndpoint_handler.Bind] err: %v", badRequestErr),
			)

			return c.JSON(http.StatusBadRequest, badRequestErr)

		}
		fmt.Printf("Filters: %v\n", listQuery.Filters)

		query, errValidate := queries.NewNormalAdmins(
			listQuery,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[getNormalAdminsEndpoint_handler.StructCtx] query validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getNormalAdminsEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*queries.GetNormalAdmins, *dtos.GetNormalAdminsResponseDto](
			ctx,
			query,
		)

		if err != nil {
			if err.Error() == usersError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[getNormalAdminsEndpoint_handler.Send] error in sending get Languages",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[getNormalAdmins.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			if err.Error() == usersError.WrapError(customErrors.ErrCategoryNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[getNormalAdminsEndpoint_handler.Send] error in sending get Languages",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[getNormalAdmins.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[getNormalAdminsEndpoint_handler.Send] error in sending get Languages",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[getNormalAdmins.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
