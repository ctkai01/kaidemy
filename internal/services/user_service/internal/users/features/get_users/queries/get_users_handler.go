package queries

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// "encoding/json"
	// "io"

	// "emperror.dev/errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_users/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login_by_google/queries"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
)

type GetUsersHandler struct {
	log            logger.Logger
	userRepository data.UserRepository
}

func NewGetUsersHandler(
	log logger.Logger,
	userRepository data.UserRepository,
) *GetUsersHandler {
	return &GetUsersHandler{log: log, userRepository: userRepository}
}

func (c *GetUsersHandler) Handle(ctx context.Context, query *GetUsers) (*dtos.GetUsersResponseDto, error) {
	// query.ListQuery.Filters = append(query.ListQuery.Filters, &utils.FilterModel{
	// 	Field: "role",
	// 	Value: "",
	// })
	users, err := c.userRepository.GetAllUsers(ctx, query.ListQuery)
	
	if err != nil {
		return nil, err
	}

	
	return &dtos.GetUsersResponseDto{
			Users: users,
		},
		nil
}
