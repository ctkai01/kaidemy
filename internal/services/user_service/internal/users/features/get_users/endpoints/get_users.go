package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/params"
	usersError "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_users/dtos"
	 "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/get_users/queries"
)

type getUsersEndpoint struct {
	params.UsersRouteParams
}

func NewGetUsersEndpoint(
	params params.UsersRouteParams,
) route.Endpoint {
	return &getUsersEndpoint{
		UsersRouteParams: params,
	}
}

func (ep *getUsersEndpoint) MapEndpoint() {
	ep.UsersGroup.GET("", ep.handler())
}

func (ep *getUsersEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()
		listQuery, err := utils.GetListQueryFromCtx(c)
		fmt.Println("Query: ", listQuery.Filters)
		
		if err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[getUsers.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getUsersEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())
		}
		query, errValidate := queries.NewGetUsers(
			listQuery,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"getUsers_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("getUsers_handler.StructCtx] err: {%v}", validationErr),
			)

			return customErrors.NewBadRequestErrorHttp(c, validationErr.Error())

		}

		result, err := mediatr.Send[*queries.GetUsers, *dtos.GetUsersResponseDto](
			ctx,
			query,
		)

		if err != nil {
			if err.Error() == usersError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"getUsers_handler.Send] error in sending",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"getUsers.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"getUsersEndpoint_handler.Send] error ",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"getUsers.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
