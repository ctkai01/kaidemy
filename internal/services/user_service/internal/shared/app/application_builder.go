package app

import (
	"os"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/config/environment"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp/contracts"
)

type UsersApplicationBuilder struct {
	contracts.ApplicationBuilder
}

func NewUserApplicationBuilder() *UsersApplicationBuilder {
	var envs []environment.Environment 

	if os.Getenv("ENV") == string(environment.Production) {
		envs = append(envs, environment.Production)
	} else {
		envs = append(envs, environment.Development)
	}
	
	builder := &UsersApplicationBuilder{fxapp.NewApplicationBuilder(envs...)}

	return builder
}

func (a *UsersApplicationBuilder) Build() *UsersApplication {

	return NewUsersApplication(
		a.GetProvides(),
		a.GetDecorates(),
		a.Options(),
		a.Logger(),
		a.Environment(),
	)
}
