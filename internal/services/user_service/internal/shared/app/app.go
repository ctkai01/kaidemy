package app

import (
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/shared/configurations/users"
)

type App struct{}

func NewApp() *App {
	return &App{}
}

func (a *App) Run() {
	// configure dependencies
	appBuilder := NewUserApplicationBuilder()
	appBuilder.ProvideModule(users.UsersServiceModule)

	app := appBuilder.Build()
	// // configure application
	app.ConfigureUsers()
	app.MapUsersEndpoints()

	app.Logger().Info("Starting user service application")
	app.Run()
}
