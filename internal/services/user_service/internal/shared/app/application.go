package app

import (
	"go.uber.org/fx"

	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/shared/configurations/users"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/config/environment"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
)

type UsersApplication struct {
	*users.UsersServiceConfigurator
}

func NewUsersApplication(
	providers []interface{},
	decorates []interface{},
	options []fx.Option,
	logger logger.Logger,
	environment environment.Environment,
) *UsersApplication {
	app := fxapp.NewApplication(providers, decorates, options, logger, environment)
	return &UsersApplication{

		UsersServiceConfigurator: users.NewUsersServiceConfigurator(app),
	}
}
