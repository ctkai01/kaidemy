package users

import (
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	"gorm.io/gorm"
)

func (ic *UsersServiceConfigurator) migrateUsers(gorm *gorm.DB) error {
	// or we could use `gorm.Migrate()`
	err := gorm.AutoMigrate(&models.User{})
	if err != nil {
		return err
	}

	return nil
}
