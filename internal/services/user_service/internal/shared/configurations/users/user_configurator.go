package users

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	"gorm.io/gorm"

	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/config"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/shared/configurations/infrastructure"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/configurations"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/shared/configurations/users/infrastructure"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/configurations"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp/contracts"
	customEcho "gitlab.com/ctkai01/kaidemy/internal/pkg/http/custom_echo"
)

type UsersServiceConfigurator struct {
	contracts.Application
	infrastructureConfigurator *infrastructure.InfrastructureConfigurator
	usersModuleConfigurator    *configurations.UsersModuleConfigurator
}

func NewUsersServiceConfigurator(app contracts.Application) *UsersServiceConfigurator {
	infraConfigurator := infrastructure.NewInfrastructureConfigurator(app)
	userModuleConfigurator := configurations.NewUsersModuleConfigurator(app)

	return &UsersServiceConfigurator{
		Application:                app,
		infrastructureConfigurator: infraConfigurator,
		usersModuleConfigurator:    userModuleConfigurator,
	}
}

func (ic *UsersServiceConfigurator) ConfigureUsers() {
	// Shared
	// Infrastructure
	ic.infrastructureConfigurator.ConfigInfrastructures()
	// Shared
	// Catalogs configurations
	ic.ResolveFunc(func(gorm *gorm.DB) error {
		err := ic.migrateUsers(gorm)
		if err != nil {
			return err
		}
		return nil
	})
	// Modules
	// Product module
	ic.usersModuleConfigurator.ConfigureUsersModule()
}

func (ic *UsersServiceConfigurator) MapUsersEndpoints() {
	// Shared
	ic.ResolveFunc(
		func(usersServer customEcho.EchoHttpServer, cfg *config.Config) error {
			usersServer.SetupDefaultMiddlewares()

			// Config catalogs root endpoint
			usersServer.RouteBuilder().
				RegisterRoutes(func(e *echo.Echo) {
					e.GET("", func(ec echo.Context) error {
						return ec.String(
							http.StatusOK,
							fmt.Sprintf(
								"%s is running...",
								cfg.AppOptions.GetMicroserviceNameUpper(),
							),
						)
					})
				})

			// Config catalogs swagger
			// ic.configSwagger(catalogsServer.RouteBuilder())

			return nil
		},
	)

	// Modules
	// Products CatalogsServiceModule endpoints
	ic.usersModuleConfigurator.MapUsersEndpoints()
	ic.usersModuleConfigurator.MapAuthEndpoints()
}
