package users

import (
	appconfig "gitlab.com/ctkai01/kaidemy/internal/services/userservice/config"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/shared/configurations/users/infrastructure"
	"gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users"
	"go.uber.org/fx"
	// internal/shared/configurations/users/infrastructure
)

var UsersServiceModule = fx.Module(
	"usersfx",
	// Shared Modules
	appconfig.Module,
	infrastructure.Module,

	// Features Modules
	users.Module,

	// Other provides
	// fx.Provide(provideCatalogsMetrics),
)
