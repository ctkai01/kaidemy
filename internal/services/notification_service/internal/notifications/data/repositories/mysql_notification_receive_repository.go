package repositories

// https://github.com/Kamva/mgm
// https://github.com/mongodb/mongo-go-driver
// https://blog.logrocket.com/how-to-use-mongodb-with-go/
// https://www.mongodb.com/docs/drivers/go/current/quick-reference/
// https://www.mongodb.com/docs/drivers/go/current/fundamentals/bson/
// https://www.mongodb.com/docs

import (
	"context"
	"fmt"

	// "errors"

	// "fmt"

	// "emperror.dev/errors"
	// uuid2 "github.com/satori/go.uuid"
	"gorm.io/gorm"

	// "go.mongodb.org/mongo-driver/mongo"
	// attribute2 "go.opentelemetry.io/otel/attribute"

	data2 "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/models"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/core/data"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/core/data"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/core/data/specification"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/gorm_mysql/repository"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/mongodb"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/mongodb/repository"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/otel/tracing"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// userErrors "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/otel/tracing/attribute"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
)

// const (
// 	productCollection = "products"
// )

type mysqlNotificationReceiveRepository struct {
	log                   logger.Logger
	gormGenericRepository data.GenericRepository[*models.NotificationReceive]
}

func NewMySqlNotificationReceiveRepository(
	log logger.Logger,
	db *gorm.DB,
) data2.NotificationReceiveRepository {
	gormRepository := repository.NewGenericGormRepository[*models.NotificationReceive](db)
	return &mysqlNotificationReceiveRepository{
		log:                   log,
		gormGenericRepository: gormRepository,
	}
}

func (p *mysqlNotificationReceiveRepository) CreateNotificationReceiveWithTransaction(
	ctx context.Context,
	tx *gorm.DB,
	notificationReceive *models.NotificationReceive,
) error {
	err := p.gormGenericRepository.AddWithTransaction(ctx, tx, notificationReceive)

	if err != nil {
		return err
	}

	return nil
}

func (p *mysqlNotificationReceiveRepository) CreateAllNotificationReceiveWithTransaction(
	ctx context.Context,
	tx *gorm.DB,
	notificationReceives []*models.NotificationReceive,
) error {
	err := p.gormGenericRepository.AddAllWithTransaction(ctx, tx, notificationReceives)

	if err != nil {
		return err
	}

	return nil
}

func (p *mysqlNotificationReceiveRepository) GetAllNotificationReceives(
	ctx context.Context,
	listQuery *utils.ListQuery,
) (*utils.ListResult[*models.NotificationReceive], error) {
	fmt.Println("In query: ", listQuery)

	result, err := p.gormGenericRepository.GetAll(ctx, listQuery)
	if err != nil {
		return nil, err
	}
	// fmt.Println("In query: ", result)

	// p.log.Infow(
	// 	"[mysqlCategoryRepository_GetAllCategories] categories loaded",
	// 	logger.Fields{"CategoriesResult": result},
	// )

	return result, nil
}

func (p *mysqlNotificationReceiveRepository) GetNotificationReceiveByUserID(ctx context.Context, userID int) ([]*models.NotificationReceive, error) {
	specUserID := specification.Equal("receive_id", userID)

	notificationReceives, err := p.gormGenericRepository.Find(ctx, specUserID)

	if err != nil {
		return nil, err
	}

	return notificationReceives, nil
}
// UpdateNotificationReceive(ctx context.Context, notificationReceive *models.NotificationReceive) error
// 	UpdateAllNotificationReceive(ctx context.Context, notificationReceives []*models.NotificationReceive) error

func (p *mysqlNotificationReceiveRepository) UpdateNotificationReceive(ctx context.Context, notificationReceive *models.NotificationReceive) error {
	err := p.gormGenericRepository.Update(ctx, notificationReceive)

	if err != nil {
		return err
	}

	return nil
}

func (p *mysqlNotificationReceiveRepository) UpdateAllNotificationReceive(ctx context.Context, notificationReceives []*models.NotificationReceive) error {
	for _, notificationReceive := range notificationReceives {
		err := p.UpdateNotificationReceive(ctx, notificationReceive)
		if err != nil {
			return err
		}
	}

	return nil
}



// func (p *mysqlNotificationReceiveRepository) CreateAllNotificationReceiveWithTransaction(
// 	ctx context.Context,
// 	tx *gorm.DB,
// 	notificationReceives []*models.NotificationReceive,
// ) error {
// 	err := p.gormGenericRepository.AddAllWithTransaction(ctx, tx, notificationReceives)

// 	if err != nil {
// 		return err
// 	}

// 	return nil
// }

// if err := mysqlRepo.db.Table(user.TableName()).Where("email = ?", email).First(&user).Error; err != nil {
// 	fmt.Println(err)
// 	if err == gorm.ErrRecordNotFound {
// 		return nil, models.ErrUserNotFound
// 	}
// 	return nil, err
// }
// return user, nil
// ctx, span := p.tracer.Start(ctx, "postgresProductRepository.CreateProduct")
// defer span.End()

// err := p.gormGenericRepository.Add(ctx, product)
// if err != nil {
// 	return nil, tracing.TraceErrFromSpan(
// 		span,
// 		errors.WrapIf(
// 			err,
// 			"[postgresProductRepository_CreateProduct.Create] error in the inserting product into the database.",
// 		),
// 	)
// }

// return nil, nil
// }
// func (p *mysqlUserRepository) GetAllProducts(
// 	ctx context.Context,
// 	listQuery *utils.ListQuery,
// ) (*utils.ListResult[*models.User], error) {
// 	ctx, span := p.tracer.Start(ctx, "postgresProductRepository.GetAllProducts")
// 	defer span.End()

// 	result, err := p.gormGenericRepository.GetAll(ctx, listQuery)
// 	if err != nil {
// 		return nil, tracing.TraceErrFromContext(
// 			ctx,
// 			errors.WrapIf(
// 				err,
// 				"[postgresProductRepository_GetAllProducts.Paginate] error in the paginate",
// 			),
// 		)
// 	}

// 	p.log.Infow(
// 		"[postgresProductRepository.GetAllProducts] products loaded",
// 		logger.Fields{"ProductsResult": result},
// 	)
// 	span.SetAttributes(attribute.Object("ProductsResult", result))

// 	return result, nil
// }

// func (p *mysqlUserRepository) SearchProducts(
// 	ctx context.Context,
// 	searchText string,
// 	listQuery *utils.ListQuery,
// ) (*utils.ListResult[*models.User], error) {
// 	ctx, span := p.tracer.Start(ctx, "postgresProductRepository.SearchProducts")
// 	span.SetAttributes(attribute2.String("SearchText", searchText))
// 	defer span.End()

// 	result, err := p.gormGenericRepository.Search(ctx, searchText, listQuery)
// 	if err != nil {
// 		return nil, tracing.TraceErrFromContext(
// 			ctx,
// 			errors.WrapIf(
// 				err,
// 				"[postgresProductRepository_SearchProducts.Paginate] error in the paginate",
// 			),
// 		)
// 	}

// 	p.log.Infow(
// 		fmt.Sprintf(
// 			"[postgresProductRepository.SearchProducts] products loaded for search term '%s'",
// 			searchText,
// 		),
// 		logger.Fields{"ProductsResult": result},
// 	)
// 	span.SetAttributes(attribute.Object("ProductsResult", result))

// 	return result, nil
// }

// func (p *mysqlUserRepository) GetProductById(
// 	ctx context.Context,
// 	uuid uuid2.UUID,
// ) (*models.User, error) {
// 	ctx, span := p.tracer.Start(ctx, "postgresProductRepository.GetProductById")
// 	span.SetAttributes(attribute2.String("ProductId", uuid.String()))
// 	defer span.End()

// 	product, err := p.gormGenericRepository.GetById(ctx, uuid)
// 	if err != nil {
// 		return nil, tracing.TraceErrFromSpan(
// 			span,
// 			errors.WrapIf(
// 				err,
// 				fmt.Sprintf(
// 					"[postgresProductRepository_GetProductById.First] can't find the product with id %s into the database.",
// 					uuid,
// 				),
// 			),
// 		)
// 	}

// 	span.SetAttributes(attribute.Object("Product", product))
// 	p.log.Infow(
// 		fmt.Sprintf(
// 			"[postgresProductRepository.GetProductById] product with id %s laoded",
// 			uuid.String(),
// 		),
// 		logger.Fields{"Product": product, "ProductId": uuid},
// 	)

// 	return product, nil
// }

// 	span.SetAttributes(attribute.Object("Product", product))
// 	p.log.Infow(
// 		fmt.Sprintf(
// 			"[postgresProductRepository.CreateProduct] product with id '%s' created",
// 			product.ProductId,
// 		),
// 		logger.Fields{"Product": product, "ProductId": product.ProductId},
// 	)

// 	return product, nil
// }

// func (p *mysqlUserRepository) UpdateProduct(
// 	ctx context.Context,
// 	updateProduct *models.User,
// ) (*models.User, error) {
// 	ctx, span := p.tracer.Start(ctx, "postgresProductRepository.UpdateProduct")
// 	defer span.End()

// 	err := p.gormGenericRepository.Update(ctx, updateProduct)
// 	if err != nil {
// 		return nil, tracing.TraceErrFromSpan(
// 			span,
// 			errors.WrapIf(
// 				err,
// 				fmt.Sprintf(
// 					"[postgresProductRepository_UpdateProduct.Save] error in updating product with id %s into the database.",
// 					updateProduct.ProductId,
// 				),
// 			),
// 		)
// 	}

// 	span.SetAttributes(attribute.Object("Product", updateProduct))
// 	p.log.Infow(
// 		fmt.Sprintf(
// 			"[postgresProductRepository.UpdateProduct] product with id '%s' updated",
// 			updateProduct.ProductId,
// 		),
// 		logger.Fields{"Product": updateProduct, "ProductId": updateProduct.ProductId},
// 	)

// 	return updateProduct, nil
// }

// func (p *mysqlUserRepository) DeleteProductByID(ctx context.Context, uuid uuid2.UUID) error {
// 	ctx, span := p.tracer.Start(ctx, "postgresProductRepository.UpdateProduct")
// 	span.SetAttributes(attribute2.String("ProductId", uuid.String()))
// 	defer span.End()

// 	err := p.gormGenericRepository.Delete(ctx, uuid)
// 	if err != nil {
// 		return tracing.TraceErrFromSpan(span, errors.WrapIf(err, fmt.Sprintf(
// 			"[postgresProductRepository_DeleteProductByID.Delete] error in the deleting product with id %s into the database.",
// 			uuid,
// 		)))
// 	}

// 	p.log.Infow(
// 		fmt.Sprintf(
// 			"[postgresProductRepository.DeleteProductByID] product with id %s deleted",
// 			uuid,
// 		),
// 		logger.Fields{"Product": uuid},
// 	)

// 	return nil
// }
