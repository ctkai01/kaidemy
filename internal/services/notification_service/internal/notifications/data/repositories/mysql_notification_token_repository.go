package repositories

// https://github.com/Kamva/mgm
// https://github.com/mongodb/mongo-go-driver
// https://blog.logrocket.com/how-to-use-mongodb-with-go/
// https://www.mongodb.com/docs/drivers/go/current/quick-reference/
// https://www.mongodb.com/docs/drivers/go/current/fundamentals/bson/
// https://www.mongodb.com/docs

import (
	"context"
	// "errors"

	// "fmt"

	// "emperror.dev/errors"
	// uuid2 "github.com/satori/go.uuid"
	"gorm.io/gorm"

	// "go.mongodb.org/mongo-driver/mongo"
	// attribute2 "go.opentelemetry.io/otel/attribute"

	data2 "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/models"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/core/data"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/core/data"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/core/data/specification"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/gorm_mysql/repository"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/mongodb"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/mongodb/repository"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/otel/tracing"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// userErrors "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/data/errors"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/otel/tracing/attribute"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
)

// const (
// 	productCollection = "products"
// )

type mysqlNotificationTokenRepository struct {
	log                   logger.Logger
	gormGenericRepository data.GenericRepository[*models.NotificationToken]
}

func NewMySqlNotificationTokenRepository(
	log logger.Logger,
	db *gorm.DB,
) data2.NotificationTokenRepository {
	gormRepository := repository.NewGenericGormRepository[*models.NotificationToken](db)
	return &mysqlNotificationTokenRepository{
		log:                   log,
		gormGenericRepository: gormRepository,
	}
}

func (p *mysqlNotificationTokenRepository) CreateNotificationTokenWithTransaction(
	ctx context.Context,
	tx *gorm.DB,
	notificationToken *models.NotificationToken,
) error {
	err := p.gormGenericRepository.AddWithTransaction(ctx, tx, notificationToken)

	if err != nil {
		return err
	}

	return nil
}

func (p *mysqlNotificationTokenRepository) CreateNotificationToken(
	ctx context.Context,
	notificationToken *models.NotificationToken,
) error {
	err := p.gormGenericRepository.Add(ctx, notificationToken)

	if err != nil {
		return err
	}

	return nil
}


// GetNotificationTokenByUserID(ctx context.Context, userID int) (*models.NotificationToken, error)

func (p *mysqlNotificationTokenRepository) GetNotificationTokenByUserID(ctx context.Context, userID int) (*models.NotificationToken, error) {
	specUserID := specification.Equal("user_id", userID)

	cartItems, err := p.gormGenericRepository.Find(ctx, specUserID)

	if err != nil {
		return nil, err
	}

	if len(cartItems) == 0 {
		return nil, customErrors.ErrNotificationTokenNotFound
	}

	return cartItems[0], nil
}

func (p *mysqlNotificationTokenRepository) UpdateNotificationToken(ctx context.Context, notificationToken *models.NotificationToken) error {
	err := p.gormGenericRepository.Update(ctx, notificationToken)

	if err != nil {
		return err
	}

	return nil
}


// if err := mysqlRepo.db.Table(user.TableName()).Where("email = ?", email).First(&user).Error; err != nil {
// 	fmt.Println(err)
// 	if err == gorm.ErrRecordNotFound {
// 		return nil, models.ErrUserNotFound
// 	}
// 	return nil, err
// }
// return user, nil
// ctx, span := p.tracer.Start(ctx, "postgresProductRepository.CreateProduct")
// defer span.End()

// err := p.gormGenericRepository.Add(ctx, product)
// if err != nil {
// 	return nil, tracing.TraceErrFromSpan(
// 		span,
// 		errors.WrapIf(
// 			err,
// 			"[postgresProductRepository_CreateProduct.Create] error in the inserting product into the database.",
// 		),
// 	)
// }

// return nil, nil
// }
// func (p *mysqlUserRepository) GetAllProducts(
// 	ctx context.Context,
// 	listQuery *utils.ListQuery,
// ) (*utils.ListResult[*models.User], error) {
// 	ctx, span := p.tracer.Start(ctx, "postgresProductRepository.GetAllProducts")
// 	defer span.End()

// 	result, err := p.gormGenericRepository.GetAll(ctx, listQuery)
// 	if err != nil {
// 		return nil, tracing.TraceErrFromContext(
// 			ctx,
// 			errors.WrapIf(
// 				err,
// 				"[postgresProductRepository_GetAllProducts.Paginate] error in the paginate",
// 			),
// 		)
// 	}

// 	p.log.Infow(
// 		"[postgresProductRepository.GetAllProducts] products loaded",
// 		logger.Fields{"ProductsResult": result},
// 	)
// 	span.SetAttributes(attribute.Object("ProductsResult", result))

// 	return result, nil
// }

// func (p *mysqlUserRepository) SearchProducts(
// 	ctx context.Context,
// 	searchText string,
// 	listQuery *utils.ListQuery,
// ) (*utils.ListResult[*models.User], error) {
// 	ctx, span := p.tracer.Start(ctx, "postgresProductRepository.SearchProducts")
// 	span.SetAttributes(attribute2.String("SearchText", searchText))
// 	defer span.End()

// 	result, err := p.gormGenericRepository.Search(ctx, searchText, listQuery)
// 	if err != nil {
// 		return nil, tracing.TraceErrFromContext(
// 			ctx,
// 			errors.WrapIf(
// 				err,
// 				"[postgresProductRepository_SearchProducts.Paginate] error in the paginate",
// 			),
// 		)
// 	}

// 	p.log.Infow(
// 		fmt.Sprintf(
// 			"[postgresProductRepository.SearchProducts] products loaded for search term '%s'",
// 			searchText,
// 		),
// 		logger.Fields{"ProductsResult": result},
// 	)
// 	span.SetAttributes(attribute.Object("ProductsResult", result))

// 	return result, nil
// }

// func (p *mysqlUserRepository) GetProductById(
// 	ctx context.Context,
// 	uuid uuid2.UUID,
// ) (*models.User, error) {
// 	ctx, span := p.tracer.Start(ctx, "postgresProductRepository.GetProductById")
// 	span.SetAttributes(attribute2.String("ProductId", uuid.String()))
// 	defer span.End()

// 	product, err := p.gormGenericRepository.GetById(ctx, uuid)
// 	if err != nil {
// 		return nil, tracing.TraceErrFromSpan(
// 			span,
// 			errors.WrapIf(
// 				err,
// 				fmt.Sprintf(
// 					"[postgresProductRepository_GetProductById.First] can't find the product with id %s into the database.",
// 					uuid,
// 				),
// 			),
// 		)
// 	}

// 	span.SetAttributes(attribute.Object("Product", product))
// 	p.log.Infow(
// 		fmt.Sprintf(
// 			"[postgresProductRepository.GetProductById] product with id %s laoded",
// 			uuid.String(),
// 		),
// 		logger.Fields{"Product": product, "ProductId": uuid},
// 	)

// 	return product, nil
// }

// 	span.SetAttributes(attribute.Object("Product", product))
// 	p.log.Infow(
// 		fmt.Sprintf(
// 			"[postgresProductRepository.CreateProduct] product with id '%s' created",
// 			product.ProductId,
// 		),
// 		logger.Fields{"Product": product, "ProductId": product.ProductId},
// 	)

// 	return product, nil
// }

// func (p *mysqlUserRepository) UpdateProduct(
// 	ctx context.Context,
// 	updateProduct *models.User,
// ) (*models.User, error) {
// 	ctx, span := p.tracer.Start(ctx, "postgresProductRepository.UpdateProduct")
// 	defer span.End()

// 	err := p.gormGenericRepository.Update(ctx, updateProduct)
// 	if err != nil {
// 		return nil, tracing.TraceErrFromSpan(
// 			span,
// 			errors.WrapIf(
// 				err,
// 				fmt.Sprintf(
// 					"[postgresProductRepository_UpdateProduct.Save] error in updating product with id %s into the database.",
// 					updateProduct.ProductId,
// 				),
// 			),
// 		)
// 	}

// 	span.SetAttributes(attribute.Object("Product", updateProduct))
// 	p.log.Infow(
// 		fmt.Sprintf(
// 			"[postgresProductRepository.UpdateProduct] product with id '%s' updated",
// 			updateProduct.ProductId,
// 		),
// 		logger.Fields{"Product": updateProduct, "ProductId": updateProduct.ProductId},
// 	)

// 	return updateProduct, nil
// }

// func (p *mysqlUserRepository) DeleteProductByID(ctx context.Context, uuid uuid2.UUID) error {
// 	ctx, span := p.tracer.Start(ctx, "postgresProductRepository.UpdateProduct")
// 	span.SetAttributes(attribute2.String("ProductId", uuid.String()))
// 	defer span.End()

// 	err := p.gormGenericRepository.Delete(ctx, uuid)
// 	if err != nil {
// 		return tracing.TraceErrFromSpan(span, errors.WrapIf(err, fmt.Sprintf(
// 			"[postgresProductRepository_DeleteProductByID.Delete] error in the deleting product with id %s into the database.",
// 			uuid,
// 		)))
// 	}

// 	p.log.Infow(
// 		fmt.Sprintf(
// 			"[postgresProductRepository.DeleteProductByID] product with id %s deleted",
// 			uuid,
// 		),
// 		logger.Fields{"Product": uuid},
// 	)

// 	return nil
// }
