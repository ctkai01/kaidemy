package notifications

import (
	// "fmt"

	customEcho "gitlab.com/ctkai01/kaidemy/internal/pkg/http/custom_echo"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "fmt"

	"github.com/labstack/echo/v4"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/data/repositories"
	"go.uber.org/fx"

	// "github.com/mehdihadeli/go-ecommerce-microservices/internal/services/catalogreadservice/internal/products/data/repositories"
	
	registerNotificationToken "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/features/register_notification_token/endpoints"
	getNotifications "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/features/get_notifications/endpoints"
	readAllNotifications "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/features/read_all_notification/endpoints"

	// searchProductV1 "github.com/mehdihadeli/go-ecommerce-microservices/internal/services/catalogreadservice/internal/products/features/searching_products/v1/endpoints"
	// customEcho "gitlab.com/ctkai01/kaidemy/internal/pkg/http/custom_echo"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/http/middlewares"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	// googleOauth "gitlab.com/ctkai01/kaidemy/internal/pkg/oauth/google"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/grpc"
)

var Module = fx.Module(
	"notificationsfx",

	// Other provides
	// fx.Provide(repositories.NewRedisProductRepository),
	// fx.Provide(NewHTTPServer),
	fx.Provide(repositories.NewMySqlEntityTypeRepository),
	fx.Provide(repositories.NewMySqlNotificationReceiveRepository),
	// fx.Provide(repositories.NewMySqlNotificationSenderRepository),
	fx.Provide(repositories.NewMySqlNotificationTokenRepository),
	fx.Provide(repositories.NewMySqlNotificationRepository),

	fx.Provide(grpc.NewNotificationGrpcService),

	fx.Provide(fx.Annotate(func(catalogsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		catalogsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			v1.Use(middlewares.AuthMiddleware)
			group := v1.Group("/notifications")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"notifications-echo-group"`))),

	fx.Provide(
		// //Course route
		route.AsRoute(registerNotificationToken.NewRegisterNotificationTokenEndpoint, "notifications-routes"),
		route.AsRoute(getNotifications.NewGetNotificationEndpoint, "notifications-routes"),
		route.AsRoute(readAllNotifications.NewReadAllNotificationEndpoint, "notifications-routes"),
		// route.AsRoute(updateCourse.NewUpdateCourseEndpoint, "courses-routes"),

		// // Curriculum route
		// route.AsRoute(createCurriculum.NewCurriculumEndpoint, "courses-routes"),
		// route.AsRoute(updateCurriculum.NewUpdateCurriculumEndpoint, "courses-routes"),
		// route.AsRoute(deleteCurriculum.NewDeleteCurriculumEndpoint, "courses-routes"),
	),
)

//   lc.Append(fx.Hook{
//     OnStart: func(ctx context.Context) error {
//       ln, err := net.Listen("tcp", srv.Addr)
//       if err != nil {
//         return err
//       }
//       fmt.Println("Starting HTTP server at", srv.Addr)
//       go srv.Serve(ln)
//       return nil
//     },
//     OnStop: func(ctx context.Context) error {
//       return srv.Shutdown(ctx)
//     },
//   })
