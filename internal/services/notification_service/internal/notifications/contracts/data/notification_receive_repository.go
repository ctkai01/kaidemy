package data

import (
	"context"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	// uuid "github.com/satori/go.uuid"

	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/models"
	"gorm.io/gorm"
)

type NotificationReceiveRepository interface {
	// GetAllProducts(
	// 	ctx context.Context,
	// 	listQuery *utils.ListQuery,
	// ) (*utils.ListResult[*models.User], error)
	// SearchProducts(
	// 	ctx context.Context,
	// 	searchText string,
	// 	listQuery *utils.ListQuery,
	// ) (*utils.ListResult[*models.User], error)
	// GetUserByID(ctx context.Context, id int) (*models.Course, error)
	// GetUserByEmail(ctx context.Context, email string) (*models.Course, error)
	// GetUserByEmailToken(ctx context.Context, emailToken string) (*models.Course, error)
	CreateNotificationReceiveWithTransaction(ctx context.Context, tx *gorm.DB, notificationReceive *models.NotificationReceive) error
	CreateAllNotificationReceiveWithTransaction(ctx context.Context, tx *gorm.DB, notificationReceives []*models.NotificationReceive) error
	// UpdateUser(ctx context.Context, user *models.Course) error
	GetAllNotificationReceives(
		ctx context.Context,
		listQuery *utils.ListQuery,
	) (*utils.ListResult[*models.NotificationReceive], error)
	GetNotificationReceiveByUserID(ctx context.Context, userID int) ([]*models.NotificationReceive, error)
	UpdateNotificationReceive(ctx context.Context, notificationReceive *models.NotificationReceive) error
	UpdateAllNotificationReceive(ctx context.Context, notificationReceives []*models.NotificationReceive) error
	
	// UpdateEmailToken(ctx context.Context, id int, mailToken string) error
	// UpdateProduct(ctx context.Context, product *models.User) (*models.User, error)
	// DeleteProductByID(ctx context.Context, uuid uuid.UUID) error
}
