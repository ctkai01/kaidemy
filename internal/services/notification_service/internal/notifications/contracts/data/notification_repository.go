package data

import (
	"context"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	// uuid "github.com/satori/go.uuid"

	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/models"
	"gorm.io/gorm"
)

type NotificationRepository interface {
	// GetAllProducts(
	// 	ctx context.Context,
	// 	listQuery *utils.ListQuery,
	// ) (*utils.ListResult[*models.User], error)
	// SearchProducts(
	// 	ctx context.Context,
	// 	searchText string,
	// 	listQuery *utils.ListQuery,
	// ) (*utils.ListResult[*models.User], error)
	// GetUserByID(ctx context.Context, id int) (*models.Course, error)
	// GetUserByEmail(ctx context.Context, email string) (*models.Course, error)
	// GetUserByEmailToken(ctx context.Context, emailToken string) (*models.Course, error)
	CreateNotification(ctx context.Context, notification *models.Notification) error
	// UpdateUser(ctx context.Context, user *models.Course) error
	CreateAllNotificationWithTransaction(ctx context.Context, tx *gorm.DB, notifications []*models.Notification) error
	CreateNotificationWithTransaction(ctx context.Context, tx *gorm.DB, notification *models.Notification) error
	
	// UpdateEmailToken(ctx context.Context, id int, mailToken string) error
	// UpdateProduct(ctx context.Context, product *models.User) (*models.User, error)
	// DeleteProductByID(ctx context.Context, uuid uuid.UUID) error
}
