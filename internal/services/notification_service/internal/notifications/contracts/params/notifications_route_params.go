package params

import (
	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"go.uber.org/fx"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
)

type NotificationsRouteParams struct {
	fx.In

	Logger           logger.Logger
	NotificationsGroup     *echo.Group `name:"notifications-echo-group"`
	Validator        *validator.Validate
}
