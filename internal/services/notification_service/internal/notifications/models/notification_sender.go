package models

import (
	"time"
)

type NotificationSender struct {
	ID             int `json:"id" gorm:"primary_key"`
	NotificationID int `json:"notification_id" gorm:"type:int;not null"`

	SenderID int    `json:"sender_id" gorm:"type:int;not null"`

	UpdatedAt *time.Time `json:"updated_at" gorm:"column:updated_at"`
	CreatedAt *time.Time `json:"created_at" gorm:"column:created_at"`
}

func (NotificationSender) TableName() string { return "notification_senders" }
