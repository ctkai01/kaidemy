package models

import (
	"time"
)

type NotificationReceive struct {
	ID int `json:"id" gorm:"primary_key"`

	NotificationID int `json:"notification_id" gorm:"type:int;not null"`

	Title     string     `json:"title" gorm:"not null"`
	Body      string     `json:"body" gorm:"not null"`
	ReceiveID int        `json:"receive_id" gorm:"type:int;not null"`
	Status    int        `json:"status" gorm:"type:int;not null;default:0"`
	Type      int        `json:"type" gorm:"type:int;not null"`
	UpdatedAt *time.Time `json:"updated_at" gorm:"column:updated_at"`
	CreatedAt *time.Time `json:"created_at" gorm:"column:created_at"`
}

func (NotificationReceive) TableName() string { return "notification_receives" }
