package models

import (
	"time"
)

type Notification struct {
	ID           int    `json:"id" gorm:"primary_key"`
	// Title        string `json:"title" gorm:"not null"`
	// Body         string `json:"body" gorm:"not null"`
	EntityTypeID int    `json:"entity_type_id" gorm:"type:int;not null"`
	EntityID     int    `json:"entity_id" gorm:"type:int;not null"`
	SenderID int    `json:"sender_id" gorm:"type:int;not null"`

	// NotificationSender   NotificationSender    `json:"notification_sender"`
	NotificationReceives []NotificationReceive `json:"notification_receives"`
	// NotificationObjectID int `json:"notification_object_id" gorm:"type:int;not null"`

	// NotifierID int `json:"notifier_id" gorm:"type:int;not null"`

	UpdatedAt *time.Time `json:"updated_at" gorm:"column:updated_at"`
	CreatedAt *time.Time `json:"created_at" gorm:"column:created_at"`
}

func (Notification) TableName() string { return "notifications" }
