package models

import (
	"time"
)

type EntityType struct {
	ID int `json:"id" gorm:"primary_key"`

	Entity string `json:"entity" gorm:"not null"`

	Description string `json:"description" gorm:"not null"`

	UpdatedAt *time.Time `json:"updated_at" gorm:"column:updated_at"`
	CreatedAt *time.Time `json:"created_at" gorm:"column:created_at"`
}

func (EntityType) TableName() string { return "entity_types" }
