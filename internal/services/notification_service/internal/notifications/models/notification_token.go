package models

import (
	"time"
)

type NotificationToken struct {
	ID     int    `json:"id" gorm:"primary_key"`
	UserID int    `json:"user_id" gorm:"type:int;unique;not null"`
	Token  string `json:"token" gorm:"not null"`

	UpdatedAt *time.Time `json:"updated_at" gorm:"column:updated_at"`
	CreatedAt *time.Time `json:"created_at" gorm:"column:created_at"`
}

func (NotificationToken) TableName() string { return "notification_tokens" }
