package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"

	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"firebase.google.com/go/messaging"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/features/push_notification/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type PushNotificationHandler struct {
	log                           logger.Logger
	firebaseMessaging             *messaging.Client
}

func NewPushNotificationHandler(
	log logger.Logger,
	firebaseMessaging *messaging.Client,

) *PushNotificationHandler {
	return &PushNotificationHandler{
		log:                           log,
		firebaseMessaging:             firebaseMessaging,
	}
}

func (c *PushNotificationHandler) Handle(ctx context.Context, command *PushNotification) (*dtos.PushNotificationResponseDto, error) {
	_, err := c.firebaseMessaging.Send(ctx, command.message)

	if err != nil {
		return nil, err
	}

	return &dtos.PushNotificationResponseDto{
		Message: "Push Notification successfully!",
	}, nil
}
