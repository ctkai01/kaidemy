package commands

import (
	"fmt"

	"firebase.google.com/go/messaging"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type PushNotification struct {
	message     *messaging.Message
}

func NewPushNotification(
	message     *messaging.Message,
) (*PushNotification, *validator.ValidateError) {
	command := &PushNotification{
		message,
	}
	errValidate := validator.Validate(command)
	fmt.Println("Err: ", errValidate)
	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
