package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/contracts/params"
	// notificationsError "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/features/register_notification_token/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/features/register_notification_token/dtos"
)

type registerNotificationTokenEndpoint struct {
	params.NotificationsRouteParams
}

func NewRegisterNotificationTokenEndpoint(
	params params.NotificationsRouteParams,
) route.Endpoint {
	return &registerNotificationTokenEndpoint{
		NotificationsRouteParams: params,
	}
}

func (ep *registerNotificationTokenEndpoint) MapEndpoint() {
	ep.NotificationsGroup.POST("/token", ep.handler())
}

func (ep *registerNotificationTokenEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.RegisterNotificationTokenRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[RegisterNotificationToken.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[RegisterNotificationToken_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())
		}

		userId := utils.GetUserId(c)

		command, errValidate := commands.NewRegisterNotificationToken(
			userId,
			request.FcmToken,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[RegisterNotificationToken_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[RegisterNotificationToken_handler.StructCtx] err: {%v}", validationErr),
			)

			return customErrors.NewBadRequestErrorHttp(c, validationErr.Error())

		}

		result, err := mediatr.Send[*commands.RegisterNotificationToken, *dtos.RegisterNotificationTokenResponseDto](
			ctx,
			command,
		)

		if err != nil {
			// if err.Error() == coursesError.WrapError(customErrors.ErrUserNotFound).Error() {
			// 	errCustom := errors.WithMessage(
			// 		err,
			// 		"[RegisterNotificationToken_handler.Send] error in sending create Answer",
			// 	)
			// 	ep.Logger.Error(
			// 		fmt.Sprintf(
			// 			"[createAnswer.Send], err: %v",
			// 			errCustom,
			// 		),
			// 	)
			// 	return customErrors.NewBadRequestErrorHttp(c, err.Error())
			// }

			err = errors.WithMessage(
				err,
				"[RegisterNotificationTokenEndpoint_handler.Send] error in sending create notification token",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[RegisterNotificationToken.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusCreated, result)
	}
}
