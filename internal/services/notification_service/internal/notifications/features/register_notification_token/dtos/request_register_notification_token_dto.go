package dtos

//https://echo.labstack.com/guide/response/

type RegisterNotificationTokenRequestDto struct {
	FcmToken string     `form:"fcm_token"`
}
