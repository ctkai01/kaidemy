package dtos

//https://echo.labstack.com/guide/response/

type RegisterNotificationTokenResponseDto struct {
	Message string        `json:"message"`
}
