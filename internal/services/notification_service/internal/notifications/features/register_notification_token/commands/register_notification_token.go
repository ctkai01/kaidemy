package commands

import (
	"fmt"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type RegisterNotificationToken struct {
	IdUser     int
	FcmToken string `validate:"required,gte=1,lte=200"`
}

func NewRegisterNotificationToken(
	idUser int,
	fcmToken string,

) (*RegisterNotificationToken, *validator.ValidateError) {
	command := &RegisterNotificationToken{
		idUser,
		fcmToken,
	}
	errValidate := validator.Validate(command)
	fmt.Println("Err: ", errValidate)
	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
