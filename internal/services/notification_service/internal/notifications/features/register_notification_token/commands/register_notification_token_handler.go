package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/features/register_notification_token/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type RegisterNotificationTokenHandler struct {
	log                         logger.Logger
	notificationTokenRepository data.NotificationTokenRepository
	grpcClient                  grpc.GrpcClient
}

func NewRegisterNotificationTokenHandler(
	log logger.Logger,
	notificationTokenRepository data.NotificationTokenRepository,
	grpcClient grpc.GrpcClient,

) *RegisterNotificationTokenHandler {
	return &RegisterNotificationTokenHandler{
		log:                         log,
		notificationTokenRepository: notificationTokenRepository,
		grpcClient:                  grpcClient,
	}
}

func (c *RegisterNotificationTokenHandler) Handle(ctx context.Context, command *RegisterNotificationToken) (*dtos.RegisterNotificationTokenResponseDto, error) {
	notificationToken, err := c.notificationTokenRepository.GetNotificationTokenByUserID(ctx, command.IdUser)

	isNotificationTokenExist := true

	if err != nil {
		if err == customErrors.ErrNotificationTokenNotFound {
			isNotificationTokenExist = false
		} else {
			return nil, err
		}
	}

	if isNotificationTokenExist {
		// Update Notification token
		notificationToken.Token = command.FcmToken
		err := c.notificationTokenRepository.UpdateNotificationToken(ctx, notificationToken)

		if err != nil {
			return nil, err
		}
	} else {
		// Create Notification token

		createNotificationToken := &models.NotificationToken{
			UserID: command.IdUser,
			Token:  command.FcmToken,
		}
		err := c.notificationTokenRepository.CreateNotificationToken(ctx, createNotificationToken)
		
		if err != nil {
			return nil, err
		}
	}

	return &dtos.RegisterNotificationTokenResponseDto{
		Message: "Register notification token successfully!",
	}, nil
}
