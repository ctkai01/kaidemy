package dtos

//https://echo.labstack.com/guide/response/

type ReadAllNotificationResponseDto struct {
	Message string        `json:"message"`
}
