package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/contracts/params"
	// notificationsError "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/features/read_all_notification/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/features/read_all_notification/dtos"
)

type readAllNotificationTokenEndpoint struct {
	params.NotificationsRouteParams
}

func NewReadAllNotificationEndpoint(
	params params.NotificationsRouteParams,
) route.Endpoint {
	return &readAllNotificationTokenEndpoint{
		NotificationsRouteParams: params,
	}
}

func (ep *readAllNotificationTokenEndpoint) MapEndpoint() {
	ep.NotificationsGroup.PUT("/read-all", ep.handler())
}

func (ep *readAllNotificationTokenEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.ReadAllNotificationRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[ReadAllNotification.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[ReadAllNotification_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())
		}

		userId := utils.GetUserId(c)

		command, errValidate := commands.NewReadAllNotification(
			userId,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[ReadAllNotification_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[ReadAllNotification_handler.StructCtx] err: {%v}", validationErr),
			)

			return customErrors.NewBadRequestErrorHttp(c, validationErr.Error())

		}

		result, err := mediatr.Send[*commands.ReadAllNotification, *dtos.ReadAllNotificationResponseDto](
			ctx,
			command,
		)

		if err != nil {
			// if err.Error() == coursesError.WrapError(customErrors.ErrUserNotFound).Error() {
			// 	errCustom := errors.WithMessage(
			// 		err,
			// 		"[RegisterNotificationToken_handler.Send] error in sending create Answer",
			// 	)
			// 	ep.Logger.Error(
			// 		fmt.Sprintf(
			// 			"[createAnswer.Send], err: %v",
			// 			errCustom,
			// 		),
			// 	)
			// 	return customErrors.NewBadRequestErrorHttp(c, err.Error())
			// }

			err = errors.WithMessage(
				err,
				"[ReadAllNotificationEndpoint_handler.Send] error in sending create notification token",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[ReadAllNotification.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
