package commands

import (
	"fmt"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type ReadAllNotification struct {
	IdUser     int
}

func NewReadAllNotification(
	idUser int,

) (*ReadAllNotification, *validator.ValidateError) {
	command := &ReadAllNotification{
		idUser,
	}
	errValidate := validator.Validate(command)
	fmt.Println("Err: ", errValidate)
	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
