package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/features/read_all_notification/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type ReadAllNotificationHandler struct {
	log                         logger.Logger
	notificationReceiveRepository data.NotificationReceiveRepository
	grpcClient                  grpc.GrpcClient
}

func NewReadAllNotificationHandler(
	log logger.Logger,
	notificationReceiveRepository data.NotificationReceiveRepository,
	grpcClient grpc.GrpcClient,

) *ReadAllNotificationHandler {
	return &ReadAllNotificationHandler{
		log:                         log,
		notificationReceiveRepository: notificationReceiveRepository,
		grpcClient:                  grpcClient,
	}
}

func (c *ReadAllNotificationHandler) Handle(ctx context.Context, command *ReadAllNotification) (*dtos.ReadAllNotificationResponseDto, error) {
	notificationReceives, err := c.notificationReceiveRepository.GetNotificationReceiveByUserID(ctx, command.IdUser)

	if err != nil {
		return nil, err
	}

	for _, notificationReceive := range notificationReceives {
		notificationReceive.Status = constants.READ_NOTIFICATION
	}

	err = c.notificationReceiveRepository.UpdateAllNotificationReceive(ctx, notificationReceives)

	if err != nil {
		return nil, err
	}

	return &dtos.ReadAllNotificationResponseDto{
		Message: "Read all notification successfully!",
	}, nil
}
