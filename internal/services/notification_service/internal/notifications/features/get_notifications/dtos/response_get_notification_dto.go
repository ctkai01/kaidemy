package dtos

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/models"

)

type GetNotificationResponseDto struct {
	Message string `json:"message"`
	Notifications *utils.ListResult[*models.NotificationReceive] `json:"notifications"`
}
