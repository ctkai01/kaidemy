package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetNotificationToken struct {
	IdUser int
	*utils.ListQuery
}

func NewGetNotificationToken(
	idUser int,
	query *utils.ListQuery,

) (*GetNotificationToken, *validator.ValidateError) {
	q := &GetNotificationToken{
		idUser,
		query,
	}
	
	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
