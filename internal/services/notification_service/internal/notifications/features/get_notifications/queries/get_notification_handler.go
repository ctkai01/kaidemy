package queries

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/features/get_notifications/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"

	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// catalogservice "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type GetNotificationHandler struct {
	log                         logger.Logger
	notificationTokenRepository data.NotificationTokenRepository
	notificationReceiveRepository data.NotificationReceiveRepository
	grpcClient                  grpc.GrpcClient
}

func NewGetNotificationHandler(
	log logger.Logger,
	notificationTokenRepository data.NotificationTokenRepository,
	notificationReceiveRepository data.NotificationReceiveRepository,
	grpcClient grpc.GrpcClient,

) *GetNotificationHandler {
	return &GetNotificationHandler{
		log:                         log,
		notificationTokenRepository: notificationTokenRepository,
		notificationReceiveRepository: notificationReceiveRepository,
		grpcClient:                  grpcClient,
	}
}

func (c *GetNotificationHandler) Handle(ctx context.Context, command *GetNotificationToken) (*dtos.GetNotificationResponseDto, error) {
	notificationReceives, err := c.notificationReceiveRepository.GetAllNotificationReceives(ctx, command.ListQuery)

	if err != nil {
		return nil, err
	}
	listResultDto, err := utils.ListResultToListResultDto[*models.NotificationReceive](notificationReceives)
	
	if err != nil {
		return nil, err
	}

	return &dtos.GetNotificationResponseDto{
		Message: "Get notification successfully!",
		Notifications: listResultDto,
	}, nil
}
