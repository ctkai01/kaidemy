package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/contracts/params"
	// notificationsError "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/features/get_notifications/queries"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/features/get_notifications/dtos"
)

type getNotificationsEndpoint struct {
	params.NotificationsRouteParams
}

func NewGetNotificationEndpoint(
	params params.NotificationsRouteParams,
) route.Endpoint {
	return &getNotificationsEndpoint{
		NotificationsRouteParams: params,
	}
}

func (ep *getNotificationsEndpoint) MapEndpoint() {
	ep.NotificationsGroup.GET("", ep.handler())
}

func (ep *getNotificationsEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		listQuery, err := utils.GetListQueryFromCtx(c)
		fmt.Println("Query: ", listQuery)
		
		if err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[getNotifications.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getNotificationsEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())
		}
		fmt.Printf("Filters: %v\n", listQuery.Filters)

		userId := utils.GetUserId(c)
		userIdStr := fmt.Sprintf("%d", userId)
	
		listQuery.Filters = append(listQuery.Filters, &utils.FilterModel {
			Field: "receive_id",
			Value: &userIdStr,
			Comparison: "equals",
		})

		query, errValidate := queries.NewGetNotificationToken(
			userId,
			listQuery,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[getNotification_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getNotification_handler.StructCtx] err: {%v}", validationErr),
			)

			return customErrors.NewBadRequestErrorHttp(c, validationErr.Error())

		}

		result, err := mediatr.Send[*queries.GetNotificationToken, *dtos.GetNotificationResponseDto](
			ctx,
			query,
		)

		if err != nil {
			// if err.Error() == coursesError.WrapError(customErrors.ErrUserNotFound).Error() {
			// 	errCustom := errors.WithMessage(
			// 		err,
			// 		"[RegisterNotificationToken_handler.Send] error in sending create Answer",
			// 	)
			// 	ep.Logger.Error(
			// 		fmt.Sprintf(
			// 			"[createAnswer.Send], err: %v",
			// 			errCustom,
			// 		),
			// 	)
			// 	return customErrors.NewBadRequestErrorHttp(c, err.Error())
			// }

			err = errors.WithMessage(
				err,
				"[GetNotificationEndpoint_handler.Send] error in sending get notification",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[GetNotification.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
