package commands

import (
	"fmt"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type NotificationPurchaseCourse struct {
	IdUser     int
	IDCourse   int
	IDCartItem   int
}

func NewNotificationPurchaseCourse(
	idUser int,
	idCourse  int,
	idCartItem  int,
) (*NotificationPurchaseCourse, *validator.ValidateError) {
	command := &NotificationPurchaseCourse{
		idUser,
		idCourse,
		idCartItem,
	}
	errValidate := validator.Validate(command)
	fmt.Println("Err: ", errValidate)
	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
