package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"encoding/json"
	"fmt"

	// // "io"
	// "strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"firebase.google.com/go/messaging"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/configurations"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/producer"
	"gorm.io/gorm"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/models"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/shared/utils"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/features/notification_purchase_course/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	notificationservice "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type NotificationPurchaseCourseHandler struct {
	log                    logger.Logger
	notificationRepository data.NotificationRepository
	// notificationSenderRepository  data.NotificationSenderRepository
	notificationReceiveRepository data.NotificationReceiveRepository
	notificationTokenRepository   data.NotificationTokenRepository
	entityTypeRepository          data.EntityTypeRepository
	grpcClient                    grpc.GrpcClient
	db                            *gorm.DB
	firebaseMessaging             *messaging.Client
	rabbitmqBuilder               configurations.RabbitMQConfigurationBuilder
}

func NewNotificationPurchaseCourseHandler(
	log logger.Logger,
	notificationRepository data.NotificationRepository,
	// notificationSenderRepository data.NotificationSenderRepository,
	notificationReceiveRepository data.NotificationReceiveRepository,
	notificationTokenRepository data.NotificationTokenRepository,
	entityTypeRepository data.EntityTypeRepository,
	grpcClient grpc.GrpcClient,
	db *gorm.DB,
	firebaseMessaging *messaging.Client,
	rabbitmqBuilder configurations.RabbitMQConfigurationBuilder,

) *NotificationPurchaseCourseHandler {
	return &NotificationPurchaseCourseHandler{
		log:                    log,
		notificationRepository: notificationRepository,
		// notificationSenderRepository:  notificationSenderRepository,
		notificationReceiveRepository: notificationReceiveRepository,
		entityTypeRepository:          entityTypeRepository,
		notificationTokenRepository:   notificationTokenRepository,
		grpcClient:                    grpcClient,
		db:                            db,
		firebaseMessaging:             firebaseMessaging,
		rabbitmqBuilder:               rabbitmqBuilder,
	}
}

func (c *NotificationPurchaseCourseHandler) Handle(ctx context.Context, command *NotificationPurchaseCourse) (*dtos.NotificationPurchaseCourseResponseDto, error) {
	conn, err := c.grpcClient.GetGrpcConnection("course_service")
	fmt.Println("Course ", err)
	if err != nil {
		return nil, err
	}
	fmt.Println("Course 1")

	connUser, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}
	fmt.Println("Course 12")

	client := notificationservice.NewCoursesServiceClient(conn)
	dataCourse, err := client.GetCourseByID(context.Background(), &notificationservice.GetCourseByIdReq{
		CourseID: int32(command.IDCourse),
	})

	if err != nil {
		return nil, err
	}

	clientUser := notificationservice.NewUsersServiceClient(connUser)
	dataUser, err := clientUser.GetUserByID(context.Background(), &notificationservice.GetUserByIdReq{
		UserId: int32(command.IdUser),
	})

	if err != nil {
		return nil, err
	}

	var errorTransaction error
	pushMessages := make([]messaging.Message, 0)
	createNotificationReceives := make([]*models.NotificationReceive, 0)

	c.db.Transaction(func(tx *gorm.DB) error {
		//Create Notification Object
		// createNotificationTx := c.db.WithContext(ctx).Begin()

		createNotification := &models.Notification{
			EntityTypeID: 1,
			EntityID:     command.IDCourse,
			SenderID:     command.IdUser,
		}

		err = c.notificationRepository.CreateNotificationWithTransaction(ctx, tx, createNotification)
		c.log.Debug("Err noti: ", err)
		if err != nil {
			// createNotificationTx.Rollback()
			errorTransaction = err
			return err
		}

		// Create Notification Sender
		// createNotificationSenderTx := c.db.WithContext(ctx).Begin()

		// createNotificationSender := &models.NotificationSender{
		// 	NotificationID: createNotification.ID,
		// 	SenderID:       command.IdUser,
		// }
		// c.log.Debug("Done d1: ", createNotificationSender)

		// err = c.notificationSenderRepository.CreateNotificationSenderWithTransaction(ctx, createNotificationSenderTx, createNotificationSender)
		// c.log.Debug("Done d: ", err)

		// if err != nil {
		// 	createNotificationTx.Rollback()
		// 	createNotificationSenderTx.Rollback()
		// 	return nil, err
		// }
		// Create Notification Receive
		// createNotificationReceivesTx := c.db.WithContext(ctx).Begin()

		// Add notification for author
		createNotificationReceives = append(createNotificationReceives, &models.NotificationReceive{
			NotificationID: createNotification.ID,
			ReceiveID:      int(dataCourse.Course.UserID),
			Type:           constants.NOTIFICATION_FOR_INSTRUCTOR,
			Title:          utils.TemplateTitlePurchaseCourseForAuthor(dataCourse.Course.Title),
			Body:           utils.TemplateBodyPurchaseCourseForAuthor(dataCourse.Course.Title, dataUser.User.Name),
		})

		notificationTokenAuthor, err := c.notificationTokenRepository.GetNotificationTokenByUserID(ctx, int(dataCourse.Course.UserID))

		if err != nil {
			errorTransaction = err
			return err
		}

		pushMessages = append(pushMessages, messaging.Message{
			Notification: &messaging.Notification{
				Title: utils.TemplateTitlePurchaseCourseForAuthor(dataCourse.Course.Title),
				Body:  utils.TemplateBodyPurchaseCourseForAuthor(dataCourse.Course.Title, dataUser.User.Name),
			},
			Token: notificationTokenAuthor.Token,
		})

		// Add notification for student
		createNotificationReceives = append(createNotificationReceives, &models.NotificationReceive{
			NotificationID: createNotification.ID,
			ReceiveID:      command.IdUser,
			Title:          utils.TemplateTitlePurchaseCourseForStudent(),
			Type:           constants.NOTIFICATION_FOR_STUDENT,
			Body:           utils.TemplateBodyPurchaseCourseForStudent(dataCourse.Course.Title),
		})

		notificationTokenStudent, err := c.notificationTokenRepository.GetNotificationTokenByUserID(ctx, command.IdUser)

		if err != nil {
			errorTransaction = err
			return err
		}
		dataSend := make(map[string]string)
		dataSend["cart_item_id"] = fmt.Sprintf("%d", command.IDCartItem)
		dataSend["type"] = constants.NOTIFICATION_PURCHASE_COURSE_STUDENT
		
		pushMessages = append(pushMessages, messaging.Message{
			Notification: &messaging.Notification{
				Title: utils.TemplateTitlePurchaseCourseForStudent(),
				Body:  utils.TemplateBodyPurchaseCourseForStudent(dataCourse.Course.Title),
			},
			Data: dataSend,
			Token: notificationTokenStudent.Token,
		})

		err = c.notificationReceiveRepository.CreateAllNotificationReceiveWithTransaction(ctx, tx, createNotificationReceives)
		c.log.Debug("Err[CreateAllNotificationReceiveWithTransaction]: ", err)
		if err != nil {
			return err
			// createNotificationTx.Rollback()
			// createNotificationSenderTx.Rollback()
			// createNotificationReceivesTx.Rollback()
		}
		// createNotificationTx.Commit()
		// createNotificationReceivesTx.Commit()
		return nil
	})

	if errorTransaction != nil {
		return nil, errorTransaction
	}
	// Emit Notification
	for _, message := range pushMessages {
		c.log.Info("Send Message: ", message)
		bodyPush, err := json.Marshal(message)

		if err != nil {
			return nil, err
		}

		c.rabbitmqBuilder.AddProducerOnQueue(&producer.RabbitMQProducerOnQueue{
			QueueName: constants.PUSH_NOTIFICATION,
			Data:      bodyPush,
		})
	}
	c.log.Debug("Done d: ")

	return &dtos.NotificationPurchaseCourseResponseDto{
		Message: "Notification purchase course successfully!",
	}, nil
}
