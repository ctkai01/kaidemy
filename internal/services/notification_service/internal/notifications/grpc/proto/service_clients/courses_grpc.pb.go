// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.3.0
// - protoc             v3.6.1
// source: courses.proto

package notifications_service

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

const (
	CoursesService_GetCourseByID_FullMethodName       = "/courses_service.CoursesService/GetCourseByID"
	CoursesService_GetCourseDetailByID_FullMethodName = "/courses_service.CoursesService/GetCourseDetailByID"
)

// CoursesServiceClient is the client API for CoursesService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type CoursesServiceClient interface {
	GetCourseByID(ctx context.Context, in *GetCourseByIdReq, opts ...grpc.CallOption) (*GetCourseByIdRes, error)
	GetCourseDetailByID(ctx context.Context, in *GetCourseDetailByIdReq, opts ...grpc.CallOption) (*GetCourseDetailByIdRes, error)
}

type coursesServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewCoursesServiceClient(cc grpc.ClientConnInterface) CoursesServiceClient {
	return &coursesServiceClient{cc}
}

func (c *coursesServiceClient) GetCourseByID(ctx context.Context, in *GetCourseByIdReq, opts ...grpc.CallOption) (*GetCourseByIdRes, error) {
	out := new(GetCourseByIdRes)
	err := c.cc.Invoke(ctx, CoursesService_GetCourseByID_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *coursesServiceClient) GetCourseDetailByID(ctx context.Context, in *GetCourseDetailByIdReq, opts ...grpc.CallOption) (*GetCourseDetailByIdRes, error) {
	out := new(GetCourseDetailByIdRes)
	err := c.cc.Invoke(ctx, CoursesService_GetCourseDetailByID_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// CoursesServiceServer is the server API for CoursesService service.
// All implementations should embed UnimplementedCoursesServiceServer
// for forward compatibility
type CoursesServiceServer interface {
	GetCourseByID(context.Context, *GetCourseByIdReq) (*GetCourseByIdRes, error)
	GetCourseDetailByID(context.Context, *GetCourseDetailByIdReq) (*GetCourseDetailByIdRes, error)
}

// UnimplementedCoursesServiceServer should be embedded to have forward compatible implementations.
type UnimplementedCoursesServiceServer struct {
}

func (UnimplementedCoursesServiceServer) GetCourseByID(context.Context, *GetCourseByIdReq) (*GetCourseByIdRes, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetCourseByID not implemented")
}
func (UnimplementedCoursesServiceServer) GetCourseDetailByID(context.Context, *GetCourseDetailByIdReq) (*GetCourseDetailByIdRes, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetCourseDetailByID not implemented")
}

// UnsafeCoursesServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to CoursesServiceServer will
// result in compilation errors.
type UnsafeCoursesServiceServer interface {
	mustEmbedUnimplementedCoursesServiceServer()
}

func RegisterCoursesServiceServer(s grpc.ServiceRegistrar, srv CoursesServiceServer) {
	s.RegisterService(&CoursesService_ServiceDesc, srv)
}

func _CoursesService_GetCourseByID_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetCourseByIdReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CoursesServiceServer).GetCourseByID(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: CoursesService_GetCourseByID_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CoursesServiceServer).GetCourseByID(ctx, req.(*GetCourseByIdReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _CoursesService_GetCourseDetailByID_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetCourseDetailByIdReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CoursesServiceServer).GetCourseDetailByID(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: CoursesService_GetCourseDetailByID_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CoursesServiceServer).GetCourseDetailByID(ctx, req.(*GetCourseDetailByIdReq))
	}
	return interceptor(ctx, in, info, handler)
}

// CoursesService_ServiceDesc is the grpc.ServiceDesc for CoursesService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var CoursesService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "courses_service.CoursesService",
	HandlerType: (*CoursesServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetCourseByID",
			Handler:    _CoursesService_GetCourseByID_Handler,
		},
		{
			MethodName: "GetCourseDetailByID",
			Handler:    _CoursesService_GetCourseDetailByID_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "courses.proto",
}
