package grpc

import (
	"context"
	"fmt"
	// "fmt"

	// productsService
	// "emperror.dev/errors"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/mapper"

	notificationsError "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/data/errors"

	notificationPurchaseCourseCommand "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/features/notification_purchase_course/commands"
	notificationPurchaseCourseDto "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/features/notification_purchase_course/dtos"

	notificationsService "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/grpc/proto/service_clients"
)

type NotificationGrpcServiceServer struct {
	logger logger.Logger
}

func NewNotificationGrpcService(
	logger logger.Logger,

) *NotificationGrpcServiceServer {

	return &NotificationGrpcServiceServer{
		logger,
	}
}

func (s *NotificationGrpcServiceServer) NotificationPurchaseCourse(ctx context.Context, req *notificationsService.NotificationPurchaseCourseReq) (*notificationsService.NotificationPurchaseCourseRes, error) {
	command, errValidate := notificationPurchaseCourseCommand.NewNotificationPurchaseCourse(
		int(req.UserID),
		int(req.CourseID),
		int(req.CartItemID),
	)
	if errValidate != nil && errValidate.ErrorType != nil {

		validationErr := customErrors.NewValidationErrorWrap(
			errValidate.ErrorType,
			"[notificationPurchaseCourse_handler.StructCtx] command validation failed",
		)
		s.logger.Errorf(
			fmt.Sprintf("[notificationPurchaseCourse_handler.StructCtx] err: {%v}", validationErr),
		)

		return nil, errValidate.ErrorType
	}

	result, err := mediatr.Send[*notificationPurchaseCourseCommand.NotificationPurchaseCourse, *notificationPurchaseCourseDto.NotificationPurchaseCourseResponseDto](
		ctx,
		command,
	)

	if err != nil {
		if err.Error() == notificationsError.WrapError(customErrors.ErrCourseNotFound).Error() {
			s.logger.Error(
				fmt.Sprintf(
					"[NotificationGrpcServiceServer_NotificationPurchaseCourse.Send]: err: %v",
					err,
				),
			)
			return nil, customErrors.ErrCourseNotFound
		}
	}

	return &notificationsService.NotificationPurchaseCourseRes{
		Message: result.Message,
	}, nil
}