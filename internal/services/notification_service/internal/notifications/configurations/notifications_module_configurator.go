package configurations

import (
	"firebase.google.com/go/messaging"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/configurations/mappings"
	"gorm.io/gorm"

	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/configurations/mediator"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/data"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp/contracts"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/configurations"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	logger2 "gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	notificationservice "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/grpc/proto/service_clients"

	googleGrpc "google.golang.org/grpc"

	grpcServer "gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/otel/tracing"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/configurations/mediator"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/contracts/data"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/grpc"
)

type NotificationsModuleConfigurator struct {
	contracts.Application
}

func NewNotificationsModuleConfigurator(
	app contracts.Application,
) *NotificationsModuleConfigurator {
	return &NotificationsModuleConfigurator{
		Application: app,
	}
}

func (c *NotificationsModuleConfigurator) ConfigureNotificationsModule() {
	c.ResolveFunc(
		func(logger logger2.Logger, 
			entityTypeRepository data.EntityTypeRepository,
			notificationRepository data.NotificationRepository,
			// notificationSenderRepository data.NotificationSenderRepository,
			notificationReceiveRepository data.NotificationReceiveRepository,
			notificationTokenRepository data.NotificationTokenRepository,
			rabbitmqBuilder configurations.RabbitMQConfigurationBuilder,
			grpcClient grpcServer.GrpcClient,
			db *gorm.DB,
			firebaseMessaging *messaging.Client,

		) error {
			// Config Notifications Mediators
			err := mediator.ConfigNotificationMediator(logger, entityTypeRepository,
				notificationRepository,
				// notificationSenderRepository,
				notificationReceiveRepository,
				notificationTokenRepository,
				rabbitmqBuilder,
				grpcClient,
				db,
				firebaseMessaging,
			)
			if err != nil {
				return err
			}

			// Config Notifications Mappings
			err = mappings.ConfigureNotificationsMappings()
			if err != nil {
				return err
			}
			return nil
		},
	)
}

func (c *NotificationsModuleConfigurator) MapNotificationsEndpoints() {
	// Config Course Http Endpoints
	c.ResolveFuncWithParamTag(func(endpoints []route.Endpoint) {
		for _, endpoint := range endpoints {
			endpoint.MapEndpoint()
		}
	}, `group:"notifications-routes"`,
	)

	c.ResolveFunc(
		func(commonGrpcServer grpcServer.GrpcServer, grpcService *grpc.NotificationGrpcServiceServer) error {
			commonGrpcServer.GrpcServiceBuilder().RegisterRoutes(func(server *googleGrpc.Server) {
				notificationservice.RegisterNotificationsServiceServer(server, grpcService)
			})

			return nil
		},
	)
}
