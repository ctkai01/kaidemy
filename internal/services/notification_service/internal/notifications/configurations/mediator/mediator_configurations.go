package mediator

import (
	// "emperror.dev/errors"
	// "github.com/mehdihadeli/go-mediatr"

	"firebase.google.com/go/messaging"
	"github.com/mehdihadeli/go-mediatr"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/contracts/data"
	"gorm.io/gorm"

	// forgotPasswordCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/forgot_password/commands"
	// forgotPasswordDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/forgot_password/dtos"
	// loginCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login/commands"
	// loginDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login/dtos"
	// loginGoogleCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login_by_google/commands"
	// loginGoogleDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login_by_google/dtos"
	// loginGoogleQuery "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login_by_google/queries"
	// registerCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/register/commands"
	// registerDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/register/dtos"
	// requestLoginGoogleCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/commands"
	// requestLoginGoogleDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	// resetPasswordCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/reset_password/commands"
	// resetPasswordDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/reset_password/dtos"

	// changePasswordCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/change_password/commands"
	// changePasswordDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/change_password/dtos"

	notificationPurchaseCourseCommand "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/features/notification_purchase_course/commands"
	notificationPurchaseCourseDtos "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/features/notification_purchase_course/dtos"

	registerNotificationTokenCommand "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/features/register_notification_token/commands"
	registerNotificationTokenDtos "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/features/register_notification_token/dtos"

	pushNotificationCommand "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/features/push_notification/commands"
	pushNotificationDtos "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/features/push_notification/dtos"

	getNotificationQueries "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/features/get_notifications/queries"
	getNotificationDtos "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/features/get_notifications/dtos"
	
	readAllNotificationCommand "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/features/read_all_notification/commands"
	readAllNotificationDtos "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/features/read_all_notification/dtos"
	
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/configurations"
)

// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"

func ConfigNotificationMediator(
	logger logger.Logger,
	entityTypeRepository data.EntityTypeRepository,
	notificationRepository data.NotificationRepository,
	// notificationSenderRepository data.NotificationSenderRepository,
	notificationReceiveRepository data.NotificationReceiveRepository,
	notificationTokenRepository data.NotificationTokenRepository,
	rabbitmqBuilder configurations.RabbitMQConfigurationBuilder,
	grpcClient grpc.GrpcClient,
	db *gorm.DB,
	firebaseMessaging *messaging.Client,
) error {
	err := mediatr.RegisterRequestHandler[*notificationPurchaseCourseCommand.NotificationPurchaseCourse, *notificationPurchaseCourseDtos.NotificationPurchaseCourseResponseDto](
		notificationPurchaseCourseCommand.NewNotificationPurchaseCourseHandler(logger, notificationRepository, notificationReceiveRepository, notificationTokenRepository, entityTypeRepository, grpcClient, db, firebaseMessaging, rabbitmqBuilder),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*registerNotificationTokenCommand.RegisterNotificationToken, *registerNotificationTokenDtos.RegisterNotificationTokenResponseDto](
		registerNotificationTokenCommand.NewRegisterNotificationTokenHandler(logger, notificationTokenRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*pushNotificationCommand.PushNotification, *pushNotificationDtos.PushNotificationResponseDto](
		pushNotificationCommand.NewPushNotificationHandler(logger, firebaseMessaging),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getNotificationQueries.GetNotificationToken, *getNotificationDtos.GetNotificationResponseDto](
		getNotificationQueries.NewGetNotificationHandler(logger, notificationTokenRepository, notificationReceiveRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*readAllNotificationCommand.ReadAllNotification, *readAllNotificationDtos.ReadAllNotificationResponseDto](
		readAllNotificationCommand.NewReadAllNotificationHandler(logger, notificationReceiveRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	return nil
}
