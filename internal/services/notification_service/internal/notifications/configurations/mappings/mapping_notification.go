package mappings

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/mapper"
	// // "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/dto"
	// coursesService "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/grpc/proto/service_clients"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/models"
)

func ConfigureNotificationsMappings() error {
	err := mapper.CreateMap[*models.NotificationReceive, *models.NotificationReceive]()
	if err != nil {
		return err
	}
	return nil
}
