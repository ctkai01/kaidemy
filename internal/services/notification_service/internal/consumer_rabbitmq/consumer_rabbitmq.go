package consumer_rabbitmq

import (
	"context"
	"encoding/json"
	"fmt"
	"log"

	"firebase.google.com/go/messaging"
	"github.com/mehdihadeli/go-mediatr"
	"github.com/rabbitmq/amqp091-go"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	notificationPurchaseCourseCommand "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/features/notification_purchase_course/commands"
	notificationPurchaseCourseDto "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/features/notification_purchase_course/dtos"

	pushNotificationCommand "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/features/push_notification/commands"
	pushNotificationDto "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/features/push_notification/dtos"
)

func NotificationPurchaseCourseHandler(d amqp091.Delivery) {
	log.Printf("Received a message : %s", d.Body)
	
	var sendNotificationPurchaseCourse constants.SendNotificationPurchaseCourse
	err := json.Unmarshal(d.Body, &sendNotificationPurchaseCourse)

	if err != nil {
		log.Println("[dataSendNotificationPurchaseCourse]: ", err)
		d.Reject(true)
	}
	fmt.Println("dataSendNotificationPurchaseCourse : ", sendNotificationPurchaseCourse)

	command, _ := notificationPurchaseCourseCommand.NewNotificationPurchaseCourse(
		int(sendNotificationPurchaseCourse.IdUser),
		int(sendNotificationPurchaseCourse.IdProduct),
		sendNotificationPurchaseCourse.IDCartItem,
	)

	_, err = mediatr.Send[*notificationPurchaseCourseCommand.NotificationPurchaseCourse, *notificationPurchaseCourseDto.NotificationPurchaseCourseResponseDto](
		context.Background(),
		command,
	)
	fmt.Println("Err 1: ", err)
	if err != nil {
		d.Reject(true)
	} else {
		d.Ack(false)
	}
}

func PushNotificationPurchaseCourseHandler(d amqp091.Delivery) {
	log.Printf("Received a message : %s", d.Body)
	
	var message messaging.Message
	err := json.Unmarshal(d.Body, &message)

	if err != nil {
		log.Println("[dataSendNotificationPurchaseCourse]: ", err)
		d.Reject(true)
	}
	
	fmt.Println("Message : ", message)

	command, _ := pushNotificationCommand.NewPushNotification(
		&message,
	)

	_, err = mediatr.Send[*pushNotificationCommand.PushNotification, *pushNotificationDto.PushNotificationResponseDto](
		context.Background(),
		command,
	)
	fmt.Println("Err: ", err)
	if err != nil {
		d.Reject(true)
	} else {
		d.Ack(false)
	}
}