package utils

import "strings"

// import (
// 	"fmt"
// 	"math/rand"
// 	"strconv"
// 	"time"

// 	"github.com/dgrijalva/jwt-go"
// 	constantsCommon "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
// 	"golang.org/x/crypto/bcrypt"
// )

func TemplateTitlePurchaseCourseForAuthor(nameCourse string) string {
	template := "Khóa học {Name} đã được mua"
	message := strings.Replace(template, "{Name}", nameCourse, -1)

	return message
}

func TemplateBodyPurchaseCourseForAuthor(nameCourse string, buyerName string) string {
	template := "Khóa học {Name} đã được mua bởi {Buyer}"
	message := strings.Replace(template, "{Name}", nameCourse, -1)
	message = strings.Replace(message, "{Buyer}", buyerName, -1)
	return message
}

func TemplateTitlePurchaseCourseForStudent() string {
	return "Mua khóa học thành công"
}

func TemplateBodyPurchaseCourseForStudent(nameCourse string) string {
	template := "Bạn đã mua khóa học {Name}"
	message := strings.Replace(template, "{Name}", nameCourse, -1)

	return message
}