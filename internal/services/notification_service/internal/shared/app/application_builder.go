package app

import (
	"os"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/config/environment"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp/contracts"
)

type NotificationsApplicationBuilder struct {
	contracts.ApplicationBuilder
}

func NewNotificationApplicationBuilder() *NotificationsApplicationBuilder {
	var envs []environment.Environment 

	if os.Getenv("ENV") == string(environment.Production) {
		envs = append(envs, environment.Production)
	} else {
		envs = append(envs, environment.Development)
	}
	builder := &NotificationsApplicationBuilder{fxapp.NewApplicationBuilder(envs...)}

	return builder
}

func (a *NotificationsApplicationBuilder) Build() *NotificationsApplication {

	return NewNotificationsApplication(
		a.GetProvides(),
		a.GetDecorates(),
		a.Options(),
		a.Logger(),
		a.Environment(),
	)
}
