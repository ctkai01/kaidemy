package app

import (
	"go.uber.org/fx"

	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/shared/configurations/notifications"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/config/environment"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
)

type NotificationsApplication struct {
	*notifications.NotificationsServiceConfigurator
	// *notifications.NotificationsServiceConfigurator
}

func NewNotificationsApplication(
	providers []interface{},
	decorates []interface{},
	options []fx.Option,
	logger logger.Logger,
	environment environment.Environment,
) *NotificationsApplication {
	app := fxapp.NewApplication(providers, decorates, options, logger, environment)
	return &NotificationsApplication{
		NotificationsServiceConfigurator: notifications.NewNotificationsServiceConfigurator(app),
	}
}
