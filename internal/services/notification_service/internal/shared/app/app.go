package app

import (
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/shared/configurations/notifications"
)

type App struct{}

func NewApp() *App {
	return &App{}
}

func (a *App) Run() {
	// configure dependencies
	appBuilder := NewNotificationApplicationBuilder()
	appBuilder.ProvideModule(notifications.NotificationsServiceModule)

	app := appBuilder.Build()
	// // configure application
	app.ConfigureNotifications()
	app.MapNotificationsEndpoints()

	app.Logger().Info("Starting notification service application")
	app.Run()
}
