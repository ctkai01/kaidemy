package notifications

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/configurations"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/consumer"
	appconfig "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/config"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/consumer_rabbitmq"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/shared/configurations/notifications/infrastructure"
	"go.uber.org/fx"
	// internal/shared/configurations/users/infrastructure
)

var NotificationsServiceModule = fx.Module(
	"notificationsfx",
	// Shared Modules
	appconfig.Module,
	infrastructure.Module,

	// Features Modules
	notifications.Module,

	// Other provides
	// fx.Provide(provideCatalogsMetrics),
	fx.Invoke(func(rabbitmqBuilder configurations.RabbitMQConfigurationBuilder) {
		rabbitmqBuilder.AddConsumerToQueue(&consumer.RabbitMQConsumerToQueue{
			QueueName:   constants.SEND_NOTIFICATION_PURCHASE_COURSE,
			HandlerFunc: consumer_rabbitmq.NotificationPurchaseCourseHandler,
		})

		rabbitmqBuilder.AddConsumerToQueue(&consumer.RabbitMQConsumerToQueue{
			QueueName:   constants.PUSH_NOTIFICATION,
			HandlerFunc: consumer_rabbitmq.PushNotificationPurchaseCourseHandler,
		})

	}),
)
