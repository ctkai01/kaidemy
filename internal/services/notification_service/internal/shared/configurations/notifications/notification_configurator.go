package notifications

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	"gorm.io/gorm"

	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/config"
	// "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/shared/configurations/infrastructure"
	// "gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/courses/configurations"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/configurations"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/shared/configurations/notifications/infrastructure"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp/contracts"
	customEcho "gitlab.com/ctkai01/kaidemy/internal/pkg/http/custom_echo"
)

type NotificationsServiceConfigurator struct {
	contracts.Application
	infrastructureConfigurator *infrastructure.InfrastructureConfigurator
	notificationsModuleConfigurator  *configurations.NotificationsModuleConfigurator
}

func NewNotificationsServiceConfigurator(app contracts.Application) *NotificationsServiceConfigurator {
	infraConfigurator := infrastructure.NewInfrastructureConfigurator(app)
	notificationModuleConfigurator := configurations.NewNotificationsModuleConfigurator(app)

	return &NotificationsServiceConfigurator{
		Application:                app,
		infrastructureConfigurator: infraConfigurator,
		notificationsModuleConfigurator:  notificationModuleConfigurator,
	}
}

func (ic *NotificationsServiceConfigurator) ConfigureNotifications() {
	// Shared
	// Infrastructure
	ic.infrastructureConfigurator.ConfigInfrastructures()
	// Shared
	// Catalogs configurations
	ic.ResolveFunc(func(gorm *gorm.DB) error {
		err := ic.migrateNotifications(gorm)
		if err != nil {
			return err
		}
		return nil
	})
	// Modules
	// Product module
	ic.notificationsModuleConfigurator.ConfigureNotificationsModule()
}

func (ic *NotificationsServiceConfigurator) MapNotificationsEndpoints() {
	// Shared
	ic.ResolveFunc(
		func(notificationsServer customEcho.EchoHttpServer, cfg *config.Config) error {
			notificationsServer.SetupDefaultMiddlewares()

			// Config catalogs root endpoint
			notificationsServer.RouteBuilder().
				RegisterRoutes(func(e *echo.Echo) {
					e.GET("", func(ec echo.Context) error {
						return ec.String(
							http.StatusOK,
							fmt.Sprintf(
								"%s is running...",
								cfg.AppOptions.GetMicroserviceNameUpper(),
							),
						)
					})
				})

			// Config catalogs swagger
			// ic.configSwagger(catalogsServer.RouteBuilder())

			return nil
		},
	)

	// Modules
	// Products CatalogsServiceModule endpoints
	ic.notificationsModuleConfigurator.MapNotificationsEndpoints()
}
