package notifications

import (
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/notifications/models"
	"gorm.io/gorm"
)

func (ic *NotificationsServiceConfigurator) migrateNotifications(gorm *gorm.DB) error {
	// or we could use `gorm.Migrate()`
	err := gorm.AutoMigrate(&models.EntityType{}, &models.Notification{}, &models.NotificationReceive{}, &models.NotificationToken{})
	// err := gorm.AutoMigrate(&models.EntityType{}, &models.Notification{}, &models.NotificationReceive{}, &models.NotificationSender{}, &models.NotificationToken{})
	
	if err != nil {
		return err
	}

	return nil
}
