package main

import (
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/ctkai01/kaidemy/internal/services/notificationservice/internal/shared/app"
)

var rootCmd = &cobra.Command{
	Use:              "notification-microservices",
	Short:            "notification-microservices based on vertical slice architecture",
	Long:             `This is a command runner or cli for api architecture in golang.`,
	TraverseChildren: true,
	Run: func(cmd *cobra.Command, args []string) {
		app.NewApp().Run()
	},
}

func main() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}
