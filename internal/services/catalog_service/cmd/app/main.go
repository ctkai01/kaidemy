package main

import (
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/shared/app"
)

var rootCmd = &cobra.Command{
	Use:              "catalog-microservices",
	Short:            "catalog-microservices based on vertical slice architecture",
	Long:             `This is a command runner or cli for api architecture in golang.`,
	TraverseChildren: true,
	Run: func(cmd *cobra.Command, args []string) {
		app.NewApp().Run()
	},
}

func main() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}
