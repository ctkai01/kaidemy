package catalogs

import (
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/models"
	"gorm.io/gorm"
)

func (ic *CatalogsServiceConfigurator) migrateCatalogs(gorm *gorm.DB) error {
	// or we could use `gorm.Migrate()`
	err := gorm.AutoMigrate(&models.Category{}, &models.Level{}, &models.Price{}, &models.Language{}, &models.IssueType{})
	if err != nil {
		return err
	}

	return nil
}
