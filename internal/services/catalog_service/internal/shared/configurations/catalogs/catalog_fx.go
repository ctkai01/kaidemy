package catalogs

import (
	appconfig "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/config"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/shared/configurations/catalogs/infrastructure"
	"go.uber.org/fx"
	// internal/shared/configurations/users/infrastructure
)

var CatalogsServiceModule = fx.Module(
	"catalogsfx",
	// Shared Modules
	appconfig.Module,
	infrastructure.Module,

	// Features Modules
	catalogs.Module,

	// Other provides
	// fx.Provide(provideCatalogsMetrics),
)
