package catalogs

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	"gorm.io/gorm"

	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/config"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/shared/configurations/infrastructure"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/configurations"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/configurations"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/shared/configurations/catalogs/infrastructure"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp/contracts"
	customEcho "gitlab.com/ctkai01/kaidemy/internal/pkg/http/custom_echo"
)

type CatalogsServiceConfigurator struct {
	contracts.Application
	infrastructureConfigurator *infrastructure.InfrastructureConfigurator
	catalogsModuleConfigurator *configurations.CatalogsModuleConfigurator
}

func NewCatalogsServiceConfigurator(app contracts.Application) *CatalogsServiceConfigurator {
	infraConfigurator := infrastructure.NewInfrastructureConfigurator(app)
	catalogModuleConfigurator := configurations.NewCatalogsModuleConfigurator(app)

	return &CatalogsServiceConfigurator{
		Application:                app,
		infrastructureConfigurator: infraConfigurator,
		catalogsModuleConfigurator: catalogModuleConfigurator,
	}
}

func (ic *CatalogsServiceConfigurator) ConfigureCatalogs() {
	// Shared
	// Infrastructure
	ic.infrastructureConfigurator.ConfigInfrastructures()
	// Shared
	// Catalogs configurations
	ic.ResolveFunc(func(gorm *gorm.DB) error {
		err := ic.migrateCatalogs(gorm)
		if err != nil {
			return err
		}
		return nil
	})
	// Modules
	// Product module
	ic.catalogsModuleConfigurator.ConfigureCatalogsModule()
}

func (ic *CatalogsServiceConfigurator) MapCatalogsEndpoints() {
	// Shared
	ic.ResolveFunc(
		func(coursesServer customEcho.EchoHttpServer, cfg *config.Config) error {
			coursesServer.SetupDefaultMiddlewares()

			// Config catalogs root endpoint
			coursesServer.RouteBuilder().
				RegisterRoutes(func(e *echo.Echo) {
					e.GET("", func(ec echo.Context) error {
						return ec.String(
							http.StatusOK,
							fmt.Sprintf(
								"%s is running...",
								cfg.AppOptions.GetMicroserviceNameUpper(),
							),
						)
					})
				})

			// Config catalogs swagger
			// ic.configSwagger(catalogsServer.RouteBuilder())

			return nil
		},
	)

	// Modules
	// Products CatalogsServiceModule endpoints
	ic.catalogsModuleConfigurator.MapCatalogsEndpoints()
}
