package app

import (
	"os"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/config/environment"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp/contracts"
)

type CatalogsApplicationBuilder struct {
	contracts.ApplicationBuilder
}

func NewCatalogApplicationBuilder() *CatalogsApplicationBuilder {
	
	var envs []environment.Environment 

	if os.Getenv("ENV") == string(environment.Production) {
		envs = append(envs, environment.Production)
	} else {
		envs = append(envs, environment.Development)
	}
	builder := &CatalogsApplicationBuilder{fxapp.NewApplicationBuilder(envs...)}

	return builder
}

func (a *CatalogsApplicationBuilder) Build() *CatalogsApplication {

	return NewCatalogsApplication(
		a.GetProvides(),
		a.GetDecorates(),
		a.Options(),
		a.Logger(),
		a.Environment(),
	)
}
