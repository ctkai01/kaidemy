package app

import (
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/shared/configurations/catalogs"
)

type App struct{}

func NewApp() *App {
	return &App{}
}

func (a *App) Run() {
	// configure dependencies
	appBuilder := NewCatalogApplicationBuilder()
	appBuilder.ProvideModule(catalogs.CatalogsServiceModule)

	app := appBuilder.Build()
	// // configure application
	app.ConfigureCatalogs()
	app.MapCatalogsEndpoints()

	app.Logger().Info("Starting catalogs service application")
	app.Run()
}
