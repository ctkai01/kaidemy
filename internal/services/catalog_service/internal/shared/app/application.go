package app

import (
	"go.uber.org/fx"

	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/shared/configurations/catalogs"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/config/environment"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
)

type CatalogsApplication struct {
	*catalogs.CatalogsServiceConfigurator
}

func NewCatalogsApplication(
	providers []interface{},
	decorates []interface{},
	options []fx.Option,
	logger logger.Logger,
	environment environment.Environment,
) *CatalogsApplication {
	app := fxapp.NewApplication(providers, decorates, options, logger, environment)
	return &CatalogsApplication{
		CatalogsServiceConfigurator: catalogs.NewCatalogsServiceConfigurator(app),
	}
}
