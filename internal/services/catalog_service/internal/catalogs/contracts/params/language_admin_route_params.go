package params

import (
	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"go.uber.org/fx"

	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/data"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
)

type LanguageAdminRouteParams struct {
	fx.In
	Logger             logger.Logger
	LanguagesGroup     *echo.Group `name:"languages-admin-echo-group"`
	Validator          *validator.Validate
	LanguageRepository data.LanguageRepository
}
