package params

import (
	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"go.uber.org/fx"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
)

type IssueTypeAdminRouteParams struct {
	fx.In
	Logger          logger.Logger
	IssueTypesGroup     *echo.Group `name:"issue-types-admin-echo-group"`
	Validator       *validator.Validate
}
