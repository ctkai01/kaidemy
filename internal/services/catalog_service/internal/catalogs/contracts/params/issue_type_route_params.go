package params

import (
	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"go.uber.org/fx"


	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
)

type IssueTypesRouteParams struct {
	fx.In
	Logger          logger.Logger
	IssueTypesGroup     *echo.Group `name:"issue-types-echo-group"`
	Validator       *validator.Validate
}
