package params

import (
	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"go.uber.org/fx"

	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/data"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
)

type LevelAdminRouteParams struct {
	fx.In
	Logger          logger.Logger
	LevelsGroup     *echo.Group `name:"levels-admin-echo-group"`
	Validator       *validator.Validate
	LevelRepository data.LevelRepository
}
