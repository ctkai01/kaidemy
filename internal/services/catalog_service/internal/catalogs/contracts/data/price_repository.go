package data

import (
	"context"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	// uuid "github.com/satori/go.uuid"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/models"
)

type PriceRepository interface {
	// GetAllProducts(
	// 	ctx context.Context,
	// 	listQuery *utils.ListQuery,
	// ) (*utils.ListResult[*models.User], error)
	// SearchProducts(
	// 	ctx context.Context,
	// 	searchText string,
	// 	listQuery *utils.ListQuery,
	// ) (*utils.ListResult[*models.User], error)
	// GetAllCategories(
	// 	ctx context.Context,
	// 	listQuery *utils.ListQuery,
	// ) (*utils.ListResult[*models.Category], error)
	// GetCategoryByIDRelation(ctx context.Context, relations []string, id int) (*models.Category, error)
	// GetUserByEmail(ctx context.Context, email string) (*models.Course, error)
	// GetUserByEmailToken(ctx context.Context, emailToken string) (*models.Course, error)
	GetAllPrices(
		ctx context.Context,
		listQuery *utils.ListQuery,
	) (*utils.ListResult[*models.Price], error)
	CreatePrice(ctx context.Context, price *models.Price) error
	GetPriceByID(ctx context.Context, id int) (*models.Price, error)

	UpdatePrice(ctx context.Context, price *models.Price) error
	DeletePriceByID(ctx context.Context, id int) error
	// GetChildrenCategoryByID(ctx context.Context, id int) ([]models.Category, error)
	// UpdateEmailToken(ctx context.Context, id int, mailToken string) error
	// UpdateProduct(ctx context.Context, product *models.User) (*models.User, error)
}
