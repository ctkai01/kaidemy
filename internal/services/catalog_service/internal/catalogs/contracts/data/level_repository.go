package data

import (
	"context"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	// uuid "github.com/satori/go.uuid"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/models"
)

type LevelRepository interface {
	// GetAllProducts(
	// 	ctx context.Context,
	// 	listQuery *utils.ListQuery,
	// ) (*utils.ListResult[*models.User], error)
	// SearchProducts(
	// 	ctx context.Context,
	// 	searchText string,
	// 	listQuery *utils.ListQuery,
	// ) (*utils.ListResult[*models.User], error)
	// GetAllCategories(
	// 	ctx context.Context,
	// 	listQuery *utils.ListQuery,
	// ) (*utils.ListResult[*models.Category], error)
	// GetCategoryByIDRelation(ctx context.Context, relations []string, id int) (*models.Category, error)
	// GetUserByEmail(ctx context.Context, email string) (*models.Course, error)
	// GetUserByEmailToken(ctx context.Context, emailToken string) (*models.Course, error)
	GetAllLevels(
		ctx context.Context,
		listQuery *utils.ListQuery,
	) (*utils.ListResult[*models.Level], error)
	CreateLevel(ctx context.Context, level *models.Level) error
	GetLevelByID(ctx context.Context, id int) (*models.Level, error)

	UpdateLevel(ctx context.Context, level *models.Level) error
	DeleteLevelByID(ctx context.Context, id int) error
	// GetChildrenCategoryByID(ctx context.Context, id int) ([]models.Category, error)
	// UpdateEmailToken(ctx context.Context, id int, mailToken string) error
	// UpdateProduct(ctx context.Context, product *models.User) (*models.User, error)
}
