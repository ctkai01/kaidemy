package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type CreateLevel struct {
	IdUser int
	Name   string `validate:"required,gte=1,lte=60"`
}

func NewCreateLevel(idUser int, name string) (*CreateLevel, *validator.ValidateError) {
	command := &CreateLevel{idUser, name}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
