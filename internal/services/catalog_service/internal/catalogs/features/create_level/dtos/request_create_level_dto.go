package dtos

//https://echo.labstack.com/guide/response/

type CreateLevelRequestDto struct {
	Name  string `form:"name"`
}
