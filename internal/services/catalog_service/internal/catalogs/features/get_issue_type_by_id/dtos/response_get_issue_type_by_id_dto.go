package dtos

import "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/models"

//https://echo.labstack.com/guide/response/

type GetIssueTypeByIdResponseDto struct {
	Message string        `json:"message"`
	IssueType   *models.IssueType `json:"issue_type"`
}
