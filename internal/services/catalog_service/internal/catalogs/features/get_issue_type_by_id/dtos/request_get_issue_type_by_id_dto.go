package dtos

//https://echo.labstack.com/guide/response/

type GetIssueTypeByIdRequestDto struct {
	IssueTypeID int `json:"-" param:"id"`
}
