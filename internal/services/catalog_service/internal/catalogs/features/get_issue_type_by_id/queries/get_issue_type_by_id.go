package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetIssueTypeById struct {
	// IdUser  int
	IdIssueType int `validate:"required,numeric"`
}

func NewGetIssueTypeById(idIssueType int) (*GetIssueTypeById, *validator.ValidateError) {
	query := &GetIssueTypeById{ idIssueType}
	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return query, nil
}
