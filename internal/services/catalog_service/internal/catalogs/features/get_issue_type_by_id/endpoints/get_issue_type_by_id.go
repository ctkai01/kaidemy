package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/params"
	catalogsError "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_issue_type_by_id/dtos"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_issue_type_by_id/queries"
)

type getIssueTypeByIdEndpoint struct {
	params.IssueTypesRouteParams
}

func NewGetIssueTypeByIdEndpoint(
	params params.IssueTypesRouteParams,
) route.Endpoint {
	return &getIssueTypeByIdEndpoint{
		IssueTypesRouteParams: params,
	}
}

func (ep *getIssueTypeByIdEndpoint) MapEndpoint() {
	ep.IssueTypesGroup.GET("/:id", ep.handler())
}

func (ep *getIssueTypeByIdEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.GetIssueTypeByIdRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[getIssueTypeById.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getIssueTypeByIdEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}

		// userId := utils.GetUserId(c)

		command, errValidate := queries.NewGetIssueTypeById(
			// userId,
			request.IssueTypeID,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[getIssueTypeByIdEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getIssueTypeByIdEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*queries.GetIssueTypeById, *dtos.GetIssueTypeByIdResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == catalogsError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[getIssueTypeByIdEndpoint_handler.Send] error in sending delete Category",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[getLevelById.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			if err.Error() == catalogsError.WrapError(customErrors.ErrCategoryNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[getLevelByIdEndpoint_handler.Send] error in sending get category by id",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[getIssueTypeById.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[getIssueTypeByIdEndpoint_handler.Send] error in sending get IssueType by id",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[getIssueTypeById.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
