package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/update_level/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	userservice "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type UpdateLevelHandler struct {
	log             logger.Logger
	levelRepository data.LevelRepository
	grpcClient      grpc.GrpcClient
}

func NewUpdatePriceHandler(
	log logger.Logger,
	levelRepository data.LevelRepository,
	grpcClient grpc.GrpcClient,

) *UpdateLevelHandler {
	return &UpdateLevelHandler{log: log, levelRepository: levelRepository, grpcClient: grpcClient}
}

func (c *UpdateLevelHandler) Handle(ctx context.Context, command *UpdateLevel) (*dtos.UpdateLevelResponseDto, error) {
	conn, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}

	client := userservice.NewUsersServiceClient(conn)
	dataUser, err := client.GetUserByID(context.Background(), &userservice.GetUserByIdReq{
		UserId: int32(command.IdUser),
	})

	if err != nil {
		return nil, err
	}

	if dataUser.User == nil {
		return nil, customErrors.ErrUserNotFound
	}

	levelUpdate, err := c.levelRepository.GetLevelByID(ctx, command.IdLevel)

	if err != nil {
		return nil, err
	}

	if levelUpdate == nil {
		return nil, customErrors.ErrLevelNotFound
	}

	if command.Name != nil {
		levelUpdate.Name = *command.Name
	}

	if err := c.levelRepository.UpdateLevel(ctx, levelUpdate); err != nil {
		return nil, err
	}

	return &dtos.UpdateLevelResponseDto{
		Message: "Update level successfully!",
		Level:   *levelUpdate,
	}, nil
}
