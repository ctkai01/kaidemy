package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type UpdateLevel struct {
	IdUser  int
	IdLevel int     `validate:"required,numeric"`
	Name    *string `validate:"omitempty,gte=1,lte=60"`
}

func NewUpdateLevel(idUser int, idLevel int, name *string) (*UpdateLevel, *validator.ValidateError) {
	command := &UpdateLevel{idUser, idLevel, name}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
