package dtos

//https://echo.labstack.com/guide/response/

type UpdateLevelRequestDto struct {
	LevelID int     `json:"-" param:"id"`
	Name    *string `json:"name" form:"name"`
}
