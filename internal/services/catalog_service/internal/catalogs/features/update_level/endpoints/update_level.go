package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/params"
	catalogsError "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/update_level/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/update_level/dtos"
)

type updateLevelEndpoint struct {
	params.LevelAdminRouteParams
}

func NewUpdateLevelEndpoint(
	params params.LevelAdminRouteParams,
) route.Endpoint {
	return &updateLevelEndpoint{
		LevelAdminRouteParams: params,
	}
}

func (ep *updateLevelEndpoint) MapEndpoint() {
	ep.LevelsGroup.PUT("/:id", ep.handler())
}

func (ep *updateLevelEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.UpdateLevelRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[updateLevel.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[updateLevelEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}

		userId := utils.GetUserId(c)

		command, errValidate := commands.NewUpdateLevel(
			userId,
			request.LevelID,
			request.Name,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[updateLevelEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[updateLevelEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.UpdateLevel, *dtos.UpdateLevelResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == catalogsError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[updateLevelEndpoint_handler.Send] error in sending update Level",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[updateLevel.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			if err.Error() == catalogsError.WrapError(customErrors.ErrLevelNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[updateLevelEndpoint_handler.Send] error in sending create Level",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[updateLevel.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[updateLevelEndpoint_handler.Send] error in sending update level",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[updateLevel.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
