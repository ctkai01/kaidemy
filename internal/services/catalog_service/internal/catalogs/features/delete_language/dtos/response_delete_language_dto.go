package dtos


//https://echo.labstack.com/guide/response/

type DeleteLanguageResponseDto struct {
	Message  string          `json:"message"`
}
