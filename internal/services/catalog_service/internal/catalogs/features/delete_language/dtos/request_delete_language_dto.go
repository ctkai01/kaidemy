package dtos

//https://echo.labstack.com/guide/response/

type DeleteLanguageRequestDto struct {
	LanguageID int `json:"-" param:"id"`
}
