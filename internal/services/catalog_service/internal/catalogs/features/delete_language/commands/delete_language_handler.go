package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// "fmt"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/delete_language/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	userservice "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type DeleteLanguageHandler struct {
	log                logger.Logger
	languageRepository data.LanguageRepository
	grpcClient         grpc.GrpcClient
}

func NewDeleteLanguageHandler(
	log logger.Logger,
	languageRepository data.LanguageRepository,
	grpcClient grpc.GrpcClient,

) *DeleteLanguageHandler {
	return &DeleteLanguageHandler{log: log, languageRepository: languageRepository, grpcClient: grpcClient}
}

func (c *DeleteLanguageHandler) Handle(ctx context.Context, command *DeleteLanguage) (*dtos.DeleteLanguageResponseDto, error) {
	conn, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}

	client := userservice.NewUsersServiceClient(conn)
	dataUser, err := client.GetUserByID(context.Background(), &userservice.GetUserByIdReq{
		UserId: int32(command.IdUser),
	})

	if err != nil {
		return nil, err
	}

	if dataUser.User == nil {
		return nil, customErrors.ErrUserNotFound
	}

	languageDelete, err := c.languageRepository.GetLanguageByID(ctx, command.IdLanguage)

	if err != nil {
		return nil, err
	}

	if languageDelete == nil {
		return nil, customErrors.ErrLanguageNotFound
	}

	if err := c.languageRepository.DeleteLanguageByID(ctx, command.IdLanguage); err != nil {
		return nil, err
	}

	return &dtos.DeleteLanguageResponseDto{
		Message: "Delete language successfully!",
	}, nil
}
