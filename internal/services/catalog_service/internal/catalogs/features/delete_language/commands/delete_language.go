package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type DeleteLanguage struct {
	IdUser     int
	IdLanguage int `validate:"required,numeric"`
}

func NewDeleteLanguage(idUser int, idLanguage int) (*DeleteLanguage, *validator.ValidateError) {
	command := &DeleteLanguage{idUser, idLanguage}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
