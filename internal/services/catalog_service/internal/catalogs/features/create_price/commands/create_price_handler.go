package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/create_price/dtos"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	userservice "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type CreatePriceHandler struct {
	log             logger.Logger
	priceRepository data.PriceRepository
	grpcClient      grpc.GrpcClient
}

func NewCreatePriceHandler(
	log logger.Logger,
	priceRepository data.PriceRepository,
	grpcClient grpc.GrpcClient,

) *CreatePriceHandler {
	return &CreatePriceHandler{log: log, priceRepository: priceRepository, grpcClient: grpcClient}
}

func (c *CreatePriceHandler) Handle(ctx context.Context, command *CreatePrice) (*dtos.CreatePriceResponseDto, error) {
	conn, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}

	client := userservice.NewUsersServiceClient(conn)
	dataUser, err := client.GetUserByID(context.Background(), &userservice.GetUserByIdReq{
		UserId: int32(command.IdUser),
	})

	if err != nil {
		return nil, err
	}

	if dataUser.User == nil {
		return nil, customErrors.ErrUserNotFound
	}

	

	if err != nil {
		return nil, err
	}

	priceCreate := &models.Price{
		Tier:  command.Tier,
		Value: command.Value,
	}

	if err := c.priceRepository.CreatePrice(ctx, priceCreate); err != nil {
		return nil, err
	}

	return &dtos.CreatePriceResponseDto{
		Message: "Create price successfully!",
		Price:   *priceCreate,
	}, nil
}
