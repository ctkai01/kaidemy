package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type CreatePrice struct {
	IdUser int
	Tier   string `validate:"required,gte=1,lte=60"`
	Value  int    `validate:"required,numeric"`
}

func NewCreatePrice(idUser int, tier string, value int) (*CreatePrice, *validator.ValidateError) {
	command := &CreatePrice{idUser, tier, value}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
