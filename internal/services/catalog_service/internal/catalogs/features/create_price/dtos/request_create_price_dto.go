package dtos

//https://echo.labstack.com/guide/response/

type CreatePriceRequestDto struct {
	Tier  string `form:"tier"`
	Value int    `form:"value"`
}
