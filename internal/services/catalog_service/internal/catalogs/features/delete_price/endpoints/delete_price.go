package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/params"
	catalogsError "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/delete_price/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/delete_price/dtos"
)

type deletePriceEndpoint struct {
	params.PricesAdminRouteParams
}

func NewDeletePriceEndpoint(
	params params.PricesAdminRouteParams,
) route.Endpoint {
	return &deletePriceEndpoint{
		PricesAdminRouteParams: params,
	}
}

func (ep *deletePriceEndpoint) MapEndpoint() {
	ep.PricesGroup.DELETE("/:id", ep.handler())
}

func (ep *deletePriceEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.DeletePriceRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[deletePrice.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[deletePriceEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}

		userId := utils.GetUserId(c)

		command, errValidate := commands.NewDeletePrice(
			userId,
			request.PriceID,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[deletePriceEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[deletePriceEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.DeletePrice, *dtos.DeletePriceResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == catalogsError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[deletePriceEndpoint_handler.Send] error in sending delete Price",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[deletePrice.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			if err.Error() == catalogsError.WrapError(customErrors.ErrPriceNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[deletePriceEndpoint_handler.Send] error in sending delete Price",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[deletePrice.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[deletePriceEndpoint_handler.Send] error in sending delete price",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[deletePrice.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
