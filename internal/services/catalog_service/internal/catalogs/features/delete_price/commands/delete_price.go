package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type DeletePrice struct {
	IdUser  int
	IdPrice int `validate:"required,numeric"`
}

func NewDeletePrice(idUser int, idPrice int) (*DeletePrice, *validator.ValidateError) {
	command := &DeletePrice{idUser, idPrice}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
