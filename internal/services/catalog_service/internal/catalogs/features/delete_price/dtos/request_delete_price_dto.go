package dtos

//https://echo.labstack.com/guide/response/

type DeletePriceRequestDto struct {
	PriceID int     `json:"-" param:"id"`
}
