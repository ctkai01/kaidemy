package dtos

import "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/models"

//https://echo.labstack.com/guide/response/

type UpdateCategoryResponseDto struct {
	Message  string          `json:"message"`
	Category models.Category `json:"category"`
}
