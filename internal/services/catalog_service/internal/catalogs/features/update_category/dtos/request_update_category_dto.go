package dtos

//https://echo.labstack.com/guide/response/

type UpdateCategoryRequestDto struct {
	CategoryID int     `json:"-" param:"id"`
	Name       *string `json:"name" form:"name"`
	ParentID   *int    `json:"parent_id" form:"parent_id"`
}
