package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"fmt"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/update_category/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	userservice "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type UpdateCategoryHandler struct {
	log                logger.Logger
	categoryRepository data.CategoryRepository
	grpcClient         grpc.GrpcClient
}

func NewUpdateCategoryHandler(
	log logger.Logger,
	categoryRepository data.CategoryRepository,
	grpcClient grpc.GrpcClient,

) *UpdateCategoryHandler {
	return &UpdateCategoryHandler{log: log, categoryRepository: categoryRepository, grpcClient: grpcClient}
}

func (c *UpdateCategoryHandler) Handle(ctx context.Context, command *UpdateCategory) (*dtos.UpdateCategoryResponseDto, error) {
	conn, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}

	client := userservice.NewUsersServiceClient(conn)
	dataUser, err := client.GetUserByID(context.Background(), &userservice.GetUserByIdReq{
		UserId: int32(command.IdUser),
	})

	if err != nil {
		return nil, err
	}

	if dataUser.User == nil {
		return nil, customErrors.ErrUserNotFound
	}

	if command.ParentId != nil && *(command.ParentId) != 0 {
		category, err := c.categoryRepository.GetCategoryByIDRelation(ctx, nil, *command.ParentId)

		if err != nil {
			return nil, err
		}

		if category == nil {
			return nil, customErrors.ErrParentCategoryNotFound
		}

		if category.ParentID != nil {
			return nil, customErrors.ErrParentCategoryNotHasParent
		}
	}

	categoryUpdate, err := c.categoryRepository.GetCategoryByIDRelation(ctx, nil, command.IdCategory)

	if err != nil {
		return nil, err
	}

	if categoryUpdate == nil {
		return nil, customErrors.ErrCategoryNotFound
	}

	categoryUpdate.Name = *command.Name

	if command.ParentId != nil && *(command.ParentId) == 0 {
		categoryUpdate.ParentID = nil
	} else {
		categoryUpdate.ParentID = command.ParentId
	}

	fmt.Println(categoryUpdate)
	if err := c.categoryRepository.UpdateCategory(ctx, categoryUpdate); err != nil {
		return nil, err
	}

	return &dtos.UpdateCategoryResponseDto{
		Message:  "Update category successfully!",
		Category: *categoryUpdate,
	}, nil
}
