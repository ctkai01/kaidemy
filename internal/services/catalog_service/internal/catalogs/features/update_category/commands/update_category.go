package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type UpdateCategory struct {
	IdUser     int
	IdCategory int     `validate:"required,numeric"`
	Name       *string `validate:"omitempty,gte=0,lte=60"`
	ParentId   *int    `validate:"omitempty,numeric"`
}

func NewUpdateCategory(idUser int, idCategory int, name *string, parentId *int) (*UpdateCategory, *validator.ValidateError) {
	command := &UpdateCategory{idUser, idCategory, name, parentId}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
