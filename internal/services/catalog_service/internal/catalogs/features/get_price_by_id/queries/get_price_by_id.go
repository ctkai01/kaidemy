package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetPriceById struct {
	IdPrice int `validate:"required,numeric"`
}

func NewGetPriceById(idPrice int) (*GetPriceById, *validator.ValidateError) {
	query := &GetPriceById{idPrice}
	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return query, nil
}
