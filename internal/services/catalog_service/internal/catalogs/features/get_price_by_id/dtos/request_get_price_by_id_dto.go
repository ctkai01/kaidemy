package dtos

//https://echo.labstack.com/guide/response/

type GetPriceByIdRequestDto struct {
	PriceID int `json:"-" param:"id"`
}
