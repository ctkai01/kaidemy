package queries

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_issue_types/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// userservice "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type GetIssueTypesHandler struct {
	log             logger.Logger
	issueTypeRepository data.IssueTypeRepository
	grpcClient      grpc.GrpcClient
}

func NewGetIssueTypesHandler(
	log logger.Logger,
	issueTypeRepository data.IssueTypeRepository,
	grpcClient grpc.GrpcClient,

) *GetIssueTypesHandler {
	return &GetIssueTypesHandler{log: log, issueTypeRepository: issueTypeRepository, grpcClient: grpcClient}
}

func (c *GetIssueTypesHandler) Handle(ctx context.Context, query *GetIssueTypes) (*dtos.GetIssueTypesResponseDto, error) {
	// conn, err := c.grpcClient.GetGrpcConnection("user_service")
	// if err != nil {
	// 	return nil, err
	// }

	// client := userservice.NewUsersServiceClient(conn)
	// dataUser, err := client.GetUserByID(context.Background(), &userservice.GetUserByIdReq{
	// 	UserId: int32(query.IdUser),
	// })

	// if err != nil {
	// 	return nil, err
	// }

	// if dataUser.User == nil {
	// 	return nil, customErrors.ErrUserNotFound
	// }

	issueTypes, err := c.issueTypeRepository.GetAllIssueTypes(ctx, query.ListQuery)
	if err != nil {
		return nil, err
	}

	listResultDto, err := utils.ListResultToListResultDto[*models.IssueType](issueTypes)

	if err != nil {
		return nil, err
	}
	return &dtos.GetIssueTypesResponseDto{
		IssueTypes: listResultDto,
	}, nil

}
