package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetIssueTypes struct {
	// IdUser int
	*utils.ListQuery
}

func NewGetIssueTypes(query *utils.ListQuery) (*GetIssueTypes, *validator.ValidateError) {
	q := &GetIssueTypes{ListQuery: query}
	// q := &GetIssueTypes{IdUser: idUser, ListQuery: query}

	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
