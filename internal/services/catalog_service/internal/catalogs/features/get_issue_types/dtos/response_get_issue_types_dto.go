package dtos

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/models"
)

//https://echo.labstack.com/guide/response/

type GetIssueTypesResponseDto struct {
	IssueTypes *utils.ListResult[*models.IssueType] `json:"issue_types"`
}
