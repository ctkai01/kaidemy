package dtos

import "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"

//https://echo.labstack.com/guide/response/

type GetIssueTypesRequestDto struct {
	*utils.ListQuery
}
