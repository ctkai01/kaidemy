package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetPrices struct {
	IdUser int
	*utils.ListQuery
}

func NewGetPrices(idUser int, query *utils.ListQuery) (*GetPrices, *validator.ValidateError) {
	q := &GetPrices{IdUser: idUser, ListQuery: query}

	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
