package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/params"
	catalogsError "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_prices/dtos"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_prices/queries"
)

type getPricesEndpoint struct {
	params.PricesRouteParams
}

func NewGetPricesEndpoint(
	params params.PricesRouteParams,
) route.Endpoint {
	return &getPricesEndpoint{
		PricesRouteParams: params,
	}
}

func (ep *getPricesEndpoint) MapEndpoint() {
	ep.PricesGroup.GET("", ep.handler())
}

func (ep *getPricesEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		listQuery, err := utils.GetListQueryFromCtx(c)

		if err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[getPrices.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getPricesEndpoint_handler.Bind] err: %v", badRequestErr),
			)

			return c.JSON(http.StatusBadRequest, badRequestErr)

		}
		fmt.Printf("Filters: %v\n", listQuery.Filters)

		userId := utils.GetUserId(c)
		query, errValidate := queries.NewGetPrices(
			userId,
			listQuery,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[getPricesEndpoint_handler.StructCtx] query validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getPricesEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*queries.GetPrices, *dtos.GetPricesResponseDto](
			ctx,
			query,
		)

		if err != nil {
			if err.Error() == catalogsError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[getPricesEndpoint_handler.Send] error in sending get Prices",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[getPrices.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			if err.Error() == catalogsError.WrapError(customErrors.ErrCategoryNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[getPricesEndpoint_handler.Send] error in sending get Prices",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[getPrices.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[getPricesEndpoint_handler.Send] error in sending get Prices",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[getPrices.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
