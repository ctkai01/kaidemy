package dtos

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/models"
)

//https://echo.labstack.com/guide/response/

type GetPricesResponseDto struct {
	Prices *utils.ListResult[*models.Price]
}
