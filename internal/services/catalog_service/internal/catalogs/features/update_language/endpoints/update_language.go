package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/params"
	catalogsError "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/update_language/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/update_language/dtos"
)

type updateLanguageEndpoint struct {
	params.LanguageAdminRouteParams
}

func NewUpdateLanguageEndpoint(
	params params.LanguageAdminRouteParams,
) route.Endpoint {
	return &updateLanguageEndpoint{
		LanguageAdminRouteParams: params,
	}
}

func (ep *updateLanguageEndpoint) MapEndpoint() {
	ep.LanguagesGroup.PUT("/:id", ep.handler())
}

func (ep *updateLanguageEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.UpdateLanguageRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[updateLanguage.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[updateLanguageEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}

		userId := utils.GetUserId(c)

		command, errValidate := commands.NewUpdateLanguage(
			userId,
			request.LanguageID,
			request.Name,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[updateLanguageEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[updateLanguageEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.UpdateLanguage, *dtos.UpdateLanguageResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == catalogsError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[updateLanguageEndpoint_handler.Send] error in sending update Language",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[updateLanguage.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			if err.Error() == catalogsError.WrapError(customErrors.ErrLanguageNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[updateLanguageEndpoint_handler.Send] error in sending create Language",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[updateLanguage.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[updateLanguageEndpoint_handler.Send] error in sending update Language",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[updateLanguage.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
