package dtos

//https://echo.labstack.com/guide/response/

type UpdateLanguageRequestDto struct {
	LanguageID int     `json:"-" param:"id"`
	Name       *string `json:"name" form:"name"`
}
