package dtos

import "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/models"

//https://echo.labstack.com/guide/response/

type UpdateLanguageResponseDto struct {
	Message  string          `json:"message"`
	Language models.Language `json:"language"`
}
