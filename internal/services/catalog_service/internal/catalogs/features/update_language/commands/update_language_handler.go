package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/update_language/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	userservice "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type UpdateLanguageHandler struct {
	log                logger.Logger
	languageRepository data.LanguageRepository
	grpcClient         grpc.GrpcClient
}

func NewUpdateLanguageHandler(
	log logger.Logger,
	languageRepository data.LanguageRepository,
	grpcClient grpc.GrpcClient,

) *UpdateLanguageHandler {
	return &UpdateLanguageHandler{log: log, languageRepository: languageRepository, grpcClient: grpcClient}
}

func (c *UpdateLanguageHandler) Handle(ctx context.Context, command *UpdateLanguage) (*dtos.UpdateLanguageResponseDto, error) {
	conn, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}

	client := userservice.NewUsersServiceClient(conn)
	dataUser, err := client.GetUserByID(context.Background(), &userservice.GetUserByIdReq{
		UserId: int32(command.IdUser),
	})

	if err != nil {
		return nil, err
	}

	if dataUser.User == nil {
		return nil, customErrors.ErrUserNotFound
	}

	languageUpdate, err := c.languageRepository.GetLanguageByID(ctx, command.IdLanguage)

	if err != nil {
		return nil, err
	}

	if languageUpdate == nil {
		return nil, customErrors.ErrLanguageNotFound
	}

	if command.Name != nil {
		languageUpdate.Name = *command.Name
	}

	if err := c.languageRepository.UpdateLanguage(ctx, languageUpdate); err != nil {
		return nil, err
	}

	return &dtos.UpdateLanguageResponseDto{
		Message:  "Update language successfully!",
		Language: *languageUpdate,
	}, nil
}
