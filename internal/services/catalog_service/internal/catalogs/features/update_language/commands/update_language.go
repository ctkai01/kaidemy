package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type UpdateLanguage struct {
	IdUser     int
	IdLanguage int     `validate:"required,numeric"`
	Name       *string `validate:"omitempty,gte=1,lte=60"`
}

func NewUpdateLanguage(idUser int, idLanguage int, name *string) (*UpdateLanguage, *validator.ValidateError) {
	command := &UpdateLanguage{idUser, idLanguage, name}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
