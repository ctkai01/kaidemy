package dtos

//https://echo.labstack.com/guide/response/

type UpdateIssueTypeRequestDto struct {
	IssueTypeID int     `json:"-" param:"id"`
	Name        *string `json:"name" form:"name"`
}
