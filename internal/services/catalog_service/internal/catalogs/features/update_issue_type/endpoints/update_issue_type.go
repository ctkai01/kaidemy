package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/params"
	catalogsError "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/update_issue_type/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/update_issue_type/dtos"
)

type updateIssueTypeEndpoint struct {
	params.IssueTypeAdminRouteParams
}

func NewUpdateIssueTypeEndpoint(
	params params.IssueTypeAdminRouteParams,
) route.Endpoint {
	return &updateIssueTypeEndpoint{
		IssueTypeAdminRouteParams: params,
	}
}

func (ep *updateIssueTypeEndpoint) MapEndpoint() {
	ep.IssueTypesGroup.PUT("/:id", ep.handler())
}

func (ep *updateIssueTypeEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.UpdateIssueTypeRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[updateIssueType.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[updateIssueTypeEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}

		userId := utils.GetUserId(c)

		command, errValidate := commands.NewUpdateIssueType(
			userId,
			request.IssueTypeID,
			request.Name,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[updateIssueTypeEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[updateIssueTypeEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.UpdateIssueType, *dtos.UpdateIssueTypeResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == catalogsError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[updateIssueTypeEndpoint_handler.Send] error in sending update IssueType",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[updateIssueType.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			if err.Error() == catalogsError.WrapError(customErrors.ErrIssueTypeNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[updateIssueTypeEndpoint_handler.Send] error in sending create IssueType",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[updateIssueType.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[updateIssueTypeEndpoint_handler.Send] error in sending update IssueType",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[updateIssueType.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
