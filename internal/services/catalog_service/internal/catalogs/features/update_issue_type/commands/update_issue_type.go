package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type UpdateIssueType struct {
	IdUser      int
	IdIssueType int     `validate:"required,numeric"`
	Name        *string `validate:"omitempty,gte=1,lte=100"`
}

func NewUpdateIssueType(idUser int, idIssueType int, name *string) (*UpdateIssueType, *validator.ValidateError) {
	command := &UpdateIssueType{idUser, idIssueType, name}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
