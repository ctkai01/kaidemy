package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/update_issue_type/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	userservice "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type UpdateIssueTypeHandler struct {
	log             logger.Logger
	issueTypeRepository data.IssueTypeRepository
	grpcClient      grpc.GrpcClient
}

func NewUpdateIssueTypeHandler(
	log logger.Logger,
	issueTypeRepository data.IssueTypeRepository,
	grpcClient grpc.GrpcClient,

) *UpdateIssueTypeHandler {
	return &UpdateIssueTypeHandler{log: log, issueTypeRepository: issueTypeRepository, grpcClient: grpcClient}
}

func (c *UpdateIssueTypeHandler) Handle(ctx context.Context, command *UpdateIssueType) (*dtos.UpdateIssueTypeResponseDto, error) {
	conn, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}

	client := userservice.NewUsersServiceClient(conn)
	dataUser, err := client.GetUserByID(context.Background(), &userservice.GetUserByIdReq{
		UserId: int32(command.IdUser),
	})

	if err != nil {
		return nil, err
	}

	if dataUser.User == nil {
		return nil, customErrors.ErrUserNotFound
	}

	issueTypeUpdate, err := c.issueTypeRepository.GetIssueTypeByID(ctx, command.IdIssueType)

	if err != nil {
		return nil, err
	}

	if issueTypeUpdate == nil {
		return nil, customErrors.ErrIssueTypeNotFound
	}

	if command.Name != nil {
		issueTypeUpdate.Name = *command.Name
	}

	if err := c.issueTypeRepository.UpdateIssueType(ctx, issueTypeUpdate); err != nil {
		return nil, err
	}

	return &dtos.UpdateIssueTypeResponseDto{
		Message: "Update issue type successfully!",
		IssueType:   *issueTypeUpdate,
	}, nil
}
