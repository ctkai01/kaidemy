package dtos

//https://echo.labstack.com/guide/response/

type GetLevelByIdRequestDto struct {
	LevelID int `json:"-" param:"id"`
}
