package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetLevelById struct {
	IdLevel int `validate:"required,numeric"`
}

func NewGetLevelById( idLevel int) (*GetLevelById, *validator.ValidateError) {
	query := &GetLevelById{idLevel}
	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return query, nil
}
