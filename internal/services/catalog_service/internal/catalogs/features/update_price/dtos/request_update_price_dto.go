package dtos

//https://echo.labstack.com/guide/response/

type UpdatePriceRequestDto struct {
	PriceID int     `json:"-" param:"id"`
	Tier       *string `json:"tier" form:"tier"`
	Value      *int    `json:"value" form:"value"`
}
