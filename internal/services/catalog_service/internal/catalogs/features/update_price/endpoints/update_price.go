package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/params"
	catalogsError "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/update_price/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/update_price/dtos"
)

type updatePriceEndpoint struct {
	params.PricesAdminRouteParams
}

func NewUpdatePriceEndpoint(
	params params.PricesAdminRouteParams,
) route.Endpoint {
	return &updatePriceEndpoint{
		PricesAdminRouteParams: params,
	}
}

func (ep *updatePriceEndpoint) MapEndpoint() {
	ep.PricesGroup.PUT("/:id", ep.handler())
}

func (ep *updatePriceEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.UpdatePriceRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[updatePrice.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[updatePriceEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}

		userId := utils.GetUserId(c)

		command, errValidate := commands.NewUpdatePrice(
			userId,
			request.PriceID,
			request.Tier,
			request.Value,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[updatePriceEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[updatePriceEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.UpdatePrice, *dtos.UpdatePriceResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == catalogsError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[update updatePriceEndpoint_handler.Send] error in sending create update Price",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[update updatePrice.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			if err.Error() == catalogsError.WrapError(customErrors.ErrPriceNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[updatePriceEndpoint_handler.Send] error in sending create Price",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[updatePrice.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[updatePriceEndpoint_handler.Send] error in sending update price",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[updatePrice.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
