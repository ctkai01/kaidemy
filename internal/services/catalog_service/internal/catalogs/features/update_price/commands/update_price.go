package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type UpdatePrice struct {
	IdUser  int
	IdPrice int     `validate:"required,numeric"`
	Tier    *string `validate:"omitempty,gte=1,lte=60"`
	Value   *int    `validate:"omitempty,numeric"`
}

func NewUpdatePrice(idUser int, idPrice int, tier *string, value *int) (*UpdatePrice, *validator.ValidateError) {
	command := &UpdatePrice{idUser, idPrice, tier, value}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
