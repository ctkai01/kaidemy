package dtos

//https://echo.labstack.com/guide/response/

type CreateIssueTypeRequestDto struct {
	Name  string `form:"name"`
}
