package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/params"
	catalogsError "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/create_issue_type/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/create_issue_type/dtos"
)

type createIssueTypeEndpoint struct {
	params.IssueTypeAdminRouteParams
}

func NewCreateIssueTypeEndpoint(
	params params.IssueTypeAdminRouteParams,
) route.Endpoint {
	return &createIssueTypeEndpoint{
		IssueTypeAdminRouteParams: params,
	}
}

func (ep *createIssueTypeEndpoint) MapEndpoint() {
	ep.IssueTypesGroup.POST("", ep.handler())
}

func (ep *createIssueTypeEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.CreateIssueTypeRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[createIssueType.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[createIssueTypeEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}
		userId := utils.GetUserId(c)

		command, errValidate := commands.NewCreateIssueType(
			userId,
			request.Name,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[createIssueTypeEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[createIssueTypeEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.CreateIssueType, *dtos.CreateIssueTypeResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == catalogsError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[createIssueTypeEndpoint_handler.Send] error in sending create IssueType",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[createIssueType.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[createIssueTypeEndpoint_handler.Send] error in sending create IssueType",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[createIssueType.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusCreated, result)
	}
}
