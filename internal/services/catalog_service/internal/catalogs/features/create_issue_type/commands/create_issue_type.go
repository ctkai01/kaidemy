package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type CreateIssueType struct {
	IdUser int
	Name   string `validate:"required,gte=1,lte=100"`
}

func NewCreateIssueType(idUser int, name string) (*CreateIssueType, *validator.ValidateError) {
	command := &CreateIssueType{idUser, name}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
