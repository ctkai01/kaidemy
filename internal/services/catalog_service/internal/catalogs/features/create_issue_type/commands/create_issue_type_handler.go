package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/create_issue_type/dtos"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/models"

	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	userservice "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type CreateIssueTypeHandler struct {
	log                 logger.Logger
	issueTypeRepository data.IssueTypeRepository
	grpcClient          grpc.GrpcClient
}

func NewCreateIssueTypeHandler(
	log logger.Logger,
	issueTypeRepository data.IssueTypeRepository,
	grpcClient grpc.GrpcClient,

) *CreateIssueTypeHandler {
	return &CreateIssueTypeHandler{log: log, issueTypeRepository: issueTypeRepository, grpcClient: grpcClient}
}

func (c *CreateIssueTypeHandler) Handle(ctx context.Context, command *CreateIssueType) (*dtos.CreateIssueTypeResponseDto, error) {
	conn, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}

	client := userservice.NewUsersServiceClient(conn)
	dataUser, err := client.GetUserByID(context.Background(), &userservice.GetUserByIdReq{
		UserId: int32(command.IdUser),
	})

	if err != nil {
		return nil, err
	}

	if dataUser.User == nil {
		return nil, customErrors.ErrUserNotFound
	}

	issueTypeCreate := &models.IssueType{
		Name: command.Name,
	}

	if err := c.issueTypeRepository.CreateIssueType(ctx, issueTypeCreate); err != nil {
		return nil, err
	}

	return &dtos.CreateIssueTypeResponseDto{
		Message:   "Create issue type successfully!",
		IssueType: *issueTypeCreate,
	}, nil
}
