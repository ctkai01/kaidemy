package dtos

//https://echo.labstack.com/guide/response/

type DeleteCategoryRequestDto struct {
	CategoryID int `json:"-" param:"id"`
}
