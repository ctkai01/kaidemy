package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"fmt"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/create_course/commands"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/update_category/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	userservice "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type DeleteCategoryHandler struct {
	log                logger.Logger
	categoryRepository data.CategoryRepository
	grpcClient         grpc.GrpcClient
}

func NewDeleteCategoryHandler(
	log logger.Logger,
	categoryRepository data.CategoryRepository,
	grpcClient grpc.GrpcClient,

) *DeleteCategoryHandler {
	return &DeleteCategoryHandler{log: log, categoryRepository: categoryRepository, grpcClient: grpcClient}
}

func (c *DeleteCategoryHandler) Handle(ctx context.Context, command *DeleteCategory) (*mediatr.Unit, error) {
	conn, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}

	client := userservice.NewUsersServiceClient(conn)
	dataUser, err := client.GetUserByID(context.Background(), &userservice.GetUserByIdReq{
		UserId: int32(command.IdUser),
	})

	if err != nil {
		return nil, err
	}

	if dataUser.User == nil {
		return nil, customErrors.ErrUserNotFound
	}

	categoryDelete, err := c.categoryRepository.GetCategoryByIDRelation(ctx, nil, command.IdCategory)
	fmt.Println(categoryDelete)
	if err != nil {
		return nil, err
	}

	if categoryDelete == nil {
		fmt.Println("TR")
		return nil, customErrors.ErrCategoryNotFound
	}

	childrenCategory, err := c.categoryRepository.GetChildrenCategoryByID(ctx, command.IdCategory)

	if err != nil {
		return nil, err
	}

	if len(childrenCategory) != 0 {
		return nil, customErrors.ErrCannotDeleteParentCategory
	}

	if err := c.categoryRepository.DeleteCategoryByID(ctx, command.IdCategory); err != nil {
		return nil, err
	}

	return &mediatr.Unit{}, nil
}
