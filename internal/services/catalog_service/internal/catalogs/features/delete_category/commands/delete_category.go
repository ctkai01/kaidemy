package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type DeleteCategory struct {
	IdUser     int
	IdCategory int `validate:"required,numeric"`
}

func NewDeleteCategory(idUser int, idCategory int) (*DeleteCategory, *validator.ValidateError) {
	command := &DeleteCategory{idUser, idCategory}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
