package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetCategories struct {
	// IdUser int
	*utils.ListQuery
	Relation int
}

func NewGetCategories(query *utils.ListQuery, relation int) (*GetCategories, *validator.ValidateError) {
	q := &GetCategories{ListQuery: query, Relation: relation}

	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
