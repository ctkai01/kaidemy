package queries

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// "encoding/json"
	"fmt"
	"strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_categories/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type GetCategoriesHandler struct {
	log                logger.Logger
	categoryRepository data.CategoryRepository
	grpcClient         grpc.GrpcClient
}

func NewGetCategoriesHandler(
	log logger.Logger,
	categoryRepository data.CategoryRepository,
	grpcClient grpc.GrpcClient,

) *GetCategoriesHandler {
	return &GetCategoriesHandler{log: log, categoryRepository: categoryRepository, grpcClient: grpcClient}
}

func (c *GetCategoriesHandler) Handle(ctx context.Context, query *GetCategories) (*dtos.GetCategoriesResponseDto, error) {
	

	fmt.Println("query: ", query.Relation)

	if query.Relation == 1 {
		// idUserStr := fmt.Sprintf("%d", query.IdUser)
		query.ListQuery.Filters = append(query.ListQuery.Filters, &utils.FilterModel{
			Field: "parent_id",
			Value: nil,
			Comparison: "equals",
		})
		categories, err := c.categoryRepository.GetAllCategoriesRelation(ctx, []string{"Children"}, query.ListQuery)
		if err != nil {
			return nil, err
		}

		listResultDto, err := utils.ListResultToListResultDto[*models.Category](categories)

		if err != nil {
			return nil, err
		}
		menuClass := make(models.MenuClass)
		menuClass[""] = ""
		menuCategories := createMenuCategories(listResultDto.Items)

		genClass(menuCategories, &menuClass)
		// menuClassJson, err := json.Marshal(menuClass)

		if err != nil {
			return nil, err
		}
		return &dtos.GetCategoriesResponseDto{
			Categories: listResultDto,
			Json: &menuClass,
		}, nil
	} else {
		categories, err := c.categoryRepository.GetAllCategories(ctx, query.ListQuery)
		if err != nil {
			return nil, err
		}

		listResultDto, err := utils.ListResultToListResultDto[*models.Category](categories)

		if err != nil {
			return nil, err
		}
		return &dtos.GetCategoriesResponseDto{Categories: listResultDto}, nil
	}

}
func genClass(menus []models.MenuCategory, menuClass *models.MenuClass) {
	for _, menu := range menus {
		if len(menu.Children) > 0 && menu.GroupName != nil {
			
			(*menuClass)[fmt.Sprintf("group/%s", *menu.GroupName)] = fmt.Sprintf("group/%s", *menu.GroupName)
			(*menuClass)[fmt.Sprintf("group-hover/%s:block", *menu.GroupName)] = fmt.Sprintf("group-hover/%s:block", *menu.GroupName)
            genClass(menu.Children, menuClass)
			
		}
	}
}

func createMenuCategories(categories []*models.Category) []models.MenuCategory {
	var menus []models.MenuCategory

	for _, category := range categories {
		groupName := strings.Join(strings.Split(strings.ToLower(category.Name), " "), "_")
		

		children := MapCategoriesToMenuCategories(category.Children)
		menus = append(menus, models.MenuCategory{
			Name: category.Name,
			ParentID: category.ParentID,
			Children: children,
			GroupName: &groupName,
		})
	}

	var menusCategories []models.MenuCategory
	groupCategories := "categories"
	menusCategories = append(menusCategories, models.MenuCategory{
		Name: "Categories",
		ParentID: nil,
		Children: menus,
		GroupName: &groupCategories,
	})

	return menusCategories
}

func MapCategoriesToMenuCategories(categories []models.Category) []models.MenuCategory {
    var menuCategories []models.MenuCategory

    for _, category := range categories {
        menuCategory := models.MenuCategory{
            Name:      category.Name,
            ParentID:  category.ParentID,
            GroupName: func(s string) *string { return &s }(strings.Join(strings.Split(strings.ToLower(category.Name), " "), "_")),
        }

        // Assuming you have a separate function to map children categories recursively
        // Here, map the children recursively by calling another function or mapping method
        // menuCategory.Children = MapCategoriesToMenuCategories(category.Children)

        // Append the mapped menu category to the resulting slice
        menuCategories = append(menuCategories, menuCategory)
    }

    return menuCategories
}