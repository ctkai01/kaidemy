package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/params"
	catalogsError "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_categories/dtos"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_categories/queries"
)

type getCategoriesEndpoint struct {
	params.CategoriesRouteParams
}

func NewGetCategoriesEndpoint(
	params params.CategoriesRouteParams,
) route.Endpoint {
	return &getCategoriesEndpoint{
		CategoriesRouteParams: params,
	}
}

func (ep *getCategoriesEndpoint) MapEndpoint() {
	ep.CategoriesGroup.GET("", ep.handler())
}

func (ep *getCategoriesEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		
		request := &dtos.GetCategoriesRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[getCategories.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getCategoriesEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}

		listQuery, err := utils.GetListQueryFromCtx(c)
		// fmt.Println("Query: ", listQuery.Filters[0])
		if err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[getCategories.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getCategoriesEndpoint_handler.Bind] err: %v", badRequestErr),
			)

			return c.JSON(http.StatusBadRequest, badRequestErr)

		}

		fmt.Printf("Filters: %v\n", listQuery.Filters)
		// request := &dtos.GetCategoriesRequestDto{ListQuery: listQuery}

		// if err := c.Bind(request); err != nil {
		// 	badRequestErr := customErrors.NewBadRequestErrorWrap(
		// 		err,
		// 		"[getCategories.Bind] error in the binding request",
		// 	)
		// 	ep.Logger.Errorf(
		// 		fmt.Sprintf("[getCategoriesEndpoint_handler.Bind] err: %v", badRequestErr),
		// 	)
		// 	return c.JSON(http.StatusBadRequest, badRequestErr)

		// }

		// userId := utils.GetUserId(c)
		fmt.Println(listQuery.Filters)
		query, errValidate := queries.NewGetCategories(
			// userId,
			listQuery,
			request.Relation,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[getCategoriesEndpoint_handler.StructCtx] query validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getCategoriesEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*queries.GetCategories, *dtos.GetCategoriesResponseDto](
			ctx,
			query,
		)

		if err != nil {
			if err.Error() == catalogsError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[getCategoriesEndpoint_handler.Send] error in sending get Categories",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[getCategories.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			if err.Error() == catalogsError.WrapError(customErrors.ErrCategoryNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[getCategoriesEndpoint_handler.Send] error in sending get categories",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[getCategories.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[getCategoriesEndpoint_handler.Send] error in sending get categories",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[getCategories.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
