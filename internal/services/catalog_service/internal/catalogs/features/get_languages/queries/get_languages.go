package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetLanguages struct {
	IdUser int
	*utils.ListQuery
}

func NewGetLanguages(idUser int, query *utils.ListQuery) (*GetLanguages, *validator.ValidateError) {
	q := &GetLanguages{IdUser: idUser, ListQuery: query}

	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
