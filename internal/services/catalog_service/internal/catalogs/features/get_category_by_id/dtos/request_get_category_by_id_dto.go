package dtos

//https://echo.labstack.com/guide/response/

type GetCategoryByIdRequestDto struct {
	CategoryID int `json:"-" param:"id"`
}
