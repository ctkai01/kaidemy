package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/params"
	catalogsError "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_category_by_id/dtos"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_category_by_id/queries"
)

type getCategoryByIdEndpoint struct {
	params.CategoriesRouteParams
}

func NewGetCategoryByIdEndpoint(
	params params.CategoriesRouteParams,
) route.Endpoint {
	return &getCategoryByIdEndpoint{
		CategoriesRouteParams: params,
	}
}

func (ep *getCategoryByIdEndpoint) MapEndpoint() {
	ep.CategoriesGroup.GET("/:id", ep.handler())
}

func (ep *getCategoryByIdEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.GetCategoryByIdRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[getCategoryById.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getCategoryByIdEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}

		// userId := utils.GetUserId(c)

		command, errValidate := queries.NewGetCategoryById(
			// userId,
			request.CategoryID,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[getCategoryByIdEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getCategoryByIdEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*queries.GetCategoryById, *dtos.GetCategoryByIdResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == catalogsError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[deleteCategoryEndpoint_handler.Send] error in sending delete Category",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[deleteCategory.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			if err.Error() == catalogsError.WrapError(customErrors.ErrCategoryNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[getCategoryByIdEndpoint_handler.Send] error in sending get category by id",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[getCategoryById.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[getCategoryByIdEndpoint_handler.Send] error in sending get category by id",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[getCategoryById.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
