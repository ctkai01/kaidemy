package queries

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_category_by_id/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type GetCategoryByIdHandler struct {
	log                logger.Logger
	categoryRepository data.CategoryRepository
	grpcClient         grpc.GrpcClient
}

func NewGetCategoryByIdHandler(
	log logger.Logger,
	categoryRepository data.CategoryRepository,
	grpcClient grpc.GrpcClient,

) *GetCategoryByIdHandler {
	return &GetCategoryByIdHandler{log: log, categoryRepository: categoryRepository, grpcClient: grpcClient}
}

func (c *GetCategoryByIdHandler) Handle(ctx context.Context, command *GetCategoryById) (*dtos.GetCategoryByIdResponseDto, error) {
	// conn, err := c.grpcClient.GetGrpcConnection("user_service")
	// if err != nil {
	// 	return nil, err
	// }

	// client := userservice.NewUsersServiceClient(conn)
	// dataUser, err := client.GetUserByID(context.Background(), &userservice.GetUserByIdReq{
	// 	UserId: int32(command.IdUser),
	// })

	// if err != nil {
	// 	return nil, err
	// }

	// if dataUser.User == nil {
	// 	return nil, customErrors.ErrUserNotFound
	// }

	category, err := c.categoryRepository.GetCategoryByIDRelation(ctx, []string{"Children", "Parent"}, command.IdCategory)

	if err != nil {
		return nil, err
	}

	if category == nil {
		return nil, customErrors.ErrCategoryNotFound
	}

	return &dtos.GetCategoryByIdResponseDto{
		Message:  "Get category by id",
		Category: category,
	}, nil
}
