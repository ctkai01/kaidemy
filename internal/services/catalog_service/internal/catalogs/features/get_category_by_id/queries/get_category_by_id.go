package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetCategoryById struct {
	// IdUser     int
	IdCategory int `validate:"required,numeric"`
}

func NewGetCategoryById(idCategory int) (*GetCategoryById, *validator.ValidateError) {
	query := &GetCategoryById{idCategory}
	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return query, nil
}
