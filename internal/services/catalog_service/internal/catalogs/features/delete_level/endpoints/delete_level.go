package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/params"
	catalogsError "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/delete_level/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/delete_level/dtos"
)

type deleteLevelEndpoint struct {
	params.LevelAdminRouteParams
}

func NewDeleteLevelEndpoint(
	params params.LevelAdminRouteParams,
) route.Endpoint {
	return &deleteLevelEndpoint{
		LevelAdminRouteParams: params,
	}
}

func (ep *deleteLevelEndpoint) MapEndpoint() {
	ep.LevelsGroup.DELETE("/:id", ep.handler())
}

func (ep *deleteLevelEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.DeleteLevelRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[deleteLevel.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[deleteLevelEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}

		userId := utils.GetUserId(c)

		command, errValidate := commands.NewDeleteLevel(
			userId,
			request.LevelID,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[deleteLevelEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[deleteLevelEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.DeleteLevel, *dtos.DeleteLevelResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == catalogsError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[deleteLevelEndpoint_handler.Send] error in sending delete Level",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[deleteLevel.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			if err.Error() == catalogsError.WrapError(customErrors.ErrLevelNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[deleteLevelEndpoint_handler.Send] error in sending delete Level",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[deleteLevel.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[deleteLevelEndpoint_handler.Send] error in sending delete Level",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[deleteLevel.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
