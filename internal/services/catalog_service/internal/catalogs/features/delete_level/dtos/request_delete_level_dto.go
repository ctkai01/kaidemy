package dtos

//https://echo.labstack.com/guide/response/

type DeleteLevelRequestDto struct {
	LevelID int     `json:"-" param:"id"`
}
