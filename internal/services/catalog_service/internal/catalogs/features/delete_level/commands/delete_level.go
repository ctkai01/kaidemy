package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type DeleteLevel struct {
	IdUser  int
	IdLevel int `validate:"required,numeric"`
}

func NewDeleteLevel(idUser int, idLevel int) (*DeleteLevel, *validator.ValidateError) {
	command := &DeleteLevel{idUser, idLevel}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
