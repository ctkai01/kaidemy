package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// "fmt"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/delete_level/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	userservice "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type DeleteLevelHandler struct {
	log             logger.Logger
	levelRepository data.LevelRepository
	grpcClient      grpc.GrpcClient
}

func NewDeleteLevelHandler(
	log logger.Logger,
	levelRepository data.LevelRepository,
	grpcClient grpc.GrpcClient,

) *DeleteLevelHandler {
	return &DeleteLevelHandler{log: log, levelRepository: levelRepository, grpcClient: grpcClient}
}

func (c *DeleteLevelHandler) Handle(ctx context.Context, command *DeleteLevel) (*dtos.DeleteLevelResponseDto, error) {
	conn, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}

	client := userservice.NewUsersServiceClient(conn)
	dataUser, err := client.GetUserByID(context.Background(), &userservice.GetUserByIdReq{
		UserId: int32(command.IdUser),
	})

	if err != nil {
		return nil, err
	}

	if dataUser.User == nil {
		return nil, customErrors.ErrUserNotFound
	}

	levelDelete, err := c.levelRepository.GetLevelByID(ctx, command.IdLevel)

	if err != nil {
		return nil, err
	}

	if levelDelete == nil {
		return nil, customErrors.ErrLevelNotFound
	}

	if err := c.levelRepository.DeleteLevelByID(ctx, command.IdLevel); err != nil {
		return nil, err
	}

	return &dtos.DeleteLevelResponseDto{
		Message: "Delete level successfully!",
	}, nil
}
