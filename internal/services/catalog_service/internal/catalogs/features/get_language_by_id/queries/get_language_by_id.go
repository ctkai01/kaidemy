package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetLanguageById struct {
	// IdUser     int
	IdLanguage int `validate:"required,numeric"`
}

func NewGetLanguageById(idLanguage int) (*GetLanguageById, *validator.ValidateError) {
	query := &GetLanguageById{idLanguage}
	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return query, nil
}
