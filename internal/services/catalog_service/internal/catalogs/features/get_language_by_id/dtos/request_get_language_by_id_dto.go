package dtos

//https://echo.labstack.com/guide/response/

type GetLanguageByIdRequestDto struct {
	LanguageID int `json:"-" param:"id"`
}
