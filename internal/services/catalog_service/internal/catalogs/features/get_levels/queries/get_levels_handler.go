package queries

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_levels/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	// userservice "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type GetLevelsHandler struct {
	log             logger.Logger
	levelRepository data.LevelRepository
	grpcClient      grpc.GrpcClient
}

func NewGetLevelsHandler(
	log logger.Logger,
	levelRepository data.LevelRepository,
	grpcClient grpc.GrpcClient,

) *GetLevelsHandler {
	return &GetLevelsHandler{log: log, levelRepository: levelRepository, grpcClient: grpcClient}
}

func (c *GetLevelsHandler) Handle(ctx context.Context, query *GetLevels) (*dtos.GetLevelsResponseDto, error) {
	// conn, err := c.grpcClient.GetGrpcConnection("user_service")
	// if err != nil {
	// 	return nil, err
	// }

	// client := userservice.NewUsersServiceClient(conn)
	// dataUser, err := client.GetUserByID(context.Background(), &userservice.GetUserByIdReq{
	// 	UserId: int32(query.IdUser),
	// })

	// if err != nil {
	// 	return nil, err
	// }

	// if dataUser.User == nil {
	// 	return nil, customErrors.ErrUserNotFound
	// }

	levels, err := c.levelRepository.GetAllLevels(ctx, query.ListQuery)
	if err != nil {
		return nil, err
	}

	listResultDto, err := utils.ListResultToListResultDto[*models.Level](levels)

	if err != nil {
		return nil, err
	}
	return &dtos.GetLevelsResponseDto{Levels: listResultDto}, nil

}
