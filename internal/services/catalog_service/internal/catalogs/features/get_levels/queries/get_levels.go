package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetLevels struct {
	// IdUser int
	*utils.ListQuery
}

func NewGetLevels(query *utils.ListQuery) (*GetLevels, *validator.ValidateError) {
	q := &GetLevels{ ListQuery: query}

	errValidate := validator.Validate(query)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return q, nil
}
