package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type DeleteIssueType struct {
	IdUser  int
	IdIssueType int `validate:"required,numeric"`
}

func NewDeleteIssueType(idUser int, idIssueType int) (*DeleteIssueType, *validator.ValidateError) {
	command := &DeleteIssueType{idUser, idIssueType}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
