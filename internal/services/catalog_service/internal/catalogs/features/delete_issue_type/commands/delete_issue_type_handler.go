package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// "fmt"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/delete_issue_type/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	userservice "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type DeleteIssueTypeHandler struct {
	log             logger.Logger
	issueTypeRepository data.IssueTypeRepository
	grpcClient      grpc.GrpcClient
}

func NewDeleteIssueTypeHandler(
	log logger.Logger,
	issueTypeRepository data.IssueTypeRepository,
	grpcClient grpc.GrpcClient,

) *DeleteIssueTypeHandler {
	return &DeleteIssueTypeHandler{log: log, issueTypeRepository: issueTypeRepository, grpcClient: grpcClient}
}

func (c *DeleteIssueTypeHandler) Handle(ctx context.Context, command *DeleteIssueType) (*dtos.DeleteIssueTypeResponseDto, error) {
	conn, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}

	client := userservice.NewUsersServiceClient(conn)
	dataUser, err := client.GetUserByID(context.Background(), &userservice.GetUserByIdReq{
		UserId: int32(command.IdUser),
	})

	if err != nil {
		return nil, err
	}

	if dataUser.User == nil {
		return nil, customErrors.ErrUserNotFound
	}

	issueTypeDelete, err := c.issueTypeRepository.GetIssueTypeByID(ctx, command.IdIssueType)

	if err != nil {
		return nil, err
	}

	if issueTypeDelete == nil {
		return nil, customErrors.ErrIssueTypeNotFound
	}

	if err := c.issueTypeRepository.DeleteIssueTypeByID(ctx, command.IdIssueType); err != nil {
		return nil, err
	}

	return &dtos.DeleteIssueTypeResponseDto{
		Message: "Delete issue type successfully!",
	}, nil
}
