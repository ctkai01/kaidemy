package dtos

//https://echo.labstack.com/guide/response/

type DeleteIssueTypeRequestDto struct {
	IssueTypeID int     `json:"-" param:"id"`
}
