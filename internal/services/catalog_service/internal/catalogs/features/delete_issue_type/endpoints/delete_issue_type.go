package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/params"
	catalogsError "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/delete_issue_type/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/delete_issue_type/dtos"
)

type deleteIssueTypeEndpoint struct {
	params.IssueTypeAdminRouteParams
}

func NewDeleteIssueTypeEndpoint(
	params params.IssueTypeAdminRouteParams,
) route.Endpoint {
	return &deleteIssueTypeEndpoint{
		IssueTypeAdminRouteParams: params,
	}
}

func (ep *deleteIssueTypeEndpoint) MapEndpoint() {
	ep.IssueTypesGroup.DELETE("/:id", ep.handler())
}

func (ep *deleteIssueTypeEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.DeleteIssueTypeRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[deleteIssueType.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[deleteIssueTypeEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}

		userId := utils.GetUserId(c)

		command, errValidate := commands.NewDeleteIssueType(
			userId,
			request.IssueTypeID,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[deleteIssueTypeEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[deleteIssueTypeEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.DeleteIssueType, *dtos.DeleteIssueTypeResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == catalogsError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[deleteIssueTypeEndpoint_handler.Send] error in sending delete IssueType",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[deleteIssueType.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			if err.Error() == catalogsError.WrapError(customErrors.ErrLevelNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[deleteIssueTypeEndpoint_handler.Send] error in sending delete IssueType",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[deleteIssueType.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[deleteIssueTypeEndpoint_handler.Send] error in sending delete IssueType",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[deleteIssueType.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
