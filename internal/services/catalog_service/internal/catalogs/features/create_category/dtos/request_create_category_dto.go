package dtos

//https://echo.labstack.com/guide/response/

type CreateCategoryRequestDto struct {
	Name         string `form:"name"`
	ParentId    *int `form:"parent_id"`
}
