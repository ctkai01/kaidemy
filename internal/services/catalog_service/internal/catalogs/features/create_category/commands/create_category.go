package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type CreateCategory struct {
	IdUser   int
	Name     string `validate:"required,gte=1,lte=60"`
	ParentId *int   `validate:"omitempty,numeric"`
}

func NewCreateCategory(idUser int, name string, parentId *int) (*CreateCategory, *validator.ValidateError) {
	command := &CreateCategory{idUser, name, parentId}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
