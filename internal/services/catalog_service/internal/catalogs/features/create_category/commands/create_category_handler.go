package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/create_category/dtos"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/models"

	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	userservice "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type CreateCategoryHandler struct {
	log                logger.Logger
	categoryRepository data.CategoryRepository
	grpcClient         grpc.GrpcClient
}

func NewCreateCategoryHandler(
	log logger.Logger,
	categoryRepository data.CategoryRepository,
	grpcClient grpc.GrpcClient,

) *CreateCategoryHandler {
	return &CreateCategoryHandler{log: log, categoryRepository: categoryRepository, grpcClient: grpcClient}
}

func (c *CreateCategoryHandler) Handle(ctx context.Context, command *CreateCategory) (*dtos.CreateCategoryResponseDto, error) {
	conn, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}

	client := userservice.NewUsersServiceClient(conn)
	dataUser, err := client.GetUserByID(context.Background(), &userservice.GetUserByIdReq{
		UserId: int32(command.IdUser),
	})

	if err != nil {
		return nil, err
	}

	if dataUser.User == nil {
		return nil, customErrors.ErrUserNotFound
	}

	if command.ParentId != nil {
		category, err := c.categoryRepository.GetCategoryByIDRelation(ctx, nil, *command.ParentId)

		if err != nil {
			return nil, err
		}

		if category == nil {
			return nil, customErrors.ErrParentCategoryNotFound
		}
	}

	categoryCreate := &models.Category{
		ParentID: command.ParentId,
		Name:     command.Name,
	}

	if err := c.categoryRepository.CreateCategory(ctx, categoryCreate); err != nil {
		return nil, err
	}

	return &dtos.CreateCategoryResponseDto{
		Message:  "Create category successfully!",
		Category: *categoryCreate,
	}, nil
}
