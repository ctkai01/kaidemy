package dtos

//https://echo.labstack.com/guide/response/

type CreateLanguageRequestDto struct {
	Name  string `form:"name"`
}
