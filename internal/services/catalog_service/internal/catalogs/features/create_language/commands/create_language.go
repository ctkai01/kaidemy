package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type CreateLanguage struct {
	IdUser int
	Name   string `validate:"required,gte=1,lte=60"`
}

func NewCreateLanguage(idUser int, name string) (*CreateLanguage, *validator.ValidateError) {
	command := &CreateLanguage{idUser, name}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
