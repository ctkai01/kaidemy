package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/create_language/dtos"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/models"

	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	userservice "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/grpc/proto/service_clients"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type CreateLanguageHandler struct {
	log                logger.Logger
	languageRepository data.LanguageRepository
	grpcClient         grpc.GrpcClient
}

func NewCreateLanguageHandler(
	log logger.Logger,
	languageRepository data.LanguageRepository,
	grpcClient grpc.GrpcClient,

) *CreateLanguageHandler {
	return &CreateLanguageHandler{log: log, languageRepository: languageRepository, grpcClient: grpcClient}
}

func (c *CreateLanguageHandler) Handle(ctx context.Context, command *CreateLanguage) (*dtos.CreateLanguageResponseDto, error) {
	conn, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}

	client := userservice.NewUsersServiceClient(conn)
	dataUser, err := client.GetUserByID(context.Background(), &userservice.GetUserByIdReq{
		UserId: int32(command.IdUser),
	})

	if err != nil {
		return nil, err
	}

	if dataUser.User == nil {
		return nil, customErrors.ErrUserNotFound
	}

	languageCreate := &models.Language{
		Name: command.Name,
	}

	if err := c.languageRepository.CreateLanguage(ctx, languageCreate); err != nil {
		return nil, err
	}

	return &dtos.CreateLanguageResponseDto{
		Message:  "Create language successfully!",
		Language: *languageCreate,
	}, nil
}
