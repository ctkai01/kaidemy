package grpc

import (
	"context"
	"fmt"

	// productsService
	"emperror.dev/errors"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/mapper"

	catalogsError "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/data/errors"

	getCategoryByIdDto "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_category_by_id/dtos"
	getCategoryIdQuery "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_category_by_id/queries"

	getLevelByIdDto "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_level_by_id/dtos"
	getLevelIdQuery "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_level_by_id/queries"

	getPriceByIdDto "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_price_by_id/dtos"
	getPriceIdQuery "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_price_by_id/queries"

	getLanguageByIdDto "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_language_by_id/dtos"
	getLanguageIdQuery "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_language_by_id/queries"

	getIssueTypeByIdDto "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_issue_type_by_id/dtos"
	getIssueTypeIdQuery "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_issue_type_by_id/queries"


	catalogsService "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/grpc/proto/service_clients"
)

type CatalogGrpcServiceServer struct {
	logger logger.Logger
}

func NewCatalogGrpcService(
	logger logger.Logger,

) *CatalogGrpcServiceServer {

	return &CatalogGrpcServiceServer{
		logger,
	}
}

func (s *CatalogGrpcServiceServer) GetCategoryByID(ctx context.Context, req *catalogsService.GetCategoryByIdReq) (*catalogsService.GetCategoryByIdRes, error) {
	query, errValidate := getCategoryIdQuery.NewGetCategoryById(
		int(req.CategoryId),
	)
	if errValidate != nil && errValidate.ErrorType != nil {

		validationErr := customErrors.NewValidationErrorWrap(
			errValidate.ErrorType,
			"[getCategoryById_handler.StructCtx] command validation failed",
		)
		s.logger.Errorf(
			fmt.Sprintf("[getCategoryById_handler.StructCtx] err: {%v}", validationErr),
		)

		return nil, errValidate.ErrorType
	}

	result, err := mediatr.Send[*getCategoryIdQuery.GetCategoryById, *getCategoryByIdDto.GetCategoryByIdResponseDto](
		ctx,
		query,
	)
	fmt.Println("Error: ", err)
	if err != nil {
		if err.Error() == catalogsError.WrapError(customErrors.ErrCategoryNotFound).Error() {
			s.logger.Error(
				fmt.Sprintf(
					"[CategoryGrpcServiceServer_GetCategoryById.Send] id: {%d}, err: %v",
					query.IdCategory,
					err,
				),
			)
			return nil, customErrors.ErrCategoryNotFound
		}
		err = errors.WithMessage(
			err,
			"[CategoryGrpcServiceServer_GetCategoryById.Send] error in sending GetCategoryById",
		)
		s.logger.Errorw(
			fmt.Sprintf(
				"[CategoryGrpcServiceServer_GetCategoryById.Send] id: {%d}, err: %v",
				query.IdCategory,
				err,
			),
			logger.Fields{"CategoryId": query.IdCategory},
		)
		return nil, err
	}
	fmt.Println("dsds", result.Category)
	category, err := mapper.Map[*catalogsService.Category](result.Category)
	if err != nil {
		err = errors.WithMessage(
			err,
			"[CategoryGrpcServiceServer_GetCategoryById.Map] error in mapping user",
		)
		return nil, err
	}

	return &catalogsService.GetCategoryByIdRes{
		Category: category,
	}, nil
}

func (s *CatalogGrpcServiceServer) GetLevelByID(ctx context.Context, req *catalogsService.GetLevelByIdReq) (*catalogsService.GetLevelByIdRes, error) {
	query, errValidate := getLevelIdQuery.NewGetLevelById(
		int(req.LevelId),
	)
	if errValidate != nil && errValidate.ErrorType != nil {

		validationErr := customErrors.NewValidationErrorWrap(
			errValidate.ErrorType,
			"[getLevelById_handler.StructCtx] command validation failed",
		)
		s.logger.Errorf(
			fmt.Sprintf("[getLevelById_handler.StructCtx] err: {%v}", validationErr),
		)

		return nil, errValidate.ErrorType
	}

	result, err := mediatr.Send[*getLevelIdQuery.GetLevelById, *getLevelByIdDto.GetLevelByIdResponseDto](
		ctx,
		query,
	)
	fmt.Println("Error: ", err)
	if err != nil {
		if err.Error() == catalogsError.WrapError(customErrors.ErrCategoryNotFound).Error() {
			s.logger.Error(
				fmt.Sprintf(
					"[LevelGrpcServiceServer_GetLevelById.Send] id: {%d}, err: %v",
					query.IdLevel,
					err,
				),
			)
			return nil, customErrors.ErrLevelNotFound
		}
		err = errors.WithMessage(
			err,
			"[LevelGrpcServiceServer_GetLevelById.Send] error in sending GetLevelById",
		)
		s.logger.Errorw(
			fmt.Sprintf(
				"[LevelGrpcServiceServer_GetLevelById.Send] id: {%d}, err: %v",
				query.IdLevel,
				err,
			),
			logger.Fields{"LevelId": query.IdLevel},
		)
		return nil, err
	}
	fmt.Println(result.Level)
	level, err := mapper.Map[*catalogsService.Level](result.Level)
	if err != nil {
		err = errors.WithMessage(
			err,
			"[LevelGrpcServiceServer_GetLevelById.Map] error in mapping user",
		)
		return nil, err
	}

	return &catalogsService.GetLevelByIdRes{
		Level: level,
	}, nil
}

func (s *CatalogGrpcServiceServer) GetPriceByID(ctx context.Context, req *catalogsService.GetPriceByIdReq) (*catalogsService.GetPriceByIdRes, error) {
	query, errValidate := getPriceIdQuery.NewGetPriceById(
		int(req.PriceId),
	)
	if errValidate != nil && errValidate.ErrorType != nil {

		validationErr := customErrors.NewValidationErrorWrap(
			errValidate.ErrorType,
			"[getPriceById_handler.StructCtx] command validation failed",
		)
		s.logger.Errorf(
			fmt.Sprintf("[getPriceById_handler.StructCtx] err: {%v}", validationErr),
		)

		return nil, errValidate.ErrorType
	}

	result, err := mediatr.Send[*getPriceIdQuery.GetPriceById, *getPriceByIdDto.GetPriceByIdResponseDto](
		ctx,
		query,
	)
	fmt.Println("Error: ", err)
	if err != nil {
		if err.Error() == catalogsError.WrapError(customErrors.ErrCategoryNotFound).Error() {
			s.logger.Error(
				fmt.Sprintf(
					"[PriceGrpcServiceServer_GetPriceById.Send] id: {%d}, err: %v",
					query.IdPrice,
					err,
				),
			)
			return nil, customErrors.ErrLevelNotFound
		}
		err = errors.WithMessage(
			err,
			"[PriceGrpcServiceServer_GetPriceById.Send] error in sending GetLevelById",
		)
		s.logger.Errorw(
			fmt.Sprintf(
				"[PriceGrpcServiceServer_GetPriceById.Send] id: {%d}, err: %v",
				query.IdPrice,
				err,
			),
			logger.Fields{"PriceId": query.IdPrice},
		)
		return nil, err
	}
	fmt.Println(result.Price)
	price, err := mapper.Map[*catalogsService.Price](result.Price)
	if err != nil {
		err = errors.WithMessage(
			err,
			"[PriceGrpcServiceServer_GetPriceById.Map] error in mapping user",
		)
		return nil, err
	}

	return &catalogsService.GetPriceByIdRes{
		Price: price,
	}, nil
}

func (s *CatalogGrpcServiceServer) GetLanguageByID(ctx context.Context, req *catalogsService.GetLanguageByIdReq) (*catalogsService.GetLanguageByIdRes, error) {
	query, errValidate := getLanguageIdQuery.NewGetLanguageById(
		// int(req.UserId),
		int(req.LanguageId),
	)
	if errValidate != nil && errValidate.ErrorType != nil {

		validationErr := customErrors.NewValidationErrorWrap(
			errValidate.ErrorType,
			"[getLanguageById_handler.StructCtx] command validation failed",
		)
		s.logger.Errorf(
			fmt.Sprintf("[getLanguageById_handler.StructCtx] err: {%v}", validationErr),
		)

		return nil, errValidate.ErrorType
	}

	result, err := mediatr.Send[*getLanguageIdQuery.GetLanguageById, *getLanguageByIdDto.GetLanguageByIdResponseDto](
		ctx,
		query,
	)
	fmt.Println("Error: ", err)
	if err != nil {
		if err.Error() == catalogsError.WrapError(customErrors.ErrCategoryNotFound).Error() {
			s.logger.Error(
				fmt.Sprintf(
					"[LanguageGrpcServiceServer_GetLanguageById.Send] id: {%d}, err: %v",
					query.IdLanguage,
					err,
				),
			)
			return nil, customErrors.ErrLevelNotFound
		}
		err = errors.WithMessage(
			err,
			"[LanguageGrpcServiceServer_GetLanguageById.Send] error in sending GetLevelById",
		)
		s.logger.Errorw(
			fmt.Sprintf(
				"[LanguageGrpcServiceServer_GetLanguageById.Send] id: {%d}, err: %v",
				query.IdLanguage,
				err,
			),
			logger.Fields{"LanguageId": query.IdLanguage},
		)
		return nil, err
	}
	fmt.Println("Language: ", result.Language)
	language, err := mapper.Map[*catalogsService.Language](result.Language)
	if err != nil {
		err = errors.WithMessage(
			err,
			"[LanguageGrpcServiceServer_GetLanguageById.Map] error in mapping user",
		)
		return nil, err
	}

	return &catalogsService.GetLanguageByIdRes{
		Language: language,
	}, nil
}

func (s *CatalogGrpcServiceServer) GetIssueTypeByID(ctx context.Context, req *catalogsService.GetIssueTypeByIdReq) (*catalogsService.GetIssueTypeByIdRes, error) {
	query, errValidate := getIssueTypeIdQuery.NewGetIssueTypeById(
		// int(req.UserId),
		int(req.IssueTypeId),
	)
	if errValidate != nil && errValidate.ErrorType != nil {

		validationErr := customErrors.NewValidationErrorWrap(
			errValidate.ErrorType,
			"[getIssueTypeById_handler.StructCtx] command validation failed",
		)
		s.logger.Errorf(
			fmt.Sprintf("[getIssueTypeById_handler.StructCtx] err: {%v}", validationErr),
		)

		return nil, errValidate.ErrorType
	}

	result, err := mediatr.Send[*getIssueTypeIdQuery.GetIssueTypeById, *getIssueTypeByIdDto.GetIssueTypeByIdResponseDto](
		ctx,
		query,
	)
	fmt.Println("Error: ", err)
	if err != nil {
		if err.Error() == catalogsError.WrapError(customErrors.ErrCategoryNotFound).Error() {
			s.logger.Error(
				fmt.Sprintf(
					"[IssueTypeGrpcServiceServer_GetIssueTypeById.Send] id: {%d}, err: %v",
					query.IdIssueType,
					err,
				),
			)
			return nil, customErrors.ErrLevelNotFound
		}
		err = errors.WithMessage(
			err,
			"[IssueTypeGrpcServiceServer_GetIssueTypeById.Send] error in sending GetIssueTypeById",
		)
		s.logger.Errorw(
			fmt.Sprintf(
				"[LevelGrpcServiceServer_GetLevelById.Send] id: {%d}, err: %v",
				query.IdIssueType,
				err,
			),
			logger.Fields{"IssueTypeId": query.IdIssueType},
		)
		return nil, err
	}
	fmt.Println(result.IssueType)
	issueType, err := mapper.Map[*catalogsService.IssueType](result.IssueType)
	if err != nil {
		err = errors.WithMessage(
			err,
			"[IssueTypeGrpcServiceServer_GetIssueTypeById.Map] error in mapping user",
		)
		return nil, err
	}

	return &catalogsService.GetIssueTypeByIdRes{
		IssueType: issueType,
	}, nil
}