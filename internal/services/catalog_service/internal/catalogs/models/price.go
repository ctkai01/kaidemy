package models

import (
	"time"
)

type Price struct {
	ID            int        `json:"id" gorm:"primary_key"`
	Tier          string     `json:"tier" gorm:"type:varchar(120);unique;not null"`
	Value         int        `json:"value" gorm:"type:int;not null"`
	UpdatedAt     *time.Time `json:"updated_at" gorm:"column:updated_at"`
	CreatedAt     *time.Time `json:"created_at" gorm:"column:created_at"`
}

func (Price) TableName() string { return "prices" }

// type ProductsList struct {
// 	TotalCount int64      `json:"totalCount" bson:"totalCount"`
// 	TotalPages int64      `json:"totalPages" bson:"totalPages"`
// 	Page       int64      `json:"page" bson:"page"`
// 	Size       int64      `json:"size" bson:"size"`
// 	Products   []*User `json:"products" bson:"products"`
// }
