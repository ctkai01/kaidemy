package models

import (
	"time"
)

type IssueType struct {
	ID        int        `json:"id" gorm:"primary_key"`
	Name      string     `json:"name" gorm:"type:varchar(120);unique;not null"`
	UpdatedAt *time.Time `json:"updated_at" gorm:"column:updated_at"`
	CreatedAt *time.Time `json:"created_at" gorm:"column:created_at"`
}

func (IssueType) TableName() string { return "issue_types" }
