package models

import (
	"time"
)

type Category struct {
	ID        int         `json:"id" gorm:"primary_key"`
	Name      string        `json:"name" gorm:"type:varchar(120);not null"`
	ParentID  *int         `json:"parent_id" gorm:"type:int"`
	Parent    *Category   `json:"parent" gorm:"foreignkey:ParentID"` // Parent relationship
	Children  []Category  `json:"children" gorm:"foreignkey:ParentID"` // Children relationship

	UpdatedAt *time.Time `json:"updated_at" gorm:"column:updated_at"`
	CreatedAt *time.Time `json:"created_at" gorm:"column:created_at"`
}

type MenuCategory struct {
	Name	string  `json:"name"`
	ParentID *int `json:"parent_id"`
	Children []MenuCategory `json:"children"`
	GroupName *string `json:"groupName"`
}
type MenuClass map[string]string

func (Category) TableName() string { return "categories" }

// type ProductsList struct {
// 	TotalCount int64      `json:"totalCount" bson:"totalCount"`
// 	TotalPages int64      `json:"totalPages" bson:"totalPages"`
// 	Page       int64      `json:"page" bson:"page"`
// 	Size       int64      `json:"size" bson:"size"`
// 	Products   []*User `json:"products" bson:"products"`
// }
