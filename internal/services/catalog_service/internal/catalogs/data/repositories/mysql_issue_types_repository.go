package repositories

// https://github.com/Kamva/mgm
// https://github.com/mongodb/mongo-go-driver
// https://blog.logrocket.com/how-to-use-mongodb-with-go/
// https://www.mongodb.com/docs/drivers/go/current/quick-reference/
// https://www.mongodb.com/docs/drivers/go/current/fundamentals/bson/
// https://www.mongodb.com/docs

import (
	"context"
	// "errors"
	// "fmt"

	// "errors"

	// "fmt"

	"emperror.dev/errors"
	// uuid2 "github.com/satori/go.uuid"
	"gorm.io/gorm"

	// "go.mongodb.org/mongo-driver/mongo"
	// attribute2 "go.opentelemetry.io/otel/attribute"

	data2 "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/models"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/core/data"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/core/data"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/gorm_mysql/repository"

	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/mongodb"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/mongodb/repository"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/otel/tracing"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	catalogErrors "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/data/errors"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/otel/tracing/attribute"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
)

// const (
// 	productCollection = "products"
// )

type mysqlIssueTypeRepository struct {
	log                   logger.Logger
	gormGenericRepository data.GenericRepository[*models.IssueType]
}

func NewMySqlIssueTypeRepository(
	log logger.Logger,
	db *gorm.DB,
) data2.IssueTypeRepository {
	gormRepository := repository.NewGenericGormRepository[*models.IssueType](db)
	return &mysqlIssueTypeRepository{
		log:                   log,
		gormGenericRepository: gormRepository,
	}
}

func (p *mysqlIssueTypeRepository) CreateIssueType(
	ctx context.Context,
	issueType *models.IssueType,
) error {
	err := p.gormGenericRepository.Add(ctx, issueType)

	if err != nil {
		if catalogErrors.CheckDuplicateIssueType(err) {
			return catalogErrors.ErrAlreadyExistIssueType
		}
		return err
	}

	return nil
}

func (p *mysqlIssueTypeRepository) GetIssueTypeByID(
	ctx context.Context,
	id int,
) (*models.IssueType, error) {
	issueType, err := p.gormGenericRepository.GetById(ctx, id)

	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return nil, customErrors.ErrIssueTypeNotFound
		}
		return nil, err
	}

	return issueType, nil
}

func (p *mysqlIssueTypeRepository) UpdateIssueType(ctx context.Context, issueType *models.IssueType) error {
	err := p.gormGenericRepository.Update(ctx, issueType)

	if err != nil {
		if catalogErrors.CheckDuplicateIssueType(err) {
			return catalogErrors.ErrAlreadyExistIssueType
		}
		return err
	}

	return nil
}

func (p *mysqlIssueTypeRepository) DeleteIssueTypeByID(ctx context.Context, id int) error {

	err := p.gormGenericRepository.Delete(ctx, id)
	if err != nil {
		return err

	}

	return nil
}

func (p *mysqlIssueTypeRepository) GetAllIssueTypes(
	ctx context.Context,
	listQuery *utils.ListQuery,
) (*utils.ListResult[*models.IssueType], error) {

	result, err := p.gormGenericRepository.GetAll(ctx, listQuery)
	if err != nil {
		return nil, errors.WrapIf(
			err,
			"[mysqlIssueTypeRepository_GetAllIssueTypes.Paginate] error in the paginate",
		)
	}

	p.log.Infow(
		"[mysqlIssueTypeRepository_GetAllIssueTypes] IssueTypes loaded",
		logger.Fields{"IssueTypesResult": result},
	)

	return result, nil
}

// func (p *mysqlPriceRepository) GetChildrenCategoryByID(ctx context.Context, id int) ([]models.Category, error) {

// 	category, err := p.gormGenericRepository.GetRelatedByID(ctx, []string{"Children"}, id)
// 	if err != nil {
// 		return nil, errors.WrapIf(err, fmt.Sprintf(
// 			"[mysqlPriceRepository_GetChildrenCategoryById] error in the children category with id %d into the database.",
// 			id,
// 		))
// 	}

// 	return category.Children, nil
// }

// func (p *mysqlPriceRepository) GetAllCategories(
// 	ctx context.Context,
// 	listQuery *utils.ListQuery,
// ) (*utils.ListResult[*models.Category], error) {
// 	fmt.Println("In query: ", listQuery)

// 	result, err := p.gormGenericRepository.GetAll(ctx, listQuery)
// 	if err != nil {
// 		return nil,errors.WrapIf(
// 				err,
// 				"[mysqlPriceRepository_GetAllCategories.Paginate] error in the paginate",
// 			)
// 	}
// 	fmt.Println("In query: ", result)

// 	p.log.Infow(
// 		"[mysqlPriceRepository_GetAllCategories] categories loaded",
// 		logger.Fields{"CategoriesResult": result},
// 	)

// 	return result, nil
// }
// if err := mysqlRepo.db.Table(user.TableName()).Where("email = ?", email).First(&user).Error; err != nil {
// 	fmt.Println(err)
// 	if err == gorm.ErrRecordNotFound {
// 		return nil, models.ErrUserNotFound
// 	}
// 	return nil, err
// }
// return user, nil
// ctx, span := p.tracer.Start(ctx, "postgresProductRepository.CreateProduct")
// defer span.End()

// err := p.gormGenericRepository.Add(ctx, product)
// if err != nil {
// 	return nil, tracing.TraceErrFromSpan(
// 		span,
// 		errors.WrapIf(
// 			err,
// 			"[postgresProductRepository_CreateProduct.Create] error in the inserting product into the database.",
// 		),
// 	)
// }

// return nil, nil
// }
// func (p *mysqlUserRepository) GetAllProducts(
// 	ctx context.Context,
// 	listQuery *utils.ListQuery,
// ) (*utils.ListResult[*models.User], error) {
// 	ctx, span := p.tracer.Start(ctx, "postgresProductRepository.GetAllProducts")
// 	defer span.End()

// 	result, err := p.gormGenericRepository.GetAll(ctx, listQuery)
// 	if err != nil {
// 		return nil, tracing.TraceErrFromContext(
// 			ctx,
// 			errors.WrapIf(
// 				err,
// 				"[postgresProductRepository_GetAllProducts.Paginate] error in the paginate",
// 			),
// 		)
// 	}

// 	p.log.Infow(
// 		"[postgresProductRepository.GetAllProducts] products loaded",
// 		logger.Fields{"ProductsResult": result},
// 	)
// 	span.SetAttributes(attribute.Object("ProductsResult", result))

// 	return result, nil
// }

// func (p *mysqlUserRepository) SearchProducts(
// 	ctx context.Context,
// 	searchText string,
// 	listQuery *utils.ListQuery,
// ) (*utils.ListResult[*models.User], error) {
// 	ctx, span := p.tracer.Start(ctx, "postgresProductRepository.SearchProducts")
// 	span.SetAttributes(attribute2.String("SearchText", searchText))
// 	defer span.End()

// 	result, err := p.gormGenericRepository.Search(ctx, searchText, listQuery)
// 	if err != nil {
// 		return nil, tracing.TraceErrFromContext(
// 			ctx,
// 			errors.WrapIf(
// 				err,
// 				"[postgresProductRepository_SearchProducts.Paginate] error in the paginate",
// 			),
// 		)
// 	}

// 	p.log.Infow(
// 		fmt.Sprintf(
// 			"[postgresProductRepository.SearchProducts] products loaded for search term '%s'",
// 			searchText,
// 		),
// 		logger.Fields{"ProductsResult": result},
// 	)
// 	span.SetAttributes(attribute.Object("ProductsResult", result))

// 	return result, nil
// }

// 	span.SetAttributes(attribute.Object("Product", product))
// 	p.log.Infow(
// 		fmt.Sprintf(
// 			"[postgresProductRepository.GetProductById] product with id %s laoded",
// 			uuid.String(),
// 		),
// 		logger.Fields{"Product": product, "ProductId": uuid},
// 	)

// 	return product, nil
// }

// 	span.SetAttributes(attribute.Object("Product", product))
// 	p.log.Infow(
// 		fmt.Sprintf(
// 			"[postgresProductRepository.CreateProduct] product with id '%s' created",
// 			product.ProductId,
// 		),
// 		logger.Fields{"Product": product, "ProductId": product.ProductId},
// 	)

// 	return product, nil
// }

// func (p *mysqlUserRepository) UpdateProduct(
// 	ctx context.Context,
// 	updateProduct *models.User,
// ) (*models.User, error) {
// 	ctx, span := p.tracer.Start(ctx, "postgresProductRepository.UpdateProduct")
// 	defer span.End()

// 	err := p.gormGenericRepository.Update(ctx, updateProduct)
// 	if err != nil {
// 		return nil, tracing.TraceErrFromSpan(
// 			span,
// 			errors.WrapIf(
// 				err,
// 				fmt.Sprintf(
// 					"[postgresProductRepository_UpdateProduct.Save] error in updating product with id %s into the database.",
// 					updateProduct.ProductId,
// 				),
// 			),
// 		)
// 	}

// 	span.SetAttributes(attribute.Object("Product", updateProduct))
// 	p.log.Infow(
// 		fmt.Sprintf(
// 			"[postgresProductRepository.UpdateProduct] product with id '%s' updated",
// 			updateProduct.ProductId,
// 		),
// 		logger.Fields{"Product": updateProduct, "ProductId": updateProduct.ProductId},
// 	)

// 	return updateProduct, nil
// }
