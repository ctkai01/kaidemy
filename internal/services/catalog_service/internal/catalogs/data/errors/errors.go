package errors

import (
	"strings"

	"emperror.dev/errors"
)

func CheckDuplicateTire(err error) bool {
	return strings.Contains(err.Error(), "Duplicate")
}

func CheckDuplicateLevel(err error) bool {
	return strings.Contains(err.Error(), "Duplicate")
}

func CheckDuplicateIssueType(err error) bool {
	return strings.Contains(err.Error(), "Duplicate")
}

var (
	ErrAlreadyExistTier = errors.New("tier already existed")
	ErrAlreadyExistLevel = errors.New("level already existed")
	ErrAlreadyExistLanguage = errors.New("language already existed")
	ErrAlreadyExistIssueType = errors.New("issue type already existed")
)

func WrapError(err error) error {
	return errors.Wrap(err, "error handling request")
}
