package mediator

import (
	// "emperror.dev/errors"
	// "github.com/mehdihadeli/go-mediatr"

	// "github.com/mehdihadeli/go-mediatr"
	"github.com/mehdihadeli/go-mediatr"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/data"

	createCategoryCommand "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/create_category/commands"
	createCategoryDtos "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/create_category/dtos"

	updateCategoryCommand "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/update_category/commands"
	updateCategoryDtos "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/update_category/dtos"

	deleteCategoryCommand "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/delete_category/commands"

	getCategoryByIdDtos "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_category_by_id/dtos"
	getCategoryByIdQuery "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_category_by_id/queries"

	getCategoriesDtos "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_categories/dtos"
	getCategoriesQuery "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_categories/queries"

	createPriceCommand "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/create_price/commands"
	createPriceDtos "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/create_price/dtos"

	updatePriceCommand "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/update_price/commands"
	updatePriceDtos "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/update_price/dtos"

	deletePriceCommand "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/delete_price/commands"
	deletePriceDtos "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/delete_price/dtos"

	getPricesDtos "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_prices/dtos"
	getPricesQuery "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_prices/queries"

	getPriceByIdDtos "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_price_by_id/dtos"
	getPriceByIdQuery "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_price_by_id/queries"

	createLevelCommand "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/create_level/commands"
	createLevelDtos "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/create_level/dtos"

	updateLevelCommand "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/update_level/commands"
	updateLevelDtos "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/update_level/dtos"

	deleteLevelCommand "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/delete_level/commands"
	deleteLevelDtos "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/delete_level/dtos"

	getLevelsDtos "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_levels/dtos"
	getLevelsQuery "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_levels/queries"

	getLevelByIdDtos "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_level_by_id/dtos"
	getLevelByIdQuery "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_level_by_id/queries"

	createLanguageCommand "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/create_language/commands"
	createLanguageDtos "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/create_language/dtos"

	updateLanguageCommand "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/update_language/commands"
	updateLanguageDtos "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/update_language/dtos"

	deleteLanguageCommand "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/delete_language/commands"
	deleteLanguageDtos "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/delete_language/dtos"

	getLanguageByIdDtos "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_language_by_id/dtos"
	getLanguageByIdQuery "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_language_by_id/queries"

	getLanguagesDtos "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_languages/dtos"
	getLanguagesQuery "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_languages/queries"

	createIssueTypeCommand "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/create_issue_type/commands"
	createIssueTypeDtos "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/create_issue_type/dtos"

	updateIssueTypeCommand "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/update_issue_type/commands"
	updateIssueTypeDtos "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/update_issue_type/dtos"

	deleteIssueTypeCommand "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/delete_issue_type/commands"
	deleteIssueTypeDtos "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/delete_issue_type/dtos"

	getIssueTypesDtos "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_issue_types/dtos"
	getIssueTypesQuery "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_issue_types/queries"

	getIssueTypesByIdDtos "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_issue_type_by_id/dtos"
	getIssueTypesByIdQuery "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_issue_type_by_id/queries"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
)

// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"

func ConfigCatalogMediator(
	logger logger.Logger,
	categoryRepository data.CategoryRepository,
	priceRepository data.PriceRepository,
	levelRepository data.LevelRepository,
	languageRepository data.LanguageRepository,
	issueTypeRepository data.IssueTypeRepository,
	grpcClient grpc.GrpcClient,
) error {
	//Category
	err := mediatr.RegisterRequestHandler[*createCategoryCommand.CreateCategory, *createCategoryDtos.CreateCategoryResponseDto](
		createCategoryCommand.NewCreateCategoryHandler(logger, categoryRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*updateCategoryCommand.UpdateCategory, *updateCategoryDtos.UpdateCategoryResponseDto](
		updateCategoryCommand.NewUpdateCategoryHandler(logger, categoryRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*deleteCategoryCommand.DeleteCategory, *mediatr.Unit](
		deleteCategoryCommand.NewDeleteCategoryHandler(logger, categoryRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getCategoryByIdQuery.GetCategoryById, *getCategoryByIdDtos.GetCategoryByIdResponseDto](
		getCategoryByIdQuery.NewGetCategoryByIdHandler(logger, categoryRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getCategoriesQuery.GetCategories, *getCategoriesDtos.GetCategoriesResponseDto](
		getCategoriesQuery.NewGetCategoriesHandler(logger, categoryRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	//Price
	err = mediatr.RegisterRequestHandler[*createPriceCommand.CreatePrice, *createPriceDtos.CreatePriceResponseDto](
		createPriceCommand.NewCreatePriceHandler(logger, priceRepository, grpcClient),
	)
	if err != nil {
		return err
	}
	// --- Update ----
	err = mediatr.RegisterRequestHandler[*updatePriceCommand.UpdatePrice, *updatePriceDtos.UpdatePriceResponseDto](
		updatePriceCommand.NewUpdatePriceHandler(logger, priceRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*deletePriceCommand.DeletePrice, *deletePriceDtos.DeletePriceResponseDto](
		deletePriceCommand.NewDeletePriceHandler(logger, priceRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getPricesQuery.GetPrices, *getPricesDtos.GetPricesResponseDto](
		getPricesQuery.NewGetPricesHandler(logger, priceRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getPriceByIdQuery.GetPriceById, *getPriceByIdDtos.GetPriceByIdResponseDto](
		getPriceByIdQuery.NewGetPriceByIdHandler(logger, priceRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	// Level
	err = mediatr.RegisterRequestHandler[*createLevelCommand.CreateLevel, *createLevelDtos.CreateLevelResponseDto](
		createLevelCommand.NewCreateLevelHandler(logger, levelRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*updateLevelCommand.UpdateLevel, *updateLevelDtos.UpdateLevelResponseDto](
		updateLevelCommand.NewUpdatePriceHandler(logger, levelRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*deleteLevelCommand.DeleteLevel, *deleteLevelDtos.DeleteLevelResponseDto](
		deleteLevelCommand.NewDeleteLevelHandler(logger, levelRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getLevelsQuery.GetLevels, *getLevelsDtos.GetLevelsResponseDto](
		getLevelsQuery.NewGetLevelsHandler(logger, levelRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getLevelByIdQuery.GetLevelById, *getLevelByIdDtos.GetLevelByIdResponseDto](
		getLevelByIdQuery.NewGetLevelByIdHandler(logger, levelRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	// Language
	err = mediatr.RegisterRequestHandler[*createLanguageCommand.CreateLanguage, *createLanguageDtos.CreateLanguageResponseDto](
		createLanguageCommand.NewCreateLanguageHandler(logger, languageRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*updateLanguageCommand.UpdateLanguage, *updateLanguageDtos.UpdateLanguageResponseDto](
		updateLanguageCommand.NewUpdateLanguageHandler(logger, languageRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*deleteLanguageCommand.DeleteLanguage, *deleteLanguageDtos.DeleteLanguageResponseDto](
		deleteLanguageCommand.NewDeleteLanguageHandler(logger, languageRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getLanguageByIdQuery.GetLanguageById, *getLanguageByIdDtos.GetLanguageByIdResponseDto](
		getLanguageByIdQuery.NewGetLanguageByIdHandler(logger, languageRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getLanguagesQuery.GetLanguages, *getLanguagesDtos.GetLanguagesResponseDto](
		getLanguagesQuery.NewGetLanguagesHandler(logger, languageRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	// Issue Type
	err = mediatr.RegisterRequestHandler[*createIssueTypeCommand.CreateIssueType, *createIssueTypeDtos.CreateIssueTypeResponseDto](
		createIssueTypeCommand.NewCreateIssueTypeHandler(logger, issueTypeRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*updateIssueTypeCommand.UpdateIssueType, *updateIssueTypeDtos.UpdateIssueTypeResponseDto](
		updateIssueTypeCommand.NewUpdateIssueTypeHandler(logger, issueTypeRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*deleteIssueTypeCommand.DeleteIssueType, *deleteIssueTypeDtos.DeleteIssueTypeResponseDto](
		deleteIssueTypeCommand.NewDeleteIssueTypeHandler(logger, issueTypeRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getIssueTypesQuery.GetIssueTypes, *getIssueTypesDtos.GetIssueTypesResponseDto](
		getIssueTypesQuery.NewGetIssueTypesHandler(logger, issueTypeRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	// Report
	err = mediatr.RegisterRequestHandler[*getIssueTypesByIdQuery.GetIssueTypeById, *getIssueTypesByIdDtos.GetIssueTypeByIdResponseDto](
		getIssueTypesByIdQuery.NewGetLevelByIdHandler(logger, issueTypeRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	return nil
}
