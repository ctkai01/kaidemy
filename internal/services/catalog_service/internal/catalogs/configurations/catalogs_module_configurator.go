package configurations

import (
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/configurations/mappings"
	// "golang.org/x/oauth2"

	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/configurations/mediator"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/data"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp/contracts"
	// googleGrpc "gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	grpcServer "gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	logger2 "gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	googleGrpc "google.golang.org/grpc"

	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/grpc"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/otel/tracing"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/configurations/mediator"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/contracts/data"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	catalogsservice "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/grpc/proto/service_clients"
)

type CatalogsModuleConfigurator struct {
	contracts.Application
}

func NewCatalogsModuleConfigurator(
	app contracts.Application,
) *CatalogsModuleConfigurator {
	return &CatalogsModuleConfigurator{
		Application: app,
	}
}

func (c *CatalogsModuleConfigurator) ConfigureCatalogsModule() {
	c.ResolveFunc(
		func(logger logger2.Logger, categoryRepository data.CategoryRepository, priceRepository data.PriceRepository, levelRepository data.LevelRepository, languageRepository data.LanguageRepository, issueTypeRepository data.IssueTypeRepository, grpcClient grpcServer.GrpcClient) error {
			// Config Catalogs Mediators
			err := mediator.ConfigCatalogMediator(logger, categoryRepository, priceRepository, levelRepository, languageRepository, issueTypeRepository, grpcClient)
			if err != nil {
				return err
			}

			// Config Catalogs Mappings
			err = mappings.ConfigureCatalogsMappings()
			if err != nil {
				return err
			}
			return nil
		},
	)
}

func (c *CatalogsModuleConfigurator) MapCatalogsEndpoints() {
	// Config Course Http Endpoints
	c.ResolveFuncWithParamTag(func(endpoints []route.Endpoint) {
		for _, endpoint := range endpoints {
			endpoint.MapEndpoint()
		}
	}, `group:"catalogs-routes"`,
	)

	c.ResolveFunc(
		func(catalogsGrpcServer grpcServer.GrpcServer, grpcService *grpc.CatalogGrpcServiceServer) error {
			catalogsGrpcServer.GrpcServiceBuilder().RegisterRoutes(func(server *googleGrpc.Server) {
				catalogsservice.RegisterCatalogsServiceServer(server, grpcService)
			})

			return nil
		},
	)
}
