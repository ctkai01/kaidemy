package mappings

import (
	catalogsService "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/grpc/proto/service_clients"
	"google.golang.org/protobuf/types/known/timestamppb"

	// "github.com/mehdihadeli/go-ecommerce-microservices/internal/services/catalogreadservice/internal/products/dto"
	// "github.com/mehdihadeli/go-ecommerce-microservices/internal/services/catalogreadservice/internal/products/models"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/mapper"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/models"
)

func ConfigureCatalogsMappings() error {
	// err := mapper.CreateMap[*models.Product, *dto.ProductDto]()
	// if err != nil {
	// 	return err
	// }

	// err = mapper.CreateMap[*models.Product, *models.Product]()
	// if err != nil {
	// 	return err
	// }
	err := mapper.CreateMap[*models.Category, *models.Category]()
	if err != nil {
		return err
	}

	err = mapper.CreateMap[*models.Price, *models.Price]()
	if err != nil {
		return err
	}

	err = mapper.CreateMap[*models.Level, *models.Level]()
	if err != nil {
		return err
	}

	err = mapper.CreateMap[*models.Language, *models.Language]()
	if err != nil {
		return err
	}

	err = mapper.CreateMap[*models.IssueType, *models.IssueType]()
	if err != nil {
		return err
	}

	err = mapper.CreateCustomMap[*models.Language, *catalogsService.Language](
		func(language *models.Language) *catalogsService.Language {
			if language == nil {
				return nil
			}
			return &catalogsService.Language{
				ID:        int32(language.ID),
				Name:      language.Name,
				CreatedAt: timestamppb.New(*language.CreatedAt),
				UpdatedAt: timestamppb.New(*language.UpdatedAt),
			}
		},
	)

	if err != nil {
		return err
	}
	err = mapper.CreateCustomMap[*models.Level, *catalogsService.Level](
		func(level *models.Level) *catalogsService.Level {
			if level == nil {
				return nil
			}
			return &catalogsService.Level{
				ID:        int32(level.ID),
				Name:      level.Name,
				CreatedAt: timestamppb.New(*level.CreatedAt),
				UpdatedAt: timestamppb.New(*level.UpdatedAt),
			}
		},
	)

	if err != nil {
		return err
	}

	err = mapper.CreateCustomMap[*models.Price, *catalogsService.Price](
		func(price *models.Price) *catalogsService.Price {
			if price == nil {
				return nil
			}
			return &catalogsService.Price{
				ID:        int32(price.ID),
				Tier:      price.Tier,
				Value:     int32(price.Value),
				CreatedAt: timestamppb.New(*price.CreatedAt),
				UpdatedAt: timestamppb.New(*price.UpdatedAt),
			}
		},
	)

	if err != nil {
		return err
	}

	err = mapper.CreateCustomMap[*models.Category, *catalogsService.Category](
		func(category *models.Category) *catalogsService.Category {
			if category == nil {
				return nil
			}
			var parentID int

			if category.ParentID == nil {
				parentID = 0
			} else {
				parentID = *category.ParentID
			}
			return &catalogsService.Category{
				ID:        int32(category.ID),
				Name:      category.Name,
				ParentID:  int32(parentID),
				CreatedAt: timestamppb.New(*category.CreatedAt),
				UpdatedAt: timestamppb.New(*category.UpdatedAt),
			}
		},
	)

	if err != nil {
		return err
	}

	err = mapper.CreateCustomMap[*models.IssueType, *catalogsService.IssueType](
		func(issueType *models.IssueType) *catalogsService.IssueType {
			if issueType == nil {
				return nil
			}
			return &catalogsService.IssueType{
				ID:        int32(issueType.ID),
				Name:      issueType.Name,
				CreatedAt: timestamppb.New(*issueType.CreatedAt),
				UpdatedAt: timestamppb.New(*issueType.UpdatedAt),
			}
		},
	)

	if err != nil {
		return err
	}
	return nil
}
