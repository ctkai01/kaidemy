package catalogs

import (
	// "fmt"

	// customEcho "gitlab.com/ctkai01/kaidemy/internal/pkg/http/custom_echo"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "fmt"

	"github.com/labstack/echo/v4"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/data/repositories"
	"gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/grpc"
	"go.uber.org/fx"

	// "github.com/mehdihadeli/go-ecommerce-microservices/internal/services/catalogreadservice/internal/products/data/repositories"
	// getProductByIdV1 "github.com/mehdihadeli/go-ecommerce-microservices/internal/services/catalogreadservice/internal/products/features/get_product_by_id/v1/endpoints"
	// changePassword "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/change_password/endpoints"
	// forgotPassword "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/forgot_password/endpoints"
	// loginStandard "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login/endpoints"
	// loginByGoogle "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login_by_google/endpoints"
	// register "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/register/endpoints"
	// requestLoginByGoogle "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/endpoints"
	// resetPassword "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/reset_password/endpoints"
	// createCourse "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/"
	createCategory "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/create_category/endpoints"
	deleteCategory "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/delete_category/endpoints"
	getCategories "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_categories/endpoints"
	getCategoryById "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_category_by_id/endpoints"
	updateCategory "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/update_category/endpoints"

	createPrice "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/create_price/endpoints"
	deletePrice "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/delete_price/endpoints"
	getPrice "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_prices/endpoints"
	updatePrice "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/update_price/endpoints"

	createLevel "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/create_level/endpoints"
	deleteLevel "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/delete_level/endpoints"
	getLevels "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_levels/endpoints"
	updateLevel "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/update_level/endpoints"

	createLanguage "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/create_language/endpoints"
	deleteLanguage "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/delete_language/endpoints"
	getLanguages "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_languages/endpoints"
	updateLanguage "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/update_language/endpoints"

	createIssueType "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/create_issue_type/endpoints"
	deleteIssueType "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/delete_issue_type/endpoints"
	getIssueTypes "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/get_issue_types/endpoints"
	updateIssueType "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/update_issue_type/endpoints"

	customEcho "gitlab.com/ctkai01/kaidemy/internal/pkg/http/custom_echo"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/http/middlewares"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	// googleOauth "gitlab.com/ctkai01/kaidemy/internal/pkg/oauth/google"
)

var Module = fx.Module(
	"catalogsfx",

	// Other provides
	// fx.Provide(repositories.NewRedisProductRepository),

	fx.Provide(repositories.NewMySqlCategoryRepository),
	fx.Provide(repositories.NewMySqlPriceRepository),
	fx.Provide(repositories.NewMySqlLevelRepository),
	fx.Provide(repositories.NewMySqlLanguageRepository),
	fx.Provide(repositories.NewMySqlIssueTypeRepository),

	fx.Provide(grpc.NewCatalogGrpcService),

	fx.Provide(fx.Annotate(func(catalogsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		catalogsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			v1.Use(middlewares.AuthMiddleware)
			v1.Use(middlewares.AdminMiddleware)
			group := v1.Group("/categories")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"categories-admin-echo-group"`))),

	fx.Provide(fx.Annotate(func(catalogsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		catalogsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			// v1.Use(middlewares.AuthMiddleware)
			group := v1.Group("/categories")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"categories-echo-group"`))),

	fx.Provide(fx.Annotate(func(catalogsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		catalogsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			v1.Use(middlewares.AuthMiddleware)
			v1.Use(middlewares.AdminMiddleware)

			group := v1.Group("/prices")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"prices-admin-echo-group"`))),

	fx.Provide(fx.Annotate(func(catalogsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		catalogsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			v1.Use(middlewares.AuthMiddleware)

			group := v1.Group("/prices")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"prices-echo-group"`))),

	fx.Provide(fx.Annotate(func(catalogsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		catalogsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			v1.Use(middlewares.AuthMiddleware)
			v1.Use(middlewares.AdminMiddleware)

			group := v1.Group("/levels")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"levels-admin-echo-group"`))),

	fx.Provide(fx.Annotate(func(catalogsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		catalogsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			// v1.Use(middlewares.AuthMiddleware)

			group := v1.Group("/levels")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"levels-echo-group"`))),

	fx.Provide(fx.Annotate(func(catalogsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		catalogsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			v1.Use(middlewares.AuthMiddleware)
			v1.Use(middlewares.AdminMiddleware)

			group := v1.Group("/languages")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"languages-admin-echo-group"`))),

	fx.Provide(fx.Annotate(func(catalogsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		catalogsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			v1.Use(middlewares.AuthMiddleware)

			group := v1.Group("/languages")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"languages-echo-group"`))),

	fx.Provide(fx.Annotate(func(catalogsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		catalogsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			v1.Use(middlewares.AuthMiddleware)
			v1.Use(middlewares.AdminMiddleware)

			group := v1.Group("/issue-types")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"issue-types-admin-echo-group"`))),

	fx.Provide(fx.Annotate(func(catalogsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		catalogsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			v1.Use(middlewares.AuthMiddleware)

			group := v1.Group("/issue-types")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"issue-types-echo-group"`))),

	fx.Provide(fx.Annotate(func(catalogsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		catalogsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			// v1.Use(middlewares.AuthMiddleware)

			group := v1.Group("/issue-types")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"issue-types-no-auth-echo-group"`))),

	fx.Provide(
		// fx.Annotate(
		// 	loginByGoogle.NewLoginByGoogleEndpoint,
		// 	fx.As(new(route.Endpoint)),
		// 	fx.ResultTags(fmt.Sprintf(`group:"%s"`, "product-routes")),
		// ),

		// Catalogs route
		route.AsRoute(createCategory.NewCreateCategoryEndpoint, "catalogs-routes"),
		route.AsRoute(updateCategory.NewUpdateCategoryEndpoint, "catalogs-routes"),
		route.AsRoute(deleteCategory.NewDeleteCategoryEndpoint, "catalogs-routes"),
		route.AsRoute(getCategoryById.NewGetCategoryByIdEndpoint, "catalogs-routes"),
		route.AsRoute(getCategories.NewGetCategoriesEndpoint, "catalogs-routes"),

		// Price route
		route.AsRoute(createPrice.NewCreatePriceEndpoint, "catalogs-routes"),
		route.AsRoute(updatePrice.NewUpdatePriceEndpoint, "catalogs-routes"),
		route.AsRoute(deletePrice.NewDeletePriceEndpoint, "catalogs-routes"),
		route.AsRoute(getPrice.NewGetPricesEndpoint, "catalogs-routes"),

		// Level route
		route.AsRoute(createLevel.NewCreateLevelEndpoint, "catalogs-routes"),
		route.AsRoute(updateLevel.NewUpdateLevelEndpoint, "catalogs-routes"),
		route.AsRoute(deleteLevel.NewDeleteLevelEndpoint, "catalogs-routes"),
		route.AsRoute(getLevels.NewGetLevelsEndpoint, "catalogs-routes"),

		//Language route
		route.AsRoute(createLanguage.NewCreateLanguageEndpoint, "catalogs-routes"),
		route.AsRoute(updateLanguage.NewUpdateLanguageEndpoint, "catalogs-routes"),
		route.AsRoute(deleteLanguage.NewDeleteLanguageEndpoint, "catalogs-routes"),
		route.AsRoute(getLanguages.NewGetLanguagesEndpoint, "catalogs-routes"),

		// Issue type route
		route.AsRoute(createIssueType.NewCreateIssueTypeEndpoint, "catalogs-routes"),
		route.AsRoute(updateIssueType.NewUpdateIssueTypeEndpoint, "catalogs-routes"),
		route.AsRoute(deleteIssueType.NewDeleteIssueTypeEndpoint, "catalogs-routes"),
		route.AsRoute(getIssueTypes.NewGetIssueTypesEndpoint, "catalogs-routes"),
	),
)

//   lc.Append(fx.Hook{
//     OnStart: func(ctx context.Context) error {
//       ln, err := net.Listen("tcp", srv.Addr)
//       if err != nil {
//         return err
//       }
//       fmt.Println("Starting HTTP server at", srv.Addr)
//       go srv.Serve(ln)
//       return nil
//     },
//     OnStop: func(ctx context.Context) error {
//       return srv.Shutdown(ctx)
//     },
//   })
