CREATE USER 'username'@'%' IDENTIFIED BY 'your_password';

CREATE DATABASE database;

GRANT ALL PRIVILEGES ON database.* TO 'username'@'%';

