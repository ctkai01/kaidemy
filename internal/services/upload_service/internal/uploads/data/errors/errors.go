package errors

import (

	"emperror.dev/errors"
)

func WrapError(err error) error {
	return errors.Wrap(err, "error handling request")
}
