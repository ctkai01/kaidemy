package uploads

import (
	// "fmt"

	// customEcho "gitlab.com/ctkai01/kaidemy/internal/pkg/http/custom_echo"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "fmt"

	"github.com/labstack/echo/v4"
	"go.uber.org/fx"

	// "github.com/mehdihadeli/go-ecommerce-microservices/internal/services/uploadreadservice/internal/products/data/repositories"
	// getProductByIdV1 "github.com/mehdihadeli/go-ecommerce-microservices/internal/services/catalogreadservice/internal/products/features/get_product_by_id/v1/endpoints"
	// changePassword "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/change_password/endpoints"
	// forgotPassword "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/forgot_password/endpoints"
	// loginStandard "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login/endpoints"
	// loginByGoogle "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login_by_google/endpoints"
	// register "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/register/endpoints"
	// requestLoginByGoogle "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/endpoints"
	// resetPassword "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/reset_password/endpoints"
	// createCourse "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/"
	uploadVideo "gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/features/upload_video/endpoints"

	// searchProductV1 "github.com/mehdihadeli/go-ecommerce-microservices/internal/services/catalogreadservice/internal/products/features/searching_products/v1/endpoints"
	customEcho "gitlab.com/ctkai01/kaidemy/internal/pkg/http/custom_echo"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/http/middlewares"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/grpc"
	// googleOauth "gitlab.com/ctkai01/kaidemy/internal/pkg/oauth/google"
)

var Module = fx.Module(
	"uploadsfx",

	// Other provides
	// fx.Provide(repositories.NewRedisProductRepository),
	// fx.Provide(NewHTTPServer),
	fx.Provide(grpc.NewUploadGrpcService),

	fx.Provide(fx.Annotate(func(uploadServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		uploadServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			v1.Use(middlewares.AuthMiddleware)
			// v1.Use(middlewares.TeacherMiddleware)
			group := v1.Group("/uploads")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"uploads-echo-group"`))),

	fx.Provide(
		// fx.Annotate(
		// 	loginByGoogle.NewLoginByGoogleEndpoint,
		// 	fx.As(new(route.Endpoint)),
		// 	fx.ResultTags(fmt.Sprintf(`group:"%s"`, "product-routes")),
		// ),

		//Auth route
		// route.AsRoute(requestLoginByGoogle.NewRequestLoginByGoogleEndpoint, "auth-routes"),
		// route.AsRoute(loginByGoogle.NewLoginByGoogleEndpoint, "auth-routes"),
		// route.AsRoute(register.NewRegisterEndpoint, "auth-routes"),
		// route.AsRoute(register.NewRegisterEndpoint, "auth-routes"),
		// route.AsRoute(loginStandard.NewLoginEndpoint, "auth-routes"),
		// route.AsRoute(forgotPassword.NewForgotPasswordEndpoint, "auth-routes"),
		// route.AsRoute(resetPassword.NewResetPasswordEndpoint, "auth-routes"),

		// //Users route
		route.AsRoute(uploadVideo.NewUploadVideoEndpoint, "uploads-routes"),
		// route.AsRoute(changeProfile.NewChangeProfileEndpoint, "users-routes"),

	),
)

//   lc.Append(fx.Hook{
//     OnStart: func(ctx context.Context) error {
//       ln, err := net.Listen("tcp", srv.Addr)
//       if err != nil {
//         return err
//       }
//       fmt.Println("Starting HTTP server at", srv.Addr)
//       go srv.Serve(ln)
//       return nil
//     },
//     OnStop: func(ctx context.Context) error {
//       return srv.Shutdown(ctx)
//     },
//   })
