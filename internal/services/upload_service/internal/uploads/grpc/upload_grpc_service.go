package grpc

import (
	// "context"
	// "bytes"
	"context"
	"fmt"
	"io"
	"net/textproto"
	"path/filepath"

	// "strings"

	// "io/ioutil"
	"mime/multipart"
	"os"

	// "fmt"
	// productsService
	"emperror.dev/errors"
	// "github.com/mehdihadeli/go-mediatr"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"github.com/mehdihadeli/go-mediatr"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/mapper"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"

	uploadsError "gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/data/errors"
	uploadVideoCommand "gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/features/upload_video/commands"
	uploadVideoDto "gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/features/upload_video/dtos"

	deleteVideoCommand "gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/features/delete_video/commands"
	deleteVideoDto "gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/features/delete_video/dtos"

	uploadResourceCommand "gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/features/upload_resource/commands"
	uploadResourceDto "gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/features/upload_resource/dtos"

	deleteResourceCommand "gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/features/delete_resource/commands"
	deleteResourceDto "gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/features/delete_resource/dtos"

	// getCategoryByIdDto "gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/features/upload_video/dtos"
	// uploadVideoCommand "gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/features/upload_video/commands"
	uploadsService "gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/grpc/proto/service_clients"
)

type UploadGrpcServiceServer struct {
	logger logger.Logger
}

func NewUploadGrpcService(
	logger logger.Logger,

) *UploadGrpcServiceServer {

	return &UploadGrpcServiceServer{
		logger,
	}
}

// uploadsService.UploadsService_UploadVideoServer
func (s *UploadGrpcServiceServer) UploadVideo(stream uploadsService.UploadsService_UploadVideoServer) error {
	var title string
	var fileType string

	tempFile, err := os.CreateTemp("", "temp_video_*.mp4")
	if err != nil {
		return err
	}
	defer tempFile.Close()

	// Create a multipart.FileHeader using the temporary file
	fileHeader := &multipart.FileHeader{
		Filename: tempFile.Name(),
		Header:   make(textproto.MIMEHeader),
	}

	sizeFile := 0
	for {
		req, err := stream.Recv()
		// title = req.Title
		if err == io.EOF {
			break
		}

		if err != nil {
			return err
		}

		if req != nil {
			title = req.Title
			fileType = req.Extension
			sizeFile += int(req.Size)
		}

		chunk := req.GetData()

		// dataBuffer = append(dataBuffer, chunk...)

		_, err = tempFile.Write(chunk)
		if err != nil {
			return err
		}
		// fmt.Println(chunk)
		// if err := file.Write(chunk); err != nil {
		// 	// return g.logError(status.Error(codes.Internal, err.Error()))
		// }
	}
	_, err = tempFile.Seek(0, io.SeekStart)
	if err != nil {
		return err
	}
	fileHeader.Size = int64(sizeFile)
	fileHeader.Header.Add("Content-Type", fileType)

	command, errValidate := uploadVideoCommand.NewUploadVideo(
		fileHeader,
		title,
	)

	if errValidate != nil && errValidate.ErrorType != nil {

		validationErr := customErrors.NewValidationErrorWrap(
			errValidate.ErrorType,
			"[uploadVideo_handler.StructCtx] command validation failed",
		)
		s.logger.Errorf(
			fmt.Sprintf("[uploadVideo_handler.StructCtx] err: {%v}", validationErr),
		)

		return errValidate.ErrorType
	}

	result, err := mediatr.Send[*uploadVideoCommand.UploadVideo, *uploadVideoDto.UploadVideoResponseDto](
		stream.Context(),
		command,
	)

	if err != nil {
		if err.Error() == uploadsError.WrapError(customErrors.ErrUserNotFound).Error() {
			errCustom := errors.WithMessage(
				err,
				"[uploadVideoEndpoint_handler.Send] error in sending create Course",
			)
			s.logger.Error(
				fmt.Sprintf(
					"[uploadVideo.Send], err: %v",
					errCustom,
				),
			)
			return err
		}

		err = errors.WithMessage(
			err,
			"[uploadVideoEndpoint_handler.Send] error in sending create Course",
		)
		s.logger.Error(
			fmt.Sprintf(
				"[uploadVideo.Send], err: %v",
				err,
			),
		)

		return err
	}

	return stream.SendAndClose(&uploadsService.UploadVideoRes{
		Url:      result.URL,
		VideoID:  result.VideoID,
		Duration: result.Duration,
		Size:     result.StorageSize,
	})
}

func (s *UploadGrpcServiceServer) DeleteVideo(ctx context.Context, req *uploadsService.DeleteVideoReq) (*uploadsService.DeleteVideoRes, error) {
	command, errValidate := deleteVideoCommand.NewDeleteVideo(
		req.VideoID,
	)

	if errValidate != nil && errValidate.ErrorType != nil {

		validationErr := customErrors.NewValidationErrorWrap(
			errValidate.ErrorType,
			"[deleteVideo_handler.StructCtx] command validation failed",
		)
		s.logger.Errorf(
			fmt.Sprintf("[deleteVideo_handler.StructCtx] err: {%v}", validationErr),
		)

		return nil, errValidate.ErrorType
	}

	result, err := mediatr.Send[*deleteVideoCommand.DeleteVideo, *deleteVideoDto.DeleteVideoResponseDto](
		ctx,
		command,
	)

	if err != nil {

		err = errors.WithMessage(
			err,
			"[deleteVideoEndpoint_handler.Send] error in sending create Course",
		)
		s.logger.Error(
			fmt.Sprintf(
				"[deleteVideo.Send], err: %v",
				err,
			),
		)

		return nil, err
	}

	return &uploadsService.DeleteVideoRes{
		Message: result.Message,
	}, nil
}

func (s *UploadGrpcServiceServer) UploadResource(stream uploadsService.UploadsService_UploadResourceServer) error {
	var title string
	var fileType string
	var resourceType int
	var fileHeader *multipart.FileHeader
	var tempFile *os.File
	sizeFile := 0

	for {
		req, err := stream.Recv()
		// title = req.Title
		if err == io.EOF {
			break
		}

		if err != nil {
			return err
		}

		if req != nil {
			title = req.Title
			fileType = req.Extension
			resourceType = int(req.Type)
			sizeFile += int(req.Size)
			if fileHeader == nil {
				// tailExtension := strings.Split(req.Extension, "/")
				extension := filepath.Ext(req.Name)
				nameWithoutExt := req.Name[:len(req.Name)-len(extension)]

				tempFile, err = os.CreateTemp("", fmt.Sprintf("%s_*%s", nameWithoutExt, extension))

				if err != nil {
					return err
				}
				defer tempFile.Close()
				fileHeader = &multipart.FileHeader{
					Filename: tempFile.Name(),
					Header:   make(textproto.MIMEHeader),
				}
			}
		}

		chunk := req.GetData()

		fmt.Println("Hello 11: ", tempFile)

		_, err = tempFile.Write(chunk)
		fmt.Println("Hello 1: ", err)
		if err != nil {
			return err
		}

	}

	_, err := tempFile.Seek(0, io.SeekStart)
	if err != nil {
		return err
	}

	fileHeader.Size = int64(sizeFile)
	fileHeader.Header.Add("Content-Type", fileType)

	command, errValidate := uploadResourceCommand.NewUploadResource(
		fileHeader,
		title,
		resourceType,
	)

	if errValidate != nil && errValidate.ErrorType != nil {

		validationErr := customErrors.NewValidationErrorWrap(
			errValidate.ErrorType,
			"[uploadResource_handler.StructCtx] command validation failed",
		)
		s.logger.Errorf(
			fmt.Sprintf("[uploadResource_handler.StructCtx] err: {%v}", validationErr),
		)

		return errValidate.ErrorType
	}

	result, err := mediatr.Send[*uploadResourceCommand.UploadResource, *uploadResourceDto.UploadResourceResponseDto](
		stream.Context(),
		command,
	)

	if err != nil {
		if err.Error() == uploadsError.WrapError(customErrors.ErrUserNotFound).Error() {
			errCustom := errors.WithMessage(
				err,
				"[uploadResourceEndpoint_handler.Send] error in sending upload resource",
			)
			s.logger.Error(
				fmt.Sprintf(
					"[uploadResource.Send], err: %v",
					errCustom,
				),
			)
			return err
		}

		err = errors.WithMessage(
			err,
			"[uploadResourceEndpoint_handler.Send] error in sending upload resource",
		)
		s.logger.Error(
			fmt.Sprintf(
				"[uploadResource.Send], err: %v",
				err,
			),
		)

		return err
	}
	return stream.SendAndClose(&uploadsService.UploadResourceRes{
		Url: result.URL,
	})
}

func (s *UploadGrpcServiceServer) DeleteResource(ctx context.Context, req *uploadsService.DeleteResourceReq) (*uploadsService.DeleteResourceRes, error) {
	command, errValidate := deleteResourceCommand.NewDeleteResource(
		req.Url,
	)

	if errValidate != nil && errValidate.ErrorType != nil {

		validationErr := customErrors.NewValidationErrorWrap(
			errValidate.ErrorType,
			"[deleteResource_handler.StructCtx] command validation failed",
		)
		s.logger.Errorf(
			fmt.Sprintf("[deleteResource_handler.StructCtx] err: {%v}", validationErr),
		)

		return nil, errValidate.ErrorType
	}

	result, err := mediatr.Send[*deleteResourceCommand.DeleteResource, *deleteResourceDto.DeleteResourceResponseDto](
		ctx,
		command,
	)

	if err != nil {

		err = errors.WithMessage(
			err,
			"[deleteResourceEndpoint_handler.Send] error in sending delete resource",
		)
		s.logger.Error(
			fmt.Sprintf(
				"[deleteResource.Send], err: %v",
				err,
			),
		)

		return nil, err
	}

	return &uploadsService.DeleteResourceRes{
		Message: result.Message,
	}, nil
}
