package mediator

import (
	// "emperror.dev/errors"
	"github.com/mehdihadeli/go-mediatr"

	// "github.com/mehdihadeli/go-mediatr"
	// forgotPasswordCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/forgot_password/commands"
	// forgotPasswordDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/forgot_password/dtos"
	// loginCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login/commands"
	// loginDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login/dtos"
	// loginGoogleCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login_by_google/commands"
	// loginGoogleDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login_by_google/dtos"
	// loginGoogleQuery "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login_by_google/queries"
	// registerCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/register/commands"
	// registerDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/register/dtos"
	// requestLoginGoogleCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/commands"
	// requestLoginGoogleDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	// resetPasswordCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/reset_password/commands"
	// resetPasswordDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/reset_password/dtos"

	// changePasswordCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/change_password/commands"
	// changePasswordDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/change_password/dtos"

	uploadVideoCommand "gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/features/upload_video/commands"
	uploadVideoDtos "gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/features/upload_video/dtos"

	deleteVideoCommand "gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/features/delete_video/commands"
	deleteVideoDtos "gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/features/delete_video/dtos"

	uploadResourceCommand "gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/features/upload_resource/commands"
	uploadResourceDtos "gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/features/upload_resource/dtos"

	deleteResourceCommand "gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/features/delete_resource/commands"
	deleteResourceDtos "gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/features/delete_resource/dtos"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/config"
)

// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"

func ConfigCatalogMediator(
	logger logger.Logger,
	grpcClient grpc.GrpcClient,
	cfg *config.Config,
) error {
	err := mediatr.RegisterRequestHandler[*uploadVideoCommand.UploadVideo, *uploadVideoDtos.UploadVideoResponseDto](
		uploadVideoCommand.NewUploadVideoHandler(logger, grpcClient, cfg),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*deleteVideoCommand.DeleteVideo, *deleteVideoDtos.DeleteVideoResponseDto](
		deleteVideoCommand.NewDeleteVideoHandler(logger, grpcClient, cfg),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*uploadResourceCommand.UploadResource, *uploadResourceDtos.UploadResourceResponseDto](
		uploadResourceCommand.NewUploadResourceHandler(logger, grpcClient, cfg),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*deleteResourceCommand.DeleteResource, *deleteResourceDtos.DeleteResourceResponseDto](
		deleteResourceCommand.NewDeleteResourceHandler(logger, grpcClient, cfg),
	)
	if err != nil {
		return err
	}

	// err = mediatr.RegisterRequestHandler[*loginGoogleQuery.LoginGoogle, *loginGoogleDtos.LoginGoogleResponseDto](
	// 	loginGoogleCommand.NewLoginGoogleHandler(logger, googleOauth, userRepository),
	// )
	// if err != nil {
	// 	return err
	// }

	// err = mediatr.RegisterRequestHandler[*registerCommand.UserCreateByNormal, *registerDtos.RegisterResponseDto](
	// 	registerCommand.NewRegisterHandler(logger, googleOauth, userRepository),
	// )
	// if err != nil {
	// 	return err
	// }

	// err = mediatr.RegisterRequestHandler[*loginCommand.UserLoginStandard, *loginDtos.LoginResponseDto](
	// 	loginCommand.NewLoginHandler(logger, googleOauth, userRepository),
	// )
	// if err != nil {
	// 	return err
	// }

	// err = mediatr.RegisterRequestHandler[*forgotPasswordCommand.ForgotPassword, *forgotPasswordDtos.ForgotPasswordResponseDto](
	// 	forgotPasswordCommand.NewForgotPasswordHandler(logger, userRepository, rabbitmqBuilder),
	// )
	// if err != nil {
	// 	return err
	// }

	// err = mediatr.RegisterRequestHandler[*resetPasswordCommand.ResetPassword, *resetPasswordDtos.ResetPassResponseDto](
	// 	resetPasswordCommand.NewResetPasswordHandler(logger, userRepository),
	// )
	// if err != nil {
	// 	return err
	// }

	// err = mediatr.RegisterRequestHandler[*changePasswordCommand.ChangePassword, *changePasswordDtos.ChangePasswordResponseDto](
	// 	changePasswordCommand.NewChangePasswordHandler(logger, userRepository),
	// )
	// if err != nil {
	// 	return err
	// }

	// err = mediatr.RegisterRequestHandler[*changeProfileCommand.ChangeProfile, *changeProfileDtos.ChangeProfileResponseDto](
	// 	changeProfileCommand.NewChangeProfileHandler(logger, userRepository, minIoConfiguration),
	// )
	// if err != nil {
	// 	return err
	// }

	// err = mediatr.RegisterRequestHandler[*loginGoogleQuery.LoginGoogle, *loginGoogleDtos.LoginGoogleResponseDto](
	// 	loginGoogleCommand.NewLoginGoogleHandler(logger, googleOauth, userRepository),
	// )
	// if err != nil {
	// 	return err
	// }

	// err := mediatr.RegisterRequestHandler[*createProductCommandV1.CreateProduct, *createProductDtosV1.CreateProductResponseDto](
	// 	createProductCommandV1.NewCreateProductHandler(
	// 		logger,
	// 		mongoProductRepository,
	// 		cacheProductRepository,
	// 		tracer,
	// 	),
	// )
	// if err != nil {
	// 	return errors.WrapIf(err, "error while registering handlers in the mediator")
	// }

	// err = mediatr.RegisterRequestHandler[*deleteProductCommandV1.DeleteProduct, *mediatr.Unit](
	// 	deleteProductCommandV1.NewDeleteProductHandler(
	// 		logger,
	// 		mongoProductRepository,
	// 		cacheProductRepository,
	// 		tracer,
	// 	),
	// )
	// if err != nil {
	// 	return errors.WrapIf(err, "error while registering handlers in the mediator")
	// }

	// err = mediatr.RegisterRequestHandler[*updateProductCommandV1.UpdateProduct, *mediatr.Unit](
	// 	updateProductCommandV1.NewUpdateProductHandler(
	// 		logger,
	// 		mongoProductRepository,
	// 		cacheProductRepository,
	// 		tracer,
	// 	),
	// )
	// if err != nil {
	// 	return errors.WrapIf(err, "error while registering handlers in the mediator")
	// }

	// err = mediatr.RegisterRequestHandler[*getProductsQueryV1.GetProducts, *getProductsDtoV1.GetProductsResponseDto](
	// 	getProductsQueryV1.NewGetProductsHandler(logger, mongoProductRepository, tracer),
	// )
	// if err != nil {
	// 	return errors.WrapIf(err, "error while registering handlers in the mediator")
	// }

	// err = mediatr.RegisterRequestHandler[*searchProductsQueryV1.SearchProducts, *searchProductsDtosV1.SearchProductsResponseDto](
	// 	searchProductsQueryV1.NewSearchProductsHandler(
	// 		logger,
	// 		mongoProductRepository,
	// 		tracer,
	// 	),
	// )
	// if err != nil {
	// 	return errors.WrapIf(err, "error while registering handlers in the mediator")
	// }

	// err = mediatr.RegisterRequestHandler[*getProductByIdQueryV1.GetProductById, *getProductByIdDtosV1.GetProductByIdResponseDto](
	// 	getProductByIdQueryV1.NewGetProductByIdHandler(
	// 		logger,
	// 		mongoProductRepository,
	// 		cacheProductRepository,
	// 		tracer,
	// 	),
	// )
	// if err != nil {
	// 	return errors.WrapIf(err, "error while registering handlers in the mediator")
	// }

	return nil
}
