package configurations

import (
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/configurations/mappings"
	// "golang.org/x/oauth2"

	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/configurations/mediator"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/data"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp/contracts"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	logger2 "gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	googleGrpc "google.golang.org/grpc"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/otel/tracing"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/configurations/mediator"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/config"
	grpcServer "gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/grpc"

	uploadsservice "gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/grpc/proto/service_clients"
)

type UploadsModuleConfigurator struct {
	contracts.Application
}

func NewUploadsModuleConfigurator(
	app contracts.Application,
) *UploadsModuleConfigurator {
	return &UploadsModuleConfigurator{
		Application: app,
	}
}

func (c *UploadsModuleConfigurator) ConfigureUploadsModule() {
	c.ResolveFunc(
		func(logger logger2.Logger, grpcClient grpc.GrpcClient, cfg *config.Config) error {
			// Config Products Mediators
			err := mediator.ConfigCatalogMediator(logger, grpcClient, cfg)
			if err != nil {
				return err
			}

			// Config Products Mappings
			// err = mappings.ConfigureProductsMappings()
			// if err != nil {
			// 	return err
			// }
			return nil
		},
	)
}

func (c *UploadsModuleConfigurator) MapUploadsEndpoints() {
	// Config Course Http Endpoints
	c.ResolveFuncWithParamTag(func(endpoints []route.Endpoint) {
		for _, endpoint := range endpoints {
			endpoint.MapEndpoint()
		}
	}, `group:"uploads-routes"`,
	)

	c.ResolveFunc(
		func(uploadsGrpcServer grpc.GrpcServer, grpcService *grpcServer.UploadGrpcServiceServer) error {
			uploadsGrpcServer.GrpcServiceBuilder().RegisterRoutes(func(server *googleGrpc.Server) {
				uploadsservice.RegisterUploadsServiceServer(server, grpcService)
			})

			return nil
		},
	)
}
