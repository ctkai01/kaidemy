package params

import (
	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"go.uber.org/fx"
)

type UploadRouteParams struct {
	fx.In

	Logger       logger.Logger
	UploadsGroup *echo.Group `name:"uploads-echo-group"`
	Validator    *validator.Validate
}
