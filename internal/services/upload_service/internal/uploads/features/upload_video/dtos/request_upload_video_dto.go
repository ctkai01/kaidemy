package dtos

//https://echo.labstack.com/guide/response/

type UploadVideoRequestDto struct {
	Title string `form:"title"`
}
