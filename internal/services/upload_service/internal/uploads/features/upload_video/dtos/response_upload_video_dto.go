package dtos

// import "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

//https://echo.labstack.com/guide/response/

type UploadVideoResponseDto struct {
	Message string `json:"message"`
	URL     string `json:"url"`
	VideoID string `json:"video_id"`
	Duration int32 `json:"duration"`
	StorageSize int64 `json:"size"`
	// Course  models.Course `json:"course"`
}
