package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/contracts/params"
	uploadsError "gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/features/upload_video/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/features/upload_video/dtos"
)

type uploadVideoEndpoint struct {
	params.UploadRouteParams
}

func NewUploadVideoEndpoint(
	params params.UploadRouteParams,
) route.Endpoint {
	return &uploadVideoEndpoint{
		UploadRouteParams: params,
	}
}

func (ep *uploadVideoEndpoint) MapEndpoint() {
	ep.UploadsGroup.POST("", ep.handler())
}

func (ep *uploadVideoEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.UploadVideoRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[uploadVideoEndpoint_handler.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[uploadVideoEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}
		file, err := c.FormFile("video")

		if err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[uploadVideo.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[uploadVideoEndpoint_handler.Bind] err: %v", badRequestErr),
			)

			return c.JSON(http.StatusBadRequest, err)
		}

		command, errValidate := commands.NewUploadVideo(
			file,
			request.Title,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[uploadVideoEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[uploadVideoEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.UploadVideo, *dtos.UploadVideoResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == uploadsError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[uploadVideoEndpoint_handler.Send] error in sending create Course",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[uploadVideo.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[uploadVideoEndpoint_handler.Send] error in sending create Course",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[uploadVideo.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusCreated, result)
	}
}
