package commands

import (
	"mime/multipart"

	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type UploadVideo struct {
	Video *multipart.FileHeader
	Title string `validate:"required,gte=1,lte=60"`
}

func NewUploadVideo(video *multipart.FileHeader, title string) (*UploadVideo, *validator.ValidateError) {
	command := &UploadVideo{video, title}

	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	if command.Video != nil {
		errValidate := checkVideoUpload(command.Video)

		if errValidate != nil && errValidate.ErrorType != nil {
			return nil, errValidate
		}
	}

	return command, nil
}

func checkVideoUpload(video *multipart.FileHeader) *validator.ValidateError {
	allowFileType := []string{"video/mp4"}
	if video.Size > 1048576*100 {
		valdiateErr := validator.ValidateError{
			ErrorValue: []*validator.CustomValidationError{
				{
					Field:   "video",
					Message: customErrors.ErrVideoToMax.Error(),
				},
			},
			ErrorType: customErrors.ErrVideoToMax,
		}
		return &valdiateErr
	}

	isSupportType := false
	for _, v := range allowFileType {
		if v == video.Header["Content-Type"][0] {
			isSupportType = true
		}
	}
	if !isSupportType {
		valdiateErr := validator.ValidateError{
			ErrorValue: []*validator.CustomValidationError{
				{
					Field:   "video",
					Message: customErrors.ErrNotVideoType.Error(),
				},
			},
			ErrorType: customErrors.ErrNotVideoType,
		}
		return &valdiateErr
	}
	return nil
}
