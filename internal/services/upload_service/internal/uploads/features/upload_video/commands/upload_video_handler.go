package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	// "gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/features/upload_video/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	"gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/config"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type GetVideoBunny struct {
	Length      int32 `json:"length"`
	StorageSize int64 `json:"storageSize"`
	Status      int   `json:"status"`
}

type UploadVideoHandler struct {
	log        logger.Logger
	grpcClient grpc.GrpcClient
	cfg        *config.Config
}

func NewUploadVideoHandler(
	log logger.Logger,
	grpcClient grpc.GrpcClient,
	cfg *config.Config,

) *UploadVideoHandler {
	return &UploadVideoHandler{log: log, grpcClient: grpcClient, cfg: cfg}
}

func (c *UploadVideoHandler) Handle(ctx context.Context, command *UploadVideo) (*dtos.UploadVideoResponseDto, error) {
	// user, err := c.courseRepository.GetUserByID(ctx, command.IdUser)
	client := &http.Client{}

	// Step 1: Upload the video
	uploadURL := "https://video.bunnycdn.com/library/155247/videos"
	uploadHeaders := map[string]string{
		"accept":       "application/json",
		"content-type": "application/json",
		"AccessKey":    c.cfg.BunnyOptions.AccessKeyStream,
	}

	uploadBody := map[string]string{
		"title": command.Title,
	}

	uploadJSON, _ := json.Marshal(uploadBody)

	uploadRequest, err := http.NewRequest("POST", uploadURL, bytes.NewBuffer(uploadJSON))
	if err != nil {
		return nil, err
	}

	for key, value := range uploadHeaders {
		uploadRequest.Header.Set(key, value)
	}

	uploadResponse, err := client.Do(uploadRequest)
	if err != nil {
		return nil, err
	}

	defer uploadResponse.Body.Close()

	var jsonResponse map[string]interface{}
	decoder := json.NewDecoder(uploadResponse.Body)
	if err := decoder.Decode(&jsonResponse); err != nil {
		return nil, err
	}

	videoGUID := jsonResponse["guid"].(string)

	// Step 2: Upload the video file
	videoURL := fmt.Sprintf("https://video.bunnycdn.com/library/155247/videos/%s", videoGUID)
	videoHeaders := map[string]string{
		"accept":       "application/json",
		"content-type": "application/octet-stream",
		"AccessKey":    c.cfg.BunnyOptions.AccessKeyStream,
	}

	// os.Open(command.Video.Filename)
	// videoFile, err := command.Video.Open()
	videoFile, err := os.Open(command.Video.Filename)
	if err != nil {
		return nil, err
	}
	defer videoFile.Close()

	videoRequest, err := http.NewRequest("PUT", videoURL, videoFile)
	if err != nil {
		return nil, err
	}

	for key, value := range videoHeaders {
		videoRequest.Header.Set(key, value)
	}

	videoResponse, err := client.Do(videoRequest)
	if err != nil {
		return nil, err
	}
	defer videoResponse.Body.Close()

	if err := os.Remove(command.Video.Filename); err != nil {
		// Handle the error if removal fails
		return nil, err
	}

	for {
		//Get Video
		getVideoHeaders := map[string]string{
			"accept":    "application/json",
			"AccessKey": c.cfg.BunnyOptions.AccessKeyStream,
		}
		getVideoURL := fmt.Sprintf("https://video.bunnycdn.com/library/155247/videos/%s", videoGUID)
		getVideoRequest, err := http.NewRequest("GET", getVideoURL, nil)
		if err != nil {
			return nil, err
		}
		for key, value := range getVideoHeaders {
			getVideoRequest.Header.Set(key, value)
		}

		getVideoResponse, err := client.Do(getVideoRequest)
		if err != nil {
			return nil, err
		}

		defer getVideoResponse.Body.Close()
		body, err := io.ReadAll(getVideoResponse.Body)

		if err != nil {
			return nil, err
		}

		var result GetVideoBunny
		err = json.Unmarshal(body, &result)
		fmt.Println("Data: ", string(body))
		if err != nil {
			return nil, err
		}
		fmt.Println("Body: ", result)

		if result.Length == 0 {
			continue
		}

		return &dtos.UploadVideoResponseDto{
			Message:     "Upload video successfully!",
			URL:         videoGUID,
			VideoID:     videoGUID,
			Duration:    result.Length,
			StorageSize: result.StorageSize,
		}, nil
	}

}
