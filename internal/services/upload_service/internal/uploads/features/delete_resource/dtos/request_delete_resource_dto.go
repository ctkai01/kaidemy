package dtos

//https://echo.labstack.com/guide/response/

type DeleteResourceRequestDto struct {
	URL string `form:"url"`
}
