package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type DeleteResource struct {
	URL string `validate:"required,gte=1,lte=100"`
}

func NewDeleteResource(url string) (*DeleteResource, *validator.ValidateError) {
	command := &DeleteResource{url}

	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
