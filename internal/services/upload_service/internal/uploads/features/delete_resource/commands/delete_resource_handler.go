package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"fmt"
	"net/http"
	"path"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	// "gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/features/delete_resource/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	"gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/config"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type DeleteResourceHandler struct {
	log        logger.Logger
	grpcClient grpc.GrpcClient
	cfg        *config.Config
}

func NewDeleteResourceHandler(
	log logger.Logger,
	grpcClient grpc.GrpcClient,
	cfg *config.Config,

) *DeleteResourceHandler {
	return &DeleteResourceHandler{log: log, grpcClient: grpcClient, cfg: cfg}
}

func (c *DeleteResourceHandler) Handle(ctx context.Context, command *DeleteResource) (*dtos.DeleteResourceResponseDto, error) {
	// user, err := c.courseRepository.GetUserByID(ctx, command.IdUser)
	client := &http.Client{}

	// Step 1: Delete the resource

	fmt.Println("RL: ", command.URL)
	filename := path.Base(command.URL)
	// https://sg.storage.bunnycdn.com/kaidemy/resource
	newURL := fmt.Sprintf("https://sg.storage.bunnycdn.com/kaidemy/resource/%s", filename)
	fmt.Println("newURL: ", newURL)
	
	deleteRequest, err := http.NewRequest("DELETE", newURL, nil)
	if err != nil {
		return nil, err
	}
	fmt.Println(c.cfg.BunnyOptions.AccessKeyStorage)
	deleteRequest.Header.Set("AccessKey", c.cfg.BunnyOptions.AccessKeyStorage)

	deleteResponse, err := client.Do(deleteRequest)
	if err != nil {
		return nil, err
	}

	defer deleteResponse.Body.Close()
	fmt.Println(string(deleteResponse.Status))
	if deleteResponse.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("HTTP request failed with status code: %d", deleteResponse.StatusCode)

	}

	return &dtos.DeleteResourceResponseDto{
		Message: "Delete resource successfully!",
	}, nil
}
