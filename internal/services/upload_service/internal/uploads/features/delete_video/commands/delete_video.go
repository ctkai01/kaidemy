package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type DeleteVideo struct {
	VideoID string `validate:"required,gte=1,lte=60`
}

func NewDeleteVideo(videoID string) (*DeleteVideo, *validator.ValidateError) {
	command := &DeleteVideo{videoID}

	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
