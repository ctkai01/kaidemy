package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"fmt"
	"net/http"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	// "gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/features/delete_video/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	"gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/config"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type DeleteVideoHandler struct {
	log        logger.Logger
	grpcClient grpc.GrpcClient
	cfg        *config.Config
}

func NewDeleteVideoHandler(
	log logger.Logger,
	grpcClient grpc.GrpcClient,
	cfg *config.Config,

) *DeleteVideoHandler {
	return &DeleteVideoHandler{log: log, grpcClient: grpcClient, cfg: cfg}
}

func (c *DeleteVideoHandler) Handle(ctx context.Context, command *DeleteVideo) (*dtos.DeleteVideoResponseDto, error) {
	// user, err := c.courseRepository.GetUserByID(ctx, command.IdUser)
	client := &http.Client{}

	// Step 1: Delete the video
	deleteURL := fmt.Sprintf("https://video.bunnycdn.com/library/155247/videos/%s", command.VideoID)

	deleteHeaders := map[string]string{
		"accept":       "application/json",
		"content-type": "application/json",
		"AccessKey":    c.cfg.BunnyOptions.AccessKeyStream,
	}

	deleteRequest, err := http.NewRequest("DELETE", deleteURL, nil)
	if err != nil {
		return nil, err
	}

	for key, value := range deleteHeaders {
		deleteRequest.Header.Set(key, value)
	}

	deleteResponse, err := client.Do(deleteRequest)
	if err != nil {
		return nil, err
	}

	defer deleteResponse.Body.Close()

	return &dtos.DeleteVideoResponseDto{
		Message: "Delete video successfully!",
	}, nil
}
