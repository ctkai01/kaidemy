package dtos

//https://echo.labstack.com/guide/response/

type DeleteVideoRequestDto struct {
	VideoID string `form:"video_id"`
}
