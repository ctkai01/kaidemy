package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"fmt"
	"net/http"
	"os"
	"strings"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	// "gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/features/upload_resource/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	"gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/config"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type UploadResourceHandler struct {
	log        logger.Logger
	grpcClient grpc.GrpcClient
	cfg        *config.Config
}

func NewUploadResourceHandler(
	log logger.Logger,
	grpcClient grpc.GrpcClient,
	cfg *config.Config,

) *UploadResourceHandler {
	return &UploadResourceHandler{log: log, grpcClient: grpcClient, cfg: cfg}
}

func (c *UploadResourceHandler) Handle(ctx context.Context, command *UploadResource) (*dtos.UploadResourceResponseDto, error) {
	// user, err := c.courseRepository.GetUserByID(ctx, command.IdUser)
	client := &http.Client{}

	// Step 1: Delete the video
	// deleteURL := fmt.Sprintf("https://video.bunnycdn.com/library/155247/videos/%s", command.VideoID)

	fileNameTemp := strings.Split(command.Asset.Filename, "/")
	fileName := fileNameTemp[len(fileNameTemp)-1]

	var (
		HOSTNAME           = "sg.storage.bunnycdn.com"            // Replace with your BunnyCDN hostname
		STORAGE_ZONE_NAME  = "kaidemy"                            // Replace with your storage zone name
		FILENAME_TO_UPLOAD = fmt.Sprintf("resource/%s", fileName) // Replace with the desired filename
		FILE_PATH          = command.Asset.Filename               // Update the file path to './dat1.jpg'
		ACCESS_KEY         = c.cfg.BunnyOptions.AccessKeyStorage  // Replace with your BunnyCDN API key
	)

	if command.Type == constants.MediaType {
		FILENAME_TO_UPLOAD = fmt.Sprintf("avatar/%s", fileName)
	}
	file, err := os.Open(FILE_PATH)

	if err != nil {
		return nil, err
	}
	defer file.Close()
	url := fmt.Sprintf("https://%s/%s/%s", HOSTNAME, STORAGE_ZONE_NAME, FILENAME_TO_UPLOAD)

	deleteRequest, err := http.NewRequest("PUT", url, file)

	if err != nil {
		return nil, err
	}
	deleteRequest.Header.Set("AccessKey", ACCESS_KEY)

	response, err := client.Do(deleteRequest)

	if err != nil {
		return nil, err
	}

	defer response.Body.Close()
	if response.StatusCode != http.StatusCreated {
		return nil, fmt.Errorf("HTTP request failed with status code: %d", response.StatusCode)
	}

	if err := os.Remove(command.Asset.Filename); err != nil {
		return nil, err
	}
	return &dtos.UploadResourceResponseDto{
		Message: "Upload file successfully!",
		URL:     fmt.Sprintf("https://kaidemy.b-cdn.net/%s", FILENAME_TO_UPLOAD),
	}, nil
}
