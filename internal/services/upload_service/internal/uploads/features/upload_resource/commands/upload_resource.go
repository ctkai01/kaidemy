package commands

import (
	"mime/multipart"

	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type UploadResource struct {
	Asset *multipart.FileHeader
	Title string `validate:"required,gte=1,lte=60"`
	Type  int    `validate:"required,gte=1,lte=2"`
}

func NewUploadResource(asset *multipart.FileHeader, title string, typeResource int) (*UploadResource, *validator.ValidateError) {
	command := &UploadResource{asset, title, typeResource}

	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	if command.Asset != nil {
		errValidate := checkResourceUpload(command.Asset)

		if errValidate != nil && errValidate.ErrorType != nil {
			return nil, errValidate
		}
	}

	return command, nil
}

func checkResourceUpload(asset *multipart.FileHeader) *validator.ValidateError {
	// allowFileType := []string{"video/mp4"}
	if asset.Size > 1048576*5 {
		valdiateErr := validator.ValidateError{
			ErrorValue: []*validator.CustomValidationError{
				{
					Field:   "resource",
					Message: customErrors.ErrAssetToMax.Error(),
				},
			},
			ErrorType: customErrors.ErrAssetToMax,
		}
		return &valdiateErr
	}

	// isSupportType := false
	// for _, v := range allowFileType {
	// 	if v == video.Header["Content-Type"][0] {
	// 		isSupportType = true
	// 	}
	// }
	// if !isSupportType {
	// 	valdiateErr := validator.ValidateError{
	// 		ErrorValue: []*validator.CustomValidationError{
	// 			{
	// 				Field:   "video",
	// 				Message: customErrors.ErrNotVideoType.Error(),
	// 			},
	// 		},
	// 		ErrorType: customErrors.ErrNotVideoType,
	// 	}
	// 	return &valdiateErr
	// }
	return nil
}
