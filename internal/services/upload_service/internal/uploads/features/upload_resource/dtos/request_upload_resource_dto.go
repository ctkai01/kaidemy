package dtos

//https://echo.labstack.com/guide/response/

type UploadResourceRequestDto struct {
	Title string `form:"title"`
}