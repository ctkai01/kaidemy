package dtos

// import "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/models"

//https://echo.labstack.com/guide/response/

type UploadResourceResponseDto struct {
	Message string `json:"message"`
	URL     string `json:"url"`
}
