package app

import (
	"go.uber.org/fx"

	"gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/shared/configurations/uploads"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/config/environment"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
)

type UploadsApplication struct {
	*uploads.UploadsServiceConfigurator
}

func NewUploadsApplication(
	providers []interface{},
	decorates []interface{},
	options []fx.Option,
	logger logger.Logger,
	environment environment.Environment,
) *UploadsApplication {
	app := fxapp.NewApplication(providers, decorates, options, logger, environment)
	return &UploadsApplication{
		UploadsServiceConfigurator: uploads.NewUploadsServiceConfigurator(app),
	}
}
