package app

import (
	"fmt"
	"os"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/config/environment"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp/contracts"
)

type UploadsApplicationBuilder struct {
	contracts.ApplicationBuilder
}

func NewUploadApplicationBuilder() *UploadsApplicationBuilder {
	var envs []environment.Environment 

	if os.Getenv("ENV") == string(environment.Production) {
		envs = append(envs, environment.Production)
	} else {
		envs = append(envs, environment.Development)
	}
	fmt.Println("Enf: ", envs)
	builder := &UploadsApplicationBuilder{fxapp.NewApplicationBuilder(envs...)}
	fmt.Println("???: ", envs)

	return builder
}

func (a *UploadsApplicationBuilder) Build() *UploadsApplication {

	return NewUploadsApplication(
		a.GetProvides(),
		a.GetDecorates(),
		a.Options(),
		a.Logger(),
		a.Environment(),
	)
}
