package app

import (
	"gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/shared/configurations/uploads"
)

type App struct{}

func NewApp() *App {
	return &App{}
}

func (a *App) Run() {
	// configure dependencies
	appBuilder := NewUploadApplicationBuilder()
	appBuilder.ProvideModule(uploads.UploadsServiceModule)

	app := appBuilder.Build()
	// // configure application
	app.ConfigureUploads()
	app.MapUploadsEndpoints()

	app.Logger().Info("Starting uploads service application")
	app.Run()
}
