package uploads

import (
	appconfig "gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/config"
	// "gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/shared/configurations/uploads"
	"gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/shared/configurations/uploads/infrastructure"
	"gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads"
	"go.uber.org/fx"
	// internal/shared/configurations/users/infrastructure
)

var UploadsServiceModule = fx.Module(
	"uploadsfx",
	// Shared Modules
	appconfig.Module,
	infrastructure.Module,

	// Features Modules
	uploads.Module,

	// Other provides
	// fx.Provide(provideCatalogsMetrics),
)
