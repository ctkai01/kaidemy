package uploads

import (
	"fmt"

	"github.com/labstack/echo/v4"

	// "gorm.io/gorm"
	"net/http"

	"gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/config"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/shared/configurations/infrastructure"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/configurations"
	"gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/shared/configurations/uploads/infrastructure"
	"gitlab.com/ctkai01/kaidemy/internal/services/uploadservice/internal/uploads/configurations"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp/contracts"
	customEcho "gitlab.com/ctkai01/kaidemy/internal/pkg/http/custom_echo"
)

type UploadsServiceConfigurator struct {
	contracts.Application
	infrastructureConfigurator *infrastructure.InfrastructureConfigurator
	uploadsModuleConfigurator  *configurations.UploadsModuleConfigurator
}

func NewUploadsServiceConfigurator(app contracts.Application) *UploadsServiceConfigurator {
	infraConfigurator := infrastructure.NewInfrastructureConfigurator(app)
	uploadModuleConfigurator := configurations.NewUploadsModuleConfigurator(app)

	return &UploadsServiceConfigurator{
		Application:                app,
		infrastructureConfigurator: infraConfigurator,
		uploadsModuleConfigurator:  uploadModuleConfigurator,
	}
}

func (ic *UploadsServiceConfigurator) ConfigureUploads() {
	// Shared
	// Infrastructure
	ic.infrastructureConfigurator.ConfigInfrastructures()
	// Shared
	// Catalogs configurations
	// ic.ResolveFunc(func(gorm *gorm.DB) error {
	// 	err := ic.migrateUploads(gorm)

	// 	if err != nil {
	// 		return err
	// 	}
	// 	return nil
	// })
	// Modules
	// Product module
	ic.uploadsModuleConfigurator.ConfigureUploadsModule()
}

func (ic *UploadsServiceConfigurator) MapUploadsEndpoints() {
	// Shared
	ic.ResolveFunc(
		func(coursesServer customEcho.EchoHttpServer, cfg *config.Config) error {
			coursesServer.SetupDefaultMiddlewares()

			// Config catalogs root endpoint
			coursesServer.RouteBuilder().
				RegisterRoutes(func(e *echo.Echo) {
					e.GET("/heath", func(ec echo.Context) error {
						return ec.String(
							http.StatusOK,
							fmt.Sprintf(
								"%s is running...",
								cfg.AppOptions.GetMicroserviceNameUpper(),
							),
						)
					})
				})

			// Config catalogs swagger
			// ic.configSwagger(catalogsServer.RouteBuilder())

			return nil
		},
	)

	// Modules
	// Products CatalogsServiceModule endpoints
	ic.uploadsModuleConfigurator.MapUploadsEndpoints()
}
