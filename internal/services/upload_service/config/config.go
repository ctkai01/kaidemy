package config

import (
	"strings"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/config"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/config/environment"
)

type Config struct {
	AppOptions   AppOptions   `mapstructure:"appOptions" env:"AppOptions"`
	BunnyOptions BunnyOptions `mapstructure:"bunnyOptions" env:"BunnyOptions"`
}

func NewConfig(env environment.Environment) (*Config, error) {
	cfg, err := config.BindConfig[*Config](env)
	if err != nil {
		return nil, err
	}

	return cfg, nil
}

type AppOptions struct {
	DeliveryType string `mapstructure:"deliveryType" env:"DeliveryType"`
	ServiceName  string `mapstructure:"serviceName"  env:"ServiceName"`
}

type BunnyOptions struct {
	AccessKeyStream  string `mapstructure:"accessKeyStream"  env:"AccessKeyStream"`
	AccessKeyStorage string `mapstructure:"accessKeyStorage"  env:"AccessKeyStorage"`
}

func (cfg *AppOptions) GetMicroserviceNameUpper() string {
	return strings.ToUpper(cfg.ServiceName)
}

func (cfg *AppOptions) GetMicroserviceName() string {
	return cfg.ServiceName
}

func (cfg *BunnyOptions) GetBunnyAccessKeyStream() string {
	return cfg.AccessKeyStream
}

func (cfg *BunnyOptions) GetBunnyAccessKeyStorage() string {
	return cfg.AccessKeyStorage
}
