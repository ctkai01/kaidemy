package consumer_rabbitmq

import (
	"context"
	"encoding/json"
	"fmt"
	"log"

	"github.com/mehdihadeli/go-mediatr"
	"github.com/rabbitmq/amqp091-go"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	createOrderCommand "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/create_order/commands"
	createOrderDto "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/create_order/dtos"
)

func PurchaseHandler(d amqp091.Delivery) {
	log.Printf("Received a message : %s", d.Body)
	
	var dataSendPurchase constants.SendPurchase 
	err := json.Unmarshal(d.Body, &dataSendPurchase)

	if err != nil {
		log.Println("[dataSendPurchase]: ", err)
		d.Reject(true)
	}
	fmt.Println("dataSendPurchase : ", dataSendPurchase)

	command, _ := createOrderCommand.NewCreateOrder(
		int(dataSendPurchase.IdInvoice),
	)

	_, err = mediatr.Send[*createOrderCommand.CreateOrder, *createOrderDto.CreateOrderResponseDto](
		context.Background(),
		command,
	)
	fmt.Println("Err: ", err)
	if err != nil {
		d.Reject(true)
	} else {
		d.Ack(false)
	}
}
