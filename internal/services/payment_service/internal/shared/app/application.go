package app

import (
	"go.uber.org/fx"

	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/shared/configurations/payments"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/config/environment"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
)

type PaymentsApplication struct {
	*payments.PaymentsServiceConfigurator
}

func NewPaymentsApplication(
	providers []interface{},
	decorates []interface{},
	options []fx.Option,
	logger logger.Logger,
	environment environment.Environment,
) *PaymentsApplication {
	app := fxapp.NewApplication(providers, decorates, options, logger, environment)
	return &PaymentsApplication{
		PaymentsServiceConfigurator: payments.NewPaymentsServiceConfigurator(app),
	}
}
