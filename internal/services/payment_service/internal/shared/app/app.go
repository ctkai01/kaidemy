package app

import (
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/shared/configurations/payments"
)

type App struct{}

func NewApp() *App {
	return &App{}
}

func (a *App) Run() {
	// configure dependencies
	appBuilder := NewPaymentApplicationBuilder()
	appBuilder.ProvideModule(payments.PaymentsServiceModule)

	app := appBuilder.Build()
	// // configure application
	app.ConfigurePayments()
	app.MapPaymentsEndpoints()

	app.Logger().Info("Starting payments service application")
	app.Run()
}
