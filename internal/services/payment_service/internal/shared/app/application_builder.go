package app

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp/contracts"
)

type PaymentsApplicationBuilder struct {
	contracts.ApplicationBuilder
}

func NewPaymentApplicationBuilder() *PaymentsApplicationBuilder {
	builder := &PaymentsApplicationBuilder{fxapp.NewApplicationBuilder()}

	return builder
}

func (a *PaymentsApplicationBuilder) Build() *PaymentsApplication {

	return NewPaymentsApplication(
		a.GetProvides(),
		a.GetDecorates(),
		a.Options(),
		a.Logger(),
		a.Environment(),
	)
}
