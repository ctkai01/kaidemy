package infrastructure

import (
	"github.com/go-playground/validator"
	"go.uber.org/fx"

	// rabbitmq2 "gitlab.com/ctkai01/kaidemy/internal/services/catalogreadservice/internal/products/configurations/rabbitmq"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/core"
	gormMysql "gitlab.com/ctkai01/kaidemy/internal/pkg/gorm_mysql"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/configurations"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/consumer"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/consumer_rabbitmq"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	customEcho "gitlab.com/ctkai01/kaidemy/internal/pkg/http/custom_echo"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/mongodb"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/otel/tracing"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/configurations"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/redis"
)

// https://pmihaylov.com/shared-components-go-microservices/
var Module = fx.Module(
	"infrastructurefx",
	// Modules
	core.Module,
	customEcho.Module,
	grpc.Module,
	gormMysql.Module,
	// .Module,
	// mongodb.Module,
	// redis.Module,
	rabbitmq.Module,

	// Other provides
	
	fx.Provide(validator.New),
	fx.Invoke(func(rabbitmqBuilder configurations.RabbitMQConfigurationBuilder) {
		rabbitmqBuilder.AddConsumerToQueue(&consumer.RabbitMQConsumerToQueue{
			QueueName:   constants.SEND_PURCHASE,
			HandlerFunc: consumer_rabbitmq.PurchaseHandler,
		})

	}),
)
