package payments

import (
	appconfig "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/config"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/shared/configurations/payments/infrastructure"
	"go.uber.org/fx"
	// internal/shared/configurations/users/infrastructure
)

var PaymentsServiceModule = fx.Module(
	"paymentsfx",
	// Shared Modules
	appconfig.Module,
	infrastructure.Module,

	// Features Modules
	payments.Module,

	// Other provides
	// fx.Provide(provideCatalogsMetrics),
)
