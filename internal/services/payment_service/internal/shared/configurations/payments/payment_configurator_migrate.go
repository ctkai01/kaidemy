package payments

import (
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/models"
	"gorm.io/gorm"
)

func (ic *PaymentsServiceConfigurator) migratePayments(gorm *gorm.DB) error {
	// or we could use `gorm.Migrate()`
	err := gorm.AutoMigrate(&models.Cart{}, &models.CartItem{}, &models.Order{}, &models.OrderItem{}, &models.Invoice{})
	if err != nil {
		return err
	}

	return nil
}
