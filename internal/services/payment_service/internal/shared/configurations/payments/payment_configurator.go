package payments

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	"gorm.io/gorm"

	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/config"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/shared/configurations/infrastructure"
	// "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/courses/configurations"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/shared/configurations/payments/infrastructure"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/configurations"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp/contracts"
	customEcho "gitlab.com/ctkai01/kaidemy/internal/pkg/http/custom_echo"
)

type PaymentsServiceConfigurator struct {
	contracts.Application
	infrastructureConfigurator *infrastructure.InfrastructureConfigurator
	paymentsModuleConfigurator *configurations.PaymentsModuleConfigurator
}

func NewPaymentsServiceConfigurator(app contracts.Application) *PaymentsServiceConfigurator {
	infraConfigurator := infrastructure.NewInfrastructureConfigurator(app)
	paymentModuleConfigurator := configurations.NewPaymentsModuleConfigurator(app)

	return &PaymentsServiceConfigurator{
		Application:                app,
		infrastructureConfigurator: infraConfigurator,
		paymentsModuleConfigurator: paymentModuleConfigurator,
	}
}

func (ic *PaymentsServiceConfigurator) ConfigurePayments() {
	// Shared
	// Infrastructure
	ic.infrastructureConfigurator.ConfigInfrastructures()
	// Shared
	// Payments configurations
	ic.ResolveFunc(func(gorm *gorm.DB) error {
		err := ic.migratePayments(gorm)
		if err != nil {
			return err
		}
		return nil
	})
	// Modules
	// Payment module
	ic.paymentsModuleConfigurator.ConfigurePaymentsModule()
}

func (ic *PaymentsServiceConfigurator) MapPaymentsEndpoints() {
	// Shared
	ic.ResolveFunc(
		func(paymentsServer customEcho.EchoHttpServer, cfg *config.Config) error {
			paymentsServer.SetupDefaultMiddlewares()

			// Config Payments root endpoint
			paymentsServer.RouteBuilder().
				RegisterRoutes(func(e *echo.Echo) {
					e.GET("", func(ec echo.Context) error {
						return ec.String(
							http.StatusOK,
							fmt.Sprintf(
								"%s is running...",
								cfg.AppOptions.GetMicroserviceNameUpper(),
							),
						)
					})
				})

			// Config catalogs swagger
			// ic.configSwagger(catalogsServer.RouteBuilder())

			return nil
		},
	)

	// Modules
	// Products PaymentsServiceModule endpoints
	ic.paymentsModuleConfigurator.MapPaymentsEndpoints()
}
