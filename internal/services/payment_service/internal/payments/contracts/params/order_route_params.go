package params

import (
	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"go.uber.org/fx"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
)

type OrdersRouteParams struct {
	fx.In
	Logger      logger.Logger
	OrdersGroup *echo.Group `name:"orders-echo-group"`
	Validator   *validator.Validate
}
