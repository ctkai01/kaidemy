package params

import (
	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"go.uber.org/fx"

	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/contracts/data"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
)

type CartsRouteParams struct {
	fx.In
	Logger         logger.Logger
	CartsGroup     *echo.Group `name:"carts-echo-group"`
	Validator      *validator.Validate
	CartRepository data.CartRepository
}
