package params

import (
	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"go.uber.org/fx"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
)

type InvoicesRouteParams struct {
	fx.In
	Logger      logger.Logger
	InvoicesGroup *echo.Group `name:"invoices-echo-group"`
	Validator   *validator.Validate
}
