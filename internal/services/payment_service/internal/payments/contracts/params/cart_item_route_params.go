package params

import (
	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"go.uber.org/fx"

	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/contracts/data"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
)

type CartItemsRouteParams struct {
	fx.In
	Logger             logger.Logger
	CartItemsGroup     *echo.Group `name:"cart-items-echo-group"`
	Validator          *validator.Validate
	CartRepository     data.CartRepository
	CartItemRepository data.CartItemRepository
}
