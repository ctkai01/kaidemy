package data

import (
	"context"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	// uuid "github.com/satori/go.uuid"

	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/models"
)

type CartItemRepository interface {
	// GetAllProducts(
	// 	ctx context.Context,
	// 	listQuery *utils.ListQuery,
	// ) (*utils.ListResult[*models.User], error)
	// SearchProducts(
	// 	ctx context.Context,
	// 	searchText string,
	// 	listQuery *utils.ListQuery,
	// ) (*utils.ListResult[*models.User], error)
	GetCartItemByID(ctx context.Context, id int) (*models.CartItem, error)
	// GetUserByEmail(ctx context.Context, email string) (*models.Course, error)
	// GetUserByEmailToken(ctx context.Context, emailToken string) (*models.Course, error)
	CreateCartItem(ctx context.Context, cartItem *models.CartItem) error
	// GetCartByUserId(ctx context.Context, idUser int) (*models.Cart, error)
	GetCartItemByCourseId(ctx context.Context, cartID int, courseID int) (*models.CartItem, error)
	GetCartItemsByCartId(ctx context.Context, cartID int) ([]*models.CartItem, error)
	DeleteCartItemByID(ctx context.Context, cartID int) error

	// UpdateUser(ctx context.Context, user *models.Course) error

	// UpdateEmailToken(ctx context.Context, id int, mailToken string) error
	// UpdateProduct(ctx context.Context, product *models.User) (*models.User, error)
	// DeleteProductByID(ctx context.Context, uuid uuid.UUID) error
}
