package data

import (
	"context"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	// uuid "github.com/satori/go.uuid"

	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/models"
	"gorm.io/gorm"
)

type OrderItemRepository interface {
	// GetAllProducts(
	// 	ctx context.Context,
	// 	listQuery *utils.ListQuery,
	// ) (*utils.ListResult[*models.User], error)
	// SearchProducts(
	// 	ctx context.Context,
	// 	searchText string,
	// 	listQuery *utils.ListQuery,
	// ) (*utils.ListResult[*models.User], error)
	// GetCartByID(ctx context.Context, id int) (*models.Cart, error)
	// GetUserByEmail(ctx context.Context, email string) (*models.Course, error)
	// GetUserByEmailToken(ctx context.Context, emailToken string) (*models.Course, error)
	CreateOrderItem(ctx context.Context, orderItem *models.OrderItem) error
	CreateMultipleOrderItem(ctx context.Context, orderItems []*models.OrderItem) error
	CreateMultipleOrderItemWithTransaction(ctx context.Context, tx *gorm.DB, orderItems []*models.OrderItem) error
	GetOrderItemByUserIdCourseID(ctx context.Context, idUser int, idCourse int) (*models.OrderItem, error)

	// UpdateUser(ctx context.Context, user *models.Course) error

	// UpdateEmailToken(ctx context.Context, id int, mailToken string) error
	// UpdateProduct(ctx context.Context, product *models.User) (*models.User, error)
	// DeleteProductByID(ctx context.Context, uuid uuid.UUID) error
}
