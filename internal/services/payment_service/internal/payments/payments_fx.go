package payments

import (
	// "fmt"

	// customEcho "gitlab.com/ctkai01/kaidemy/internal/pkg/http/custom_echo"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "fmt"

	"github.com/labstack/echo/v4"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/data/repositories"
	"go.uber.org/fx"

	// "github.com/mehdihadeli/go-ecommerce-microservices/internal/services/catalogreadservice/internal/products/data/repositories"
	// getProductByIdV1 "github.com/mehdihadeli/go-ecommerce-microservices/internal/services/catalogreadservice/internal/products/features/get_product_by_id/v1/endpoints"
	// changePassword "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/change_password/endpoints"
	// forgotPassword "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/forgot_password/endpoints"
	// loginStandard "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login/endpoints"
	// loginByGoogle "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login_by_google/endpoints"
	// register "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/register/endpoints"
	// requestLoginByGoogle "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/endpoints"
	// resetPassword "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/reset_password/endpoints"
	// createCourse "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/"
	// createCourse "gitlab.com/ctkai01/kaidemy/internal/services/catalogservice/internal/catalogs/features/create_course/endpoints"
	createCart "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/create_cart/endpoints"
	createCartItem "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/create_cart_item/endpoints"
	deleteCartItem "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/delete_cart_item/endpoints"
	createPayment "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/create_payment/endpoints"
	createInvoice "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/create_invoice/endpoints"
	getCart "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/get_cart/endpoints"

	// searchProductV1 "github.com/mehdihadeli/go-ecommerce-microservices/internal/services/catalogreadservice/internal/products/features/searching_products/v1/endpoints"
	customEcho "gitlab.com/ctkai01/kaidemy/internal/pkg/http/custom_echo"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/http/middlewares"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	// googleOauth "gitlab.com/ctkai01/kaidemy/internal/pkg/oauth/google"
)

var Module = fx.Module(
	"paymentsfx",

	// Other provides
	// fx.Provide(repositories.NewRedisProductRepository),
	// fx.Provide(NewHTTPServer),

	fx.Provide(repositories.NewMySqlCartRepository),
	fx.Provide(repositories.NewMySqlCartItemRepository),
	fx.Provide(repositories.NewMySqlOrderRepository),
	fx.Provide(repositories.NewMySqlOrderItemRepository),
	fx.Provide(repositories.NewMySqlInvoiceRepository),

	fx.Provide(fx.Annotate(func(paymentsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		paymentsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			v1.Use(middlewares.AuthMiddleware)
			group := v1.Group("/carts")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"carts-echo-group"`))),

	fx.Provide(fx.Annotate(func(paymentsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		paymentsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			v1.Use(middlewares.AuthMiddleware)
			group := v1.Group("/cart-items")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"cart-items-echo-group"`))),

	fx.Provide(fx.Annotate(func(paymentsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		paymentsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			v1.Use(middlewares.AuthMiddleware)
			group := v1.Group("/orders")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"orders-echo-group"`))),

	fx.Provide(fx.Annotate(func(paymentsServer customEcho.EchoHttpServer) *echo.Group {
		var g *echo.Group
		paymentsServer.RouteBuilder().RegisterGroupFunc("/api/v1", func(v1 *echo.Group) {
			v1.Use(middlewares.AuthMiddleware)
			group := v1.Group("/invoices")
			g = group
		})

		return g
	}, fx.ResultTags(`name:"invoices-echo-group"`))),
	// invoices-echo-group
	fx.Provide(
		// fx.Annotate(
		// 	loginByGoogle.NewLoginByGoogleEndpoint,
		// 	fx.As(new(route.Endpoint)),
		// 	fx.ResultTags(fmt.Sprintf(`group:"%s"`, "product-routes")),
		// ),

		//Cart route
		route.AsRoute(createCart.NewCreateCartEndpoint, "payments-routes"),
		route.AsRoute(getCart.NewGetCartEndpoint, "payments-routes"),
		
		route.AsRoute(createCartItem.NewCreateCartItemEndpoint, "payments-routes"),

		route.AsRoute(deleteCartItem.NewDeleteCartItemEndpoint, "payments-routes"),
		route.AsRoute(createPayment.NewCreatePaymentEndpoint, "payments-routes"),
		
		route.AsRoute(createInvoice.NewCreateInvoiceEndpoint, "payments-routes"),
	// route.AsRoute(register.NewRegisterEndpoint, "auth-routes"),
	// route.AsRoute(register.NewRegisterEndpoint, "auth-routes"),
	// route.AsRoute(loginStandard.NewLoginEndpoint, "auth-routes"),
	// route.AsRoute(forgotPassword.NewForgotPasswordEndpoint, "auth-routes"),
	// route.AsRoute(resetPassword.NewResetPasswordEndpoint, "auth-routes"),

	// //Users route
	// route.AsRoute(createCourse.NewCreateCourseEndpoint, "courses-routes"),
	// route.AsRoute(changeProfile.NewChangeProfileEndpoint, "users-routes"),

	),
)

//   lc.Append(fx.Hook{
//     OnStart: func(ctx context.Context) error {
//       ln, err := net.Listen("tcp", srv.Addr)
//       if err != nil {
//         return err
//       }
//       fmt.Println("Starting HTTP server at", srv.Addr)
//       go srv.Serve(ln)
//       return nil
//     },
//     OnStop: func(ctx context.Context) error {
//       return srv.Shutdown(ctx)
//     },
//   })
