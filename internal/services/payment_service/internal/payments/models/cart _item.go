package models

import (
	"time"
)

type CartItem struct {
	ID int `json:"id" gorm:"primary_key"`

	CartID int `json:"card_id" gorm:"type:int;not null"`
	CourseID int `json:"course_id" gorm:"type:int;not null"`

	UpdatedAt *time.Time `json:"updated_at" gorm:"column:updated_at"`
	CreatedAt *time.Time `json:"created_at" gorm:"column:created_at"`
}

func (CartItem) TableName() string { return "cart_items" }

// type ProductsList struct {
// 	TotalCount int64      `json:"totalCount" bson:"totalCount"`
// 	TotalPages int64      `json:"totalPages" bson:"totalPages"`
// 	Page       int64      `json:"page" bson:"page"`
// 	Size       int64      `json:"size" bson:"size"`
// 	Products   []*User `json:"products" bson:"products"`
// }
