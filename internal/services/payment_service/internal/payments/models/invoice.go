package models

import (
	"time"
)

type Invoice struct {
	ID     int `json:"id" gorm:"primary_key"`
	UserID int `json:"user_id" gorm:"type:int;not null"`

	OrderID   int        `json:"order_id" gorm:"type:int"`
	Status    int        `json:"status" gorm:"type:int;not null"`
	Total     float64    `json:"total" gorm:"not null"`
	UpdatedAt *time.Time `json:"updated_at" gorm:"column:updated_at"`
	CreatedAt *time.Time `json:"created_at" gorm:"column:created_at"`
}

func (Invoice) TableName() string { return "invoices" }
