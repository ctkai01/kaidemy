package models

import (
	"time"
	paymentservice "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/grpc/proto/service_clients"

)

type Cart struct {
	ID int `json:"id" gorm:"primary_key"`

	UserID int `json:"user_id" gorm:"type:int;not null"`
	Key *string 
	CartItems []CartItem `json:"cart_items" gorm:"constraint:OnDelete:CASCADE"`
	UpdatedAt *time.Time `json:"updated_at" gorm:"column:updated_at"`
	CreatedAt *time.Time `json:"created_at" gorm:"column:created_at"`
}

func (Cart) TableName() string { return "carts" }


type CartResponse struct {
	ID int `json:"id" `

	UserID int `json:"user_id"`
	CartItems []*CartItemResponse `json:"cart_items"`
	UpdatedAt *time.Time `json:"updated_at"`
	CreatedAt *time.Time `json:"created_at"`
}

type CartItemResponse struct {
	ID int `json:"id" `
	CartID int `json:"card_id" gorm:"type:int;not null"`
	Course *paymentservice.CourseDetail `json:"course"`

	UpdatedAt *time.Time `json:"updated_at"`
	CreatedAt *time.Time `json:"created_at"`
}

// type CourseReponse

// type ProductsList struct {
// 	TotalCount int64      `json:"totalCount" bson:"totalCount"`
// 	TotalPages int64      `json:"totalPages" bson:"totalPages"`
// 	Page       int64      `json:"page" bson:"page"`
// 	Size       int64      `json:"size" bson:"size"`
// 	Products   []*User `json:"products" bson:"products"`
// }
