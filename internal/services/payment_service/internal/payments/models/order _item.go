package models

import (
	"time"
)

type OrderItem struct {
	ID int `json:"id" gorm:"primary_key"`

	OrderID  int     `json:"order_id" gorm:"type:int;not null"`
	CourseID int     `json:"course_id" gorm:"type:int;not null"`
	UserID   int     `json:"user_id" gorm:"type:int;not null"`
	Price    float64 `json:"price" gorm:"not null"`

	UpdatedAt *time.Time `json:"updated_at" gorm:"column:updated_at"`
	CreatedAt *time.Time `json:"created_at" gorm:"column:created_at"`
}

func (OrderItem) TableName() string { return "order_items" }

// type ProductsList struct {
// 	TotalCount int64      `json:"totalCount" bson:"totalCount"`
// 	TotalPages int64      `json:"totalPages" bson:"totalPages"`
// 	Page       int64      `json:"page" bson:"page"`
// 	Size       int64      `json:"size" bson:"size"`
// 	Products   []*User `json:"products" bson:"products"`
// }
