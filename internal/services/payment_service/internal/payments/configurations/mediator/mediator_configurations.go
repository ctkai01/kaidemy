package mediator

import (
	// "emperror.dev/errors"
	// "github.com/mehdihadeli/go-mediatr"

	"github.com/mehdihadeli/go-mediatr"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/contracts/data"
	"gorm.io/gorm"

	// forgotPasswordCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/forgot_password/commands"
	// forgotPasswordDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/forgot_password/dtos"
	// loginCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login/commands"
	// loginDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login/dtos"
	// loginGoogleCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login_by_google/commands"
	// loginGoogleDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login_by_google/dtos"
	// loginGoogleQuery "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/login_by_google/queries"
	// registerCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/register/commands"
	// registerDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/register/dtos"
	// requestLoginGoogleCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/commands"
	// requestLoginGoogleDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	// resetPasswordCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/reset_password/commands"
	// resetPasswordDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/reset_password/dtos"

	// changePasswordCommand "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/change_password/commands"
	// changePasswordDtos "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/features/change_password/dtos"

	createCartCommand "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/create_cart/commands"
	createCartDtos "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/create_cart/dtos"

	createCartItemCommand "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/create_cart_item/commands"
	createCartItemDtos "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/create_cart_item/dtos"

	deleteCartItemCommand "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/delete_cart_item/commands"
	deleteCartItemDtos "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/delete_cart_item/dtos"

	createPaymentCommand "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/create_payment/commands"
	createPaymentDtos "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/create_payment/dtos"

	createInvoiceCommand "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/create_invoice/commands"
	createInvoiceDtos "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/create_invoice/dtos"

	createOrderCommand "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/create_order/commands"
	createOrderDtos "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/create_order/dtos"

	getCartDtos "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/get_cart/dtos"
	getCartQueries "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/get_cart/queries"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/configurations"
)

// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"

func ConfigPaymentMediator(
	logger logger.Logger,
	cartRepository data.CartRepository,
	cartItemRepository data.CartItemRepository,
	grpcClient grpc.GrpcClient,
	orderRepository data.OrderRepository,
	orderItemRepository data.OrderItemRepository,
	invoiceRepository data.InvoiceRepository,
	rabbitmqBuilder configurations.RabbitMQConfigurationBuilder,
	db *gorm.DB,

) error {
	err := mediatr.RegisterRequestHandler[*createCartCommand.CreateCart, *createCartDtos.CreateCartResponseDto](
		createCartCommand.NewCreateCartHandler(logger, cartRepository),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*createCartItemCommand.CreateCartItem, *createCartItemDtos.CreateCartItemResponseDto](
		createCartItemCommand.NewCreateCartItemHandler(logger, cartRepository, cartItemRepository, orderItemRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*deleteCartItemCommand.DeleteCartItem, *deleteCartItemDtos.DeleteCartItemResponseDto](
		deleteCartItemCommand.NewDeleteCartItemHandler(logger, cartRepository, cartItemRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*createPaymentCommand.CreatePayment, *createPaymentDtos.CreatePaymentResponseDto](
		createPaymentCommand.NewCreatePaymentHandler(logger, cartRepository, cartItemRepository, grpcClient, orderRepository, orderItemRepository),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*createInvoiceCommand.CreateInvoice, *createInvoiceDtos.CreateInvoiceResponseDto](
		createInvoiceCommand.NewCreateInvoiceHandler(logger, cartRepository, cartItemRepository, grpcClient, orderRepository, orderItemRepository, invoiceRepository, rabbitmqBuilder),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*createOrderCommand.CreateOrder, *createOrderDtos.CreateOrderResponseDto](
		createOrderCommand.NewCreateOrderHandler(logger, cartRepository, cartItemRepository, grpcClient, orderRepository, orderItemRepository, invoiceRepository, rabbitmqBuilder, db),
	)
	if err != nil {
		return err
	}

	err = mediatr.RegisterRequestHandler[*getCartQueries.GetCart, *getCartDtos.GetCartResponseDto](
		getCartQueries.NewGetCartHandler(logger, cartRepository, grpcClient),
	)
	if err != nil {
		return err
	}

	// 	getOrderQueries
	// getOrderDtos
	// err = mediatr.RegisterRequestHandler[*loginGoogleQuery.LoginGoogle, *loginGoogleDtos.LoginGoogleResponseDto](
	// 	loginGoogleCommand.NewLoginGoogleHandler(logger, googleOauth, userRepository),
	// )
	// if err != nil {
	// 	return err
	// }

	// err = mediatr.RegisterRequestHandler[*registerCommand.UserCreateByNormal, *registerDtos.RegisterResponseDto](
	// 	registerCommand.NewRegisterHandler(logger, googleOauth, userRepository),
	// )
	// if err != nil {
	// 	return err
	// }

	// err = mediatr.RegisterRequestHandler[*loginCommand.UserLoginStandard, *loginDtos.LoginResponseDto](
	// 	loginCommand.NewLoginHandler(logger, googleOauth, userRepository),
	// )
	// if err != nil {
	// 	return err
	// }

	// err = mediatr.RegisterRequestHandler[*forgotPasswordCommand.ForgotPassword, *forgotPasswordDtos.ForgotPasswordResponseDto](
	// 	forgotPasswordCommand.NewForgotPasswordHandler(logger, userRepository, rabbitmqBuilder),
	// )
	// if err != nil {
	// 	return err
	// }

	// err = mediatr.RegisterRequestHandler[*resetPasswordCommand.ResetPassword, *resetPasswordDtos.ResetPassResponseDto](
	// 	resetPasswordCommand.NewResetPasswordHandler(logger, userRepository),
	// )
	// if err != nil {
	// 	return err
	// }

	// err = mediatr.RegisterRequestHandler[*changePasswordCommand.ChangePassword, *changePasswordDtos.ChangePasswordResponseDto](
	// 	changePasswordCommand.NewChangePasswordHandler(logger, userRepository),
	// )
	// if err != nil {
	// 	return err
	// }

	// err = mediatr.RegisterRequestHandler[*changeProfileCommand.ChangeProfile, *changeProfileDtos.ChangeProfileResponseDto](
	// 	changeProfileCommand.NewChangeProfileHandler(logger, userRepository, minIoConfiguration),
	// )
	// if err != nil {
	// 	return err
	// }

	// err = mediatr.RegisterRequestHandler[*loginGoogleQuery.LoginGoogle, *loginGoogleDtos.LoginGoogleResponseDto](
	// 	loginGoogleCommand.NewLoginGoogleHandler(logger, googleOauth, userRepository),
	// )
	// if err != nil {
	// 	return err
	// }

	// err := mediatr.RegisterRequestHandler[*createProductCommandV1.CreateProduct, *createProductDtosV1.CreateProductResponseDto](
	// 	createProductCommandV1.NewCreateProductHandler(
	// 		logger,
	// 		mongoProductRepository,
	// 		cacheProductRepository,
	// 		tracer,
	// 	),
	// )
	// if err != nil {
	// 	return errors.WrapIf(err, "error while registering handlers in the mediator")
	// }

	// err = mediatr.RegisterRequestHandler[*deleteProductCommandV1.DeleteProduct, *mediatr.Unit](
	// 	deleteProductCommandV1.NewDeleteProductHandler(
	// 		logger,
	// 		mongoProductRepository,
	// 		cacheProductRepository,
	// 		tracer,
	// 	),
	// )
	// if err != nil {
	// 	return errors.WrapIf(err, "error while registering handlers in the mediator")
	// }

	// err = mediatr.RegisterRequestHandler[*updateProductCommandV1.UpdateProduct, *mediatr.Unit](
	// 	updateProductCommandV1.NewUpdateProductHandler(
	// 		logger,
	// 		mongoProductRepository,
	// 		cacheProductRepository,
	// 		tracer,
	// 	),
	// )
	// if err != nil {
	// 	return errors.WrapIf(err, "error while registering handlers in the mediator")
	// }

	// err = mediatr.RegisterRequestHandler[*getProductsQueryV1.GetProducts, *getProductsDtoV1.GetProductsResponseDto](
	// 	getProductsQueryV1.NewGetProductsHandler(logger, mongoProductRepository, tracer),
	// )
	// if err != nil {
	// 	return errors.WrapIf(err, "error while registering handlers in the mediator")
	// }

	// err = mediatr.RegisterRequestHandler[*searchProductsQueryV1.SearchProducts, *searchProductsDtosV1.SearchProductsResponseDto](
	// 	searchProductsQueryV1.NewSearchProductsHandler(
	// 		logger,
	// 		mongoProductRepository,
	// 		tracer,
	// 	),
	// )
	// if err != nil {
	// 	return errors.WrapIf(err, "error while registering handlers in the mediator")
	// }

	// err = mediatr.RegisterRequestHandler[*getProductByIdQueryV1.GetProductById, *getProductByIdDtosV1.GetProductByIdResponseDto](
	// 	getProductByIdQueryV1.NewGetProductByIdHandler(
	// 		logger,
	// 		mongoProductRepository,
	// 		cacheProductRepository,
	// 		tracer,
	// 	),
	// )
	// if err != nil {
	// 	return errors.WrapIf(err, "error while registering handlers in the mediator")
	// }

	return nil
}
