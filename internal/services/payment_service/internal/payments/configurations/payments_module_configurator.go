package configurations

import (
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/configurations/mappings"
	"gorm.io/gorm"
	// "golang.org/x/oauth2"

	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/configurations/mediator"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/contracts/data"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/fxapp/contracts"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	logger2 "gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/configurations"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/otel/tracing"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/configurations/mediator"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
)

type PaymentsModuleConfigurator struct {
	contracts.Application
}

func NewPaymentsModuleConfigurator(
	app contracts.Application,
) *PaymentsModuleConfigurator {
	return &PaymentsModuleConfigurator{
		Application: app,
	}
}

func (c *PaymentsModuleConfigurator) ConfigurePaymentsModule() {
	c.ResolveFunc(
		func(logger logger2.Logger, cartRepository data.CartRepository, cartItemRepository data.CartItemRepository, grpcClient grpc.GrpcClient, orderRepository data.OrderRepository, orderItemRepository data.OrderItemRepository, purchaseRepository data.InvoiceRepository, rabbitmqBuilder configurations.RabbitMQConfigurationBuilder, db *gorm.DB) error {
			// Config Payments Mediators
			err := mediator.ConfigPaymentMediator(logger, cartRepository, cartItemRepository, grpcClient, orderRepository, orderItemRepository, purchaseRepository, rabbitmqBuilder, db)
			if err != nil {
				return err
			}

			// Config Payments Mappings
			err = mappings.ConfigurePaymentsMappings()
			if err != nil {
				return err
			}
			return nil
		},
	)
}

func (c *PaymentsModuleConfigurator) MapPaymentsEndpoints() {
	// Config Course Http Endpoints
	c.ResolveFuncWithParamTag(func(endpoints []route.Endpoint) {
		for _, endpoint := range endpoints {
			endpoint.MapEndpoint()
		}
	}, `group:"payments-routes"`,
	)
}
