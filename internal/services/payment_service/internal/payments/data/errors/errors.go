package errors

import (
	// "strings"

	"emperror.dev/errors"
)

// func CheckDuplicateEmail(err error) bool {
// 	return strings.Contains(err.Error(), "Duplicate")
// }

var (
	ErrAlreadyCart = errors.New("cart already existed")
)

func WrapError(err error) error {
	return errors.Wrap(err, "error handling request")
}
