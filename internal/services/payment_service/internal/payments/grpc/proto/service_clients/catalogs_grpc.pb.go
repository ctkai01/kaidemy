// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.3.0
// - protoc             v3.6.1
// source: catalogs.proto

package courses_service

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

const (
	CatalogsService_GetCategoryByID_FullMethodName  = "/catalogs_service.CatalogsService/GetCategoryByID"
	CatalogsService_GetLevelByID_FullMethodName     = "/catalogs_service.CatalogsService/GetLevelByID"
	CatalogsService_GetPriceByID_FullMethodName     = "/catalogs_service.CatalogsService/GetPriceByID"
	CatalogsService_GetLanguageByID_FullMethodName  = "/catalogs_service.CatalogsService/GetLanguageByID"
	CatalogsService_GetIssueTypeByID_FullMethodName = "/catalogs_service.CatalogsService/GetIssueTypeByID"
)

// CatalogsServiceClient is the client API for CatalogsService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type CatalogsServiceClient interface {
	// rpc CreateProduct(CreateProductReq) returns (CreateProductRes);
	// rpc UpdateProduct(UpdateProductReq) returns (UpdateProductRes);
	GetCategoryByID(ctx context.Context, in *GetCategoryByIdReq, opts ...grpc.CallOption) (*GetCategoryByIdRes, error)
	GetLevelByID(ctx context.Context, in *GetLevelByIdReq, opts ...grpc.CallOption) (*GetLevelByIdRes, error)
	GetPriceByID(ctx context.Context, in *GetPriceByIdReq, opts ...grpc.CallOption) (*GetPriceByIdRes, error)
	GetLanguageByID(ctx context.Context, in *GetLanguageByIdReq, opts ...grpc.CallOption) (*GetLanguageByIdRes, error)
	GetIssueTypeByID(ctx context.Context, in *GetIssueTypeByIdReq, opts ...grpc.CallOption) (*GetIssueTypeByIdRes, error)
}

type catalogsServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewCatalogsServiceClient(cc grpc.ClientConnInterface) CatalogsServiceClient {
	return &catalogsServiceClient{cc}
}

func (c *catalogsServiceClient) GetCategoryByID(ctx context.Context, in *GetCategoryByIdReq, opts ...grpc.CallOption) (*GetCategoryByIdRes, error) {
	out := new(GetCategoryByIdRes)
	err := c.cc.Invoke(ctx, CatalogsService_GetCategoryByID_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *catalogsServiceClient) GetLevelByID(ctx context.Context, in *GetLevelByIdReq, opts ...grpc.CallOption) (*GetLevelByIdRes, error) {
	out := new(GetLevelByIdRes)
	err := c.cc.Invoke(ctx, CatalogsService_GetLevelByID_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *catalogsServiceClient) GetPriceByID(ctx context.Context, in *GetPriceByIdReq, opts ...grpc.CallOption) (*GetPriceByIdRes, error) {
	out := new(GetPriceByIdRes)
	err := c.cc.Invoke(ctx, CatalogsService_GetPriceByID_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *catalogsServiceClient) GetLanguageByID(ctx context.Context, in *GetLanguageByIdReq, opts ...grpc.CallOption) (*GetLanguageByIdRes, error) {
	out := new(GetLanguageByIdRes)
	err := c.cc.Invoke(ctx, CatalogsService_GetLanguageByID_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *catalogsServiceClient) GetIssueTypeByID(ctx context.Context, in *GetIssueTypeByIdReq, opts ...grpc.CallOption) (*GetIssueTypeByIdRes, error) {
	out := new(GetIssueTypeByIdRes)
	err := c.cc.Invoke(ctx, CatalogsService_GetIssueTypeByID_FullMethodName, in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// CatalogsServiceServer is the server API for CatalogsService service.
// All implementations should embed UnimplementedCatalogsServiceServer
// for forward compatibility
type CatalogsServiceServer interface {
	// rpc CreateProduct(CreateProductReq) returns (CreateProductRes);
	// rpc UpdateProduct(UpdateProductReq) returns (UpdateProductRes);
	GetCategoryByID(context.Context, *GetCategoryByIdReq) (*GetCategoryByIdRes, error)
	GetLevelByID(context.Context, *GetLevelByIdReq) (*GetLevelByIdRes, error)
	GetPriceByID(context.Context, *GetPriceByIdReq) (*GetPriceByIdRes, error)
	GetLanguageByID(context.Context, *GetLanguageByIdReq) (*GetLanguageByIdRes, error)
	GetIssueTypeByID(context.Context, *GetIssueTypeByIdReq) (*GetIssueTypeByIdRes, error)
}

// UnimplementedCatalogsServiceServer should be embedded to have forward compatible implementations.
type UnimplementedCatalogsServiceServer struct {
}

func (UnimplementedCatalogsServiceServer) GetCategoryByID(context.Context, *GetCategoryByIdReq) (*GetCategoryByIdRes, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetCategoryByID not implemented")
}
func (UnimplementedCatalogsServiceServer) GetLevelByID(context.Context, *GetLevelByIdReq) (*GetLevelByIdRes, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetLevelByID not implemented")
}
func (UnimplementedCatalogsServiceServer) GetPriceByID(context.Context, *GetPriceByIdReq) (*GetPriceByIdRes, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetPriceByID not implemented")
}
func (UnimplementedCatalogsServiceServer) GetLanguageByID(context.Context, *GetLanguageByIdReq) (*GetLanguageByIdRes, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetLanguageByID not implemented")
}
func (UnimplementedCatalogsServiceServer) GetIssueTypeByID(context.Context, *GetIssueTypeByIdReq) (*GetIssueTypeByIdRes, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetIssueTypeByID not implemented")
}

// UnsafeCatalogsServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to CatalogsServiceServer will
// result in compilation errors.
type UnsafeCatalogsServiceServer interface {
	mustEmbedUnimplementedCatalogsServiceServer()
}

func RegisterCatalogsServiceServer(s grpc.ServiceRegistrar, srv CatalogsServiceServer) {
	s.RegisterService(&CatalogsService_ServiceDesc, srv)
}

func _CatalogsService_GetCategoryByID_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetCategoryByIdReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CatalogsServiceServer).GetCategoryByID(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: CatalogsService_GetCategoryByID_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CatalogsServiceServer).GetCategoryByID(ctx, req.(*GetCategoryByIdReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _CatalogsService_GetLevelByID_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetLevelByIdReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CatalogsServiceServer).GetLevelByID(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: CatalogsService_GetLevelByID_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CatalogsServiceServer).GetLevelByID(ctx, req.(*GetLevelByIdReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _CatalogsService_GetPriceByID_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetPriceByIdReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CatalogsServiceServer).GetPriceByID(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: CatalogsService_GetPriceByID_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CatalogsServiceServer).GetPriceByID(ctx, req.(*GetPriceByIdReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _CatalogsService_GetLanguageByID_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetLanguageByIdReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CatalogsServiceServer).GetLanguageByID(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: CatalogsService_GetLanguageByID_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CatalogsServiceServer).GetLanguageByID(ctx, req.(*GetLanguageByIdReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _CatalogsService_GetIssueTypeByID_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetIssueTypeByIdReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CatalogsServiceServer).GetIssueTypeByID(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: CatalogsService_GetIssueTypeByID_FullMethodName,
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CatalogsServiceServer).GetIssueTypeByID(ctx, req.(*GetIssueTypeByIdReq))
	}
	return interceptor(ctx, in, info, handler)
}

// CatalogsService_ServiceDesc is the grpc.ServiceDesc for CatalogsService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var CatalogsService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "catalogs_service.CatalogsService",
	HandlerType: (*CatalogsServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetCategoryByID",
			Handler:    _CatalogsService_GetCategoryByID_Handler,
		},
		{
			MethodName: "GetLevelByID",
			Handler:    _CatalogsService_GetLevelByID_Handler,
		},
		{
			MethodName: "GetPriceByID",
			Handler:    _CatalogsService_GetPriceByID_Handler,
		},
		{
			MethodName: "GetLanguageByID",
			Handler:    _CatalogsService_GetLanguageByID_Handler,
		},
		{
			MethodName: "GetIssueTypeByID",
			Handler:    _CatalogsService_GetIssueTypeByID_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "catalogs.proto",
}
