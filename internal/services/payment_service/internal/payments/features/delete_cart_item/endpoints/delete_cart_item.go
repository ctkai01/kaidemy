package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/contracts/params"
	paymentsError "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/delete_cart_item/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/delete_cart_item/dtos"
)

type deleteCartItemEndpoint struct {
	params.CartItemsRouteParams
}

func NewDeleteCartItemEndpoint(
	params params.CartItemsRouteParams,
) route.Endpoint {
	return &deleteCartItemEndpoint{
		CartItemsRouteParams: params,
	}
}

func (ep *deleteCartItemEndpoint) MapEndpoint() {
	ep.CartItemsGroup.DELETE("/:id", ep.handler())
}

func (ep *deleteCartItemEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.DeleteCartItemRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[deleteCartItem.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[deleteCartItemEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}

		userId := utils.GetUserId(c)
		
		command, errValidate := commands.NewDeleteCartItem(
			userId,
			request.CartItemID,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[deleteCartItemEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[deleteCartItemEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.DeleteCartItem, *dtos.DeleteCartItemResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == paymentsError.WrapError(customErrors.ErrorCannotPermission).Error() {
				errCustom := errors.WithMessage(
					err,
					"[deleteCartItemEndpoint_handler.Send] error in sending delete Cart Item",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[deleteCartItem.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewForbiddenErrorHttp(c, err.Error())
			}

			err = errors.WithMessage(
				err,
				"[deleteCartItemEndpoint_handler.Send] error in sending delete Cart Item",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[deleteCartItem.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
