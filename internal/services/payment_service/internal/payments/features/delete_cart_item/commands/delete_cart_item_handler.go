package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/contracts/data"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/delete_cart_item/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	// paymentsError "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/data/errors"

	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type DeleteCartItemHandler struct {
	log                logger.Logger
	cartItemRepository data.CartItemRepository
	cartRepository     data.CartRepository
	grpcClient         grpc.GrpcClient
}

func NewDeleteCartItemHandler(
	log logger.Logger,
	cartRepository data.CartRepository,
	cartItemRepository data.CartItemRepository,
	grpcClient grpc.GrpcClient,
) *DeleteCartItemHandler {
	return &DeleteCartItemHandler{log: log, cartRepository: cartRepository, cartItemRepository: cartItemRepository, grpcClient: grpcClient}
}

func (c *DeleteCartItemHandler) Handle(ctx context.Context, command *DeleteCartItem) (*dtos.DeleteCartItemResponseDto, error) {
	// CartItem -> Cart
	// Cart -> UserID
	cartItem, err := c.cartItemRepository.GetCartItemByID(ctx, command.IdCartItem)

	if err != nil {
		return nil, err
	}

	cart, err := c.cartRepository.GetCartByID(ctx, cartItem.CartID)

	if err != nil {
		return nil, err
	}

	if cart.UserID != command.IdUser {
		return nil, customErrors.ErrorCannotPermission
	}

	err = c.cartItemRepository.DeleteCartItemByID(ctx, cartItem.ID)

	
	if err != nil {
		return nil, err
	}
	

	return &dtos.DeleteCartItemResponseDto{
		Message:  "Delete cart item successfully!",
	}, nil
}
