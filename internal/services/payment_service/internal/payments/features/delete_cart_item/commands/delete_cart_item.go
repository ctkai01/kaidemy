package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type DeleteCartItem struct {
	IdUser        int
	IdCartItem        int
}

func NewDeleteCartItem(idUser int, idCartItem int) (*DeleteCartItem, *validator.ValidateError) {
	command := &DeleteCartItem{idUser, idCartItem }
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
