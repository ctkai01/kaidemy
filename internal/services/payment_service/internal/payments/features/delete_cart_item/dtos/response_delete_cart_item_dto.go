package dtos

//https://echo.labstack.com/guide/response/

type DeleteCartItemResponseDto struct {
	Message  string          `json:"message"`
}
