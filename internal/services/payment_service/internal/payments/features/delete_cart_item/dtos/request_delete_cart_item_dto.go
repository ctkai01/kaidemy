package dtos

//https://echo.labstack.com/guide/response/

type DeleteCartItemRequestDto struct {
	CartItemID int `json:"-" param:"id"`
}
