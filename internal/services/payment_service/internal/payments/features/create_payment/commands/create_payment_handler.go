package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"time"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/contracts/data"
	// "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/create_payment/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	// paymentsError "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/data/errors"
	stripe "github.com/stripe/stripe-go/v76"
	"github.com/stripe/stripe-go/v76/checkout/session"
	stripeProduct "github.com/stripe/stripe-go/v76/product"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	paymentservice "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/grpc/proto/service_clients"
)

type CreatePaymentHandler struct {
	log                 logger.Logger
	cartItemRepository  data.CartItemRepository
	cartRepository      data.CartRepository
	orderRepository     data.OrderRepository
	orderItemRepository data.OrderItemRepository
	grpcClient          grpc.GrpcClient
}

func NewCreatePaymentHandler(
	log logger.Logger,
	cartRepository data.CartRepository,
	cartItemRepository data.CartItemRepository,
	grpcClient grpc.GrpcClient,
	orderRepository data.OrderRepository,
	orderItemRepository data.OrderItemRepository,
) *CreatePaymentHandler {
	return &CreatePaymentHandler{log: log,
		cartRepository:      cartRepository,
		cartItemRepository:  cartItemRepository,
		grpcClient:          grpcClient,
		orderRepository:     orderRepository,
		orderItemRepository: orderItemRepository,
	}
}

func (c *CreatePaymentHandler) Handle(ctx context.Context, command *CreatePayment) (*dtos.CreatePaymentResponseDto, error) {

	cart, err := c.cartRepository.GetCartByUserId(ctx, command.IdUser)

	isNotExistCart := false
	if err != nil {
		if err == customErrors.ErrCartNotFound {
			isNotExistCart = true
		} else {
			return nil, err
		}
	}

	if isNotExistCart {
		return nil, customErrors.ErrHasNotCart
	}

	cartItems, err := c.cartItemRepository.GetCartItemsByCartId(ctx, cart.ID)

	if err != nil {
		return nil, err
	}

	conn, err := c.grpcClient.GetGrpcConnection("course_service")
	if err != nil {
		return nil, err
	}

	client := paymentservice.NewCoursesServiceClient(conn)
	// dataCourse, err := client.GetCourseByID(context.Background(), &paymentservice.GetCourseByIdReq{
	// 	CourseID: int32(command.IdCourse),
	// })
	connUser, err := c.grpcClient.GetGrpcConnection("user_service")
	if err != nil {
		return nil, err
	}

	clientUser := paymentservice.NewUsersServiceClient(connUser)

	connCatalog, err := c.grpcClient.GetGrpcConnection("catalog_service")
	if err != nil {
		return nil, err
	}

	clientCatalog := paymentservice.NewCatalogsServiceClient(connCatalog)

	checkoutSessionLineItemParams := make([]*stripe.CheckoutSessionLineItemParams, 0)
	stripe.Key = "sk_test_51NUmJOFEsDSfMpHunO8Zscoo1KwMISvWpj00QwhHnLTjYQVkQF1uH0WBgCd8pWgfd5gAKBguz9MfszFPlQwOVPXl00ZUdeBaS5"
	totalPrice := 0
	authorAccountStripeID := ""
	for _, cartItem := range cartItems {
		dataCourse, err := client.GetCourseByID(context.Background(), &paymentservice.GetCourseByIdReq{
			CourseID: int32(cartItem.CourseID),
		})

		if err != nil {
			return nil, err
		}

		dataPrice, err := clientCatalog.GetPriceByID(context.Background(), &paymentservice.GetPriceByIdReq{
			PriceId: dataCourse.Course.PriceID,
		})

		if err != nil {
			return nil, err
		}

		totalPrice += int(dataPrice.Price.Value)

		p, _ := stripeProduct.Get(dataCourse.Course.ProductStripeID, nil)
		if err != nil {
			return nil, err
		}
		if authorAccountStripeID == "" {
			userAuthor, err := clientUser.GetUserByID(context.Background(), &paymentservice.GetUserByIdReq{
				UserId: dataCourse.Course.UserID,
			})

			if err != nil {
				return nil, err
			}

			authorAccountStripeID = userAuthor.User.AccountStripeID
		}

		// fmt.Println("p.DefaultPrice: ", cartItem.)

		checkoutSessionLineItemParams = append(checkoutSessionLineItemParams, &stripe.CheckoutSessionLineItemParams{
			// Provide the exact Price ID (for example, pr_1234) of the product you want to sell
			Price:    stripe.String(p.DefaultPrice.ID),
			Quantity: stripe.Int64(1),
		})
	}
	key := generateHashKey(fmt.Sprintf("%d", time.Now().Unix()))

	applicationFee := totalPrice * 10 / 100
	fmt.Println("applicationFee: ", applicationFee)
	fmt.Println("totalPrice: ", totalPrice)
	checkoutSessionParams := &stripe.CheckoutSessionParams{
		LineItems:  checkoutSessionLineItemParams,
		Mode:       stripe.String(string(stripe.CheckoutSessionModePayment)),
		SuccessURL: stripe.String(fmt.Sprintf("http://localhost:5173/checkout-success?key=%s", key)),
		CancelURL:  stripe.String("http://localhost:5173/checkout-cancel"),
		PaymentIntentData: &stripe.CheckoutSessionPaymentIntentDataParams{
			ApplicationFeeAmount: stripe.Int64(int64(300)),
			// ApplicationFeeAmount: stripe.Int64(int64(applicationFee)),
			TransferData: &stripe.CheckoutSessionPaymentIntentDataTransferDataParams{
				Destination: stripe.String(authorAccountStripeID),
			},
		},
	}

	s, err := session.New(checkoutSessionParams)

	if err != nil {
		return nil, err
	}
	cart.Key = &key
	if err := c.cartRepository.UpdateCart(ctx, cart); err != nil {
		return nil, err
	}

	return &dtos.CreatePaymentResponseDto{
		Message: "Create purchase successfully!",
		URL:     s.URL,
	}, nil
}

func generateHashKey(input string) string {
	hash := sha256.New()
	hash.Write([]byte(input))
	hashed := hash.Sum(nil)
	return hex.EncodeToString(hashed)
}
