package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type CreatePayment struct {
	IdUser        int
}

func NewCreatePayment(idUser int ) (*CreatePayment, *validator.ValidateError) {
	command := &CreatePayment{idUser }
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
