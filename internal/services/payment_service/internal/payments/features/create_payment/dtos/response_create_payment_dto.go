package dtos

//https://echo.labstack.com/guide/response/

type CreatePaymentResponseDto struct {
	Message string `json:"message"`
	URL     string `json:"url"`
}
