package dtos

import "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/models"

//https://echo.labstack.com/guide/response/

type GetCartResponseDto struct {
	Message string      `json:"message"`
	Cart    *models.CartResponse `json:"cart"`
}
