package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/contracts/params"
	paymentsError "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/get_cart/queries"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/get_cart/dtos"
)

type getCartEndpoint struct {
	params.CartsRouteParams
}

func NewGetCartEndpoint(
	params params.CartsRouteParams,
) route.Endpoint {
	return &getCartEndpoint{
		CartsRouteParams: params,
	}
}

func (ep *getCartEndpoint) MapEndpoint() {
	ep.CartsGroup.GET("", ep.handler())
}

func (ep *getCartEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		userId := utils.GetUserId(c)
		fmt.Println("UserID: ", userId)
		command, errValidate := queries.NewGetCart(
			userId,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[getCartEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[getCartEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*queries.GetCart, *dtos.GetCartResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == paymentsError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[getCartEndpoint_handler.Send] error in sending create Cart",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[getCart.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}
			// if err.Error() == paymentsError.WrapError(paymentsError.ErrAlreadyCart).Error() {
			// 	errCustom := errors.WithMessage(
			// 		err,
			// 		"[getCartEndpoint_handler.Send] error in sending create Cart",
			// 	)
			// 	ep.Logger.Error(
			// 		fmt.Sprintf(
			// 			"[getCart.Send], err: %v",
			// 			errCustom,
			// 		),
			// 	)
			// 	return customErrors.NewConflictErrorHttp(c, err.Error())
			// }

			err = errors.WithMessage(
				err,
				"[getCartEndpoint_handler.Send] error in sending create Cart",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[getCart.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusOK, result)
	}
}
