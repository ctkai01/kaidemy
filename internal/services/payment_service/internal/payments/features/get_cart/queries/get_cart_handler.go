package queries

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"fmt"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/contracts/data"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/get_cart/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	paymentservice "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/grpc/proto/service_clients"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/models"

	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type getCartHandler struct {
	log            logger.Logger
	cartRepository data.CartRepository
	grpcClient     grpc.GrpcClient
}

func NewGetCartHandler(
	log logger.Logger,
	cartRepository data.CartRepository,
	grpcClient grpc.GrpcClient,

) *getCartHandler {
	return &getCartHandler{log: log, cartRepository: cartRepository, grpcClient: grpcClient}
}

func (c *getCartHandler) Handle(ctx context.Context, command *GetCart) (*dtos.GetCartResponseDto, error) {
	cart, err := c.cartRepository.GetCartByUserIdRelation(ctx, []string{"CartItems"}, command.IdUser)
	var cartResponse *models.CartResponse

	// isNotExistCart := false
	if err != nil {
		if err == customErrors.ErrCartNotFound {
			// isNotExistCart = true
		} else {
			return nil, err
		}
	}

	if cart != nil {
		fmt.Println(" cart: ", cart.ID)
		fmt.Println(" cartResponse: ", cartResponse)

		conn, err := c.grpcClient.GetGrpcConnection("course_service")

		if err != nil {
			return nil, err
		}
		client := paymentservice.NewCoursesServiceClient(conn)

		cartItems := make([]*models.CartItemResponse, 0)

		for _, cartItem := range cart.CartItems {
			dataCourse, err := client.GetCourseDetailByID(ctx, &paymentservice.GetCourseDetailByIdReq{
				CourseID: int32(cartItem.CourseID),
			})
			if err != nil {
				return nil, err
			}
			cartItems = append(cartItems, &models.CartItemResponse{
				ID:        cartItem.ID,
				CartID:    cartItem.CartID,
				Course:    dataCourse.Course,
				UpdatedAt: cartItem.UpdatedAt,
				CreatedAt: cartItem.CreatedAt,
			})
			fmt.Println("Data course: ", dataCourse.Course)
		}

		cartResponse = &models.CartResponse{
			ID:        cart.ID,
			UserID:    cart.UserID,
			CartItems: cartItems,
			UpdatedAt: cart.UpdatedAt,
			CreatedAt: cart.CreatedAt,
		}
	}

	fmt.Println("Cart: ", cart)
	return &dtos.GetCartResponseDto{
		Message: "Get cart successfully!",
		Cart:    cartResponse,
	}, nil
}

// func marshalCartItems(cartItems []*CartItemResponse) ([]byte, error) {
//     if cartItems == nil {
//         return []byte("[]"), nil // Return empty array if cartItems is nil
//     }

//     return json.Marshal(cartItems)
// }