package queries

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type GetCart struct {
	IdUser        int
}

func NewGetCart(idUser int) (*GetCart, *validator.ValidateError) {
	command := &GetCart{idUser }
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
