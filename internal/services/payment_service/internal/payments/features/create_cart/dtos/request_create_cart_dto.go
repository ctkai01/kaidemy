package dtos

//https://echo.labstack.com/guide/response/

type CreateCartRequestDto struct {
	Title         string `form:"title"`
	CategoryID    string `form:"category_id"`
	SubCategoryID string `form:"sub_category_id"`
}
