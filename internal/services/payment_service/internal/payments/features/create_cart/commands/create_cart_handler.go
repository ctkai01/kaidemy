package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"fmt"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/models"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/create_cart/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	paymentsError "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/data/errors"

	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type CreateCartHandler struct {
	log            logger.Logger
	cartRepository data.CartRepository
}

func NewCreateCartHandler(
	log logger.Logger,
	cartRepository data.CartRepository,

) *CreateCartHandler {
	return &CreateCartHandler{log: log, cartRepository: cartRepository}
}

func (c *CreateCartHandler) Handle(ctx context.Context, command *CreateCart) (*dtos.CreateCartResponseDto, error) {
	cart, err := c.cartRepository.GetCartByUserId(ctx, command.IdUser)

	isNotExistCart := false
	if err != nil {
		if err == customErrors.ErrCartNotFound {
			isNotExistCart = true
		} else {
			return nil, err
		}
	}
	fmt.Println("Cart: ", cart)

	if isNotExistCart {
		cartCreate := &models.Cart{
			UserID: command.IdUser,
		}

		err := c.cartRepository.CreateCart(ctx, cartCreate)

		if err != nil {
			return nil, err
		}
		cartCreate.CartItems = make([]models.CartItem, 0)
		return &dtos.CreateCartResponseDto{
			Message: "Create cart successfully!",
			Cart: *cartCreate,
		}, nil
	} else {
		return nil, paymentsError.ErrAlreadyCart
	}
}
