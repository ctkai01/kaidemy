package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type CreateCart struct {
	IdUser        int
}

func NewCreateCart(idUser int) (*CreateCart, *validator.ValidateError) {
	command := &CreateCart{idUser }
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
