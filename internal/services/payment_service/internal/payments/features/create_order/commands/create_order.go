package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type CreateOrder struct {
	InvoiceID int
}

func NewCreateOrder(invoiceID int) (*CreateOrder, *validator.ValidateError) {
	command := &CreateOrder{invoiceID}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
