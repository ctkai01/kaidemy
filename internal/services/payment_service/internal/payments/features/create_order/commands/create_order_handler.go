package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"encoding/json"
	"fmt"

	// "fmt"
	// "math/rand"
	// "time"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gorm.io/gorm"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/models"
	// "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/create_order/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	stripe "github.com/stripe/stripe-go/v76"
	stripePrice "github.com/stripe/stripe-go/v76/price"
	stripeProduct "github.com/stripe/stripe-go/v76/product"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/configurations"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/producer"

	// paymentsError "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/data/errors"
	// customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	paymentservice "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/grpc/proto/service_clients"
)

type CreateOrderHandler struct {
	log                 logger.Logger
	cartItemRepository  data.CartItemRepository
	cartRepository      data.CartRepository
	orderRepository     data.OrderRepository
	orderItemRepository data.OrderItemRepository
	invoiceRepository   data.InvoiceRepository
	grpcClient          grpc.GrpcClient
	rabbitmqBuilder     configurations.RabbitMQConfigurationBuilder
	db                  *gorm.DB
}

func NewCreateOrderHandler(
	log logger.Logger,
	cartRepository data.CartRepository,
	cartItemRepository data.CartItemRepository,
	grpcClient grpc.GrpcClient,
	orderRepository data.OrderRepository,
	orderItemRepository data.OrderItemRepository,
	invoiceRepository data.InvoiceRepository,
	rabbitmqBuilder configurations.RabbitMQConfigurationBuilder,
	db *gorm.DB,
) *CreateOrderHandler {
	return &CreateOrderHandler{
		log:                 log,
		cartRepository:      cartRepository,
		cartItemRepository:  cartItemRepository,
		grpcClient:          grpcClient,
		orderRepository:     orderRepository,
		orderItemRepository: orderItemRepository,
		invoiceRepository:   invoiceRepository,
		rabbitmqBuilder:     rabbitmqBuilder,
		db:                  db,
	}
}

func (c *CreateOrderHandler) Handle(ctx context.Context, command *CreateOrder) (*dtos.CreateOrderResponseDto, error) {
	// get invoice
	invoice, err := c.invoiceRepository.GetInvoiceById(ctx, command.InvoiceID)

	if err != nil {
		return nil, err
	}

	// fmt.Println("DB: ", c.db)

	// Create Order
	orderCreate := &models.Order{
		UserID: invoice.UserID,
	}
	createOrderTx := c.db.WithContext(ctx).Begin()
	// err = c.orderRepository.CreateOrder(ctx, orderCreate)
	err = c.orderRepository.CreateOrderWithTransaction(ctx, createOrderTx, orderCreate)

	if err != nil {
		createOrderTx.Rollback()
		return nil, err
	}

	// Get cart by user
	cart, err := c.cartRepository.GetCartByUserId(ctx, invoice.UserID)

	isNotExistCart := false
	if err != nil {
		if err == customErrors.ErrCartNotFound {
			isNotExistCart = true
		} else {
			createOrderTx.Rollback()
			return nil, err
		}
	}

	if isNotExistCart {
		createOrderTx.Rollback()
		return nil, customErrors.ErrHasNotCart
	}

	// Get cart item by user
	cartItems, err := c.cartItemRepository.GetCartItemsByCartId(ctx, cart.ID)

	if err != nil {
		createOrderTx.Rollback()
		return nil, err
	}

	conn, err := c.grpcClient.GetGrpcConnection("course_service")

	if err != nil {
		createOrderTx.Rollback()
		return nil, err
	}

	client := paymentservice.NewCoursesServiceClient(conn)
	stripe.Key = "sk_test_51NUmJOFEsDSfMpHunO8Zscoo1KwMISvWpj00QwhHnLTjYQVkQF1uH0WBgCd8pWgfd5gAKBguz9MfszFPlQwOVPXl00ZUdeBaS5"
	orderItems := make([]*models.OrderItem, 0)

	var totalMoney float64
	// Create multiple order items
	for _, cartItem := range cartItems {

		dataCourse, err := client.GetCourseByID(context.Background(), &paymentservice.GetCourseByIdReq{
			CourseID: int32(cartItem.CourseID),
		})

		if err != nil {
			createOrderTx.Rollback()
			return nil, err
		}

		productStripe, err := stripeProduct.Get(dataCourse.Course.ProductStripeID, nil)

		if err != nil {
			createOrderTx.Rollback()
			return nil, err
		}

		priceStripe, err := stripePrice.Get(productStripe.DefaultPrice.ID, nil)

		if err != nil {
			createOrderTx.Rollback()
			return nil, err
		}
		orderItems = append(orderItems, &models.OrderItem{
			OrderID:  orderCreate.ID,
			CourseID: cartItem.CourseID,
			UserID:   invoice.UserID,
			Price:    float64(priceStripe.UnitAmount) / 100,
		})
		totalMoney += float64(priceStripe.UnitAmount) / 100
		fmt.Println("Total: ", priceStripe.UnitAmount)
	}
	createMultipleOrderItemTx := c.db.WithContext(ctx).Begin()
	err = c.orderItemRepository.CreateMultipleOrderItemWithTransaction(ctx, createMultipleOrderItemTx, orderItems)

	if err != nil {
		createOrderTx.Rollback()
		createMultipleOrderItemTx.Rollback()
		return nil, err
	}

	// Update invoice
	invoice.Status = constants.INVOICE_SUCCESS
	invoice.OrderID = orderCreate.ID
	invoice.Total = totalMoney
	updateInvoiceTx := c.db.WithContext(ctx).Begin()

	err = c.invoiceRepository.UpdateInvoiceWithTransaction(ctx, updateInvoiceTx, invoice)

	if err != nil {
		createOrderTx.Rollback()
		createMultipleOrderItemTx.Rollback()
		updateInvoiceTx.Rollback()
		return nil, err
	}

	// //Delete cart current
	deleteCartByIDTx := c.db.WithContext(ctx).Begin()

	err = c.cartRepository.DeleteCartByIDWithTransaction(ctx, deleteCartByIDTx, cart.ID)

	if err != nil {
		createOrderTx.Rollback()
		createMultipleOrderItemTx.Rollback()
		updateInvoiceTx.Rollback()
		deleteCartByIDTx.Rollback()
		return nil, err
	}

	for _, cartItem := range cartItems {
		body := constants.SendNotificationPurchaseCourse{
			// IdInvoice: invoiceCreate.ID,
			IdProduct: cartItem.CourseID,
			IdUser:    orderCreate.UserID,
			IDCartItem: cartItem.ID,
		}
		bodySendNotificationPurchase, err := json.Marshal(body)

		if err != nil {
			createOrderTx.Rollback()
			createMultipleOrderItemTx.Rollback()
			updateInvoiceTx.Rollback()
			deleteCartByIDTx.Rollback()
			return nil, err
		}

		dataRegisterCourse := constants.RegisterCourse{
			IdCourse: cartItem.CourseID,
			IdUser:   orderCreate.UserID,
		}

		bodyRegisterCourse, err := json.Marshal(dataRegisterCourse)

		if err != nil {
			createOrderTx.Rollback()
			createMultipleOrderItemTx.Rollback()
			updateInvoiceTx.Rollback()
			deleteCartByIDTx.Rollback()
			return nil, err
		}

		c.rabbitmqBuilder.AddProducerOnQueue(&producer.RabbitMQProducerOnQueue{
			QueueName: constants.SEND_NOTIFICATION_PURCHASE_COURSE,
			Data:      bodySendNotificationPurchase,
		})

		c.rabbitmqBuilder.AddProducerOnQueue(&producer.RabbitMQProducerOnQueue{
			QueueName: constants.REGISTER_COURSE,
			Data:      bodyRegisterCourse,
		})
	}
	// cart.Key = nil
	// if err := c.cartRepository.UpdateCart(ctx, cart); err != nil {
	// 	createOrderTx.Rollback()
	// 	createMultipleOrderItemTx.Rollback()
	// 	updateInvoiceTx.Rollback()
	// 	deleteCartByIDTx.Rollback()
	// 	return nil, err
	// }
	//Register course

	createOrderTx.Commit()
	createMultipleOrderItemTx.Commit()
	updateInvoiceTx.Commit()
	deleteCartByIDTx.Commit()
	
	

	return &dtos.CreateOrderResponseDto{
		Message: "Create order successfully!",
	}, nil
}
