package dtos

//https://echo.labstack.com/guide/response/

type CreateOrderResponseDto struct {
	Message string `json:"message"`
}
