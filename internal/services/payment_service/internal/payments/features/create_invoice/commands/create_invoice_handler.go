package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	"encoding/json"

	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/models"
	// "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/create_invoice/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/configurations"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/producer"
	// stripe "github.com/stripe/stripe-go/v76"
	// stripeProduct "github.com/stripe/stripe-go/v76/product"
	// stripePrice "github.com/stripe/stripe-go/v76/price"
	// paymentsError "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/data/errors"
	// paymentservice "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/grpc/proto/service_clients"
)

type CreateInvoiceHandler struct {
	log                 logger.Logger
	cartItemRepository  data.CartItemRepository
	cartRepository      data.CartRepository
	orderRepository     data.OrderRepository
	orderItemRepository data.OrderItemRepository
	invoiceRepository   data.InvoiceRepository
	grpcClient          grpc.GrpcClient
	rabbitmqBuilder     configurations.RabbitMQConfigurationBuilder
}

func NewCreateInvoiceHandler(
	log logger.Logger,
	cartRepository data.CartRepository,
	cartItemRepository data.CartItemRepository,
	grpcClient grpc.GrpcClient,
	orderRepository data.OrderRepository,
	orderItemRepository data.OrderItemRepository,
	invoiceRepository data.InvoiceRepository,
	rabbitmqBuilder configurations.RabbitMQConfigurationBuilder,
) *CreateInvoiceHandler {
	return &CreateInvoiceHandler{
		log:                 log,
		cartRepository:      cartRepository,
		cartItemRepository:  cartItemRepository,
		grpcClient:          grpcClient,
		orderRepository:     orderRepository,
		orderItemRepository: orderItemRepository,
		invoiceRepository:   invoiceRepository,
		rabbitmqBuilder:     rabbitmqBuilder,
	}
}

func (c *CreateInvoiceHandler) Handle(ctx context.Context, command *CreateInvoice) (*dtos.CreateInvoiceResponseDto, error) {

	cart, err := c.cartRepository.GetCartByUserId(ctx, command.IdUser)

	if err != nil {
		return nil, err
	}

	if cart.Key == nil {
		return nil, customErrors.ErrNotKeyCart
	} else {
		if *cart.Key != command.Key {
			return nil, customErrors.ErrKeyVerifyIncorrect
		}
	}

	// create invoice
	invoiceCreate := &models.Invoice{
		Status: constants.INVOICE_PENDING,
		UserID: command.IdUser,
	}
	c.invoiceRepository.CreateInvoice(ctx, invoiceCreate)
	// c.purchaseRepository.CreatePurchase()
	body := constants.SendPurchase{
		IdInvoice: invoiceCreate.ID,
	}

	bodySendPurchase, err := json.Marshal(body)

	if err != nil {
		return nil, err
	}

	c.rabbitmqBuilder.AddProducerOnQueue(&producer.RabbitMQProducerOnQueue{
		QueueName: constants.SEND_PURCHASE,
		Data:      bodySendPurchase,
	})

	return &dtos.CreateInvoiceResponseDto{
		Message: "Create invoice successfully!",
	}, nil
}
