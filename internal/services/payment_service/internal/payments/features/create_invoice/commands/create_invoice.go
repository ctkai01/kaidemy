package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type CreateInvoice struct {
	IdUser int
	Key    string
}

func NewCreateInvoice(idUser int, key string) (*CreateInvoice, *validator.ValidateError) {
	command := &CreateInvoice{idUser, key}
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
