package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/contracts/params"
	// paymentsError "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/create_invoice/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/create_invoice/dtos"
)

type createInvoiceEndpoint struct {
	params.InvoicesRouteParams
}

func NewCreateInvoiceEndpoint(
	params params.InvoicesRouteParams,
) route.Endpoint {
	return &createInvoiceEndpoint{
		InvoicesRouteParams: params,
	}
}

func (ep *createInvoiceEndpoint) MapEndpoint() {
	ep.InvoicesGroup.POST("", ep.handler())
}

func (ep *createInvoiceEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.CreateInvoiceRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[createInvoice.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[createInvoiceEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}

		userId := utils.GetUserId(c)
		
		command, errValidate := commands.NewCreateInvoice(
			userId,
			request.Key,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[createInvoiceEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[createInvoiceEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.CreateInvoice, *dtos.CreateInvoiceResponseDto](
			ctx,
			command,
		)

		if err != nil {
			// if err.Error() == paymentsError.WrapError(customErrors.ErrUserNotFound).Error() {
			// 	errCustom := errors.WithMessage(
			// 		err,
			// 		"[createCartItemEndpoint_handler.Send] error in sending create Cart Item",
			// 	)
			// 	ep.Logger.Error(
			// 		fmt.Sprintf(
			// 			"[createCartItem.Send], err: %v",
			// 			errCustom,
			// 		),
			// 	)
			// 	return customErrors.NewBadRequestErrorHttp(c, err.Error())
			// }
			err = errors.WithMessage(
				err,
				"[createOrderEndpoint_handler.Send] error in sending create order",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[createOrder.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusCreated, result)
	}
}
