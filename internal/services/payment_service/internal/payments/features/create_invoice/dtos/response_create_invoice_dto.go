package dtos

//https://echo.labstack.com/guide/response/

type CreateInvoiceResponseDto struct {
	Message string `json:"message"`
}
