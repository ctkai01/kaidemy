package endpoints

import (
	"fmt"
	"net/http"

	// "strings"

	// "emperror.dev/errors"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
	"github.com/mehdihadeli/go-mediatr"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/web/route"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/contracts/params"
	paymentsError "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/data/errors"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/create_cart_item/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/create_cart_item/dtos"
)

type createCartItemEndpoint struct {
	params.CartItemsRouteParams
}

func NewCreateCartItemEndpoint(
	params params.CartItemsRouteParams,
) route.Endpoint {
	return &createCartItemEndpoint{
		CartItemsRouteParams: params,
	}
}

func (ep *createCartItemEndpoint) MapEndpoint() {
	ep.CartItemsGroup.POST("", ep.handler())
}

func (ep *createCartItemEndpoint) handler() echo.HandlerFunc {
	return func(c echo.Context) error {
		ctx := c.Request().Context()

		request := &dtos.CreateCartItemRequestDto{}
		if err := c.Bind(request); err != nil {
			badRequestErr := customErrors.NewBadRequestErrorWrap(
				err,
				"[createCartItem.Bind] error in the binding request",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[createCartItemEndpoint_handler.Bind] err: %v", badRequestErr),
			)
			return customErrors.NewBadRequestErrorHttp(c, badRequestErr.Error())

		}

		userId := utils.GetUserId(c)
		
		command, errValidate := commands.NewCreateCartItem(
			userId,
			request.CourseID,
		)

		if errValidate != nil && errValidate.ErrorType != nil {

			validationErr := customErrors.NewValidationErrorWrap(
				errValidate.ErrorType,
				"[createCartItemEndpoint_handler.StructCtx] command validation failed",
			)
			ep.Logger.Errorf(
				fmt.Sprintf("[createCartItemEndpoint_handler.StructCtx] err: {%v}", validationErr),
			)

			return c.JSON(http.StatusBadRequest, errValidate.ErrorValue)
		}

		result, err := mediatr.Send[*commands.CreateCartItem, *dtos.CreateCartItemResponseDto](
			ctx,
			command,
		)

		if err != nil {
			if err.Error() == paymentsError.WrapError(customErrors.ErrUserNotFound).Error() {
				errCustom := errors.WithMessage(
					err,
					"[createCartItemEndpoint_handler.Send] error in sending create Cart Item",
				)
				ep.Logger.Error(
					fmt.Sprintf(
						"[createCartItem.Send], err: %v",
						errCustom,
					),
				)
				return customErrors.NewBadRequestErrorHttp(c, err.Error())
			}
			// if err.Error() == paymentsError.WrapError(paymentsError.ErrAlreadyCart).Error() {
			// 	errCustom := errors.WithMessage(
			// 		err,
			// 		"[createCartEndpoint_handler.Send] error in sending create Cart",
			// 	)
			// 	ep.Logger.Error(
			// 		fmt.Sprintf(
			// 			"[createCart.Send], err: %v",
			// 			errCustom,
			// 		),
			// 	)
			// 	return customErrors.NewConflictErrorHttp(c, err.Error())
			// }

			err = errors.WithMessage(
				err,
				"[createCartItemEndpoint_handler.Send] error in sending create Cart Item",
			)
			ep.Logger.Error(
				fmt.Sprintf(
					"[createCartItem.Send], err: %v",
					err,
				),
			)

			return customErrors.NewInternalServerErrorHttp(c, err.Error())
		}

		return c.JSON(http.StatusCreated, result)
	}
}
