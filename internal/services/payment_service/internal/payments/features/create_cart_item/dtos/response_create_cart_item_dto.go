package dtos

import "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/models"

//https://echo.labstack.com/guide/response/

type CreateCartItemResponseDto struct {
	Message  string          `json:"message"`
	CartItem *models.CartItemResponse `json:"cart_item"`
}
