package commands

import (
	// "github.com/ctkai/kaidemy/internal/services/userservice/internal/users/features/request_login_by_google/dtos"
	"context"
	// "gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/constants"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/shared/utils"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/contracts/data"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/models"

	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/"
	// "gitlab.com/ctkai01/kaidemy/internal/services/courseservice/internal/courses/features/create_course/commands"
	"gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/features/create_cart_item/dtos"
	// "gitlab.com/ctkai01/kaidemy/internal/services/userservice/internal/users/models"
	// "golang.org/x/oauth2"
	// paymentsError "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/data/errors"
	paymentservice "gitlab.com/ctkai01/kaidemy/internal/services/paymentservice/internal/payments/grpc/proto/service_clients"

	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
)

type CreateCartItemHandler struct {
	log                 logger.Logger
	cartItemRepository  data.CartItemRepository
	cartRepository      data.CartRepository
	orderItemRepository data.OrderItemRepository
	grpcClient          grpc.GrpcClient
}

func NewCreateCartItemHandler(
	log logger.Logger,
	cartRepository data.CartRepository,
	cartItemRepository data.CartItemRepository,
	orderItemRepository data.OrderItemRepository,
	grpcClient grpc.GrpcClient,
) *CreateCartItemHandler {
	return &CreateCartItemHandler{log: log, cartRepository: cartRepository, cartItemRepository: cartItemRepository, grpcClient: grpcClient, orderItemRepository: orderItemRepository}
}

func (c *CreateCartItemHandler) Handle(ctx context.Context, command *CreateCartItem) (*dtos.CreateCartItemResponseDto, error) {

	cart, err := c.cartRepository.GetCartByUserId(ctx, command.IdUser)

	isNotExistCart := false
	if err != nil {
		if err == customErrors.ErrCartNotFound {
			isNotExistCart = true
		} else {
			return nil, err
		}
	}

	if isNotExistCart {
		return nil, customErrors.ErrHasNotCart
	}

	//Check already bought this course ?
	_, err = c.orderItemRepository.GetOrderItemByUserIdCourseID(ctx, command.IdUser, command.IdCourse)
	isNotExistOrderItem := false
	if err != nil {
		if err == customErrors.ErrOrderItemNotFound {
			isNotExistOrderItem = true
		}
	}

	if !isNotExistOrderItem {
		return nil, customErrors.ErrAlreadyOrderItem
	}

	conn, err := c.grpcClient.GetGrpcConnection("course_service")
	if err != nil {
		return nil, err
	}

	client := paymentservice.NewCoursesServiceClient(conn)

	dataCourse, err := client.GetCourseDetailByID(ctx, &paymentservice.GetCourseDetailByIdReq{
		CourseID: int32(command.IdCourse),
	})
	if err != nil {
		return nil, err
	}

	if dataCourse.Course == nil {
		return nil, customErrors.ErrCourseNotFound
	}

	_, err = c.cartItemRepository.GetCartItemByCourseId(ctx, cart.ID, command.IdCourse)

	isNotExistCartItem := false
	if err != nil {
		if err == customErrors.ErrCartItemNotFound {
			isNotExistCartItem = true
		} else {
			return nil, err
		}
	}

	if !isNotExistCartItem {
		return nil, customErrors.ErrAlreadyAddCartItem
	}

	cartItemCreate := &models.CartItem{
		CartID:   cart.ID,
		CourseID: command.IdCourse,
	}

	err = c.cartItemRepository.CreateCartItem(ctx, cartItemCreate)

	if err != nil {
		return nil, err
	}

	cartItemResponse := &models.CartItemResponse{
		ID:        cartItemCreate.ID,
		CartID:    cartItemCreate.CartID,
		Course:    dataCourse.Course,
		UpdatedAt: cartItemCreate.UpdatedAt,
		CreatedAt: cartItemCreate.CreatedAt,
	}

	return &dtos.CreateCartItemResponseDto{
		Message:  "Add cart item successfully!",
		CartItem: cartItemResponse,
	}, nil
}
