package commands

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils/validator"
)

type CreateCartItem struct {
	IdUser        int
	IdCourse        int
}

func NewCreateCartItem(idUser int, courseID int) (*CreateCartItem, *validator.ValidateError) {
	command := &CreateCartItem{idUser, courseID }
	errValidate := validator.Validate(command)

	if errValidate != nil && errValidate.ErrorType != nil {
		return nil, errValidate
	}

	return command, nil
}
