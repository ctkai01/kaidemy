module gitlab.com/ctkai01/kaidemy/internal/services/paymentservice

go 1.19

require (
	github.com/stripe/stripe-go/v76 v76.2.0 // indirect
	gitlab.com/ctkai01/kaidemy/internal/pkg v0.0.0-20231223195150-f20ec3b11024 // indirect
)
