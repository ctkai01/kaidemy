package repository

import (
	"context"
	"fmt"
	"reflect"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/core/data"

	"emperror.dev/errors"

	"github.com/iancoleman/strcase"
	"gorm.io/gorm"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/core/data/specification"
	gormPostgres "gitlab.com/ctkai01/kaidemy/internal/pkg/gorm_mysql"
	customErrors "gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/custom_errors"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/mapper"
	reflectionHelper "gitlab.com/ctkai01/kaidemy/internal/pkg/reflection/reflection_helper"
	typeMapper "gitlab.com/ctkai01/kaidemy/internal/pkg/reflection/type_mappper"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
)

// gorm generic repository
type gormGenericRepository[TDataModel interface{}, TEntity interface{}] struct {
	db *gorm.DB
}

// NewGenericGormRepositoryWithDataModel create new gorm generic repository
func NewGenericGormRepositoryWithDataModel[TDataModel interface{}, TEntity interface{}](db *gorm.DB) data.GenericRepositoryWithDataModel[TDataModel, TEntity] {
	return &gormGenericRepository[TDataModel, TEntity]{
		db: db,
	}
}

// NewGenericGormRepository create new gorm generic repository
func NewGenericGormRepository[TEntity interface{}](db *gorm.DB) data.GenericRepository[TEntity] {
	return &gormGenericRepository[TEntity, TEntity]{
		db: db,
	}
}

func (r *gormGenericRepository[TDataModel, TEntity]) CustomDB(ctx context.Context) *gorm.DB {
	return r.db
}

func (r *gormGenericRepository[TDataModel, TEntity]) Add(ctx context.Context, entity TEntity) error {
	dataModelType := typeMapper.GetTypeFromGeneric[TDataModel]()
	modelType := typeMapper.GetTypeFromGeneric[TEntity]()

	if modelType == dataModelType {
		err := r.db.WithContext(ctx).Create(entity).Error
		if err != nil {
			return err
		}
		return nil
	} else {
		dataModel, err := mapper.Map[TDataModel](entity)
		if err != nil {
			return err
		}
		err = r.db.WithContext(ctx).Create(dataModel).Error
		if err != nil {
			return err
		}
		e, err := mapper.Map[TEntity](dataModel)
		if err != nil {
			return err
		}
		reflectionHelper.SetValue[TEntity](entity, e)
	}
	return nil
}

// func (r *gormGenericRepository[TDataModel, TEntity]) AddWithTransaction(ctx context.Context, entity TEntity) error {
// 	dataModelType := typeMapper.GetTypeFromGeneric[TDataModel]()
// 	modelType := typeMapper.GetTypeFromGeneric[TEntity]()

// 	if modelType == dataModelType {
// 		err := r.db.WithContext(ctx).Create(entity).Error
// 		if err != nil {
// 			return err
// 		}
// 		return nil
// 	} else {
// 		dataModel, err := mapper.Map[TDataModel](entity)
// 		if err != nil {
// 			return err
// 		}
// 		err = r.db.WithContext(ctx).Create(dataModel).Error
// 		if err != nil {
// 			return err
// 		}
// 		e, err := mapper.Map[TEntity](dataModel)
// 		if err != nil {
// 			return err
// 		}
// 		reflectionHelper.SetValue[TEntity](entity, e)
// 	}
// 	return nil
// }

func (r *gormGenericRepository[TDataModel, TEntity]) AddWithTransaction(ctx context.Context, tx *gorm.DB, entity TEntity) error {
	dataModelType := typeMapper.GetTypeFromGeneric[TDataModel]()
	modelType := typeMapper.GetTypeFromGeneric[TEntity]()

	if modelType == dataModelType {
		err := tx.WithContext(ctx).Create(entity).Error
		if err != nil {
			return err
		}
		return nil
	} else {
		dataModel, err := mapper.Map[TDataModel](entity)
		if err != nil {
			return err
		}
		err = tx.WithContext(ctx).Create(dataModel).Error
		if err != nil {
			return err
		}
		e, err := mapper.Map[TEntity](dataModel)
		if err != nil {
			return err
		}
		reflectionHelper.SetValue[TEntity](entity, e)
	}
	return nil

}

func (r *gormGenericRepository[TDataModel, TEntity]) AddAllWithTransaction(ctx context.Context, tx *gorm.DB, entities []TEntity) error {
	for _, entity := range entities {
		err := r.AddWithTransaction(ctx, tx, entity)
		if err != nil {
			return err
		}
	}

	return nil
}

func (r *gormGenericRepository[TDataModel, TEntity]) AddAll(ctx context.Context, entities []TEntity) error {
	for _, entity := range entities {
		err := r.Add(ctx, entity)
		if err != nil {
			return err
		}
	}

	return nil
}

func (r *gormGenericRepository[TDataModel, TEntity]) GetById(ctx context.Context, id int) (TEntity, error) {
	dataModelType := typeMapper.GetTypeFromGeneric[TDataModel]()
	modelType := typeMapper.GetTypeFromGeneric[TEntity]()
	if modelType == dataModelType {
		var model TEntity
		if err := r.db.WithContext(ctx).First(&model, id).Error; err != nil {
			if errors.Is(err, gorm.ErrRecordNotFound) {
				return *new(TEntity), customErrors.NewNotFoundErrorWrap(err, fmt.Sprintf("can't find the entity with id %s into the database.", id))
			}
			return *new(TEntity), errors.WrapIf(err, fmt.Sprintf("can't find the entity with id %s into the database.", id))
		}
		return model, nil
	} else {
		var dataModel TDataModel
		if err := r.db.WithContext(ctx).First(&dataModel, id).Error; err != nil {
			if errors.Is(err, gorm.ErrRecordNotFound) {
				return *new(TEntity), customErrors.NewNotFoundErrorWrap(err, fmt.Sprintf("can't find the entity with id %s into the database.", id))
			}
			return *new(TEntity), errors.WrapIf(err, fmt.Sprintf("can't find the entity with id %s into the database.", id))
		}
		entity, err := mapper.Map[TEntity](dataModel)
		if err != nil {
			return *new(TEntity), err
		}
		return entity, nil
	}
}

func (r *gormGenericRepository[TDataModel, TEntity]) GetByEmail(ctx context.Context, email string) (TEntity, error) {
	dataModelType := typeMapper.GetTypeFromGeneric[TDataModel]()
	modelType := typeMapper.GetTypeFromGeneric[TEntity]()
	if modelType == dataModelType {
		var model TEntity
		if err := r.db.WithContext(ctx).Where("email = ?", email).First(&model).Error; err != nil {
			if errors.Is(err, gorm.ErrRecordNotFound) {
				return *new(TEntity), gorm.ErrRecordNotFound
			}
			return *new(TEntity), errors.WrapIf(err, fmt.Sprintf("can't find the entity with email %s into the database.", email))
		}
		return model, nil
	} else {
		var dataModel TDataModel
		if err := r.db.WithContext(ctx).Where("email = ?", email).First(&dataModel).Error; err != nil {
			if errors.Is(err, gorm.ErrRecordNotFound) {
				return *new(TEntity), gorm.ErrRecordNotFound
			}
			return *new(TEntity), errors.WrapIf(err, fmt.Sprintf("can't find the entity with email %s into the database.", email))
		}
		entity, err := mapper.Map[TEntity](dataModel)
		if err != nil {
			return *new(TEntity), err
		}
		return entity, nil
	}
}

func (r *gormGenericRepository[TDataModel, TEntity]) GetByEmailToken(ctx context.Context, emailToken string) (TEntity, error) {
	dataModelType := typeMapper.GetTypeFromGeneric[TDataModel]()
	modelType := typeMapper.GetTypeFromGeneric[TEntity]()
	if modelType == dataModelType {
		var model TEntity
		if err := r.db.WithContext(ctx).Where("email_token = ?", emailToken).First(&model).Error; err != nil {
			if errors.Is(err, gorm.ErrRecordNotFound) {
				return *new(TEntity), gorm.ErrRecordNotFound
			}
			return *new(TEntity), errors.WrapIf(err, fmt.Sprintf("can't find the entity with email token %s into the database.", emailToken))
		}
		return model, nil
	} else {
		var dataModel TDataModel
		if err := r.db.WithContext(ctx).Where("email_token = ?", emailToken).First(&dataModel).Error; err != nil {
			if errors.Is(err, gorm.ErrRecordNotFound) {
				return *new(TEntity), gorm.ErrRecordNotFound
			}
			return *new(TEntity), errors.WrapIf(err, fmt.Sprintf("can't find the entity with email token %s into the database.", emailToken))
		}
		entity, err := mapper.Map[TEntity](dataModel)
		if err != nil {
			return *new(TEntity), err
		}
		return entity, nil
	}
}

func (r *gormGenericRepository[TDataModel, TEntity]) GetAll(ctx context.Context, listQuery *utils.ListQuery) (*utils.ListResult[TEntity], error) {
	dataModelType := typeMapper.GetTypeFromGeneric[TDataModel]()
	modelType := typeMapper.GetTypeFromGeneric[TEntity]()
	if modelType == dataModelType {

		result, err := gormPostgres.Paginate[TEntity](ctx, listQuery, r.db)
		if err != nil {
			return nil, err
		}

		return result, nil
	} else {
		result, err := gormPostgres.Paginate[TDataModel](ctx, listQuery, r.db)
		if err != nil {
			return nil, err
		}
		models, err := utils.ListResultToListResultDto[TEntity](result)
		if err != nil {
			return nil, err
		}
		return models, nil
	}
}

func (r *gormGenericRepository[TDataModel, TEntity]) Search(ctx context.Context, searchTerm string, listQuery *utils.ListQuery) (*utils.ListResult[TEntity], error) {
	dataModelType := typeMapper.GetTypeFromGeneric[TDataModel]()
	modelType := typeMapper.GetTypeFromGeneric[TEntity]()
	if modelType == dataModelType {
		fields := reflectionHelper.GetAllFields(typeMapper.GetTypeFromGeneric[TEntity]())
		query := r.db

		for _, field := range fields {
			if field.Type.Kind() != reflect.String {
				continue
			}
			f := strcase.ToSnake(field.Name)
			whereQuery := fmt.Sprintf("%s IN (?)", f)
			query = r.db.Where(whereQuery, searchTerm)
		}

		result, err := gormPostgres.Paginate[TEntity](ctx, listQuery, query)
		if err != nil {
			return nil, err
		}
		return result, nil
	} else {
		query := r.db
		fields := reflectionHelper.GetAllFields(typeMapper.GetTypeFromGeneric[TDataModel]())

		for _, field := range fields {
			if field.Type.Kind() != reflect.String {
				continue
			}
			f := strcase.ToSnake(field.Name)
			whereQuery := fmt.Sprintf("%s IN (?)", f)
			query = r.db.WithContext(ctx).Where(whereQuery, searchTerm)
		}
		result, err := gormPostgres.Paginate[TDataModel](ctx, listQuery, query)
		if err != nil {
			return nil, err
		}
		models, err := utils.ListResultToListResultDto[TEntity](result)
		if err != nil {
			return nil, err
		}
		return models, nil
	}
}

func (r *gormGenericRepository[TDataModel, TEntity]) GetByFilter(ctx context.Context, filters map[string]interface{}) ([]TEntity, error) {
	dataModelType := typeMapper.GetTypeFromGeneric[TDataModel]()
	modelType := typeMapper.GetTypeFromGeneric[TEntity]()
	if modelType == dataModelType {
		var models []TEntity
		err := r.db.WithContext(ctx).Where(filters).Find(&models).Error
		if err != nil {
			return nil, err
		}
		return models, nil
	} else {
		var dataModels []TDataModel
		err := r.db.WithContext(ctx).Where(filters).Find(&dataModels).Error
		if err != nil {
			return nil, err
		}
		models, err := mapper.Map[[]TEntity](dataModels)
		if err != nil {
			return nil, err
		}
		return models, nil
	}
}

func (r *gormGenericRepository[TDataModel, TEntity]) GetByFuncFilter(ctx context.Context, filterFunc func(TEntity) bool) ([]TEntity, error) {
	return *new([]TEntity), nil
}

func (r *gormGenericRepository[TDataModel, TEntity]) FirstOrDefault(ctx context.Context, filters map[string]interface{}) (TEntity, error) {
	return *new(TEntity), nil
}

func (r *gormGenericRepository[TDataModel, TEntity]) Update(ctx context.Context, entity TEntity) error {
	dataModelType := typeMapper.GetTypeFromGeneric[TDataModel]()
	modelType := typeMapper.GetTypeFromGeneric[TEntity]()
	if modelType == dataModelType {
		err := r.db.WithContext(ctx).Save(entity).Error
		if err != nil {
			return err
		}
	} else {
		dataModel, err := mapper.Map[TDataModel](entity)
		if err != nil {
			return err
		}
		err = r.db.WithContext(ctx).Save(dataModel).Error
		if err != nil {
			return err
		}
		e, err := mapper.Map[TEntity](dataModel)
		if err != nil {
			return err
		}
		reflectionHelper.SetValue[TEntity](entity, e)
	}

	return nil
}

func (r *gormGenericRepository[TDataModel, TEntity]) UpdateWithTransaction(ctx context.Context, tx *gorm.DB, entity TEntity) error {
	dataModelType := typeMapper.GetTypeFromGeneric[TDataModel]()
	modelType := typeMapper.GetTypeFromGeneric[TEntity]()
	if modelType == dataModelType {
		err := tx.Save(entity).Error
		if err != nil {
			return err
		}
	} else {
		dataModel, err := mapper.Map[TDataModel](entity)
		if err != nil {
			return err
		}
		err = tx.Save(dataModel).Error
		if err != nil {
			return err
		}
		e, err := mapper.Map[TEntity](dataModel)
		if err != nil {
			return err
		}
		reflectionHelper.SetValue[TEntity](entity, e)
	}

	return nil
}

func (r gormGenericRepository[TDataModel, TEntity]) UpdateAll(ctx context.Context, entities []TEntity) error {
	for _, e := range entities {
		err := r.Update(ctx, e)
		if err != nil {
			return err
		}
	}

	return nil
}

func (r *gormGenericRepository[TDataModel, TEntity]) Delete(ctx context.Context, id int) error {
	entity, err := r.GetById(ctx, id)
	if err != nil {
		return err
	}

	err = r.db.WithContext(ctx).Delete(entity, id).Error
	if err != nil {
		return err
	}

	return nil
}

func (r *gormGenericRepository[TDataModel, TEntity]) DeleteWithTransaction(ctx context.Context, tx *gorm.DB, id int) error {
	entity, err := r.GetById(ctx, id)
	if err != nil {
		return err
	}

	err = tx.Delete(entity, id).Error
	if err != nil {
		return err
	}

	return nil
}

func (r *gormGenericRepository[TDataModel, TEntity]) SkipTake(ctx context.Context, skip int, take int) ([]TEntity, error) {
	dataModelType := typeMapper.GetTypeFromGeneric[TDataModel]()
	modelType := typeMapper.GetTypeFromGeneric[TEntity]()
	if modelType == dataModelType {
		var models []TEntity
		err := r.db.WithContext(ctx).Offset(skip).Limit(take).Find(&models).Error
		if err != nil {
			return nil, err
		}
		return models, nil
	} else {
		var dataModels []TDataModel
		err := r.db.WithContext(ctx).Offset(skip).Limit(take).Find(&dataModels).Error
		if err != nil {
			return nil, err
		}
		models, err := mapper.Map[[]TEntity](dataModels)
		if err != nil {
			return nil, err
		}
		return models, nil
	}
}

func (r *gormGenericRepository[TDataModel, TEntity]) Count(ctx context.Context) int64 {
	var dataModel TDataModel
	var count int64
	r.db.WithContext(ctx).Model(&dataModel).Count(&count)
	return count
}

func (r *gormGenericRepository[TDataModel, TEntity]) Find(ctx context.Context, specification specification.Specification) ([]TEntity, error) {
	dataModelType := typeMapper.GetTypeFromGeneric[TDataModel]()
	modelType := typeMapper.GetTypeFromGeneric[TEntity]()
	if modelType == dataModelType {
		var models []TEntity
		err := r.db.WithContext(ctx).Where(specification.GetQuery(), specification.GetValues()...).Find(&models).Error
		if err != nil {
			return nil, err
		}
		return models, nil
	} else {
		var dataModels []TDataModel
		err := r.db.WithContext(ctx).Where(specification.GetQuery(), specification.GetValues()...).Find(&dataModels).Error
		if err != nil {
			return nil, err
		}
		models, err := mapper.Map[[]TEntity](dataModels)
		if err != nil {
			return nil, err
		}
		return models, nil
	}
}

func (r *gormGenericRepository[TDataModel, TEntity]) FindWithRelation(ctx context.Context, specification specification.Specification, preload []string) ([]TEntity, error) {
	dataModelType := typeMapper.GetTypeFromGeneric[TDataModel]()
	modelType := typeMapper.GetTypeFromGeneric[TEntity]()
	if modelType == dataModelType {
		var models []TEntity

		db := r.db.WithContext(ctx).Where(specification.GetQuery(), specification.GetValues()...)

		for _, association := range preload {
			db = db.Preload(association, func(db *gorm.DB) *gorm.DB {
				return db.Order("created_at desc") // Order orders by the Amount field in descending order
			})
		}
		err := db.Find(&models).Error
		if err != nil {
			return nil, err
		}
		return models, nil
	} else {
		var dataModels []TDataModel
		db := r.db.WithContext(ctx).Where(specification.GetQuery(), specification.GetValues()...)
		
		for _, association := range preload {
			db = db.Preload(association, func(db *gorm.DB) *gorm.DB {
				return db.Order("created_at desc") // Order orders by the Amount field in descending order
			})
		}
		err := db.Find(&dataModels).Error
		if err != nil {
			return nil, err
		}
		models, err := mapper.Map[[]TEntity](dataModels)
		if err != nil {
			return nil, err
		}
		return models, nil
	}
}

func (r *gormGenericRepository[TDataModel, TEntity]) FindWithRelationPaginate(ctx context.Context, preload []string, listQuery *utils.ListQuery) (*utils.ListResult[TEntity], error) {
	dataModelType := typeMapper.GetTypeFromGeneric[TDataModel]()
	modelType := typeMapper.GetTypeFromGeneric[TEntity]()
	if modelType == dataModelType {
		// var models []TEntity
		fmt.Println("Hey")
		// db := r.db.WithContext(ctx).Where(specification.GetQuery(), specification.GetValues()...).Offset(listQuery.GetOffset()).
		// Limit(listQuery.GetLimit()).
		// Order(listQuery.GetOrderBy())
		db := r.db.WithContext(ctx)
		for _, association := range preload {
			db = db.Preload(association)
		}
		result, err := gormPostgres.Paginate[TEntity](ctx, listQuery, db)
		if err != nil {
			return nil, err
		}

		return result, nil
	} else {
		db := r.db.WithContext(ctx)
		for _, association := range preload {
			db = db.Preload(association)
		}
		result, err := gormPostgres.Paginate[TEntity](ctx, listQuery, db)
		if err != nil {
			return nil, err
		}

		return result, nil
	}
}

func (r *gormGenericRepository[TDataModel, TEntity]) GetRelatedByID(ctx context.Context, preload []string, id int) (TEntity, error) {
	dataModelType := typeMapper.GetTypeFromGeneric[TDataModel]()
	modelType := typeMapper.GetTypeFromGeneric[TEntity]()

	if modelType == dataModelType {
		var models TEntity
		query := r.db

		for _, association := range preload {
			query = query.Preload(association)
		}
		if err := query.First(&models, id).Error; err != nil {
			return *new(TEntity), err
		}

		return models, nil
	} else {
		var dataModels TDataModel
		query := r.db

		for _, association := range preload {
			query = query.Preload(association)
		}
		if err := query.First(&dataModels, id).Error; err != nil {
			return *new(TEntity), err
		}

		models, err := mapper.Map[TEntity](dataModels)
		if err != nil {
			return *new(TEntity), err
		}

		return models, nil
	}

}
