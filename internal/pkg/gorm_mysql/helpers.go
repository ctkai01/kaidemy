package gormPostgres

import (
	"context"
	"fmt"
	"strings"

	"emperror.dev/errors"
	"gorm.io/gorm"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
)

// Ref: https://dev.to/rafaelgfirmino/pagination-using-gorm-scopes-3k5f

func Paginate[T any](
	ctx context.Context,
	listQuery *utils.ListQuery,
	db *gorm.DB,
) (*utils.ListResult[T], error) {
	var items []T
	var totalRows int64

	// generate where query
	query := db.WithContext(ctx)
		
	fmt.Println("GetOrderBy: ", listQuery.GetOrderBy())
	fmt.Println("listQuery.Filters: ", listQuery.Filters)

	
	if listQuery.Filters != nil {
		for _, filter := range listQuery.Filters {

			column := filter.Field
			action := filter.Comparison
			value := filter.Value
			switch action {
			case "equals":
				whereQuery := fmt.Sprintf("%s = ?", column)
				if value != nil {
					query = query.WithContext(ctx).Where(whereQuery, *value)
				} else {
					whereQuery := fmt.Sprintf("%s IS NULL", column)

					query = query.Debug().WithContext(ctx).Where(whereQuery)
				}
				break
			case "contains":
				whereQuery := fmt.Sprintf("%s LIKE ?", column)
				if value != nil {
					query = query.WithContext(ctx).Where(whereQuery, "%"+*value+"%")
				} else {
					query = query.WithContext(ctx).Where(whereQuery, "%%")
				}

				break
			case "in":
				whereQuery := fmt.Sprintf("%s IN (?)", column)

				queryArray := strings.Split(*value, ",")
				query = query.WithContext(ctx).Where(whereQuery, queryArray)
				break

			}
		}
	}
	query.WithContext(ctx).Model(&items).Count(&totalRows)

	query = query.Offset(listQuery.GetOffset()).
		Limit(listQuery.GetLimit()).
		Order(listQuery.GetOrderBy())

	if err := query.Find(&items).Error; err != nil {
		return nil, errors.WrapIf(err, "error in finding products.")
	}
	// query.WithContext(ctx).Count(&totalRows)

	return utils.NewListResult[T](items, listQuery.GetSize(), listQuery.GetPage(), totalRows), nil
}
