package gormPostgres

import (
	// "database/sql"
	"fmt"

	"emperror.dev/errors"
	// "github.com/uptrace/bun/driver/pgdriver"

	gormMysql "gorm.io/driver/mysql"
	"gorm.io/gorm"

	defaultLogger "gitlab.com/ctkai01/kaidemy/internal/pkg/logger/default_logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger/external/gromlog"
)

func NewGorm(cfg *GormOptions,) (*gorm.DB, error) {
	if cfg.DBName == "" {
		return nil, errors.New("DBName is required in the config.")
	}

	// err := createDB(cfg)
	// if err != nil {
	// 	return nil, err
	// }
		
	var dataSourceName string
// dsn := fmt.Sprintf("%s:%s@(%s)/%s?charset=utf8&parseTime=True&loc=Local", cfg.DbUser, cfg.DbPass, cfg.DbHost, cfg.DbName)
	dataSourceName = fmt.Sprintf("%s:%s@(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		cfg.User,
		cfg.Password,
		cfg.Host,
		cfg.Port,
		cfg.DBName,
	)
	fmt.Println("Data source: ", dataSourceName)
	fmt.Println("Data host: ", cfg.Host)
	gormDb, err := gorm.Open(
		gormMysql.Open(dataSourceName),
		&gorm.Config{Logger: gromlog.NewGormCustomLogger(defaultLogger.Logger)},
	)
	fmt.Println("Err gorm: ", err)
	if err != nil {
		return nil, err
	}

	return gormDb, nil
}

// func createDB(cfg *GormOptions) error {
// 	// we should choose a default database in the connection, but because we don't have a database yet we specify postgres default database 'postgres'
// 	datasource := fmt.Sprintf("mysql://%s:%s@%s:%d/%s?sslmode=disable",
// 		cfg.User,
// 		cfg.Password,
// 		cfg.Host,
// 		cfg.Port,
// 		"mysql",
// 	)

// 	sqldb := sql.OpenDB(pgdriver.NewConnector(pgdriver.WithDSN(datasource)))

// 	var exists int
// 	rows, err := sqldb.Query(
// 		fmt.Sprintf("SELECT 1 FROM  pg_catalog.pg_database WHERE datname='%s'", cfg.DBName),
// 	)
// 	if err != nil {
// 		return err
// 	}

// 	if rows.Next() {
// 		err = rows.Scan(&exists)
// 		if err != nil {
// 			return err
// 		}
// 	}

// 	if exists == 1 {
// 		return nil
// 	}

// 	_, err = sqldb.Exec(fmt.Sprintf("CREATE DATABASE %s", cfg.DBName))
// 	if err != nil {
// 		return err
// 	}

// 	defer sqldb.Close()

// 	return nil
// }
