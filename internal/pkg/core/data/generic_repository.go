package data

import (
	"context"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/core/data/specification"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/utils"
	"gorm.io/gorm"
)

type GenericRepositoryWithDataModel[TDataModel interface{}, TEntity interface{}] interface {
	Add(ctx context.Context, entity TEntity) error
	AddAll(ctx context.Context, entities []TEntity) error
	GetById(ctx context.Context, id int) (TEntity, error)
	GetByEmail(ctx context.Context, email string) (TEntity, error)
	GetByEmailToken(ctx context.Context, emailToken string) (TEntity, error)
	GetByFilter(ctx context.Context, filters map[string]interface{}) ([]TEntity, error)
	GetByFuncFilter(ctx context.Context, filterFunc func(TEntity) bool) ([]TEntity, error)
	GetAll(ctx context.Context, listQuery *utils.ListQuery) (*utils.ListResult[TEntity], error)
	FirstOrDefault(ctx context.Context, filters map[string]interface{}) (TEntity, error)
	Search(ctx context.Context, searchTerm string, listQuery *utils.ListQuery) (*utils.ListResult[TEntity], error)
	Update(ctx context.Context, entity TEntity) error
	UpdateAll(ctx context.Context, entities []TEntity) error
	Delete(ctx context.Context, id int) error
	SkipTake(ctx context.Context, skip int, take int) ([]TEntity, error)
	Count(ctx context.Context) int64
	Find(ctx context.Context, specification specification.Specification) ([]TEntity, error)
	FindWithRelation(ctx context.Context, specification specification.Specification, preload []string) ([]TEntity, error)
	FindWithRelationPaginate(ctx context.Context, preload []string, listQuery *utils.ListQuery) (*utils.ListResult[TEntity], error)

	GetRelatedByID(ctx context.Context, preload []string, id int) (TEntity, error)
	DeleteWithTransaction(ctx context.Context, tx *gorm.DB, id int) error
	UpdateWithTransaction(ctx context.Context, tx *gorm.DB, entity TEntity) error
	AddWithTransaction(ctx context.Context, tx *gorm.DB, entity TEntity) error
	AddAllWithTransaction(ctx context.Context, tx *gorm.DB, entities []TEntity) error
	
	
	CustomDB(ctx context.Context) *gorm.DB
}

type GenericRepository[TEntity interface{}] interface {
	GenericRepositoryWithDataModel[TEntity, TEntity]
}
