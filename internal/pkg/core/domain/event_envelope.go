package domain

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/core/metadata"
)

type EventEnvelope struct {
	EventData interface{}
	Metadata  metadata.Metadata
}
