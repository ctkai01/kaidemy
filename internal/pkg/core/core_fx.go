package core

import (
	"go.uber.org/fx"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/core/serializer"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/core/serializer/json"
	defaultLogger "gitlab.com/ctkai01/kaidemy/internal/pkg/logger/default_logger"
)

// Module provided to fxlog
// https://uber-go.github.io/fx/modules.html
var Module = fx.Module(
	"corefx",
	fx.Provide(
		json.NewDefaultSerializer,
		serializer.NewDefaultEventSerializer,
		serializer.NewDefaultMetadataSerializer,
	),
	fx.Invoke(defaultLogger.SetupDefaultLogger),
)
