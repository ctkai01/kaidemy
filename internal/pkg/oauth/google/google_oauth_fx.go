package google

import "go.uber.org/fx"

var Module = fx.Module(
	"googleauthfx",
	fx.Provide(
		provideConfig,
		NewGoogleOauthConfig,
	),
)