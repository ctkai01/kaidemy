package google

import (
	"github.com/iancoleman/strcase"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/config"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/config/environment"
	typeMapper "gitlab.com/ctkai01/kaidemy/internal/pkg/reflection/type_mappper"
)

var optionName = strcase.ToLowerCamel(typeMapper.GetTypeNameByT[GoogleOauthOptions]())

type GoogleOauthOptions struct {
	ClientID     string `mapstructure:"clientID"`
	ClientSecret string `mapstructure:"clientSecret"`
	RedirectURL  string `mapstructure:"redirectURL"`
}

func provideConfig(environment environment.Environment) (*GoogleOauthOptions, error) {
	return config.BindConfigKey[*GoogleOauthOptions](optionName, environment)
}
