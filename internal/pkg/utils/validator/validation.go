package validator

import (
	"fmt"

	"github.com/go-playground/validator"
)

var validate *validator.Validate

type CustomValidationError struct {
	Field   string `json:"field"`
	Message string `json:"message"`
}

type ValidateError struct {
	ErrorValue []*CustomValidationError
	ErrorType  error
}

func init() {
	validate = validator.New()
}

// Validate attempts to validate the lead's values.
func Validate(input interface{}) *ValidateError {
	if err := validate.Struct(input); err != nil {
		// this check ensures there wasn't an error
		// with the validation process itself
		validationErrors := []*CustomValidationError{}

		// Iterate over validation errors and convert them to custom format
		for _, validationErr := range err.(validator.ValidationErrors) {
			fieldName := validationErr.Field()
			errorMessage := fmt.Sprintf("Validation failed on '%s' tag", validationErr.Tag())

			// Add custom validation error to the slice
			validationErrors = append(validationErrors, &CustomValidationError{
				Field:   fieldName,
				Message: errorMessage,
			})
		}
		
		if _, ok := err.(*validator.InvalidValidationError); ok {
			return &ValidateError{
				ErrorValue: validationErrors,
				ErrorType:  err,
			}
		}

		return &ValidateError{
			ErrorValue: validationErrors,
			ErrorType:  err,
		}
	}
	return nil
}