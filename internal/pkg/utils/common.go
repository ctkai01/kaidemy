package utils

import (
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
)

func Encode(id int, role int) (string, error) {
	claims := jwt.MapClaims{
		"sub":  id,
		"role": role,
		"exp":  time.Now().Add(time.Hour * 72).Unix(),
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	t, err := token.SignedString(constants.JwtSecret)
	if err != nil {
		return "", err
	}
	return t, nil
}

func GetUserId(c echo.Context) int {
	userId, _ := c.Get("userId").(int)

	return userId
}

func GetUserRole(c echo.Context) int {
	userRole, _ := c.Get("userRole").(int)

	return userRole
}

