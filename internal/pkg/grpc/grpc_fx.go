package grpc

import (
	"context"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/grpc/config"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"go.uber.org/fx"
)

var Module = fx.Module("grpcfx",

	fx.Provide(
		config.ProvideConfig,
		NewGrpcServer,
		NewGrpcClient,
	),
	fx.Invoke(registerHooks),
)

func registerHooks(
	lc fx.Lifecycle,
	grpcServer GrpcServer,
	grpcClient GrpcClient,
	logger logger.Logger,
	options *config.GrpcOptions,
) {
	lc.Append(fx.Hook{
		OnStart: func(ctx context.Context) error {
			go func() {
				// if (ctx.Err() == nil), context not canceled or deadlined
				if err := grpcServer.RunGrpcServer(nil); err != nil {
					// do a fatal for going to OnStop process
					logger.Fatalf("(GrpcServer.RunGrpcServer) error in running server: {%v}", err)
				}
			}()

			logger.Infof(
				"%s is listening on Host:{%s} Grpc PORT: {%s}",
				options.Server.Name,
				options.Server.Host,
				options.Server.Port,
			)

			return nil
		},
		OnStop: func(ctx context.Context) error {
			// https://github.com/uber-go/fx/blob/v1.20.0/app.go#L573
			// this ctx is just for stopping callbacks or OnStop callbacks, and it has short timeout 15s, and it is not alive in whole lifetime app
			grpcServer.GracefulShutdown()
			logger.Info("server shutdown gracefully")

			if err := grpcClient.CloseAll(); err != nil {
				logger.Errorf("error in closing grpc-client: %v", err)
			} else {
				logger.Info("grpc-client closed gracefully")
			}

			return nil
		},
	})
}
