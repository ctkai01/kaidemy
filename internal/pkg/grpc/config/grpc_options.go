package config

import (
	"github.com/iancoleman/strcase"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/config"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/config/environment"
	typeMapper "gitlab.com/ctkai01/kaidemy/internal/pkg/reflection/type_mappper"
)

var optionName = strcase.ToLowerCamel(typeMapper.GetTypeNameByT[GrpcOptions]())

type GrpcOptions struct {
	Server  GrpcConfig   `mapstructure:"server"        env:"Server"`
	Clients []GrpcConfig `mapstructure:"clients"        env:"Clients"`
}

type GrpcConfig struct {
	Port string `mapstructure:"port"        env:"TcpPort"`
	Host string `mapstructure:"host"        env:"Host"`
	Name string `mapstructure:"name"        env:"Name"`
}

func ProvideConfig(environment environment.Environment) (*GrpcOptions, error) {
	return config.BindConfigKey[*GrpcOptions](optionName, environment)
}
