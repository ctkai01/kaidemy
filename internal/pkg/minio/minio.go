package minio

import (
	// "database/sql"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	// "github.com/uptrace/bun/driver/pgdriver"
)

func NewMinIO(cfg *MinIoOptions) (*minio.Client, error) {
	minioClient, err := minio.New(cfg.Endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(cfg.AccessKey, cfg.SecretKey, ""),
		Secure: false, // Change to true if using HTTPS
	})
	if err != nil {
		return nil, err
	}

	return minioClient, nil
}