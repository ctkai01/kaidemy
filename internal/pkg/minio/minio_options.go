package minio

import (
	// "fmt"

	"github.com/iancoleman/strcase"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/config"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/config/environment"
	typeMapper "gitlab.com/ctkai01/kaidemy/internal/pkg/reflection/type_mappper"
)

var optionName = strcase.ToLowerCamel(typeMapper.GetTypeNameByT[MinIoOptions]())

type MinIoOptions struct {
	Endpoint  string `mapstructure:"endpoint"`
	AccessKey string `mapstructure:"accessKey"`
	SecretKey string `mapstructure:"secretKey"`
}

// func (h *MinIoOptions) Dns() string {
// 	datasource := fmt.Sprintf("mysql://%s:%s@%s:%d/%s?sslmode=disable",
// 		h.User,
// 		h.Password,
// 		h.Host,
// 		h.Port,
// 		h.DBName,
// 	)

// 	return datasource
// }

func provideConfig(environment environment.Environment) (*MinIoOptions, error) {
	return config.BindConfigKey[*MinIoOptions](optionName, environment)
}
