package minio

import (
	"context"
	"mime/multipart"

	"github.com/minio/minio-go/v7"
)

type MinIoConfiguration interface {
	UploadAvatar(bucketName string, objectName string, file multipart.File, fileType string) (string, error)
}

type minIoBuilder struct {
	client *minio.Client
}

func NewMinIoBuilder(client *minio.Client) MinIoConfiguration {
	return &minIoBuilder{
		client: client,
	}
}

func (mB *minIoBuilder) UploadAvatar(bucketName string, objectName string, file multipart.File, fileType string) (string, error) {
	err := mB.client.MakeBucket(context.Background(), bucketName, minio.MakeBucketOptions{})
	if err != nil {
		exists, errBucketExists := mB.client.BucketExists(context.Background(), bucketName)
		if errBucketExists == nil && exists {
			// Bucket already exists
		} else {
			return "", err
		}
	}

	policy := `{
		"Version": "2012-10-17",
		"Statement": [
			{
			"Action": [
				"s3:GetObject"
			],
			"Effect": "Allow",
			"Principal": "*",
			"Resource": [
				"arn:aws:s3:::` + bucketName + `/*"
			]
			}
		]
		}`

	err = mB.client.SetBucketPolicy(context.Background(), bucketName, policy)
	if err != nil {
		return "", err
	}

	// Upload the object
	info, err := mB.client.PutObject(context.Background(), bucketName, objectName, file, -1, minio.PutObjectOptions{
		ContentType: fileType, // Update with the appropriate content type
		// UserMetadata:    metadata,
	})

	if err != nil {
		return "", err
	}

	return info.Location, nil
}
