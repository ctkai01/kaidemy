package minio

import (
	"go.uber.org/fx"
)

// Module provided to fxlog
// https://uber-go.github.io/fx/modules.html
var Module = fx.Module(
	"miniofx",
	fx.Provide(
		provideConfig,
		NewMinIO,
		NewMinIoBuilder,
	),
)
