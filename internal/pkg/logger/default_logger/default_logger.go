package defaultLogger

import (
	"fmt"
	"os"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger/config"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger/logrous"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger/models"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger/zap"
)

var Logger logger.Logger

func SetupDefaultLogger() {
	logType := os.Getenv("LogConfig_LogType")
	fmt.Println("Default logger")
	switch logType {
	case "Zap", "":
		Logger = zap.NewZapLogger(
			&config.LogOptions{LogType: models.Zap, CallerEnabled: false},
			constants.Dev,
		)
		break
	case "Logrus":
		Logger = logrous.NewLogrusLogger(
			&config.LogOptions{LogType: models.Logrus, CallerEnabled: false},
			constants.Dev,
		)
		break
	default:
	}
}
