package consumer

import "github.com/rabbitmq/amqp091-go"

// "github.com/rabbitmq/amqp091-go"
// "gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/types"

type RabbitMQConsumerToQueue struct {
	QueueName   string
	HandlerFunc func(amqp091.Delivery)
}
