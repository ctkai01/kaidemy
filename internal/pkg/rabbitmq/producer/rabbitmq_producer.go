package producer

// "github.com/rabbitmq/amqp091-go"
// "gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/types"

type RabbitMQProducerOnQueue struct {
	QueueName string
	Data      []byte
}
