package configurations

import (
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/consumer"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/producer"
)

type RabbitMQConfigurationBuilder interface {
	// AddProducer(producer producer.RabbitMQProducer) RabbitMQConfigurationBuilder
	// AddConsumer(consumer consumer.RabbitMQConsumer) RabbitMQConfigurationBuilder
	// AddProducer() RabbitMQConfigurationBuilder
	// AddConsumer() RabbitMQConfigurationBuilder
	AddProducerOnQueue(producer *producer.RabbitMQProducerOnQueue) RabbitMQConfigurationBuilder
	AddConsumerToQueue(consumer *consumer.RabbitMQConsumerToQueue) RabbitMQConfigurationBuilder
	// Build() RabbitMQConfigurationBuilder
}
