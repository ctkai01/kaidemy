package bus

import (
	// "errors"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/config"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/types"
)

func NewRabbitMqBus(
	cfg *config.RabbitmqOptions,
	logger logger.Logger,
) (types.IConnection, error) {
	logger.Info("NewRabbitMqBus")

	conn, err := types.NewRabbitMQConnection(cfg)

	if err != nil {
		return nil, err
	}

	return conn, nil
}
