package bus

import (
	"context"
	"time"

	"github.com/rabbitmq/amqp091-go"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/configurations"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/consumer"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/producer"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/rabbitmq/types"
)

type rabbitMqBuilder struct {
	conn   types.IConnection
	logger logger.Logger
	// producers []*producer.RabbitMQProducer
	// consumers []*consumer.RabbitMQConsumer
}

func NewRabbitMqBuilder(
	conn types.IConnection,
	logger logger.Logger,
) configurations.RabbitMQConfigurationBuilder {
	logger.Info("NewRabbitMqBuilder")

	return &rabbitMqBuilder{
		conn:   conn,
		logger: logger,
		// producers: []*producer.RabbitMQProducer{},
		// consumers: []*consumer.RabbitMQConsumer{},
	}
}

func (rbBuider *rabbitMqBuilder) AddProducerOnQueue(producer *producer.RabbitMQProducerOnQueue) configurations.RabbitMQConfigurationBuilder {
	ch, err := rbBuider.conn.Channel()

	if err != nil {
		rbBuider.logger.Error(err)
	}
	q, err := ch.QueueDeclare(
		producer.QueueName, // name
		false,              // durable
		false,              // delete when unused
		false,              // exclusive
		false,              // no-wait
		nil,                // arguments
	)

	if err != nil {
		rbBuider.logger.Error(err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)

	defer cancel()

	err = ch.PublishWithContext(ctx,
		"",     // exchange
		q.Name, // routing key
		false,  // mandatory
		false,  // immediate
		amqp091.Publishing{
			ContentType: "text/plain",
			Body:        producer.Data,
		})
	if err != nil {
		rbBuider.logger.Error(err)
	}

	defer ch.Close()

	return rbBuider
}

func (rbBuider *rabbitMqBuilder) AddConsumerToQueue(consumer *consumer.RabbitMQConsumerToQueue) configurations.RabbitMQConfigurationBuilder {
	// rbBuider.consumers = append(rbBuider.consumers, &consumer)
	ch, err := rbBuider.conn.Channel()

	if err != nil {
		rbBuider.logger.Error(err)
	}

	q, err := ch.QueueDeclare(
		consumer.QueueName, // name
		false,              // durable
		false,              // delete when unused
		false,              // exclusive
		false,              // no-wait
		nil,                // arguments
	)

	if err != nil {
		rbBuider.logger.Error(err)
	}

	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		false,  // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)

	if err != nil {
		rbBuider.logger.Error(err)
	}

	go consumeLoop(msgs, consumer.HandlerFunc)

	return rbBuider
}

func consumeLoop(deliveries <-chan amqp091.Delivery, handlerFunc func(d amqp091.Delivery)) {
	for d := range deliveries {
		// Invoke the handlerFunc func we passed as parameter.
		handlerFunc(d)
	}
}
