package es

import (
	"fmt"

	"go.uber.org/fx"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/es/contracts/projection"
)

func AsProjection(handler interface{}) interface{} {
	return fx.Annotate(
		handler,
		fx.As(new(projection.IProjection)),
		fx.ResultTags(fmt.Sprintf(`group:"projections"`)),
	)
}
