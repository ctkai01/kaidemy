package projection

import (
	"context"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/es/models"
)

type IProjection interface {
	ProcessEvent(ctx context.Context, streamEvent *models.StreamEvent) error
}
