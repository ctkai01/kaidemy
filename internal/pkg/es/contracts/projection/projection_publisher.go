package projection

import (
	"context"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/es/models"
)

type IProjectionPublisher interface {
	Publish(ctx context.Context, streamEvent *models.StreamEvent) error
}
