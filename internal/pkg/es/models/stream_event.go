package models

import (
	uuid "github.com/satori/go.uuid"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/core/domain"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/core/metadata"
)

type StreamEvent struct {
	EventID  uuid.UUID
	Version  int64
	Position int64
	Event    domain.IDomainEvent
	Metadata metadata.Metadata
}
