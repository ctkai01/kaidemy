package constants

type SendNotificationPurchaseCourse struct {
	IdProduct  int `json:"invoice_id"`
	IdUser     int `json:"user_id"`
	IDCartItem int `json:"cart_item_id"`
}

const (
	NOTIFICATION_FOR_INSTRUCTOR = 0
	NOTIFICATION_FOR_STUDENT    = 1
)

const (
	UNREAD_NOTIFICATION = 0
	READ_NOTIFICATION   = 1
)

const (
	NOTIFICATION_PURCHASE_COURSE_STUDENT = "NOTIFICATION_PURCHASE_COURSE_STUDENT"
)
