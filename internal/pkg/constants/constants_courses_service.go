package constants

var (
	BEGINNER     = 0
	INTERMEDIATE = 1
	EXPERT       = 2
	ALL          = 3

	DRAFT__STATUS = 0
	LIVE__STATUS  = 1

	LECTURE_TYPE = 1
	QUIZ_TYPE    = 2

	STANDARD_TYPE  = 1
	WISH_LIST_TYPE = 2
	ARCHIE         = 3

	GET_DETAIL_TITLE_TYPE = 1
	GET_INTRODUCE_TYPE    = 2
	GET_DETAIL_TYPE       = 3

	REVIEW_INIT_STATUS    = 0
	REVIEW_PENDING_STATUS = 1
	REVIEW_VERIFY_STATUS  = 2

	MOST_REVIEW_SORT = "most_review"
	HIGHEST_SORT     = "highest_rated"
	NEWEST_SORT      = "newest"
	OLDEST_SORT      = "oldest"

	SHORT_DURATION = "short"
	EXTRA_SHORT_DURATION = "extraShort"

	RESPONSE_REPLY = "response_reply"
	NO_RESPONSE_REPLY = "no_response_reply"

)

type RegisterCourse struct {
	IdCourse int `json:"course_id"`
	IdUser   int `json:"user_id"`
}
