package constants

const (
	INVOICE_PENDING = 0
	INVOICE_SUCCESS = 1
)

type SendPurchase struct {
	IdInvoice int `json:"invoice_id"`
}