package constants

const (
	AccountNormal   = 0
	AccountGoogle   = 1
	AccountFacebook = 2
)

const (
	SUPPER_ADMIN = 0
	ADMIN = 1
	NORMAL_USER = 2
	TEACHER = 3
)

var (
	ACCOUNT_STRIPE_PENDING = 1
	ACCOUNT_STRIPE_VERIFY = 2
)

const DEFAULT_ROLE = NORMAL_USER
type ROLE int

type MailForgotPassword struct {
	Email string `json:"email"`
	Token string `json:"token"`
	Name string `json:"name"`
}