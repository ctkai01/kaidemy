package firebase

import (
	"context"
	"os"

	"google.golang.org/api/option"

	firebase "firebase.google.com/go"
	"firebase.google.com/go/messaging"
)

func NewMessagingFirebaseClient() (*messaging.Client, error) {
	content, err := os.ReadFile("./config/kaidemy-firebase-adminsdk.json")
	
	if err != nil {
		return nil, err
	}

	opts := []option.ClientOption{option.WithCredentialsJSON(content)}

	fb, err := firebase.NewApp(context.Background(), nil, opts...)

	if err != nil {
		return nil, err
	}
	messaging, err := fb.Messaging(context.Background())

	if err != nil {
		return nil, err
	}

	return messaging, nil
}
