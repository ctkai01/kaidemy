package firebase

import "go.uber.org/fx"

var Module = fx.Module(
	"firebasemessingfx",
	fx.Provide(
		NewMessagingFirebaseClient,
	),
)