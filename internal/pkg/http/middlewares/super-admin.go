package middlewares

import (
	"net/http"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"

	"github.com/labstack/echo/v4"
)

func SuperAdminMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		role := c.Get("userRole").(int)

		if role != constants.SUPPER_ADMIN {
			return c.JSON(http.StatusForbidden, map[string]interface{}{
				"message": constants.ErrForbiddenTitle,
			})
		}

		return next(c)
	}
}
