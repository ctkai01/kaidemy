package middlewares

import (
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/constants"

	"github.com/labstack/echo/v4"
)

type CustomClaims struct {
	jwt.StandardClaims
	Subject int `json:"sub,omitempty"`
	Role    int `json:"role,omitempty"`
}

func AuthMiddleware(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		authHeader := c.Request().Header.Get("Authorization")

		// Check if the Authorization header is present
		if authHeader == "" {
			return c.JSON(http.StatusUnauthorized, map[string]interface{}{
				"message": constants.ErrUnauthorizedTitle,
			})
		}
		// Extract the token from the header
		tokenString := strings.Replace(authHeader, "Bearer ", "", 1)

		claims := &CustomClaims{}
		token, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
			return constants.JwtSecret, nil
		})

		if err != nil {
			return c.JSON(http.StatusUnauthorized, map[string]interface{}{
				"message": constants.ErrUnauthorizedTitle,
			})
		}

		if !token.Valid {
			return c.JSON(http.StatusUnauthorized, map[string]interface{}{
				"message": constants.ErrInvalidToken,
			})
		}
		// Set the user ID in the Echo context's Locals
		c.Set("userId", claims.Subject)
		c.Set("userRole", claims.Role)

		return next(c)
	}
}
