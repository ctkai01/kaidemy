package customHadnlers

import (
	"github.com/labstack/echo/v4"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/http/http_errors/problemDetails"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
	errorUtils "gitlab.com/ctkai01/kaidemy/internal/pkg/utils/error_utils"
)

func ProblemHandlerFunc(err error, c echo.Context, logger logger.Logger) {
	prb := problemDetails.ParseError(err)
	if prb != nil {
		if !c.Response().Committed {
			if _, err := problemDetails.WriteTo(prb, c.Response()); err != nil {
				logger.Error(err)
			}
		}
	} else {
		if !c.Response().Committed {
			prb := problemDetails.NewInternalServerProblemDetail(err.Error(), errorUtils.ErrorsWithStack(err))
			if _, err := problemDetails.WriteTo(prb, c.Response()); err != nil {
				logger.Error(err)
			}
		}
	}
}
