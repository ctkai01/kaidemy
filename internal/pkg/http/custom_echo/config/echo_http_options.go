package config

import (
	"fmt"
	"net/url"

	"github.com/iancoleman/strcase"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/config"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/config/environment"
	typeMapper "gitlab.com/ctkai01/kaidemy/internal/pkg/reflection/type_mappper"
)

var optionName = strcase.ToLowerCamel(typeMapper.GetTypeNameByT[EchoHttpOptions]())

type EchoHttpOptions struct {
	Port                string   `mapstructure:"port"                validate:"required" env:"TcpPort"`
	Development         bool     `mapstructure:"development"                             env:"Development"`
	BasePath            string   `mapstructure:"basePath"            validate:"required" env:"BasePath"`
	DebugErrorsResponse bool     `mapstructure:"debugErrorsResponse"                     env:"DebugErrorsResponse"`
	IgnoreLogUrls       []string `mapstructure:"ignoreLogUrls"`
	Timeout             int      `mapstructure:"timeout"                                 env:"Timeout"`
	Host                string   `mapstructure:"host"                                    env:"Host"`
	Name                string   `mapstructure:"name"                                    env:"Name"`
}

func (c *EchoHttpOptions) Address() string {
	return fmt.Sprintf("%s%s", c.Host, c.Port)
}

func (c *EchoHttpOptions) BasePathAddress() string {
	path, err := url.JoinPath(c.Address(), c.BasePath)
	if err != nil {
		return ""
	}
	return path
}

func ProvideConfig(environment environment.Environment) (*EchoHttpOptions, error) {
	return config.BindConfigKey[*EchoHttpOptions](optionName, environment)
}
