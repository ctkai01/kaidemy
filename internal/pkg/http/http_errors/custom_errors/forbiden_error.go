package customErrors

import (
	"net/http"

	"emperror.dev/errors"
	"github.com/labstack/echo/v4"
)

func NewForbiddenError(message string) error {
	ne := &forbiddenError{
		CustomError: NewCustomError(nil, http.StatusForbidden, message),
	}
	stackErr := errors.WithStackIf(ne)

	return stackErr
}

func NewForbiddenErrorHttp(c echo.Context, message string) error {
	ue := NewCustomErrorHttp(http.StatusForbidden, message)
	return c.JSON(http.StatusForbidden, ue)
}


func NewForbiddenErrorWrap(err error, message string) error {
	ne := &forbiddenError{
		CustomError: NewCustomError(err, http.StatusForbidden, message),
	}
	stackErr := errors.WithStackIf(ne)

	return stackErr
}

type forbiddenError struct {
	CustomError
}

type ForbiddenError interface {
	CustomError
	IsForbiddenError() bool
}

func (f *forbiddenError) IsForbiddenError() bool {
	return true
}

func IsForbiddenError(err error) bool {
	var forbiddenError ForbiddenError
	//us, ok := grpc_errors.Cause(err).(ForbiddenError)
	if errors.As(err, &forbiddenError) {
		return forbiddenError.IsForbiddenError()
	}

	return false
}
