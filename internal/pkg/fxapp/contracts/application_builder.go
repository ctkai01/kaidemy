package contracts

import (
	"go.uber.org/fx"

	"gitlab.com/ctkai01/kaidemy/internal/pkg/config/environment"
	"gitlab.com/ctkai01/kaidemy/internal/pkg/logger"
)

type ApplicationBuilder interface {
	ProvideModule(module fx.Option)
	Provide(constructors ...interface{})
	Decorate(constructors ...interface{})
	Build() Application

	GetProvides() []interface{}
	GetDecorates() []interface{}
	Options() []fx.Option
	Logger() logger.Logger
	Environment() environment.Environment
}
